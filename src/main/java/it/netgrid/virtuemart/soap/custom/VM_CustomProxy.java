package it.netgrid.virtuemart.soap.custom;

public class VM_CustomProxy implements it.netgrid.virtuemart.soap.custom.VM_Custom_PortType {
  private String _endpoint = null;
  private it.netgrid.virtuemart.soap.custom.VM_Custom_PortType vM_Custom_PortType = null;
  
  public VM_CustomProxy() {
    _initVM_CustomProxy();
  }
  
  public VM_CustomProxy(String endpoint) {
    _endpoint = endpoint;
    _initVM_CustomProxy();
  }
  
  private void _initVM_CustomProxy() {
    try {
      vM_Custom_PortType = (new it.netgrid.virtuemart.soap.custom.VM_Custom_ServiceLocator()).getVM_CustomSOAP();
      if (vM_Custom_PortType != null) {
        if (_endpoint != null)
          ((javax.xml.rpc.Stub)vM_Custom_PortType)._setProperty("javax.xml.rpc.service.endpoint.address", _endpoint);
        else
          _endpoint = (String)((javax.xml.rpc.Stub)vM_Custom_PortType)._getProperty("javax.xml.rpc.service.endpoint.address");
      }
      
    }
    catch (javax.xml.rpc.ServiceException serviceException) {}
  }
  
  public String getEndpoint() {
    return _endpoint;
  }
  
  public void setEndpoint(String endpoint) {
    _endpoint = endpoint;
    if (vM_Custom_PortType != null)
      ((javax.xml.rpc.Stub)vM_Custom_PortType)._setProperty("javax.xml.rpc.service.endpoint.address", _endpoint);
    
  }
  
  public it.netgrid.virtuemart.soap.custom.VM_Custom_PortType getVM_Custom_PortType() {
    if (vM_Custom_PortType == null)
      _initVM_CustomProxy();
    return vM_Custom_PortType;
  }
  
  public it.netgrid.virtuemart.soap.custom.CommonReturn method1(it.netgrid.virtuemart.soap.custom.Method1Input parameters) throws java.rmi.RemoteException{
    if (vM_Custom_PortType == null)
      _initVM_CustomProxy();
    return vM_Custom_PortType.method1(parameters);
  }
  
  public it.netgrid.virtuemart.soap.custom.CommonReturn method2(it.netgrid.virtuemart.soap.custom.Method2Input parameters) throws java.rmi.RemoteException{
    if (vM_Custom_PortType == null)
      _initVM_CustomProxy();
    return vM_Custom_PortType.method2(parameters);
  }
  
  
}