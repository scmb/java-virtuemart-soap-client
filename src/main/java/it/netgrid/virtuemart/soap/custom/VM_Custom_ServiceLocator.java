/**
 * VM_Custom_ServiceLocator.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package it.netgrid.virtuemart.soap.custom;

public class VM_Custom_ServiceLocator extends org.apache.axis.client.Service implements it.netgrid.virtuemart.soap.custom.VM_Custom_Service {

    public VM_Custom_ServiceLocator() {
    }


    public VM_Custom_ServiceLocator(org.apache.axis.EngineConfiguration config) {
        super(config);
    }

    public VM_Custom_ServiceLocator(java.lang.String wsdlLoc, javax.xml.namespace.QName sName) throws javax.xml.rpc.ServiceException {
        super(wsdlLoc, sName);
    }

    // Use to get a proxy class for VM_CustomSOAP
    private java.lang.String VM_CustomSOAP_address = "http://localhost/administrator/components/com_vm_soa/services/VM_CustomizedService.php";

    public java.lang.String getVM_CustomSOAPAddress() {
        return VM_CustomSOAP_address;
    }

    // The WSDD service name defaults to the port name.
    private java.lang.String VM_CustomSOAPWSDDServiceName = "VM_CustomSOAP";

    public java.lang.String getVM_CustomSOAPWSDDServiceName() {
        return VM_CustomSOAPWSDDServiceName;
    }

    public void setVM_CustomSOAPWSDDServiceName(java.lang.String name) {
        VM_CustomSOAPWSDDServiceName = name;
    }

    public it.netgrid.virtuemart.soap.custom.VM_Custom_PortType getVM_CustomSOAP() throws javax.xml.rpc.ServiceException {
       java.net.URL endpoint;
        try {
            endpoint = new java.net.URL(VM_CustomSOAP_address);
        }
        catch (java.net.MalformedURLException e) {
            throw new javax.xml.rpc.ServiceException(e);
        }
        return getVM_CustomSOAP(endpoint);
    }

    public it.netgrid.virtuemart.soap.custom.VM_Custom_PortType getVM_CustomSOAP(java.net.URL portAddress) throws javax.xml.rpc.ServiceException {
        try {
            it.netgrid.virtuemart.soap.custom.VM_CustomSOAPStub _stub = new it.netgrid.virtuemart.soap.custom.VM_CustomSOAPStub(portAddress, this);
            _stub.setPortName(getVM_CustomSOAPWSDDServiceName());
            return _stub;
        }
        catch (org.apache.axis.AxisFault e) {
            return null;
        }
    }

    public void setVM_CustomSOAPEndpointAddress(java.lang.String address) {
        VM_CustomSOAP_address = address;
    }

    /**
     * For the given interface, get the stub implementation.
     * If this service has no port for the given interface,
     * then ServiceException is thrown.
     */
    public java.rmi.Remote getPort(Class serviceEndpointInterface) throws javax.xml.rpc.ServiceException {
        try {
            if (it.netgrid.virtuemart.soap.custom.VM_Custom_PortType.class.isAssignableFrom(serviceEndpointInterface)) {
                it.netgrid.virtuemart.soap.custom.VM_CustomSOAPStub _stub = new it.netgrid.virtuemart.soap.custom.VM_CustomSOAPStub(new java.net.URL(VM_CustomSOAP_address), this);
                _stub.setPortName(getVM_CustomSOAPWSDDServiceName());
                return _stub;
            }
        }
        catch (java.lang.Throwable t) {
            throw new javax.xml.rpc.ServiceException(t);
        }
        throw new javax.xml.rpc.ServiceException("There is no stub implementation for the interface:  " + (serviceEndpointInterface == null ? "null" : serviceEndpointInterface.getName()));
    }

    /**
     * For the given interface, get the stub implementation.
     * If this service has no port for the given interface,
     * then ServiceException is thrown.
     */
    public java.rmi.Remote getPort(javax.xml.namespace.QName portName, Class serviceEndpointInterface) throws javax.xml.rpc.ServiceException {
        if (portName == null) {
            return getPort(serviceEndpointInterface);
        }
        java.lang.String inputPortName = portName.getLocalPart();
        if ("VM_CustomSOAP".equals(inputPortName)) {
            return getVM_CustomSOAP();
        }
        else  {
            java.rmi.Remote _stub = getPort(serviceEndpointInterface);
            ((org.apache.axis.client.Stub) _stub).setPortName(portName);
            return _stub;
        }
    }

    public javax.xml.namespace.QName getServiceName() {
        return new javax.xml.namespace.QName("http://www.virtuemart.net/VM_Custom/", "VM_Custom");
    }

    private java.util.HashSet ports = null;

    public java.util.Iterator getPorts() {
        if (ports == null) {
            ports = new java.util.HashSet();
            ports.add(new javax.xml.namespace.QName("http://www.virtuemart.net/VM_Custom/", "VM_CustomSOAP"));
        }
        return ports.iterator();
    }

    /**
    * Set the endpoint address for the specified port name.
    */
    public void setEndpointAddress(java.lang.String portName, java.lang.String address) throws javax.xml.rpc.ServiceException {
        
if ("VM_CustomSOAP".equals(portName)) {
            setVM_CustomSOAPEndpointAddress(address);
        }
        else 
{ // Unknown Port Name
            throw new javax.xml.rpc.ServiceException(" Cannot set Endpoint Address for Unknown Port" + portName);
        }
    }

    /**
    * Set the endpoint address for the specified port name.
    */
    public void setEndpointAddress(javax.xml.namespace.QName portName, java.lang.String address) throws javax.xml.rpc.ServiceException {
        setEndpointAddress(portName.getLocalPart(), address);
    }

}
