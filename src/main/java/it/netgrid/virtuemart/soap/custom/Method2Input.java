/**
 * Method2Input.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package it.netgrid.virtuemart.soap.custom;

public class Method2Input  implements java.io.Serializable {
    private it.netgrid.virtuemart.soap.custom.LoginInfo loginInfo;

    private java.lang.String params1;

    private java.lang.String params2;

    private java.lang.String params3;

    public Method2Input() {
    }

    public Method2Input(
           it.netgrid.virtuemart.soap.custom.LoginInfo loginInfo,
           java.lang.String params1,
           java.lang.String params2,
           java.lang.String params3) {
           this.loginInfo = loginInfo;
           this.params1 = params1;
           this.params2 = params2;
           this.params3 = params3;
    }


    /**
     * Gets the loginInfo value for this Method2Input.
     * 
     * @return loginInfo
     */
    public it.netgrid.virtuemart.soap.custom.LoginInfo getLoginInfo() {
        return loginInfo;
    }


    /**
     * Sets the loginInfo value for this Method2Input.
     * 
     * @param loginInfo
     */
    public void setLoginInfo(it.netgrid.virtuemart.soap.custom.LoginInfo loginInfo) {
        this.loginInfo = loginInfo;
    }


    /**
     * Gets the params1 value for this Method2Input.
     * 
     * @return params1
     */
    public java.lang.String getParams1() {
        return params1;
    }


    /**
     * Sets the params1 value for this Method2Input.
     * 
     * @param params1
     */
    public void setParams1(java.lang.String params1) {
        this.params1 = params1;
    }


    /**
     * Gets the params2 value for this Method2Input.
     * 
     * @return params2
     */
    public java.lang.String getParams2() {
        return params2;
    }


    /**
     * Sets the params2 value for this Method2Input.
     * 
     * @param params2
     */
    public void setParams2(java.lang.String params2) {
        this.params2 = params2;
    }


    /**
     * Gets the params3 value for this Method2Input.
     * 
     * @return params3
     */
    public java.lang.String getParams3() {
        return params3;
    }


    /**
     * Sets the params3 value for this Method2Input.
     * 
     * @param params3
     */
    public void setParams3(java.lang.String params3) {
        this.params3 = params3;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof Method2Input)) return false;
        Method2Input other = (Method2Input) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.loginInfo==null && other.getLoginInfo()==null) || 
             (this.loginInfo!=null &&
              this.loginInfo.equals(other.getLoginInfo()))) &&
            ((this.params1==null && other.getParams1()==null) || 
             (this.params1!=null &&
              this.params1.equals(other.getParams1()))) &&
            ((this.params2==null && other.getParams2()==null) || 
             (this.params2!=null &&
              this.params2.equals(other.getParams2()))) &&
            ((this.params3==null && other.getParams3()==null) || 
             (this.params3!=null &&
              this.params3.equals(other.getParams3())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getLoginInfo() != null) {
            _hashCode += getLoginInfo().hashCode();
        }
        if (getParams1() != null) {
            _hashCode += getParams1().hashCode();
        }
        if (getParams2() != null) {
            _hashCode += getParams2().hashCode();
        }
        if (getParams3() != null) {
            _hashCode += getParams3().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(Method2Input.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://www.virtuemart.net/VM_Custom/", "Method2Input"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("loginInfo");
        elemField.setXmlName(new javax.xml.namespace.QName("", "loginInfo"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.virtuemart.net/VM_Custom/", "loginInfo"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("params1");
        elemField.setXmlName(new javax.xml.namespace.QName("", "params1"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("params2");
        elemField.setXmlName(new javax.xml.namespace.QName("", "params2"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("params3");
        elemField.setXmlName(new javax.xml.namespace.QName("", "params3"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
