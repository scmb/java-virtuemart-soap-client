/**
 * VM_Custom_Service.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package it.netgrid.virtuemart.soap.custom;

public interface VM_Custom_Service extends javax.xml.rpc.Service {
    public java.lang.String getVM_CustomSOAPAddress();

    public it.netgrid.virtuemart.soap.custom.VM_Custom_PortType getVM_CustomSOAP() throws javax.xml.rpc.ServiceException;

    public it.netgrid.virtuemart.soap.custom.VM_Custom_PortType getVM_CustomSOAP(java.net.URL portAddress) throws javax.xml.rpc.ServiceException;
}
