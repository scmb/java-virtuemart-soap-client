/**
 * VM_Custom_PortType.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package it.netgrid.virtuemart.soap.custom;

public interface VM_Custom_PortType extends java.rmi.Remote {
    public it.netgrid.virtuemart.soap.custom.CommonReturn method1(it.netgrid.virtuemart.soap.custom.Method1Input parameters) throws java.rmi.RemoteException;
    public it.netgrid.virtuemart.soap.custom.CommonReturn method2(it.netgrid.virtuemart.soap.custom.Method2Input parameters) throws java.rmi.RemoteException;
}
