/**
 * VM_Categories_PortType.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package it.netgrid.virtuemart.soap.categories;

public interface VM_Categories_PortType extends java.rmi.Remote {
    public it.netgrid.virtuemart.soap.categories.Categorie[] getAllCategories(it.netgrid.virtuemart.soap.categories.GetAllCategoriesInput parameters) throws java.rmi.RemoteException;
    public it.netgrid.virtuemart.soap.categories.Categorie[] getChildsCategories(it.netgrid.virtuemart.soap.categories.GetChildsCategoriesRequestInput parameters) throws java.rmi.RemoteException;
    public it.netgrid.virtuemart.soap.categories.CommonReturn addCategory(it.netgrid.virtuemart.soap.categories.AddCategoryInput parameters) throws java.rmi.RemoteException;
    public it.netgrid.virtuemart.soap.categories.CommonReturn deleteCategory(it.netgrid.virtuemart.soap.categories.DeleteCategoryInput parameters) throws java.rmi.RemoteException;
    public it.netgrid.virtuemart.soap.categories.AvalaibleImage[] getAvailableImages(it.netgrid.virtuemart.soap.categories.GetAvalaibleImageInput parameters) throws java.rmi.RemoteException;
    public it.netgrid.virtuemart.soap.categories.CommonReturn updateCategory(it.netgrid.virtuemart.soap.categories.AddCategoryInput parameters) throws java.rmi.RemoteException;
    public it.netgrid.virtuemart.soap.categories.Media[] getMediaCategory(it.netgrid.virtuemart.soap.categories.GetMediaCategoriesInput parameters) throws java.rmi.RemoteException;
    public it.netgrid.virtuemart.soap.categories.CommonReturn addMediaCategory(it.netgrid.virtuemart.soap.categories.AddMediaCategoryInput parameters) throws java.rmi.RemoteException;
    public it.netgrid.virtuemart.soap.categories.CommonReturn deleteMediaCategory(it.netgrid.virtuemart.soap.categories.DelMediaInput parameters) throws java.rmi.RemoteException;
    public it.netgrid.virtuemart.soap.categories.CommonReturn uploadFile(it.netgrid.virtuemart.soap.categories.UploadInput parameters) throws java.rmi.RemoteException;
    public it.netgrid.virtuemart.soap.categories.Template[] getTemplates(it.netgrid.virtuemart.soap.categories.GetTemplateInput parameters) throws java.rmi.RemoteException;
}
