/**
 * GetAvalaibleImageInput.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package it.netgrid.virtuemart.soap.categories;

public class GetAvalaibleImageInput  implements java.io.Serializable {
    /**
	 * 
	 */
	private static final long serialVersionUID = 6238591222633051876L;

	private it.netgrid.virtuemart.soap.categories.LoginInfo loginInfo;

    private java.lang.String img_type;

    public GetAvalaibleImageInput() {
    }

    public GetAvalaibleImageInput(
           it.netgrid.virtuemart.soap.categories.LoginInfo loginInfo,
           java.lang.String img_type) {
           this.loginInfo = loginInfo;
           this.img_type = img_type;
    }


    /**
     * Gets the loginInfo value for this GetAvalaibleImageInput.
     * 
     * @return loginInfo
     */
    public it.netgrid.virtuemart.soap.categories.LoginInfo getLoginInfo() {
        return loginInfo;
    }


    /**
     * Sets the loginInfo value for this GetAvalaibleImageInput.
     * 
     * @param loginInfo
     */
    public void setLoginInfo(it.netgrid.virtuemart.soap.categories.LoginInfo loginInfo) {
        this.loginInfo = loginInfo;
    }


    /**
     * Gets the img_type value for this GetAvalaibleImageInput.
     * 
     * @return img_type
     */
    public java.lang.String getImg_type() {
        return img_type;
    }


    /**
     * Sets the img_type value for this GetAvalaibleImageInput.
     * 
     * @param img_type
     */
    public void setImg_type(java.lang.String img_type) {
        this.img_type = img_type;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof GetAvalaibleImageInput)) return false;
        GetAvalaibleImageInput other = (GetAvalaibleImageInput) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.loginInfo==null && other.getLoginInfo()==null) || 
             (this.loginInfo!=null &&
              this.loginInfo.equals(other.getLoginInfo()))) &&
            ((this.img_type==null && other.getImg_type()==null) || 
             (this.img_type!=null &&
              this.img_type.equals(other.getImg_type())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getLoginInfo() != null) {
            _hashCode += getLoginInfo().hashCode();
        }
        if (getImg_type() != null) {
            _hashCode += getImg_type().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(GetAvalaibleImageInput.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://www.virtuemart.net/VM_Categories/", "GetAvalaibleImageInput"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("loginInfo");
        elemField.setXmlName(new javax.xml.namespace.QName("", "loginInfo"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.virtuemart.net/VM_Categories/", "loginInfo"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("img_type");
        elemField.setXmlName(new javax.xml.namespace.QName("", "img_type"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
