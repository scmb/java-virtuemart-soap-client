/**
 * Media.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package it.netgrid.virtuemart.soap.categories;

public class Media  implements java.io.Serializable {
    /**
	 * 
	 */
	private static final long serialVersionUID = 4788547306139393912L;

	private java.lang.String virtuemart_media_id;

    private java.lang.String virtuemart_vendor_id;

    private java.lang.String file_title;

    private java.lang.String file_description;

    private java.lang.String file_meta;

    private java.lang.String file_mimetype;

    private java.lang.String file_type;

    private java.lang.String file_url;

    private java.lang.String file_url_thumb;

    private java.lang.String file_is_product_image;

    private java.lang.String file_is_downloadable;

    private java.lang.String file_is_forSale;

    private java.lang.String file_params;

    private java.lang.String ordering;

    private java.lang.String shared;

    private java.lang.String published;

    private byte[] attachValue;

    public Media() {
    }

    public Media(
           java.lang.String virtuemart_media_id,
           java.lang.String virtuemart_vendor_id,
           java.lang.String file_title,
           java.lang.String file_description,
           java.lang.String file_meta,
           java.lang.String file_mimetype,
           java.lang.String file_type,
           java.lang.String file_url,
           java.lang.String file_url_thumb,
           java.lang.String file_is_product_image,
           java.lang.String file_is_downloadable,
           java.lang.String file_is_forSale,
           java.lang.String file_params,
           java.lang.String ordering,
           java.lang.String shared,
           java.lang.String published,
           byte[] attachValue) {
           this.virtuemart_media_id = virtuemart_media_id;
           this.virtuemart_vendor_id = virtuemart_vendor_id;
           this.file_title = file_title;
           this.file_description = file_description;
           this.file_meta = file_meta;
           this.file_mimetype = file_mimetype;
           this.file_type = file_type;
           this.file_url = file_url;
           this.file_url_thumb = file_url_thumb;
           this.file_is_product_image = file_is_product_image;
           this.file_is_downloadable = file_is_downloadable;
           this.file_is_forSale = file_is_forSale;
           this.file_params = file_params;
           this.ordering = ordering;
           this.shared = shared;
           this.published = published;
           this.attachValue = attachValue;
    }


    /**
     * Gets the virtuemart_media_id value for this Media.
     * 
     * @return virtuemart_media_id
     */
    public java.lang.String getVirtuemart_media_id() {
        return virtuemart_media_id;
    }


    /**
     * Sets the virtuemart_media_id value for this Media.
     * 
     * @param virtuemart_media_id
     */
    public void setVirtuemart_media_id(java.lang.String virtuemart_media_id) {
        this.virtuemart_media_id = virtuemart_media_id;
    }


    /**
     * Gets the virtuemart_vendor_id value for this Media.
     * 
     * @return virtuemart_vendor_id
     */
    public java.lang.String getVirtuemart_vendor_id() {
        return virtuemart_vendor_id;
    }


    /**
     * Sets the virtuemart_vendor_id value for this Media.
     * 
     * @param virtuemart_vendor_id
     */
    public void setVirtuemart_vendor_id(java.lang.String virtuemart_vendor_id) {
        this.virtuemart_vendor_id = virtuemart_vendor_id;
    }


    /**
     * Gets the file_title value for this Media.
     * 
     * @return file_title
     */
    public java.lang.String getFile_title() {
        return file_title;
    }


    /**
     * Sets the file_title value for this Media.
     * 
     * @param file_title
     */
    public void setFile_title(java.lang.String file_title) {
        this.file_title = file_title;
    }


    /**
     * Gets the file_description value for this Media.
     * 
     * @return file_description
     */
    public java.lang.String getFile_description() {
        return file_description;
    }


    /**
     * Sets the file_description value for this Media.
     * 
     * @param file_description
     */
    public void setFile_description(java.lang.String file_description) {
        this.file_description = file_description;
    }


    /**
     * Gets the file_meta value for this Media.
     * 
     * @return file_meta
     */
    public java.lang.String getFile_meta() {
        return file_meta;
    }


    /**
     * Sets the file_meta value for this Media.
     * 
     * @param file_meta
     */
    public void setFile_meta(java.lang.String file_meta) {
        this.file_meta = file_meta;
    }


    /**
     * Gets the file_mimetype value for this Media.
     * 
     * @return file_mimetype
     */
    public java.lang.String getFile_mimetype() {
        return file_mimetype;
    }


    /**
     * Sets the file_mimetype value for this Media.
     * 
     * @param file_mimetype
     */
    public void setFile_mimetype(java.lang.String file_mimetype) {
        this.file_mimetype = file_mimetype;
    }


    /**
     * Gets the file_type value for this Media.
     * 
     * @return file_type
     */
    public java.lang.String getFile_type() {
        return file_type;
    }


    /**
     * Sets the file_type value for this Media.
     * 
     * @param file_type
     */
    public void setFile_type(java.lang.String file_type) {
        this.file_type = file_type;
    }


    /**
     * Gets the file_url value for this Media.
     * 
     * @return file_url
     */
    public java.lang.String getFile_url() {
        return file_url;
    }


    /**
     * Sets the file_url value for this Media.
     * 
     * @param file_url
     */
    public void setFile_url(java.lang.String file_url) {
        this.file_url = file_url;
    }


    /**
     * Gets the file_url_thumb value for this Media.
     * 
     * @return file_url_thumb
     */
    public java.lang.String getFile_url_thumb() {
        return file_url_thumb;
    }


    /**
     * Sets the file_url_thumb value for this Media.
     * 
     * @param file_url_thumb
     */
    public void setFile_url_thumb(java.lang.String file_url_thumb) {
        this.file_url_thumb = file_url_thumb;
    }


    /**
     * Gets the file_is_product_image value for this Media.
     * 
     * @return file_is_product_image
     */
    public java.lang.String getFile_is_product_image() {
        return file_is_product_image;
    }


    /**
     * Sets the file_is_product_image value for this Media.
     * 
     * @param file_is_product_image
     */
    public void setFile_is_product_image(java.lang.String file_is_product_image) {
        this.file_is_product_image = file_is_product_image;
    }


    /**
     * Gets the file_is_downloadable value for this Media.
     * 
     * @return file_is_downloadable
     */
    public java.lang.String getFile_is_downloadable() {
        return file_is_downloadable;
    }


    /**
     * Sets the file_is_downloadable value for this Media.
     * 
     * @param file_is_downloadable
     */
    public void setFile_is_downloadable(java.lang.String file_is_downloadable) {
        this.file_is_downloadable = file_is_downloadable;
    }


    /**
     * Gets the file_is_forSale value for this Media.
     * 
     * @return file_is_forSale
     */
    public java.lang.String getFile_is_forSale() {
        return file_is_forSale;
    }


    /**
     * Sets the file_is_forSale value for this Media.
     * 
     * @param file_is_forSale
     */
    public void setFile_is_forSale(java.lang.String file_is_forSale) {
        this.file_is_forSale = file_is_forSale;
    }


    /**
     * Gets the file_params value for this Media.
     * 
     * @return file_params
     */
    public java.lang.String getFile_params() {
        return file_params;
    }


    /**
     * Sets the file_params value for this Media.
     * 
     * @param file_params
     */
    public void setFile_params(java.lang.String file_params) {
        this.file_params = file_params;
    }


    /**
     * Gets the ordering value for this Media.
     * 
     * @return ordering
     */
    public java.lang.String getOrdering() {
        return ordering;
    }


    /**
     * Sets the ordering value for this Media.
     * 
     * @param ordering
     */
    public void setOrdering(java.lang.String ordering) {
        this.ordering = ordering;
    }


    /**
     * Gets the shared value for this Media.
     * 
     * @return shared
     */
    public java.lang.String getShared() {
        return shared;
    }


    /**
     * Sets the shared value for this Media.
     * 
     * @param shared
     */
    public void setShared(java.lang.String shared) {
        this.shared = shared;
    }


    /**
     * Gets the published value for this Media.
     * 
     * @return published
     */
    public java.lang.String getPublished() {
        return published;
    }


    /**
     * Sets the published value for this Media.
     * 
     * @param published
     */
    public void setPublished(java.lang.String published) {
        this.published = published;
    }


    /**
     * Gets the attachValue value for this Media.
     * 
     * @return attachValue
     */
    public byte[] getAttachValue() {
        return attachValue;
    }


    /**
     * Sets the attachValue value for this Media.
     * 
     * @param attachValue
     */
    public void setAttachValue(byte[] attachValue) {
        this.attachValue = attachValue;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof Media)) return false;
        Media other = (Media) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.virtuemart_media_id==null && other.getVirtuemart_media_id()==null) || 
             (this.virtuemart_media_id!=null &&
              this.virtuemart_media_id.equals(other.getVirtuemart_media_id()))) &&
            ((this.virtuemart_vendor_id==null && other.getVirtuemart_vendor_id()==null) || 
             (this.virtuemart_vendor_id!=null &&
              this.virtuemart_vendor_id.equals(other.getVirtuemart_vendor_id()))) &&
            ((this.file_title==null && other.getFile_title()==null) || 
             (this.file_title!=null &&
              this.file_title.equals(other.getFile_title()))) &&
            ((this.file_description==null && other.getFile_description()==null) || 
             (this.file_description!=null &&
              this.file_description.equals(other.getFile_description()))) &&
            ((this.file_meta==null && other.getFile_meta()==null) || 
             (this.file_meta!=null &&
              this.file_meta.equals(other.getFile_meta()))) &&
            ((this.file_mimetype==null && other.getFile_mimetype()==null) || 
             (this.file_mimetype!=null &&
              this.file_mimetype.equals(other.getFile_mimetype()))) &&
            ((this.file_type==null && other.getFile_type()==null) || 
             (this.file_type!=null &&
              this.file_type.equals(other.getFile_type()))) &&
            ((this.file_url==null && other.getFile_url()==null) || 
             (this.file_url!=null &&
              this.file_url.equals(other.getFile_url()))) &&
            ((this.file_url_thumb==null && other.getFile_url_thumb()==null) || 
             (this.file_url_thumb!=null &&
              this.file_url_thumb.equals(other.getFile_url_thumb()))) &&
            ((this.file_is_product_image==null && other.getFile_is_product_image()==null) || 
             (this.file_is_product_image!=null &&
              this.file_is_product_image.equals(other.getFile_is_product_image()))) &&
            ((this.file_is_downloadable==null && other.getFile_is_downloadable()==null) || 
             (this.file_is_downloadable!=null &&
              this.file_is_downloadable.equals(other.getFile_is_downloadable()))) &&
            ((this.file_is_forSale==null && other.getFile_is_forSale()==null) || 
             (this.file_is_forSale!=null &&
              this.file_is_forSale.equals(other.getFile_is_forSale()))) &&
            ((this.file_params==null && other.getFile_params()==null) || 
             (this.file_params!=null &&
              this.file_params.equals(other.getFile_params()))) &&
            ((this.ordering==null && other.getOrdering()==null) || 
             (this.ordering!=null &&
              this.ordering.equals(other.getOrdering()))) &&
            ((this.shared==null && other.getShared()==null) || 
             (this.shared!=null &&
              this.shared.equals(other.getShared()))) &&
            ((this.published==null && other.getPublished()==null) || 
             (this.published!=null &&
              this.published.equals(other.getPublished()))) &&
            ((this.attachValue==null && other.getAttachValue()==null) || 
             (this.attachValue!=null &&
              java.util.Arrays.equals(this.attachValue, other.getAttachValue())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getVirtuemart_media_id() != null) {
            _hashCode += getVirtuemart_media_id().hashCode();
        }
        if (getVirtuemart_vendor_id() != null) {
            _hashCode += getVirtuemart_vendor_id().hashCode();
        }
        if (getFile_title() != null) {
            _hashCode += getFile_title().hashCode();
        }
        if (getFile_description() != null) {
            _hashCode += getFile_description().hashCode();
        }
        if (getFile_meta() != null) {
            _hashCode += getFile_meta().hashCode();
        }
        if (getFile_mimetype() != null) {
            _hashCode += getFile_mimetype().hashCode();
        }
        if (getFile_type() != null) {
            _hashCode += getFile_type().hashCode();
        }
        if (getFile_url() != null) {
            _hashCode += getFile_url().hashCode();
        }
        if (getFile_url_thumb() != null) {
            _hashCode += getFile_url_thumb().hashCode();
        }
        if (getFile_is_product_image() != null) {
            _hashCode += getFile_is_product_image().hashCode();
        }
        if (getFile_is_downloadable() != null) {
            _hashCode += getFile_is_downloadable().hashCode();
        }
        if (getFile_is_forSale() != null) {
            _hashCode += getFile_is_forSale().hashCode();
        }
        if (getFile_params() != null) {
            _hashCode += getFile_params().hashCode();
        }
        if (getOrdering() != null) {
            _hashCode += getOrdering().hashCode();
        }
        if (getShared() != null) {
            _hashCode += getShared().hashCode();
        }
        if (getPublished() != null) {
            _hashCode += getPublished().hashCode();
        }
        if (getAttachValue() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getAttachValue());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getAttachValue(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(Media.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://www.virtuemart.net/VM_Categories/", "Media"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("virtuemart_media_id");
        elemField.setXmlName(new javax.xml.namespace.QName("", "virtuemart_media_id"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("virtuemart_vendor_id");
        elemField.setXmlName(new javax.xml.namespace.QName("", "virtuemart_vendor_id"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("file_title");
        elemField.setXmlName(new javax.xml.namespace.QName("", "file_title"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("file_description");
        elemField.setXmlName(new javax.xml.namespace.QName("", "file_description"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("file_meta");
        elemField.setXmlName(new javax.xml.namespace.QName("", "file_meta"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("file_mimetype");
        elemField.setXmlName(new javax.xml.namespace.QName("", "file_mimetype"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("file_type");
        elemField.setXmlName(new javax.xml.namespace.QName("", "file_type"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("file_url");
        elemField.setXmlName(new javax.xml.namespace.QName("", "file_url"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("file_url_thumb");
        elemField.setXmlName(new javax.xml.namespace.QName("", "file_url_thumb"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("file_is_product_image");
        elemField.setXmlName(new javax.xml.namespace.QName("", "file_is_product_image"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("file_is_downloadable");
        elemField.setXmlName(new javax.xml.namespace.QName("", "file_is_downloadable"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("file_is_forSale");
        elemField.setXmlName(new javax.xml.namespace.QName("", "file_is_forSale"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("file_params");
        elemField.setXmlName(new javax.xml.namespace.QName("", "file_params"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("ordering");
        elemField.setXmlName(new javax.xml.namespace.QName("", "ordering"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("shared");
        elemField.setXmlName(new javax.xml.namespace.QName("", "shared"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("published");
        elemField.setXmlName(new javax.xml.namespace.QName("", "published"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("attachValue");
        elemField.setXmlName(new javax.xml.namespace.QName("", "attachValue"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "base64Binary"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
