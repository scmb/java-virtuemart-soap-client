/**
 * Categorie.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package it.netgrid.virtuemart.soap.categories;

public class Categorie  implements java.io.Serializable {
    /**
	 * 
	 */
	private static final long serialVersionUID = 6049552426230690308L;

	private java.lang.String id;

    private java.lang.String vendor_id;

    private java.lang.String name;

    private java.lang.String slug;

    private java.lang.String description;

    private java.lang.String category_parent_id;

    private java.lang.String category_template;

    private java.lang.String category_layout;

    private java.lang.String category_product_layout;

    private java.lang.String products_per_row;

    private java.lang.String limit_list_start;

    private java.lang.String limit_list_step;

    private java.lang.String limit_list_max;

    private java.lang.String limit_list_initial;

    private java.lang.String hits;

    private java.lang.String published;

    private java.lang.String numberofproducts;

    private java.lang.String metarobot;

    private java.lang.String metaauthor;

    private java.lang.String metadesc;

    private java.lang.String metakey;

    private java.lang.String img_uri;

    private java.lang.String img_thumb_uri;

    private java.lang.String shared;

    private java.lang.String ordering;

    private java.lang.String customtitle;

    public Categorie() {
    }

    public Categorie(
           java.lang.String id,
           java.lang.String vendor_id,
           java.lang.String name,
           java.lang.String slug,
           java.lang.String description,
           java.lang.String category_parent_id,
           java.lang.String category_template,
           java.lang.String category_layout,
           java.lang.String category_product_layout,
           java.lang.String products_per_row,
           java.lang.String limit_list_start,
           java.lang.String limit_list_step,
           java.lang.String limit_list_max,
           java.lang.String limit_list_initial,
           java.lang.String hits,
           java.lang.String published,
           java.lang.String numberofproducts,
           java.lang.String metarobot,
           java.lang.String metaauthor,
           java.lang.String metadesc,
           java.lang.String metakey,
           java.lang.String img_uri,
           java.lang.String img_thumb_uri,
           java.lang.String shared,
           java.lang.String ordering,
           java.lang.String customtitle) {
           this.id = id;
           this.vendor_id = vendor_id;
           this.name = name;
           this.slug = slug;
           this.description = description;
           this.category_parent_id = category_parent_id;
           this.category_template = category_template;
           this.category_layout = category_layout;
           this.category_product_layout = category_product_layout;
           this.products_per_row = products_per_row;
           this.limit_list_start = limit_list_start;
           this.limit_list_step = limit_list_step;
           this.limit_list_max = limit_list_max;
           this.limit_list_initial = limit_list_initial;
           this.hits = hits;
           this.published = published;
           this.numberofproducts = numberofproducts;
           this.metarobot = metarobot;
           this.metaauthor = metaauthor;
           this.metadesc = metadesc;
           this.metakey = metakey;
           this.img_uri = img_uri;
           this.img_thumb_uri = img_thumb_uri;
           this.shared = shared;
           this.ordering = ordering;
           this.customtitle = customtitle;
    }


    /**
     * Gets the id value for this Categorie.
     * 
     * @return id
     */
    public java.lang.String getId() {
        return id;
    }


    /**
     * Sets the id value for this Categorie.
     * 
     * @param id
     */
    public void setId(java.lang.String id) {
        this.id = id;
    }


    /**
     * Gets the vendor_id value for this Categorie.
     * 
     * @return vendor_id
     */
    public java.lang.String getVendor_id() {
        return vendor_id;
    }


    /**
     * Sets the vendor_id value for this Categorie.
     * 
     * @param vendor_id
     */
    public void setVendor_id(java.lang.String vendor_id) {
        this.vendor_id = vendor_id;
    }


    /**
     * Gets the name value for this Categorie.
     * 
     * @return name
     */
    public java.lang.String getName() {
        return name;
    }


    /**
     * Sets the name value for this Categorie.
     * 
     * @param name
     */
    public void setName(java.lang.String name) {
        this.name = name;
    }


    /**
     * Gets the slug value for this Categorie.
     * 
     * @return slug
     */
    public java.lang.String getSlug() {
        return slug;
    }


    /**
     * Sets the slug value for this Categorie.
     * 
     * @param slug
     */
    public void setSlug(java.lang.String slug) {
        this.slug = slug;
    }


    /**
     * Gets the description value for this Categorie.
     * 
     * @return description
     */
    public java.lang.String getDescription() {
        return description;
    }


    /**
     * Sets the description value for this Categorie.
     * 
     * @param description
     */
    public void setDescription(java.lang.String description) {
        this.description = description;
    }


    /**
     * Gets the category_parent_id value for this Categorie.
     * 
     * @return category_parent_id
     */
    public java.lang.String getCategory_parent_id() {
        return category_parent_id;
    }


    /**
     * Sets the category_parent_id value for this Categorie.
     * 
     * @param category_parent_id
     */
    public void setCategory_parent_id(java.lang.String category_parent_id) {
        this.category_parent_id = category_parent_id;
    }


    /**
     * Gets the category_template value for this Categorie.
     * 
     * @return category_template
     */
    public java.lang.String getCategory_template() {
        return category_template;
    }


    /**
     * Sets the category_template value for this Categorie.
     * 
     * @param category_template
     */
    public void setCategory_template(java.lang.String category_template) {
        this.category_template = category_template;
    }


    /**
     * Gets the category_layout value for this Categorie.
     * 
     * @return category_layout
     */
    public java.lang.String getCategory_layout() {
        return category_layout;
    }


    /**
     * Sets the category_layout value for this Categorie.
     * 
     * @param category_layout
     */
    public void setCategory_layout(java.lang.String category_layout) {
        this.category_layout = category_layout;
    }


    /**
     * Gets the category_product_layout value for this Categorie.
     * 
     * @return category_product_layout
     */
    public java.lang.String getCategory_product_layout() {
        return category_product_layout;
    }


    /**
     * Sets the category_product_layout value for this Categorie.
     * 
     * @param category_product_layout
     */
    public void setCategory_product_layout(java.lang.String category_product_layout) {
        this.category_product_layout = category_product_layout;
    }


    /**
     * Gets the products_per_row value for this Categorie.
     * 
     * @return products_per_row
     */
    public java.lang.String getProducts_per_row() {
        return products_per_row;
    }


    /**
     * Sets the products_per_row value for this Categorie.
     * 
     * @param products_per_row
     */
    public void setProducts_per_row(java.lang.String products_per_row) {
        this.products_per_row = products_per_row;
    }


    /**
     * Gets the limit_list_start value for this Categorie.
     * 
     * @return limit_list_start
     */
    public java.lang.String getLimit_list_start() {
        return limit_list_start;
    }


    /**
     * Sets the limit_list_start value for this Categorie.
     * 
     * @param limit_list_start
     */
    public void setLimit_list_start(java.lang.String limit_list_start) {
        this.limit_list_start = limit_list_start;
    }


    /**
     * Gets the limit_list_step value for this Categorie.
     * 
     * @return limit_list_step
     */
    public java.lang.String getLimit_list_step() {
        return limit_list_step;
    }


    /**
     * Sets the limit_list_step value for this Categorie.
     * 
     * @param limit_list_step
     */
    public void setLimit_list_step(java.lang.String limit_list_step) {
        this.limit_list_step = limit_list_step;
    }


    /**
     * Gets the limit_list_max value for this Categorie.
     * 
     * @return limit_list_max
     */
    public java.lang.String getLimit_list_max() {
        return limit_list_max;
    }


    /**
     * Sets the limit_list_max value for this Categorie.
     * 
     * @param limit_list_max
     */
    public void setLimit_list_max(java.lang.String limit_list_max) {
        this.limit_list_max = limit_list_max;
    }


    /**
     * Gets the limit_list_initial value for this Categorie.
     * 
     * @return limit_list_initial
     */
    public java.lang.String getLimit_list_initial() {
        return limit_list_initial;
    }


    /**
     * Sets the limit_list_initial value for this Categorie.
     * 
     * @param limit_list_initial
     */
    public void setLimit_list_initial(java.lang.String limit_list_initial) {
        this.limit_list_initial = limit_list_initial;
    }


    /**
     * Gets the hits value for this Categorie.
     * 
     * @return hits
     */
    public java.lang.String getHits() {
        return hits;
    }


    /**
     * Sets the hits value for this Categorie.
     * 
     * @param hits
     */
    public void setHits(java.lang.String hits) {
        this.hits = hits;
    }


    /**
     * Gets the published value for this Categorie.
     * 
     * @return published
     */
    public java.lang.String getPublished() {
        return published;
    }


    /**
     * Sets the published value for this Categorie.
     * 
     * @param published
     */
    public void setPublished(java.lang.String published) {
        this.published = published;
    }


    /**
     * Gets the numberofproducts value for this Categorie.
     * 
     * @return numberofproducts
     */
    public java.lang.String getNumberofproducts() {
        return numberofproducts;
    }


    /**
     * Sets the numberofproducts value for this Categorie.
     * 
     * @param numberofproducts
     */
    public void setNumberofproducts(java.lang.String numberofproducts) {
        this.numberofproducts = numberofproducts;
    }


    /**
     * Gets the metarobot value for this Categorie.
     * 
     * @return metarobot
     */
    public java.lang.String getMetarobot() {
        return metarobot;
    }


    /**
     * Sets the metarobot value for this Categorie.
     * 
     * @param metarobot
     */
    public void setMetarobot(java.lang.String metarobot) {
        this.metarobot = metarobot;
    }


    /**
     * Gets the metaauthor value for this Categorie.
     * 
     * @return metaauthor
     */
    public java.lang.String getMetaauthor() {
        return metaauthor;
    }


    /**
     * Sets the metaauthor value for this Categorie.
     * 
     * @param metaauthor
     */
    public void setMetaauthor(java.lang.String metaauthor) {
        this.metaauthor = metaauthor;
    }


    /**
     * Gets the metadesc value for this Categorie.
     * 
     * @return metadesc
     */
    public java.lang.String getMetadesc() {
        return metadesc;
    }


    /**
     * Sets the metadesc value for this Categorie.
     * 
     * @param metadesc
     */
    public void setMetadesc(java.lang.String metadesc) {
        this.metadesc = metadesc;
    }


    /**
     * Gets the metakey value for this Categorie.
     * 
     * @return metakey
     */
    public java.lang.String getMetakey() {
        return metakey;
    }


    /**
     * Sets the metakey value for this Categorie.
     * 
     * @param metakey
     */
    public void setMetakey(java.lang.String metakey) {
        this.metakey = metakey;
    }


    /**
     * Gets the img_uri value for this Categorie.
     * 
     * @return img_uri
     */
    public java.lang.String getImg_uri() {
        return img_uri;
    }


    /**
     * Sets the img_uri value for this Categorie.
     * 
     * @param img_uri
     */
    public void setImg_uri(java.lang.String img_uri) {
        this.img_uri = img_uri;
    }


    /**
     * Gets the img_thumb_uri value for this Categorie.
     * 
     * @return img_thumb_uri
     */
    public java.lang.String getImg_thumb_uri() {
        return img_thumb_uri;
    }


    /**
     * Sets the img_thumb_uri value for this Categorie.
     * 
     * @param img_thumb_uri
     */
    public void setImg_thumb_uri(java.lang.String img_thumb_uri) {
        this.img_thumb_uri = img_thumb_uri;
    }


    /**
     * Gets the shared value for this Categorie.
     * 
     * @return shared
     */
    public java.lang.String getShared() {
        return shared;
    }


    /**
     * Sets the shared value for this Categorie.
     * 
     * @param shared
     */
    public void setShared(java.lang.String shared) {
        this.shared = shared;
    }


    /**
     * Gets the ordering value for this Categorie.
     * 
     * @return ordering
     */
    public java.lang.String getOrdering() {
        return ordering;
    }


    /**
     * Sets the ordering value for this Categorie.
     * 
     * @param ordering
     */
    public void setOrdering(java.lang.String ordering) {
        this.ordering = ordering;
    }


    /**
     * Gets the customtitle value for this Categorie.
     * 
     * @return customtitle
     */
    public java.lang.String getCustomtitle() {
        return customtitle;
    }


    /**
     * Sets the customtitle value for this Categorie.
     * 
     * @param customtitle
     */
    public void setCustomtitle(java.lang.String customtitle) {
        this.customtitle = customtitle;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof Categorie)) return false;
        Categorie other = (Categorie) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.id==null && other.getId()==null) || 
             (this.id!=null &&
              this.id.equals(other.getId()))) &&
            ((this.vendor_id==null && other.getVendor_id()==null) || 
             (this.vendor_id!=null &&
              this.vendor_id.equals(other.getVendor_id()))) &&
            ((this.name==null && other.getName()==null) || 
             (this.name!=null &&
              this.name.equals(other.getName()))) &&
            ((this.slug==null && other.getSlug()==null) || 
             (this.slug!=null &&
              this.slug.equals(other.getSlug()))) &&
            ((this.description==null && other.getDescription()==null) || 
             (this.description!=null &&
              this.description.equals(other.getDescription()))) &&
            ((this.category_parent_id==null && other.getCategory_parent_id()==null) || 
             (this.category_parent_id!=null &&
              this.category_parent_id.equals(other.getCategory_parent_id()))) &&
            ((this.category_template==null && other.getCategory_template()==null) || 
             (this.category_template!=null &&
              this.category_template.equals(other.getCategory_template()))) &&
            ((this.category_layout==null && other.getCategory_layout()==null) || 
             (this.category_layout!=null &&
              this.category_layout.equals(other.getCategory_layout()))) &&
            ((this.category_product_layout==null && other.getCategory_product_layout()==null) || 
             (this.category_product_layout!=null &&
              this.category_product_layout.equals(other.getCategory_product_layout()))) &&
            ((this.products_per_row==null && other.getProducts_per_row()==null) || 
             (this.products_per_row!=null &&
              this.products_per_row.equals(other.getProducts_per_row()))) &&
            ((this.limit_list_start==null && other.getLimit_list_start()==null) || 
             (this.limit_list_start!=null &&
              this.limit_list_start.equals(other.getLimit_list_start()))) &&
            ((this.limit_list_step==null && other.getLimit_list_step()==null) || 
             (this.limit_list_step!=null &&
              this.limit_list_step.equals(other.getLimit_list_step()))) &&
            ((this.limit_list_max==null && other.getLimit_list_max()==null) || 
             (this.limit_list_max!=null &&
              this.limit_list_max.equals(other.getLimit_list_max()))) &&
            ((this.limit_list_initial==null && other.getLimit_list_initial()==null) || 
             (this.limit_list_initial!=null &&
              this.limit_list_initial.equals(other.getLimit_list_initial()))) &&
            ((this.hits==null && other.getHits()==null) || 
             (this.hits!=null &&
              this.hits.equals(other.getHits()))) &&
            ((this.published==null && other.getPublished()==null) || 
             (this.published!=null &&
              this.published.equals(other.getPublished()))) &&
            ((this.numberofproducts==null && other.getNumberofproducts()==null) || 
             (this.numberofproducts!=null &&
              this.numberofproducts.equals(other.getNumberofproducts()))) &&
            ((this.metarobot==null && other.getMetarobot()==null) || 
             (this.metarobot!=null &&
              this.metarobot.equals(other.getMetarobot()))) &&
            ((this.metaauthor==null && other.getMetaauthor()==null) || 
             (this.metaauthor!=null &&
              this.metaauthor.equals(other.getMetaauthor()))) &&
            ((this.metadesc==null && other.getMetadesc()==null) || 
             (this.metadesc!=null &&
              this.metadesc.equals(other.getMetadesc()))) &&
            ((this.metakey==null && other.getMetakey()==null) || 
             (this.metakey!=null &&
              this.metakey.equals(other.getMetakey()))) &&
            ((this.img_uri==null && other.getImg_uri()==null) || 
             (this.img_uri!=null &&
              this.img_uri.equals(other.getImg_uri()))) &&
            ((this.img_thumb_uri==null && other.getImg_thumb_uri()==null) || 
             (this.img_thumb_uri!=null &&
              this.img_thumb_uri.equals(other.getImg_thumb_uri()))) &&
            ((this.shared==null && other.getShared()==null) || 
             (this.shared!=null &&
              this.shared.equals(other.getShared()))) &&
            ((this.ordering==null && other.getOrdering()==null) || 
             (this.ordering!=null &&
              this.ordering.equals(other.getOrdering()))) &&
            ((this.customtitle==null && other.getCustomtitle()==null) || 
             (this.customtitle!=null &&
              this.customtitle.equals(other.getCustomtitle())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getId() != null) {
            _hashCode += getId().hashCode();
        }
        if (getVendor_id() != null) {
            _hashCode += getVendor_id().hashCode();
        }
        if (getName() != null) {
            _hashCode += getName().hashCode();
        }
        if (getSlug() != null) {
            _hashCode += getSlug().hashCode();
        }
        if (getDescription() != null) {
            _hashCode += getDescription().hashCode();
        }
        if (getCategory_parent_id() != null) {
            _hashCode += getCategory_parent_id().hashCode();
        }
        if (getCategory_template() != null) {
            _hashCode += getCategory_template().hashCode();
        }
        if (getCategory_layout() != null) {
            _hashCode += getCategory_layout().hashCode();
        }
        if (getCategory_product_layout() != null) {
            _hashCode += getCategory_product_layout().hashCode();
        }
        if (getProducts_per_row() != null) {
            _hashCode += getProducts_per_row().hashCode();
        }
        if (getLimit_list_start() != null) {
            _hashCode += getLimit_list_start().hashCode();
        }
        if (getLimit_list_step() != null) {
            _hashCode += getLimit_list_step().hashCode();
        }
        if (getLimit_list_max() != null) {
            _hashCode += getLimit_list_max().hashCode();
        }
        if (getLimit_list_initial() != null) {
            _hashCode += getLimit_list_initial().hashCode();
        }
        if (getHits() != null) {
            _hashCode += getHits().hashCode();
        }
        if (getPublished() != null) {
            _hashCode += getPublished().hashCode();
        }
        if (getNumberofproducts() != null) {
            _hashCode += getNumberofproducts().hashCode();
        }
        if (getMetarobot() != null) {
            _hashCode += getMetarobot().hashCode();
        }
        if (getMetaauthor() != null) {
            _hashCode += getMetaauthor().hashCode();
        }
        if (getMetadesc() != null) {
            _hashCode += getMetadesc().hashCode();
        }
        if (getMetakey() != null) {
            _hashCode += getMetakey().hashCode();
        }
        if (getImg_uri() != null) {
            _hashCode += getImg_uri().hashCode();
        }
        if (getImg_thumb_uri() != null) {
            _hashCode += getImg_thumb_uri().hashCode();
        }
        if (getShared() != null) {
            _hashCode += getShared().hashCode();
        }
        if (getOrdering() != null) {
            _hashCode += getOrdering().hashCode();
        }
        if (getCustomtitle() != null) {
            _hashCode += getCustomtitle().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(Categorie.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://www.virtuemart.net/VM_Categories/", "Categorie"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("id");
        elemField.setXmlName(new javax.xml.namespace.QName("", "id"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("vendor_id");
        elemField.setXmlName(new javax.xml.namespace.QName("", "vendor_id"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("name");
        elemField.setXmlName(new javax.xml.namespace.QName("", "name"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("slug");
        elemField.setXmlName(new javax.xml.namespace.QName("", "slug"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("description");
        elemField.setXmlName(new javax.xml.namespace.QName("", "description"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("category_parent_id");
        elemField.setXmlName(new javax.xml.namespace.QName("", "category_parent_id"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("category_template");
        elemField.setXmlName(new javax.xml.namespace.QName("", "category_template"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("category_layout");
        elemField.setXmlName(new javax.xml.namespace.QName("", "category_layout"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("category_product_layout");
        elemField.setXmlName(new javax.xml.namespace.QName("", "category_product_layout"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("products_per_row");
        elemField.setXmlName(new javax.xml.namespace.QName("", "products_per_row"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("limit_list_start");
        elemField.setXmlName(new javax.xml.namespace.QName("", "limit_list_start"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("limit_list_step");
        elemField.setXmlName(new javax.xml.namespace.QName("", "limit_list_step"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("limit_list_max");
        elemField.setXmlName(new javax.xml.namespace.QName("", "limit_list_max"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("limit_list_initial");
        elemField.setXmlName(new javax.xml.namespace.QName("", "limit_list_initial"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("hits");
        elemField.setXmlName(new javax.xml.namespace.QName("", "hits"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("published");
        elemField.setXmlName(new javax.xml.namespace.QName("", "published"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("numberofproducts");
        elemField.setXmlName(new javax.xml.namespace.QName("", "numberofproducts"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("metarobot");
        elemField.setXmlName(new javax.xml.namespace.QName("", "metarobot"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("metaauthor");
        elemField.setXmlName(new javax.xml.namespace.QName("", "metaauthor"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("metadesc");
        elemField.setXmlName(new javax.xml.namespace.QName("", "metadesc"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("metakey");
        elemField.setXmlName(new javax.xml.namespace.QName("", "metakey"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("img_uri");
        elemField.setXmlName(new javax.xml.namespace.QName("", "img_uri"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("img_thumb_uri");
        elemField.setXmlName(new javax.xml.namespace.QName("", "img_thumb_uri"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("shared");
        elemField.setXmlName(new javax.xml.namespace.QName("", "shared"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("ordering");
        elemField.setXmlName(new javax.xml.namespace.QName("", "ordering"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("customtitle");
        elemField.setXmlName(new javax.xml.namespace.QName("", "customtitle"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
