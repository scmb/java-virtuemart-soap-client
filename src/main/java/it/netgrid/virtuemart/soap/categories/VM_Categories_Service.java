/**
 * VM_Categories_Service.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package it.netgrid.virtuemart.soap.categories;

public interface VM_Categories_Service extends javax.xml.rpc.Service {
    public java.lang.String getVM_CategoriesSOAPAddress();

    public it.netgrid.virtuemart.soap.categories.VM_Categories_PortType getVM_CategoriesSOAP() throws javax.xml.rpc.ServiceException;

    public it.netgrid.virtuemart.soap.categories.VM_Categories_PortType getVM_CategoriesSOAP(java.net.URL portAddress) throws javax.xml.rpc.ServiceException;
}
