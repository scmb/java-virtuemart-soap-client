package it.netgrid.virtuemart.soap.categories;

public class VM_CategoriesProxy implements it.netgrid.virtuemart.soap.categories.VM_Categories_PortType {
  private String _endpoint = null;
  private it.netgrid.virtuemart.soap.categories.VM_Categories_PortType vM_Categories_PortType = null;
  
  public VM_CategoriesProxy() {
    _initVM_CategoriesProxy();
  }
  
  public VM_CategoriesProxy(String endpoint) {
    _endpoint = endpoint;
    _initVM_CategoriesProxy();
  }
  
  private void _initVM_CategoriesProxy() {
    try {
      vM_Categories_PortType = (new it.netgrid.virtuemart.soap.categories.VM_Categories_ServiceLocator()).getVM_CategoriesSOAP();
      if (vM_Categories_PortType != null) {
        if (_endpoint != null)
          ((javax.xml.rpc.Stub)vM_Categories_PortType)._setProperty("javax.xml.rpc.service.endpoint.address", _endpoint);
        else
          _endpoint = (String)((javax.xml.rpc.Stub)vM_Categories_PortType)._getProperty("javax.xml.rpc.service.endpoint.address");
      }
      
    }
    catch (javax.xml.rpc.ServiceException serviceException) {}
  }
  
  public String getEndpoint() {
    return _endpoint;
  }
  
  public void setEndpoint(String endpoint) {
    _endpoint = endpoint;
    if (vM_Categories_PortType != null)
      ((javax.xml.rpc.Stub)vM_Categories_PortType)._setProperty("javax.xml.rpc.service.endpoint.address", _endpoint);
    
  }
  
  public it.netgrid.virtuemart.soap.categories.VM_Categories_PortType getVM_Categories_PortType() {
    if (vM_Categories_PortType == null)
      _initVM_CategoriesProxy();
    return vM_Categories_PortType;
  }
  
  public it.netgrid.virtuemart.soap.categories.Categorie[] getAllCategories(it.netgrid.virtuemart.soap.categories.GetAllCategoriesInput parameters) throws java.rmi.RemoteException{
    if (vM_Categories_PortType == null)
      _initVM_CategoriesProxy();
    return vM_Categories_PortType.getAllCategories(parameters);
  }
  
  public it.netgrid.virtuemart.soap.categories.Categorie[] getChildsCategories(it.netgrid.virtuemart.soap.categories.GetChildsCategoriesRequestInput parameters) throws java.rmi.RemoteException{
    if (vM_Categories_PortType == null)
      _initVM_CategoriesProxy();
    return vM_Categories_PortType.getChildsCategories(parameters);
  }
  
  public it.netgrid.virtuemart.soap.categories.CommonReturn addCategory(it.netgrid.virtuemart.soap.categories.AddCategoryInput parameters) throws java.rmi.RemoteException{
    if (vM_Categories_PortType == null)
      _initVM_CategoriesProxy();
    return vM_Categories_PortType.addCategory(parameters);
  }
  
  public it.netgrid.virtuemart.soap.categories.CommonReturn deleteCategory(it.netgrid.virtuemart.soap.categories.DeleteCategoryInput parameters) throws java.rmi.RemoteException{
    if (vM_Categories_PortType == null)
      _initVM_CategoriesProxy();
    return vM_Categories_PortType.deleteCategory(parameters);
  }
  
  public it.netgrid.virtuemart.soap.categories.AvalaibleImage[] getAvailableImages(it.netgrid.virtuemart.soap.categories.GetAvalaibleImageInput parameters) throws java.rmi.RemoteException{
    if (vM_Categories_PortType == null)
      _initVM_CategoriesProxy();
    return vM_Categories_PortType.getAvailableImages(parameters);
  }
  
  public it.netgrid.virtuemart.soap.categories.CommonReturn updateCategory(it.netgrid.virtuemart.soap.categories.AddCategoryInput parameters) throws java.rmi.RemoteException{
    if (vM_Categories_PortType == null)
      _initVM_CategoriesProxy();
    return vM_Categories_PortType.updateCategory(parameters);
  }
  
  public it.netgrid.virtuemart.soap.categories.Media[] getMediaCategory(it.netgrid.virtuemart.soap.categories.GetMediaCategoriesInput parameters) throws java.rmi.RemoteException{
    if (vM_Categories_PortType == null)
      _initVM_CategoriesProxy();
    return vM_Categories_PortType.getMediaCategory(parameters);
  }
  
  public it.netgrid.virtuemart.soap.categories.CommonReturn addMediaCategory(it.netgrid.virtuemart.soap.categories.AddMediaCategoryInput parameters) throws java.rmi.RemoteException{
    if (vM_Categories_PortType == null)
      _initVM_CategoriesProxy();
    return vM_Categories_PortType.addMediaCategory(parameters);
  }
  
  public it.netgrid.virtuemart.soap.categories.CommonReturn deleteMediaCategory(it.netgrid.virtuemart.soap.categories.DelMediaInput parameters) throws java.rmi.RemoteException{
    if (vM_Categories_PortType == null)
      _initVM_CategoriesProxy();
    return vM_Categories_PortType.deleteMediaCategory(parameters);
  }
  
  public it.netgrid.virtuemart.soap.categories.CommonReturn uploadFile(it.netgrid.virtuemart.soap.categories.UploadInput parameters) throws java.rmi.RemoteException{
    if (vM_Categories_PortType == null)
      _initVM_CategoriesProxy();
    return vM_Categories_PortType.uploadFile(parameters);
  }
  
  public it.netgrid.virtuemart.soap.categories.Template[] getTemplates(it.netgrid.virtuemart.soap.categories.GetTemplateInput parameters) throws java.rmi.RemoteException{
    if (vM_Categories_PortType == null)
      _initVM_CategoriesProxy();
    return vM_Categories_PortType.getTemplates(parameters);
  }
  
  
}