/**
 * VM_CategoriesSOAPStub.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package it.netgrid.virtuemart.soap.categories;

public class VM_CategoriesSOAPStub extends org.apache.axis.client.Stub implements it.netgrid.virtuemart.soap.categories.VM_Categories_PortType {
    private java.util.Vector cachedSerClasses = new java.util.Vector();
    private java.util.Vector cachedSerQNames = new java.util.Vector();
    private java.util.Vector cachedSerFactories = new java.util.Vector();
    private java.util.Vector cachedDeserFactories = new java.util.Vector();

    static org.apache.axis.description.OperationDesc [] _operations;

    static {
        _operations = new org.apache.axis.description.OperationDesc[11];
        _initOperationDesc1();
        _initOperationDesc2();
    }

    private static void _initOperationDesc1(){
        org.apache.axis.description.OperationDesc oper;
        org.apache.axis.description.ParameterDesc param;
        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("GetAllCategories");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://www.virtuemart.net/VM_Categories/", "GetAllCategoriesRequest"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.virtuemart.net/VM_Categories/", "GetAllCategoriesInput"), it.netgrid.virtuemart.soap.categories.GetAllCategoriesInput.class, false, false);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("http://www.virtuemart.net/VM_Categories/", "CategorieArray"));
        oper.setReturnClass(it.netgrid.virtuemart.soap.categories.Categorie[].class);
        oper.setReturnQName(new javax.xml.namespace.QName("http://www.virtuemart.net/VM_Categories/", "GetAllCategoriesResponse"));
        param = oper.getReturnParamDesc();
        param.setItemQName(new javax.xml.namespace.QName("", "Categorie"));
        oper.setStyle(org.apache.axis.constants.Style.DOCUMENT);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[0] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("GetChildsCategories");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://www.virtuemart.net/VM_Categories/", "GetChildsCategoriesRequest"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.virtuemart.net/VM_Categories/", "GetChildsCategoriesRequestInput"), it.netgrid.virtuemart.soap.categories.GetChildsCategoriesRequestInput.class, false, false);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("http://www.virtuemart.net/VM_Categories/", "CategorieArray"));
        oper.setReturnClass(it.netgrid.virtuemart.soap.categories.Categorie[].class);
        oper.setReturnQName(new javax.xml.namespace.QName("http://www.virtuemart.net/VM_Categories/", "GetChildsCategoriesResponse"));
        param = oper.getReturnParamDesc();
        param.setItemQName(new javax.xml.namespace.QName("", "Categorie"));
        oper.setStyle(org.apache.axis.constants.Style.DOCUMENT);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[1] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("AddCategory");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://www.virtuemart.net/VM_Categories/", "AddCategoryRequest"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.virtuemart.net/VM_Categories/", "AddCategoryInput"), it.netgrid.virtuemart.soap.categories.AddCategoryInput.class, false, false);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("http://www.virtuemart.net/VM_Categories/", "commonReturn"));
        oper.setReturnClass(it.netgrid.virtuemart.soap.categories.CommonReturn.class);
        oper.setReturnQName(new javax.xml.namespace.QName("http://www.virtuemart.net/VM_Categories/", "AddCategoryResponse"));
        oper.setStyle(org.apache.axis.constants.Style.DOCUMENT);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[2] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("DeleteCategory");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://www.virtuemart.net/VM_Categories/", "DeleteCategoryRequest"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.virtuemart.net/VM_Categories/", "DeleteCategoryInput"), it.netgrid.virtuemart.soap.categories.DeleteCategoryInput.class, false, false);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("http://www.virtuemart.net/VM_Categories/", "commonReturn"));
        oper.setReturnClass(it.netgrid.virtuemart.soap.categories.CommonReturn.class);
        oper.setReturnQName(new javax.xml.namespace.QName("http://www.virtuemart.net/VM_Categories/", "DeleteCategoryResponse"));
        oper.setStyle(org.apache.axis.constants.Style.DOCUMENT);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[3] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("GetAvailableImages");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://www.virtuemart.net/VM_Categories/", "GetAvailableImagesRequest"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.virtuemart.net/VM_Categories/", "GetAvalaibleImageInput"), it.netgrid.virtuemart.soap.categories.GetAvalaibleImageInput.class, false, false);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("http://www.virtuemart.net/VM_Categories/", "AvalaibleImageArray"));
        oper.setReturnClass(it.netgrid.virtuemart.soap.categories.AvalaibleImage[].class);
        oper.setReturnQName(new javax.xml.namespace.QName("http://www.virtuemart.net/VM_Categories/", "GetAvailableImagesResponse"));
        param = oper.getReturnParamDesc();
        param.setItemQName(new javax.xml.namespace.QName("", "AvalaibleImage"));
        oper.setStyle(org.apache.axis.constants.Style.DOCUMENT);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[4] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("UpdateCategory");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://www.virtuemart.net/VM_Categories/", "UpdateCategoryRequest"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.virtuemart.net/VM_Categories/", "AddCategoryInput"), it.netgrid.virtuemart.soap.categories.AddCategoryInput.class, false, false);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("http://www.virtuemart.net/VM_Categories/", "commonReturn"));
        oper.setReturnClass(it.netgrid.virtuemart.soap.categories.CommonReturn.class);
        oper.setReturnQName(new javax.xml.namespace.QName("http://www.virtuemart.net/VM_Categories/", "UpdateCategoryResponse"));
        oper.setStyle(org.apache.axis.constants.Style.DOCUMENT);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[5] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("GetMediaCategory");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://www.virtuemart.net/VM_Categories/", "GetMediaCategoryRequest"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.virtuemart.net/VM_Categories/", "GetMediaCategoriesInput"), it.netgrid.virtuemart.soap.categories.GetMediaCategoriesInput.class, false, false);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("http://www.virtuemart.net/VM_Categories/", "Medias"));
        oper.setReturnClass(it.netgrid.virtuemart.soap.categories.Media[].class);
        oper.setReturnQName(new javax.xml.namespace.QName("http://www.virtuemart.net/VM_Categories/", "GetMediaCategoryResponse"));
        param = oper.getReturnParamDesc();
        param.setItemQName(new javax.xml.namespace.QName("", "Media"));
        oper.setStyle(org.apache.axis.constants.Style.DOCUMENT);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[6] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("AddMediaCategory");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://www.virtuemart.net/VM_Categories/", "AddMediaCategoryRequest"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.virtuemart.net/VM_Categories/", "AddMediaCategoryInput"), it.netgrid.virtuemart.soap.categories.AddMediaCategoryInput.class, false, false);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("http://www.virtuemart.net/VM_Categories/", "commonReturn"));
        oper.setReturnClass(it.netgrid.virtuemart.soap.categories.CommonReturn.class);
        oper.setReturnQName(new javax.xml.namespace.QName("http://www.virtuemart.net/VM_Categories/", "AddMediaCategoryResponse"));
        oper.setStyle(org.apache.axis.constants.Style.DOCUMENT);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[7] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("DeleteMediaCategory");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://www.virtuemart.net/VM_Categories/", "DeleteMediaCategoryRequest"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.virtuemart.net/VM_Categories/", "delMediaInput"), it.netgrid.virtuemart.soap.categories.DelMediaInput.class, false, false);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("http://www.virtuemart.net/VM_Categories/", "commonReturn"));
        oper.setReturnClass(it.netgrid.virtuemart.soap.categories.CommonReturn.class);
        oper.setReturnQName(new javax.xml.namespace.QName("http://www.virtuemart.net/VM_Categories/", "DeleteMediaCategoryResponse"));
        oper.setStyle(org.apache.axis.constants.Style.DOCUMENT);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[8] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("UploadFile");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://www.virtuemart.net/VM_Categories/", "UploadFileRequest"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.virtuemart.net/VM_Categories/", "UploadInput"), it.netgrid.virtuemart.soap.categories.UploadInput.class, false, false);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("http://www.virtuemart.net/VM_Categories/", "commonReturn"));
        oper.setReturnClass(it.netgrid.virtuemart.soap.categories.CommonReturn.class);
        oper.setReturnQName(new javax.xml.namespace.QName("http://www.virtuemart.net/VM_Categories/", "UploadFileResponse"));
        oper.setStyle(org.apache.axis.constants.Style.DOCUMENT);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[9] = oper;

    }

    private static void _initOperationDesc2(){
        org.apache.axis.description.OperationDesc oper;
        org.apache.axis.description.ParameterDesc param;
        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("GetTemplates");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://www.virtuemart.net/VM_Categories/", "GetTemplatesRequest"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.virtuemart.net/VM_Categories/", "GetTemplateInput"), it.netgrid.virtuemart.soap.categories.GetTemplateInput.class, false, false);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("http://www.virtuemart.net/VM_Categories/", "TemplateArray"));
        oper.setReturnClass(it.netgrid.virtuemart.soap.categories.Template[].class);
        oper.setReturnQName(new javax.xml.namespace.QName("http://www.virtuemart.net/VM_Categories/", "GetTemplatesResponse"));
        param = oper.getReturnParamDesc();
        param.setItemQName(new javax.xml.namespace.QName("", "Template"));
        oper.setStyle(org.apache.axis.constants.Style.DOCUMENT);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[10] = oper;

    }

    public VM_CategoriesSOAPStub() throws org.apache.axis.AxisFault {
         this(null);
    }

    public VM_CategoriesSOAPStub(java.net.URL endpointURL, javax.xml.rpc.Service service) throws org.apache.axis.AxisFault {
         this(service);
         super.cachedEndpoint = endpointURL;
    }

    public VM_CategoriesSOAPStub(javax.xml.rpc.Service service) throws org.apache.axis.AxisFault {
        if (service == null) {
            super.service = new org.apache.axis.client.Service();
        } else {
            super.service = service;
        }
        ((org.apache.axis.client.Service)super.service).setTypeMappingVersion("1.2");
            java.lang.Class cls;
            javax.xml.namespace.QName qName;
            javax.xml.namespace.QName qName2;
            java.lang.Class beansf = org.apache.axis.encoding.ser.BeanSerializerFactory.class;
            java.lang.Class beandf = org.apache.axis.encoding.ser.BeanDeserializerFactory.class;
            java.lang.Class enumsf = org.apache.axis.encoding.ser.EnumSerializerFactory.class;
            java.lang.Class enumdf = org.apache.axis.encoding.ser.EnumDeserializerFactory.class;
            java.lang.Class arraysf = org.apache.axis.encoding.ser.ArraySerializerFactory.class;
            java.lang.Class arraydf = org.apache.axis.encoding.ser.ArrayDeserializerFactory.class;
            java.lang.Class simplesf = org.apache.axis.encoding.ser.SimpleSerializerFactory.class;
            java.lang.Class simpledf = org.apache.axis.encoding.ser.SimpleDeserializerFactory.class;
            java.lang.Class simplelistsf = org.apache.axis.encoding.ser.SimpleListSerializerFactory.class;
            java.lang.Class simplelistdf = org.apache.axis.encoding.ser.SimpleListDeserializerFactory.class;
            qName = new javax.xml.namespace.QName("http://www.virtuemart.net/VM_Categories/", "AddCategoryInput");
            cachedSerQNames.add(qName);
            cls = it.netgrid.virtuemart.soap.categories.AddCategoryInput.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://www.virtuemart.net/VM_Categories/", "AddMediaCategoryInput");
            cachedSerQNames.add(qName);
            cls = it.netgrid.virtuemart.soap.categories.AddMediaCategoryInput.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://www.virtuemart.net/VM_Categories/", "AvalaibleImage");
            cachedSerQNames.add(qName);
            cls = it.netgrid.virtuemart.soap.categories.AvalaibleImage.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://www.virtuemart.net/VM_Categories/", "AvalaibleImageArray");
            cachedSerQNames.add(qName);
            cls = it.netgrid.virtuemart.soap.categories.AvalaibleImage[].class;
            cachedSerClasses.add(cls);
            qName = new javax.xml.namespace.QName("http://www.virtuemart.net/VM_Categories/", "AvalaibleImage");
            qName2 = new javax.xml.namespace.QName("", "AvalaibleImage");
            cachedSerFactories.add(new org.apache.axis.encoding.ser.ArraySerializerFactory(qName, qName2));
            cachedDeserFactories.add(new org.apache.axis.encoding.ser.ArrayDeserializerFactory());

            qName = new javax.xml.namespace.QName("http://www.virtuemart.net/VM_Categories/", "Categorie");
            cachedSerQNames.add(qName);
            cls = it.netgrid.virtuemart.soap.categories.Categorie.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://www.virtuemart.net/VM_Categories/", "CategorieArray");
            cachedSerQNames.add(qName);
            cls = it.netgrid.virtuemart.soap.categories.Categorie[].class;
            cachedSerClasses.add(cls);
            qName = new javax.xml.namespace.QName("http://www.virtuemart.net/VM_Categories/", "Categorie");
            qName2 = new javax.xml.namespace.QName("", "Categorie");
            cachedSerFactories.add(new org.apache.axis.encoding.ser.ArraySerializerFactory(qName, qName2));
            cachedDeserFactories.add(new org.apache.axis.encoding.ser.ArrayDeserializerFactory());

            qName = new javax.xml.namespace.QName("http://www.virtuemart.net/VM_Categories/", "commonReturn");
            cachedSerQNames.add(qName);
            cls = it.netgrid.virtuemart.soap.categories.CommonReturn.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://www.virtuemart.net/VM_Categories/", "DeleteCategoryInput");
            cachedSerQNames.add(qName);
            cls = it.netgrid.virtuemart.soap.categories.DeleteCategoryInput.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://www.virtuemart.net/VM_Categories/", "delMediaInput");
            cachedSerQNames.add(qName);
            cls = it.netgrid.virtuemart.soap.categories.DelMediaInput.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://www.virtuemart.net/VM_Categories/", "GetAllCategoriesInput");
            cachedSerQNames.add(qName);
            cls = it.netgrid.virtuemart.soap.categories.GetAllCategoriesInput.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://www.virtuemart.net/VM_Categories/", "GetAvalaibleImageInput");
            cachedSerQNames.add(qName);
            cls = it.netgrid.virtuemart.soap.categories.GetAvalaibleImageInput.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://www.virtuemart.net/VM_Categories/", "GetChildsCategoriesRequestInput");
            cachedSerQNames.add(qName);
            cls = it.netgrid.virtuemart.soap.categories.GetChildsCategoriesRequestInput.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://www.virtuemart.net/VM_Categories/", "GetMediaCategoriesInput");
            cachedSerQNames.add(qName);
            cls = it.netgrid.virtuemart.soap.categories.GetMediaCategoriesInput.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://www.virtuemart.net/VM_Categories/", "GetTemplateInput");
            cachedSerQNames.add(qName);
            cls = it.netgrid.virtuemart.soap.categories.GetTemplateInput.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://www.virtuemart.net/VM_Categories/", "loginInfo");
            cachedSerQNames.add(qName);
            cls = it.netgrid.virtuemart.soap.categories.LoginInfo.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://www.virtuemart.net/VM_Categories/", "Media");
            cachedSerQNames.add(qName);
            cls = it.netgrid.virtuemart.soap.categories.Media.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://www.virtuemart.net/VM_Categories/", "Medias");
            cachedSerQNames.add(qName);
            cls = it.netgrid.virtuemart.soap.categories.Media[].class;
            cachedSerClasses.add(cls);
            qName = new javax.xml.namespace.QName("http://www.virtuemart.net/VM_Categories/", "Media");
            qName2 = new javax.xml.namespace.QName("", "Media");
            cachedSerFactories.add(new org.apache.axis.encoding.ser.ArraySerializerFactory(qName, qName2));
            cachedDeserFactories.add(new org.apache.axis.encoding.ser.ArrayDeserializerFactory());

            qName = new javax.xml.namespace.QName("http://www.virtuemart.net/VM_Categories/", "Template");
            cachedSerQNames.add(qName);
            cls = it.netgrid.virtuemart.soap.categories.Template.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://www.virtuemart.net/VM_Categories/", "TemplateArray");
            cachedSerQNames.add(qName);
            cls = it.netgrid.virtuemart.soap.categories.Template[].class;
            cachedSerClasses.add(cls);
            qName = new javax.xml.namespace.QName("http://www.virtuemart.net/VM_Categories/", "Template");
            qName2 = new javax.xml.namespace.QName("", "Template");
            cachedSerFactories.add(new org.apache.axis.encoding.ser.ArraySerializerFactory(qName, qName2));
            cachedDeserFactories.add(new org.apache.axis.encoding.ser.ArrayDeserializerFactory());

            qName = new javax.xml.namespace.QName("http://www.virtuemart.net/VM_Categories/", "UploadInput");
            cachedSerQNames.add(qName);
            cls = it.netgrid.virtuemart.soap.categories.UploadInput.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

    }

    protected org.apache.axis.client.Call createCall() throws java.rmi.RemoteException {
        try {
            org.apache.axis.client.Call _call = super._createCall();
            if (super.maintainSessionSet) {
                _call.setMaintainSession(super.maintainSession);
            }
            if (super.cachedUsername != null) {
                _call.setUsername(super.cachedUsername);
            }
            if (super.cachedPassword != null) {
                _call.setPassword(super.cachedPassword);
            }
            if (super.cachedEndpoint != null) {
                _call.setTargetEndpointAddress(super.cachedEndpoint);
            }
            if (super.cachedTimeout != null) {
                _call.setTimeout(super.cachedTimeout);
            }
            if (super.cachedPortName != null) {
                _call.setPortName(super.cachedPortName);
            }
            java.util.Enumeration keys = super.cachedProperties.keys();
            while (keys.hasMoreElements()) {
                java.lang.String key = (java.lang.String) keys.nextElement();
                _call.setProperty(key, super.cachedProperties.get(key));
            }
            // All the type mapping information is registered
            // when the first call is made.
            // The type mapping information is actually registered in
            // the TypeMappingRegistry of the service, which
            // is the reason why registration is only needed for the first call.
            synchronized (this) {
                if (firstCall()) {
                    // must set encoding style before registering serializers
                    _call.setEncodingStyle(null);
                    for (int i = 0; i < cachedSerFactories.size(); ++i) {
                        java.lang.Class cls = (java.lang.Class) cachedSerClasses.get(i);
                        javax.xml.namespace.QName qName =
                                (javax.xml.namespace.QName) cachedSerQNames.get(i);
                        java.lang.Object x = cachedSerFactories.get(i);
                        if (x instanceof Class) {
                            java.lang.Class sf = (java.lang.Class)
                                 cachedSerFactories.get(i);
                            java.lang.Class df = (java.lang.Class)
                                 cachedDeserFactories.get(i);
                            _call.registerTypeMapping(cls, qName, sf, df, false);
                        }
                        else if (x instanceof javax.xml.rpc.encoding.SerializerFactory) {
                            org.apache.axis.encoding.SerializerFactory sf = (org.apache.axis.encoding.SerializerFactory)
                                 cachedSerFactories.get(i);
                            org.apache.axis.encoding.DeserializerFactory df = (org.apache.axis.encoding.DeserializerFactory)
                                 cachedDeserFactories.get(i);
                            _call.registerTypeMapping(cls, qName, sf, df, false);
                        }
                    }
                }
            }
            return _call;
        }
        catch (java.lang.Throwable _t) {
            throw new org.apache.axis.AxisFault("Failure trying to get the Call object", _t);
        }
    }

    public it.netgrid.virtuemart.soap.categories.Categorie[] getAllCategories(it.netgrid.virtuemart.soap.categories.GetAllCategoriesInput parameters) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[0]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("http://www.virtuemart.net/VM_Categories/GetAllCategories");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("", "GetAllCategories"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {parameters});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (it.netgrid.virtuemart.soap.categories.Categorie[]) _resp;
            } catch (java.lang.Exception _exception) {
                return (it.netgrid.virtuemart.soap.categories.Categorie[]) org.apache.axis.utils.JavaUtils.convert(_resp, it.netgrid.virtuemart.soap.categories.Categorie[].class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public it.netgrid.virtuemart.soap.categories.Categorie[] getChildsCategories(it.netgrid.virtuemart.soap.categories.GetChildsCategoriesRequestInput parameters) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[1]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("http://www.virtuemart.net/VM_Categories/GetChildsCategories");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("", "GetChildsCategories"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {parameters});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (it.netgrid.virtuemart.soap.categories.Categorie[]) _resp;
            } catch (java.lang.Exception _exception) {
                return (it.netgrid.virtuemart.soap.categories.Categorie[]) org.apache.axis.utils.JavaUtils.convert(_resp, it.netgrid.virtuemart.soap.categories.Categorie[].class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public it.netgrid.virtuemart.soap.categories.CommonReturn addCategory(it.netgrid.virtuemart.soap.categories.AddCategoryInput parameters) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[2]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("http://www.virtuemart.net/VM_Categories/AddCategory");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("", "AddCategory"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {parameters});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (it.netgrid.virtuemart.soap.categories.CommonReturn) _resp;
            } catch (java.lang.Exception _exception) {
                return (it.netgrid.virtuemart.soap.categories.CommonReturn) org.apache.axis.utils.JavaUtils.convert(_resp, it.netgrid.virtuemart.soap.categories.CommonReturn.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public it.netgrid.virtuemart.soap.categories.CommonReturn deleteCategory(it.netgrid.virtuemart.soap.categories.DeleteCategoryInput parameters) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[3]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("http://www.virtuemart.net/VM_Categories/DeleteCategory");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("", "DeleteCategory"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {parameters});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (it.netgrid.virtuemart.soap.categories.CommonReturn) _resp;
            } catch (java.lang.Exception _exception) {
                return (it.netgrid.virtuemart.soap.categories.CommonReturn) org.apache.axis.utils.JavaUtils.convert(_resp, it.netgrid.virtuemart.soap.categories.CommonReturn.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public it.netgrid.virtuemart.soap.categories.AvalaibleImage[] getAvailableImages(it.netgrid.virtuemart.soap.categories.GetAvalaibleImageInput parameters) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[4]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("http://www.virtuemart.net/VM_Categories/GetAvailableImages");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("", "GetAvailableImages"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {parameters});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (it.netgrid.virtuemart.soap.categories.AvalaibleImage[]) _resp;
            } catch (java.lang.Exception _exception) {
                return (it.netgrid.virtuemart.soap.categories.AvalaibleImage[]) org.apache.axis.utils.JavaUtils.convert(_resp, it.netgrid.virtuemart.soap.categories.AvalaibleImage[].class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public it.netgrid.virtuemart.soap.categories.CommonReturn updateCategory(it.netgrid.virtuemart.soap.categories.AddCategoryInput parameters) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[5]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("http://www.virtuemart.net/VM_Categories/UpdateCategory");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("", "UpdateCategory"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {parameters});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (it.netgrid.virtuemart.soap.categories.CommonReturn) _resp;
            } catch (java.lang.Exception _exception) {
                return (it.netgrid.virtuemart.soap.categories.CommonReturn) org.apache.axis.utils.JavaUtils.convert(_resp, it.netgrid.virtuemart.soap.categories.CommonReturn.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public it.netgrid.virtuemart.soap.categories.Media[] getMediaCategory(it.netgrid.virtuemart.soap.categories.GetMediaCategoriesInput parameters) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[6]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("http://www.virtuemart.net/VM_Categories/GetMediaCategory");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("", "GetMediaCategory"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {parameters});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (it.netgrid.virtuemart.soap.categories.Media[]) _resp;
            } catch (java.lang.Exception _exception) {
                return (it.netgrid.virtuemart.soap.categories.Media[]) org.apache.axis.utils.JavaUtils.convert(_resp, it.netgrid.virtuemart.soap.categories.Media[].class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public it.netgrid.virtuemart.soap.categories.CommonReturn addMediaCategory(it.netgrid.virtuemart.soap.categories.AddMediaCategoryInput parameters) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[7]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("http://www.virtuemart.net/VM_Categories/AddMediaCategory");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("", "AddMediaCategory"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {parameters});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (it.netgrid.virtuemart.soap.categories.CommonReturn) _resp;
            } catch (java.lang.Exception _exception) {
                return (it.netgrid.virtuemart.soap.categories.CommonReturn) org.apache.axis.utils.JavaUtils.convert(_resp, it.netgrid.virtuemart.soap.categories.CommonReturn.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public it.netgrid.virtuemart.soap.categories.CommonReturn deleteMediaCategory(it.netgrid.virtuemart.soap.categories.DelMediaInput parameters) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[8]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("http://www.virtuemart.net/VM_Categories/DeleteMediaCategory");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("", "DeleteMediaCategory"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {parameters});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (it.netgrid.virtuemart.soap.categories.CommonReturn) _resp;
            } catch (java.lang.Exception _exception) {
                return (it.netgrid.virtuemart.soap.categories.CommonReturn) org.apache.axis.utils.JavaUtils.convert(_resp, it.netgrid.virtuemart.soap.categories.CommonReturn.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public it.netgrid.virtuemart.soap.categories.CommonReturn uploadFile(it.netgrid.virtuemart.soap.categories.UploadInput parameters) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[9]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("http://www.virtuemart.net/VM_Categories/UploadFile");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("", "UploadFile"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {parameters});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (it.netgrid.virtuemart.soap.categories.CommonReturn) _resp;
            } catch (java.lang.Exception _exception) {
                return (it.netgrid.virtuemart.soap.categories.CommonReturn) org.apache.axis.utils.JavaUtils.convert(_resp, it.netgrid.virtuemart.soap.categories.CommonReturn.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public it.netgrid.virtuemart.soap.categories.Template[] getTemplates(it.netgrid.virtuemart.soap.categories.GetTemplateInput parameters) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[10]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("http://www.virtuemart.net/VM_Categories/GetTemplates");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("", "GetTemplates"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {parameters});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (it.netgrid.virtuemart.soap.categories.Template[]) _resp;
            } catch (java.lang.Exception _exception) {
                return (it.netgrid.virtuemart.soap.categories.Template[]) org.apache.axis.utils.JavaUtils.convert(_resp, it.netgrid.virtuemart.soap.categories.Template[].class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

}
