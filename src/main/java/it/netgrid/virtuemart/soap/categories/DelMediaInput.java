/**
 * DelMediaInput.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package it.netgrid.virtuemart.soap.categories;

public class DelMediaInput  implements java.io.Serializable {
    /**
	 * 
	 */
	private static final long serialVersionUID = -8782619779400849546L;

	private it.netgrid.virtuemart.soap.categories.LoginInfo loginInfo;

    private java.lang.String category_id;

    private java.lang.String media_id;

    public DelMediaInput() {
    }

    public DelMediaInput(
           it.netgrid.virtuemart.soap.categories.LoginInfo loginInfo,
           java.lang.String category_id,
           java.lang.String media_id) {
           this.loginInfo = loginInfo;
           this.category_id = category_id;
           this.media_id = media_id;
    }


    /**
     * Gets the loginInfo value for this DelMediaInput.
     * 
     * @return loginInfo
     */
    public it.netgrid.virtuemart.soap.categories.LoginInfo getLoginInfo() {
        return loginInfo;
    }


    /**
     * Sets the loginInfo value for this DelMediaInput.
     * 
     * @param loginInfo
     */
    public void setLoginInfo(it.netgrid.virtuemart.soap.categories.LoginInfo loginInfo) {
        this.loginInfo = loginInfo;
    }


    /**
     * Gets the category_id value for this DelMediaInput.
     * 
     * @return category_id
     */
    public java.lang.String getCategory_id() {
        return category_id;
    }


    /**
     * Sets the category_id value for this DelMediaInput.
     * 
     * @param category_id
     */
    public void setCategory_id(java.lang.String category_id) {
        this.category_id = category_id;
    }


    /**
     * Gets the media_id value for this DelMediaInput.
     * 
     * @return media_id
     */
    public java.lang.String getMedia_id() {
        return media_id;
    }


    /**
     * Sets the media_id value for this DelMediaInput.
     * 
     * @param media_id
     */
    public void setMedia_id(java.lang.String media_id) {
        this.media_id = media_id;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof DelMediaInput)) return false;
        DelMediaInput other = (DelMediaInput) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.loginInfo==null && other.getLoginInfo()==null) || 
             (this.loginInfo!=null &&
              this.loginInfo.equals(other.getLoginInfo()))) &&
            ((this.category_id==null && other.getCategory_id()==null) || 
             (this.category_id!=null &&
              this.category_id.equals(other.getCategory_id()))) &&
            ((this.media_id==null && other.getMedia_id()==null) || 
             (this.media_id!=null &&
              this.media_id.equals(other.getMedia_id())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getLoginInfo() != null) {
            _hashCode += getLoginInfo().hashCode();
        }
        if (getCategory_id() != null) {
            _hashCode += getCategory_id().hashCode();
        }
        if (getMedia_id() != null) {
            _hashCode += getMedia_id().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(DelMediaInput.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://www.virtuemart.net/VM_Categories/", "delMediaInput"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("loginInfo");
        elemField.setXmlName(new javax.xml.namespace.QName("", "loginInfo"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.virtuemart.net/VM_Categories/", "loginInfo"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("category_id");
        elemField.setXmlName(new javax.xml.namespace.QName("", "category_id"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("media_id");
        elemField.setXmlName(new javax.xml.namespace.QName("", "media_id"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
