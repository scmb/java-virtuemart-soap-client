/**
 * GetAllCategoriesInput.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package it.netgrid.virtuemart.soap.categories;

public class GetAllCategoriesInput  implements java.io.Serializable {
    /**
	 * 
	 */
	private static final long serialVersionUID = -7026840122279698868L;

	private it.netgrid.virtuemart.soap.categories.LoginInfo loginInfo;

    private java.lang.String category_publish;

    private java.lang.String category_id;

    public GetAllCategoriesInput() {
    }

    public GetAllCategoriesInput(
           it.netgrid.virtuemart.soap.categories.LoginInfo loginInfo,
           java.lang.String category_publish,
           java.lang.String category_id) {
           this.loginInfo = loginInfo;
           this.category_publish = category_publish;
           this.category_id = category_id;
    }


    /**
     * Gets the loginInfo value for this GetAllCategoriesInput.
     * 
     * @return loginInfo
     */
    public it.netgrid.virtuemart.soap.categories.LoginInfo getLoginInfo() {
        return loginInfo;
    }


    /**
     * Sets the loginInfo value for this GetAllCategoriesInput.
     * 
     * @param loginInfo
     */
    public void setLoginInfo(it.netgrid.virtuemart.soap.categories.LoginInfo loginInfo) {
        this.loginInfo = loginInfo;
    }


    /**
     * Gets the category_publish value for this GetAllCategoriesInput.
     * 
     * @return category_publish
     */
    public java.lang.String getCategory_publish() {
        return category_publish;
    }


    /**
     * Sets the category_publish value for this GetAllCategoriesInput.
     * 
     * @param category_publish
     */
    public void setCategory_publish(java.lang.String category_publish) {
        this.category_publish = category_publish;
    }


    /**
     * Gets the category_id value for this GetAllCategoriesInput.
     * 
     * @return category_id
     */
    public java.lang.String getCategory_id() {
        return category_id;
    }


    /**
     * Sets the category_id value for this GetAllCategoriesInput.
     * 
     * @param category_id
     */
    public void setCategory_id(java.lang.String category_id) {
        this.category_id = category_id;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof GetAllCategoriesInput)) return false;
        GetAllCategoriesInput other = (GetAllCategoriesInput) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.loginInfo==null && other.getLoginInfo()==null) || 
             (this.loginInfo!=null &&
              this.loginInfo.equals(other.getLoginInfo()))) &&
            ((this.category_publish==null && other.getCategory_publish()==null) || 
             (this.category_publish!=null &&
              this.category_publish.equals(other.getCategory_publish()))) &&
            ((this.category_id==null && other.getCategory_id()==null) || 
             (this.category_id!=null &&
              this.category_id.equals(other.getCategory_id())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getLoginInfo() != null) {
            _hashCode += getLoginInfo().hashCode();
        }
        if (getCategory_publish() != null) {
            _hashCode += getCategory_publish().hashCode();
        }
        if (getCategory_id() != null) {
            _hashCode += getCategory_id().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(GetAllCategoriesInput.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://www.virtuemart.net/VM_Categories/", "GetAllCategoriesInput"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("loginInfo");
        elemField.setXmlName(new javax.xml.namespace.QName("", "loginInfo"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.virtuemart.net/VM_Categories/", "loginInfo"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("category_publish");
        elemField.setXmlName(new javax.xml.namespace.QName("", "category_publish"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("category_id");
        elemField.setXmlName(new javax.xml.namespace.QName("", "category_id"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
