package it.netgrid.virtuemart.soap;

import it.netgrid.virtuemart.soap.categories.VM_Categories_PortType;
import it.netgrid.virtuemart.soap.custom.VM_Custom_PortType;
import it.netgrid.virtuemart.soap.orders.VM_Order_PortType;
import it.netgrid.virtuemart.soap.products.VM_Product_PortType;
import it.netgrid.virtuemart.soap.sqlqueries.VM_SQLQueries_PortType;
import it.netgrid.virtuemart.soap.users.VM_Users_PortType;

public interface VirtuemartServiceProvider {
	
	public static final String OK = "0";
	
	public static final String TRUE = "1";
	
	public static final String FALSE = "0";
	
	public VM_Categories_PortType getCategoriesSOAP();
	
	public VM_Product_PortType getProductSOAP();
	
	public VM_Custom_PortType getCustomSOAP();
	
	public VM_Order_PortType getOrdersSOAP();
	
	public VM_SQLQueries_PortType getSQLQueriesSOAP();
	
	public VM_Users_PortType getUsersSOAP();
	
}
