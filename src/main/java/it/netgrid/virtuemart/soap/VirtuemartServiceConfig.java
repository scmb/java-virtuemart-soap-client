package it.netgrid.virtuemart.soap;

public interface VirtuemartServiceConfig {
	
	public String getBaseUrl();
	public String getVmUsername();
	public String getVmPassword();
	
}
