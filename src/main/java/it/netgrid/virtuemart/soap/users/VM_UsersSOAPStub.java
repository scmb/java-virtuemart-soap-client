/**
 * VM_UsersSOAPStub.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package it.netgrid.virtuemart.soap.users;

public class VM_UsersSOAPStub extends org.apache.axis.client.Stub implements it.netgrid.virtuemart.soap.users.VM_Users_PortType {
    private java.util.Vector cachedSerClasses = new java.util.Vector();
    private java.util.Vector cachedSerQNames = new java.util.Vector();
    private java.util.Vector cachedSerFactories = new java.util.Vector();
    private java.util.Vector cachedDeserFactories = new java.util.Vector();

    static org.apache.axis.description.OperationDesc [] _operations;

    static {
        _operations = new org.apache.axis.description.OperationDesc[51];
        _initOperationDesc1();
        _initOperationDesc2();
        _initOperationDesc3();
        _initOperationDesc4();
        _initOperationDesc5();
        _initOperationDesc6();
    }

    private static void _initOperationDesc1(){
        org.apache.axis.description.OperationDesc oper;
        org.apache.axis.description.ParameterDesc param;
        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("GetUsers");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://www.virtuemart.net/VM_Users/", "GetUsersRequest"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.virtuemart.net/VM_Users/", "GetUsersInput"), it.netgrid.virtuemart.soap.users.GetUsersInput.class, false, false);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("http://www.virtuemart.net/VM_Users/", "UsersArray"));
        oper.setReturnClass(it.netgrid.virtuemart.soap.users.User[].class);
        oper.setReturnQName(new javax.xml.namespace.QName("http://www.virtuemart.net/VM_Users/", "GetUsersResponse"));
        param = oper.getReturnParamDesc();
        param.setItemQName(new javax.xml.namespace.QName("", "User"));
        oper.setStyle(org.apache.axis.constants.Style.DOCUMENT);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[0] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("Authentification");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://www.virtuemart.net/VM_Users/", "authRequest"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.virtuemart.net/VM_Users/", "loginInfo"), it.netgrid.virtuemart.soap.users.LoginInfo.class, false, false);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("http://www.virtuemart.net/VM_Users/", "AuthOutput"));
        oper.setReturnClass(it.netgrid.virtuemart.soap.users.AuthOutput.class);
        oper.setReturnQName(new javax.xml.namespace.QName("http://www.virtuemart.net/VM_Users/", "authResponse"));
        oper.setStyle(org.apache.axis.constants.Style.DOCUMENT);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[1] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("AddUser");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://www.virtuemart.net/VM_Users/", "addUserRequest"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.virtuemart.net/VM_Users/", "AddUserInput"), it.netgrid.virtuemart.soap.users.AddUserInput.class, false, false);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("http://www.virtuemart.net/VM_Users/", "commonReturn"));
        oper.setReturnClass(it.netgrid.virtuemart.soap.users.CommonReturn.class);
        oper.setReturnQName(new javax.xml.namespace.QName("http://www.virtuemart.net/VM_Users/", "addUserResponse"));
        oper.setStyle(org.apache.axis.constants.Style.DOCUMENT);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[2] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("DeleteUser");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://www.virtuemart.net/VM_Users/", "deleteUserRequest"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.virtuemart.net/VM_Users/", "DeleteUserInput"), it.netgrid.virtuemart.soap.users.DeleteUserInput.class, false, false);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("http://www.virtuemart.net/VM_Users/", "commonReturn"));
        oper.setReturnClass(it.netgrid.virtuemart.soap.users.CommonReturn.class);
        oper.setReturnQName(new javax.xml.namespace.QName("http://www.virtuemart.net/VM_Users/", "deleteUserResponse"));
        oper.setStyle(org.apache.axis.constants.Style.DOCUMENT);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[3] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("SendMail");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://www.virtuemart.net/VM_Users/", "sendMailRequest"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.virtuemart.net/VM_Users/", "SendMailInput"), it.netgrid.virtuemart.soap.users.SendMailInput.class, false, false);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("http://www.virtuemart.net/VM_Users/", "commonReturn"));
        oper.setReturnClass(it.netgrid.virtuemart.soap.users.CommonReturn.class);
        oper.setReturnQName(new javax.xml.namespace.QName("http://www.virtuemart.net/VM_Users/", "sendMailResponse"));
        oper.setStyle(org.apache.axis.constants.Style.DOCUMENT);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[4] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("GetUserFromEmailOrUsername");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://www.virtuemart.net/VM_Users/", "getUserFromEmailOrUsernameRequest"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.virtuemart.net/VM_Users/", "getUserFromEmailOrUsernameInput"), it.netgrid.virtuemart.soap.users.GetUserFromEmailOrUsernameInput.class, false, false);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("http://www.virtuemart.net/VM_Users/", "UsersArray"));
        oper.setReturnClass(it.netgrid.virtuemart.soap.users.User[].class);
        oper.setReturnQName(new javax.xml.namespace.QName("http://www.virtuemart.net/VM_Users/", "getUserFromEmailOrUsernameResponse"));
        param = oper.getReturnParamDesc();
        param.setItemQName(new javax.xml.namespace.QName("", "User"));
        oper.setStyle(org.apache.axis.constants.Style.DOCUMENT);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[5] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("GetAllCountryCode");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://www.virtuemart.net/VM_Users/", "getAllCountryCodeRequest"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.virtuemart.net/VM_Users/", "loginInfo"), it.netgrid.virtuemart.soap.users.LoginInfo.class, false, false);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("http://www.virtuemart.net/VM_Users/", "CountryArray"));
        oper.setReturnClass(it.netgrid.virtuemart.soap.users.Country[].class);
        oper.setReturnQName(new javax.xml.namespace.QName("http://www.virtuemart.net/VM_Users/", "getAllCountryCodeResponse"));
        param = oper.getReturnParamDesc();
        param.setItemQName(new javax.xml.namespace.QName("", "Country"));
        oper.setStyle(org.apache.axis.constants.Style.DOCUMENT);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[6] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("GetAuthGroup");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://www.virtuemart.net/VM_Users/", "getAuthGroupRequest"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.virtuemart.net/VM_Users/", "loginInfo"), it.netgrid.virtuemart.soap.users.LoginInfo.class, false, false);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("http://www.virtuemart.net/VM_Users/", "AuthGroupArray"));
        oper.setReturnClass(it.netgrid.virtuemart.soap.users.AuthGroup[].class);
        oper.setReturnQName(new javax.xml.namespace.QName("http://www.virtuemart.net/VM_Users/", "getAuthGroupResponse"));
        param = oper.getReturnParamDesc();
        param.setItemQName(new javax.xml.namespace.QName("", "AuthGroup"));
        oper.setStyle(org.apache.axis.constants.Style.DOCUMENT);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[7] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("AddAuthGroup");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://www.virtuemart.net/VM_Users/", "addAuthGroupRequest"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.virtuemart.net/VM_Users/", "AddAuthGroupInput"), it.netgrid.virtuemart.soap.users.AddAuthGroupInput.class, false, false);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("http://www.virtuemart.net/VM_Users/", "commonReturn"));
        oper.setReturnClass(it.netgrid.virtuemart.soap.users.CommonReturn.class);
        oper.setReturnQName(new javax.xml.namespace.QName("http://www.virtuemart.net/VM_Users/", "addAuthGroupResponse"));
        oper.setStyle(org.apache.axis.constants.Style.DOCUMENT);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[8] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("DeleteAuthGroup");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://www.virtuemart.net/VM_Users/", "deleteAuthGroupRequest"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.virtuemart.net/VM_Users/", "DelAuthGroupInput"), it.netgrid.virtuemart.soap.users.DelAuthGroupInput.class, false, false);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("http://www.virtuemart.net/VM_Users/", "commonReturn"));
        oper.setReturnClass(it.netgrid.virtuemart.soap.users.CommonReturn.class);
        oper.setReturnQName(new javax.xml.namespace.QName("http://www.virtuemart.net/VM_Users/", "deleteAuthGroupResponse"));
        oper.setStyle(org.apache.axis.constants.Style.DOCUMENT);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[9] = oper;

    }

    private static void _initOperationDesc2(){
        org.apache.axis.description.OperationDesc oper;
        org.apache.axis.description.ParameterDesc param;
        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("GetAllStates");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://www.virtuemart.net/VM_Users/", "getAllStatesRequest"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.virtuemart.net/VM_Users/", "GetStatesInput"), it.netgrid.virtuemart.soap.users.GetStatesInput.class, false, false);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("http://www.virtuemart.net/VM_Users/", "StateArray"));
        oper.setReturnClass(it.netgrid.virtuemart.soap.users.State[].class);
        oper.setReturnQName(new javax.xml.namespace.QName("http://www.virtuemart.net/VM_Users/", "getAllStatesResponse"));
        param = oper.getReturnParamDesc();
        param.setItemQName(new javax.xml.namespace.QName("", "State"));
        oper.setStyle(org.apache.axis.constants.Style.DOCUMENT);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[10] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("AddStates");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://www.virtuemart.net/VM_Users/", "addStatesRequest"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.virtuemart.net/VM_Users/", "AddStatesInput"), it.netgrid.virtuemart.soap.users.AddStatesInput.class, false, false);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("http://www.virtuemart.net/VM_Users/", "commonReturn"));
        oper.setReturnClass(it.netgrid.virtuemart.soap.users.CommonReturn.class);
        oper.setReturnQName(new javax.xml.namespace.QName("http://www.virtuemart.net/VM_Users/", "addStatesResponse"));
        oper.setStyle(org.apache.axis.constants.Style.DOCUMENT);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[11] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("DeleteStates");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://www.virtuemart.net/VM_Users/", "deleteStatesRequest"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.virtuemart.net/VM_Users/", "DelInput"), it.netgrid.virtuemart.soap.users.DelInput.class, false, false);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("http://www.virtuemart.net/VM_Users/", "commonReturn"));
        oper.setReturnClass(it.netgrid.virtuemart.soap.users.CommonReturn.class);
        oper.setReturnQName(new javax.xml.namespace.QName("http://www.virtuemart.net/VM_Users/", "deleteStatesResponse"));
        oper.setStyle(org.apache.axis.constants.Style.DOCUMENT);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[12] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("GetShopperGroup");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://www.virtuemart.net/VM_Users/", "getShopperGroupRequest"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.virtuemart.net/VM_Users/", "loginInfo"), it.netgrid.virtuemart.soap.users.LoginInfo.class, false, false);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("http://www.virtuemart.net/VM_Users/", "ShopperGroupArray"));
        oper.setReturnClass(it.netgrid.virtuemart.soap.users.ShopperGroup[].class);
        oper.setReturnQName(new javax.xml.namespace.QName("http://www.virtuemart.net/VM_Users/", "getShopperGroupResponse"));
        param = oper.getReturnParamDesc();
        param.setItemQName(new javax.xml.namespace.QName("", "ShopperGroup"));
        oper.setStyle(org.apache.axis.constants.Style.DOCUMENT);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[13] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("AddShopperGroup");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://www.virtuemart.net/VM_Users/", "addShopperGroupRequest"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.virtuemart.net/VM_Users/", "AddShopperGroupsInput"), it.netgrid.virtuemart.soap.users.AddShopperGroupsInput.class, false, false);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("http://www.virtuemart.net/VM_Users/", "commonReturn"));
        oper.setReturnClass(it.netgrid.virtuemart.soap.users.CommonReturn.class);
        oper.setReturnQName(new javax.xml.namespace.QName("http://www.virtuemart.net/VM_Users/", "addShopperGroupResponse"));
        oper.setStyle(org.apache.axis.constants.Style.DOCUMENT);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[14] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("UpdateShopperGroup");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://www.virtuemart.net/VM_Users/", "updateShopperGroupRequest"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.virtuemart.net/VM_Users/", "AddShopperGroupsInput"), it.netgrid.virtuemart.soap.users.AddShopperGroupsInput.class, false, false);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("http://www.virtuemart.net/VM_Users/", "commonReturn"));
        oper.setReturnClass(it.netgrid.virtuemart.soap.users.CommonReturn.class);
        oper.setReturnQName(new javax.xml.namespace.QName("http://www.virtuemart.net/VM_Users/", "updateShopperGroupResponse"));
        oper.setStyle(org.apache.axis.constants.Style.DOCUMENT);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[15] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("DeleteShopperGroup");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://www.virtuemart.net/VM_Users/", "deleteShopperGroupRequest"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.virtuemart.net/VM_Users/", "DelInput"), it.netgrid.virtuemart.soap.users.DelInput.class, false, false);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("http://www.virtuemart.net/VM_Users/", "commonReturn"));
        oper.setReturnClass(it.netgrid.virtuemart.soap.users.CommonReturn.class);
        oper.setReturnQName(new javax.xml.namespace.QName("http://www.virtuemart.net/VM_Users/", "deleteShopperGroupResponse"));
        oper.setStyle(org.apache.axis.constants.Style.DOCUMENT);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[16] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("UpdateUser");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://www.virtuemart.net/VM_Users/", "updateUserRequest"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.virtuemart.net/VM_Users/", "AddUserInput"), it.netgrid.virtuemart.soap.users.AddUserInput.class, false, false);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("http://www.virtuemart.net/VM_Users/", "commonReturn"));
        oper.setReturnClass(it.netgrid.virtuemart.soap.users.CommonReturn.class);
        oper.setReturnQName(new javax.xml.namespace.QName("http://www.virtuemart.net/VM_Users/", "updateUserResponse"));
        oper.setStyle(org.apache.axis.constants.Style.DOCUMENT);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[17] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("GetAllVendor");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://www.virtuemart.net/VM_Users/", "getAllVendorRequest"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.virtuemart.net/VM_Users/", "GetVendorsInput"), it.netgrid.virtuemart.soap.users.GetVendorsInput.class, false, false);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("http://www.virtuemart.net/VM_Users/", "VendorArray"));
        oper.setReturnClass(it.netgrid.virtuemart.soap.users.Vendor[].class);
        oper.setReturnQName(new javax.xml.namespace.QName("http://www.virtuemart.net/VM_Users/", "getAllVendorResponse"));
        param = oper.getReturnParamDesc();
        param.setItemQName(new javax.xml.namespace.QName("", "Vendor"));
        oper.setStyle(org.apache.axis.constants.Style.DOCUMENT);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[18] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("AddVendor");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://www.virtuemart.net/VM_Users/", "addVendorRequest"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.virtuemart.net/VM_Users/", "AddVendorInput"), it.netgrid.virtuemart.soap.users.AddVendorInput.class, false, false);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("http://www.virtuemart.net/VM_Users/", "commonReturn"));
        oper.setReturnClass(it.netgrid.virtuemart.soap.users.CommonReturn.class);
        oper.setReturnQName(new javax.xml.namespace.QName("http://www.virtuemart.net/VM_Users/", "addVendorResponse"));
        oper.setStyle(org.apache.axis.constants.Style.DOCUMENT);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[19] = oper;

    }

    private static void _initOperationDesc3(){
        org.apache.axis.description.OperationDesc oper;
        org.apache.axis.description.ParameterDesc param;
        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("UpdateVendor");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://www.virtuemart.net/VM_Users/", "updateVendorRequest"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.virtuemart.net/VM_Users/", "AddVendorInput"), it.netgrid.virtuemart.soap.users.AddVendorInput.class, false, false);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("http://www.virtuemart.net/VM_Users/", "commonReturn"));
        oper.setReturnClass(it.netgrid.virtuemart.soap.users.CommonReturn.class);
        oper.setReturnQName(new javax.xml.namespace.QName("http://www.virtuemart.net/VM_Users/", "updateVendorResponse"));
        oper.setStyle(org.apache.axis.constants.Style.DOCUMENT);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[20] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("DeleteVendor");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://www.virtuemart.net/VM_Users/", "deleteVendorRequest"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.virtuemart.net/VM_Users/", "DelInput"), it.netgrid.virtuemart.soap.users.DelInput.class, false, false);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("http://www.virtuemart.net/VM_Users/", "commonReturn"));
        oper.setReturnClass(it.netgrid.virtuemart.soap.users.CommonReturn.class);
        oper.setReturnQName(new javax.xml.namespace.QName("http://www.virtuemart.net/VM_Users/", "deleteVendorResponse"));
        oper.setStyle(org.apache.axis.constants.Style.DOCUMENT);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[21] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("GetAllVendorCategory");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://www.virtuemart.net/VM_Users/", "getAllVendorCategoryRequest"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.virtuemart.net/VM_Users/", "loginInfo"), it.netgrid.virtuemart.soap.users.LoginInfo.class, false, false);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("http://www.virtuemart.net/VM_Users/", "VendorCategoryArray"));
        oper.setReturnClass(it.netgrid.virtuemart.soap.users.VendorCategory[].class);
        oper.setReturnQName(new javax.xml.namespace.QName("http://www.virtuemart.net/VM_Users/", "getAllVendorCategoryResponse"));
        param = oper.getReturnParamDesc();
        param.setItemQName(new javax.xml.namespace.QName("", "VendorCategory"));
        oper.setStyle(org.apache.axis.constants.Style.DOCUMENT);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[22] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("AddVendorCategory");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://www.virtuemart.net/VM_Users/", "addVendorCategoryRequest"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.virtuemart.net/VM_Users/", "AddVendorCatInput"), it.netgrid.virtuemart.soap.users.AddVendorCatInput.class, false, false);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("http://www.virtuemart.net/VM_Users/", "commonReturn"));
        oper.setReturnClass(it.netgrid.virtuemart.soap.users.CommonReturn.class);
        oper.setReturnQName(new javax.xml.namespace.QName("http://www.virtuemart.net/VM_Users/", "addVendorCategoryResponse"));
        oper.setStyle(org.apache.axis.constants.Style.DOCUMENT);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[23] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("UpdateVendorCategory");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://www.virtuemart.net/VM_Users/", "updateVendorCategoryRequest"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.virtuemart.net/VM_Users/", "AddVendorCatInput"), it.netgrid.virtuemart.soap.users.AddVendorCatInput.class, false, false);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("http://www.virtuemart.net/VM_Users/", "commonReturn"));
        oper.setReturnClass(it.netgrid.virtuemart.soap.users.CommonReturn.class);
        oper.setReturnQName(new javax.xml.namespace.QName("http://www.virtuemart.net/VM_Users/", "updateVendorCategoryResponse"));
        oper.setStyle(org.apache.axis.constants.Style.DOCUMENT);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[24] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("DeleteVendorCategory");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://www.virtuemart.net/VM_Users/", "deleteVendorCategoryRequest"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.virtuemart.net/VM_Users/", "DelInput"), it.netgrid.virtuemart.soap.users.DelInput.class, false, false);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("http://www.virtuemart.net/VM_Users/", "commonReturn"));
        oper.setReturnClass(it.netgrid.virtuemart.soap.users.CommonReturn.class);
        oper.setReturnQName(new javax.xml.namespace.QName("http://www.virtuemart.net/VM_Users/", "deleteVendorCategoryResponse"));
        oper.setStyle(org.apache.axis.constants.Style.DOCUMENT);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[25] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("GetAllManufacturer");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://www.virtuemart.net/VM_Users/", "getAllManufacturerRequest"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.virtuemart.net/VM_Users/", "loginInfo"), it.netgrid.virtuemart.soap.users.LoginInfo.class, false, false);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("http://www.virtuemart.net/VM_Users/", "ManufacturerArray"));
        oper.setReturnClass(it.netgrid.virtuemart.soap.users.Manufacturer[].class);
        oper.setReturnQName(new javax.xml.namespace.QName("http://www.virtuemart.net/VM_Users/", "getAllManufacturerResponse"));
        param = oper.getReturnParamDesc();
        param.setItemQName(new javax.xml.namespace.QName("", "Manufacturer"));
        oper.setStyle(org.apache.axis.constants.Style.DOCUMENT);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[26] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("AddManufacturer");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://www.virtuemart.net/VM_Users/", "addManufacturerRequest"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.virtuemart.net/VM_Users/", "AddManufacturerInput"), it.netgrid.virtuemart.soap.users.AddManufacturerInput.class, false, false);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("http://www.virtuemart.net/VM_Users/", "commonReturn"));
        oper.setReturnClass(it.netgrid.virtuemart.soap.users.CommonReturn.class);
        oper.setReturnQName(new javax.xml.namespace.QName("http://www.virtuemart.net/VM_Users/", "addManufacturerResponse"));
        oper.setStyle(org.apache.axis.constants.Style.DOCUMENT);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[27] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("UpdateManufacturer");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://www.virtuemart.net/VM_Users/", "updateManufacturerRequest"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.virtuemart.net/VM_Users/", "AddManufacturerInput"), it.netgrid.virtuemart.soap.users.AddManufacturerInput.class, false, false);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("http://www.virtuemart.net/VM_Users/", "commonReturn"));
        oper.setReturnClass(it.netgrid.virtuemart.soap.users.CommonReturn.class);
        oper.setReturnQName(new javax.xml.namespace.QName("http://www.virtuemart.net/VM_Users/", "updateManufacturerResponse"));
        oper.setStyle(org.apache.axis.constants.Style.DOCUMENT);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[28] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("DeleteManufacturer");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://www.virtuemart.net/VM_Users/", "deleteManufacturerRequest"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.virtuemart.net/VM_Users/", "DelInput"), it.netgrid.virtuemart.soap.users.DelInput.class, false, false);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("http://www.virtuemart.net/VM_Users/", "commonReturn"));
        oper.setReturnClass(it.netgrid.virtuemart.soap.users.CommonReturn.class);
        oper.setReturnQName(new javax.xml.namespace.QName("http://www.virtuemart.net/VM_Users/", "deleteManufacturerResponse"));
        oper.setStyle(org.apache.axis.constants.Style.DOCUMENT);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[29] = oper;

    }

    private static void _initOperationDesc4(){
        org.apache.axis.description.OperationDesc oper;
        org.apache.axis.description.ParameterDesc param;
        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("GetAllManufacturerCat");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://www.virtuemart.net/VM_Users/", "getAllManufacturerCatRequest"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.virtuemart.net/VM_Users/", "loginInfo"), it.netgrid.virtuemart.soap.users.LoginInfo.class, false, false);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("http://www.virtuemart.net/VM_Users/", "ManufacturerCatArray"));
        oper.setReturnClass(it.netgrid.virtuemart.soap.users.ManufacturerCat[].class);
        oper.setReturnQName(new javax.xml.namespace.QName("http://www.virtuemart.net/VM_Users/", "getAllManufacturerCatResponse"));
        param = oper.getReturnParamDesc();
        param.setItemQName(new javax.xml.namespace.QName("", "ManufacturerCat"));
        oper.setStyle(org.apache.axis.constants.Style.DOCUMENT);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[30] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("AddManufacturerCat");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://www.virtuemart.net/VM_Users/", "addManufacturerCatRequest"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.virtuemart.net/VM_Users/", "AddManufacturerCatInput"), it.netgrid.virtuemart.soap.users.AddManufacturerCatInput.class, false, false);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("http://www.virtuemart.net/VM_Users/", "commonReturn"));
        oper.setReturnClass(it.netgrid.virtuemart.soap.users.CommonReturn.class);
        oper.setReturnQName(new javax.xml.namespace.QName("http://www.virtuemart.net/VM_Users/", "addManufacturerCatResponse"));
        oper.setStyle(org.apache.axis.constants.Style.DOCUMENT);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[31] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("UpdateManufacturerCat");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://www.virtuemart.net/VM_Users/", "updateManufacturerCatRequest"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.virtuemart.net/VM_Users/", "AddManufacturerCatInput"), it.netgrid.virtuemart.soap.users.AddManufacturerCatInput.class, false, false);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("http://www.virtuemart.net/VM_Users/", "commonReturn"));
        oper.setReturnClass(it.netgrid.virtuemart.soap.users.CommonReturn.class);
        oper.setReturnQName(new javax.xml.namespace.QName("http://www.virtuemart.net/VM_Users/", "updateManufacturerCatResponse"));
        oper.setStyle(org.apache.axis.constants.Style.DOCUMENT);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[32] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("DeleteManufacturerCat");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://www.virtuemart.net/VM_Users/", "deleteManufacturerCatRequest"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.virtuemart.net/VM_Users/", "DelInput"), it.netgrid.virtuemart.soap.users.DelInput.class, false, false);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("http://www.virtuemart.net/VM_Users/", "commonReturn"));
        oper.setReturnClass(it.netgrid.virtuemart.soap.users.CommonReturn.class);
        oper.setReturnQName(new javax.xml.namespace.QName("http://www.virtuemart.net/VM_Users/", "deleteManufacturerCatResponse"));
        oper.setStyle(org.apache.axis.constants.Style.DOCUMENT);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[33] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("GetAvailableVendorImages");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://www.virtuemart.net/VM_Users/", "GetAvailableImagesRequest"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.virtuemart.net/VM_Users/", "GetAvalaibleImageInput"), it.netgrid.virtuemart.soap.users.GetAvalaibleImageInput.class, false, false);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("http://www.virtuemart.net/VM_Users/", "AvalaibleImageArray"));
        oper.setReturnClass(it.netgrid.virtuemart.soap.users.AvalaibleImage[].class);
        oper.setReturnQName(new javax.xml.namespace.QName("http://www.virtuemart.net/VM_Users/", "GetAvailableImagesResponse"));
        param = oper.getReturnParamDesc();
        param.setItemQName(new javax.xml.namespace.QName("", "AvalaibleImage"));
        oper.setStyle(org.apache.axis.constants.Style.DOCUMENT);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[34] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("GetVersions");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://www.virtuemart.net/VM_Users/", "GetVersionsRequest"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.virtuemart.net/VM_Users/", "loginInfo"), it.netgrid.virtuemart.soap.users.LoginInfo.class, false, false);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("http://www.virtuemart.net/VM_Users/", "Version"));
        oper.setReturnClass(it.netgrid.virtuemart.soap.users.Version.class);
        oper.setReturnQName(new javax.xml.namespace.QName("http://www.virtuemart.net/VM_Users/", "GetVersionsResponse"));
        oper.setStyle(org.apache.axis.constants.Style.DOCUMENT);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[35] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("GetAdditionalUserInfo");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://www.virtuemart.net/VM_Users/", "GetAdditionalUserInfoRequest"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.virtuemart.net/VM_Users/", "GetAdditionalUserInfoInput"), it.netgrid.virtuemart.soap.users.GetAdditionalUserInfoInput.class, false, false);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("http://www.virtuemart.net/VM_Users/", "UsersArray"));
        oper.setReturnClass(it.netgrid.virtuemart.soap.users.User[].class);
        oper.setReturnQName(new javax.xml.namespace.QName("http://www.virtuemart.net/VM_Users/", "GetAdditionalUserInfoResponse"));
        param = oper.getReturnParamDesc();
        param.setItemQName(new javax.xml.namespace.QName("", "User"));
        oper.setStyle(org.apache.axis.constants.Style.DOCUMENT);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[36] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("GetSessions");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://www.virtuemart.net/VM_Users/", "GetSessionsRequest"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.virtuemart.net/VM_Users/", "GetSessionsInput"), it.netgrid.virtuemart.soap.users.GetSessionsInput.class, false, false);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("http://www.virtuemart.net/VM_Users/", "Sessions"));
        oper.setReturnClass(it.netgrid.virtuemart.soap.users.Session[].class);
        oper.setReturnQName(new javax.xml.namespace.QName("http://www.virtuemart.net/VM_Users/", "GetSessionsResponse"));
        param = oper.getReturnParamDesc();
        param.setItemQName(new javax.xml.namespace.QName("", "Session"));
        oper.setStyle(org.apache.axis.constants.Style.DOCUMENT);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[37] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("GetWaitingList");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://www.virtuemart.net/VM_Users/", "GetWaitingListRequest"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.virtuemart.net/VM_Users/", "GetWaitingListInput"), it.netgrid.virtuemart.soap.users.GetWaitingListInput.class, false, false);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("http://www.virtuemart.net/VM_Users/", "WaitingLists"));
        oper.setReturnClass(it.netgrid.virtuemart.soap.users.WaitingList[].class);
        oper.setReturnQName(new javax.xml.namespace.QName("http://www.virtuemart.net/VM_Users/", "GetWaitingListResponse"));
        param = oper.getReturnParamDesc();
        param.setItemQName(new javax.xml.namespace.QName("", "WaitingList"));
        oper.setStyle(org.apache.axis.constants.Style.DOCUMENT);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[38] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("NotifyWaitingList");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://www.virtuemart.net/VM_Users/", "NotifyWaitingListRequest"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.virtuemart.net/VM_Users/", "NotifyWaitingListInput"), it.netgrid.virtuemart.soap.users.NotifyWaitingListInput.class, false, false);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("http://www.virtuemart.net/VM_Users/", "commonReturn"));
        oper.setReturnClass(it.netgrid.virtuemart.soap.users.CommonReturn.class);
        oper.setReturnQName(new javax.xml.namespace.QName("http://www.virtuemart.net/VM_Users/", "NotifyWaitingListResponse"));
        oper.setStyle(org.apache.axis.constants.Style.DOCUMENT);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[39] = oper;

    }

    private static void _initOperationDesc5(){
        org.apache.axis.description.OperationDesc oper;
        org.apache.axis.description.ParameterDesc param;
        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("GetUserInfoFromOrderID");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://www.virtuemart.net/VM_Users/", "GetUserInfoFromOrderIDRequest"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.virtuemart.net/VM_Users/", "GetUserInfoFromOrderIDInput"), it.netgrid.virtuemart.soap.users.GetUserInfoFromOrderIDInput.class, false, false);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("http://www.virtuemart.net/VM_Users/", "UsersArray"));
        oper.setReturnClass(it.netgrid.virtuemart.soap.users.User[].class);
        oper.setReturnQName(new javax.xml.namespace.QName("http://www.virtuemart.net/VM_Users/", "GetUserInfoFromOrderIDResponse"));
        param = oper.getReturnParamDesc();
        param.setItemQName(new javax.xml.namespace.QName("", "User"));
        oper.setStyle(org.apache.axis.constants.Style.DOCUMENT);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[40] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("GetUserfields");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://www.virtuemart.net/VM_Users/", "GetUserfieldsRequest"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.virtuemart.net/VM_Users/", "GenericGetInput"), it.netgrid.virtuemart.soap.users.GenericGetInput.class, false, false);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("http://www.virtuemart.net/VM_Users/", "Userfields"));
        oper.setReturnClass(it.netgrid.virtuemart.soap.users.Userfield[].class);
        oper.setReturnQName(new javax.xml.namespace.QName("http://www.virtuemart.net/VM_Users/", "GetUserfieldsResponse"));
        param = oper.getReturnParamDesc();
        param.setItemQName(new javax.xml.namespace.QName("", "Userfield"));
        oper.setStyle(org.apache.axis.constants.Style.DOCUMENT);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[41] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("GetMediaVendor");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://www.virtuemart.net/VM_Users/", "GetMediaVendorRequest"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.virtuemart.net/VM_Users/", "GetMediaVendorInput"), it.netgrid.virtuemart.soap.users.GetMediaVendorInput.class, false, false);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("http://www.virtuemart.net/VM_Users/", "Medias"));
        oper.setReturnClass(it.netgrid.virtuemart.soap.users.Media[].class);
        oper.setReturnQName(new javax.xml.namespace.QName("http://www.virtuemart.net/VM_Users/", "GetMediaVendorResponse"));
        param = oper.getReturnParamDesc();
        param.setItemQName(new javax.xml.namespace.QName("", "Media"));
        oper.setStyle(org.apache.axis.constants.Style.DOCUMENT);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[42] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("GetMediaManufacturer");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://www.virtuemart.net/VM_Users/", "GetMediaManufacturerRequest"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.virtuemart.net/VM_Users/", "GetMediaManufacturerInput"), it.netgrid.virtuemart.soap.users.GetMediaManufacturerInput.class, false, false);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("http://www.virtuemart.net/VM_Users/", "Medias"));
        oper.setReturnClass(it.netgrid.virtuemart.soap.users.Media[].class);
        oper.setReturnQName(new javax.xml.namespace.QName("http://www.virtuemart.net/VM_Users/", "GetMediaManufacturerResponse"));
        param = oper.getReturnParamDesc();
        param.setItemQName(new javax.xml.namespace.QName("", "Media"));
        oper.setStyle(org.apache.axis.constants.Style.DOCUMENT);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[43] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("AddMediaVendor");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://www.virtuemart.net/VM_Users/", "AddMediaVendorRequest"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.virtuemart.net/VM_Users/", "AddMediaVendorInput"), it.netgrid.virtuemart.soap.users.AddMediaVendorInput.class, false, false);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("http://www.virtuemart.net/VM_Users/", "commonReturn"));
        oper.setReturnClass(it.netgrid.virtuemart.soap.users.CommonReturn.class);
        oper.setReturnQName(new javax.xml.namespace.QName("http://www.virtuemart.net/VM_Users/", "AddMediaVendorResponse"));
        oper.setStyle(org.apache.axis.constants.Style.DOCUMENT);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[44] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("AddMediaManufacturer");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://www.virtuemart.net/VM_Users/", "AddMediaManufacturerRequest"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.virtuemart.net/VM_Users/", "AddMediaManufacturerInput"), it.netgrid.virtuemart.soap.users.AddMediaManufacturerInput.class, false, false);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("http://www.virtuemart.net/VM_Users/", "commonReturn"));
        oper.setReturnClass(it.netgrid.virtuemart.soap.users.CommonReturn.class);
        oper.setReturnQName(new javax.xml.namespace.QName("http://www.virtuemart.net/VM_Users/", "AddMediaManufacturerResponse"));
        oper.setStyle(org.apache.axis.constants.Style.DOCUMENT);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[45] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("DeleteMediaVendor");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://www.virtuemart.net/VM_Users/", "DeleteMediaVendorRequest"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.virtuemart.net/VM_Users/", "delMediaInput"), it.netgrid.virtuemart.soap.users.DelMediaInput.class, false, false);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("http://www.virtuemart.net/VM_Users/", "commonReturn"));
        oper.setReturnClass(it.netgrid.virtuemart.soap.users.CommonReturn.class);
        oper.setReturnQName(new javax.xml.namespace.QName("http://www.virtuemart.net/VM_Users/", "DeleteMediaVendorResponse"));
        oper.setStyle(org.apache.axis.constants.Style.DOCUMENT);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[46] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("DeleteMediaManufacturer");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://www.virtuemart.net/VM_Users/", "DeleteMediaManufacturerRequest"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.virtuemart.net/VM_Users/", "delMediaInput"), it.netgrid.virtuemart.soap.users.DelMediaInput.class, false, false);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("http://www.virtuemart.net/VM_Users/", "commonReturn"));
        oper.setReturnClass(it.netgrid.virtuemart.soap.users.CommonReturn.class);
        oper.setReturnQName(new javax.xml.namespace.QName("http://www.virtuemart.net/VM_Users/", "DeleteMediaManufacturerResponse"));
        oper.setStyle(org.apache.axis.constants.Style.DOCUMENT);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[47] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("AddShippingAdress");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://www.virtuemart.net/VM_Users/", "AddShippingAdressRequest"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.virtuemart.net/VM_Users/", "AddShippingAdressInput"), it.netgrid.virtuemart.soap.users.AddShippingAdressInput.class, false, false);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("http://www.virtuemart.net/VM_Users/", "commonReturn"));
        oper.setReturnClass(it.netgrid.virtuemart.soap.users.CommonReturn.class);
        oper.setReturnQName(new javax.xml.namespace.QName("http://www.virtuemart.net/VM_Users/", "AddShippingAdressResponse"));
        oper.setStyle(org.apache.axis.constants.Style.DOCUMENT);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[48] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("UpdateShippingAdress");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://www.virtuemart.net/VM_Users/", "UpdateShippingAdressRequest"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.virtuemart.net/VM_Users/", "AddShippingAdressInput"), it.netgrid.virtuemart.soap.users.AddShippingAdressInput.class, false, false);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("http://www.virtuemart.net/VM_Users/", "commonReturn"));
        oper.setReturnClass(it.netgrid.virtuemart.soap.users.CommonReturn.class);
        oper.setReturnQName(new javax.xml.namespace.QName("http://www.virtuemart.net/VM_Users/", "UpdateShippingAdressResponse"));
        oper.setStyle(org.apache.axis.constants.Style.DOCUMENT);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[49] = oper;

    }

    private static void _initOperationDesc6(){
        org.apache.axis.description.OperationDesc oper;
        org.apache.axis.description.ParameterDesc param;
        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("DeleteShippingAdress");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://www.virtuemart.net/VM_Users/", "DeleteShippingAdressRequest"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.virtuemart.net/VM_Users/", "DelShippingAdressInput"), it.netgrid.virtuemart.soap.users.DelShippingAdressInput.class, false, false);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("http://www.virtuemart.net/VM_Users/", "commonReturn"));
        oper.setReturnClass(it.netgrid.virtuemart.soap.users.CommonReturn.class);
        oper.setReturnQName(new javax.xml.namespace.QName("http://www.virtuemart.net/VM_Users/", "DeleteShippingAdressResponse"));
        oper.setStyle(org.apache.axis.constants.Style.DOCUMENT);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[50] = oper;

    }

    public VM_UsersSOAPStub() throws org.apache.axis.AxisFault {
         this(null);
    }

    public VM_UsersSOAPStub(java.net.URL endpointURL, javax.xml.rpc.Service service) throws org.apache.axis.AxisFault {
         this(service);
         super.cachedEndpoint = endpointURL;
    }

    public VM_UsersSOAPStub(javax.xml.rpc.Service service) throws org.apache.axis.AxisFault {
        if (service == null) {
            super.service = new org.apache.axis.client.Service();
        } else {
            super.service = service;
        }
        ((org.apache.axis.client.Service)super.service).setTypeMappingVersion("1.2");
            java.lang.Class cls;
            javax.xml.namespace.QName qName;
            javax.xml.namespace.QName qName2;
            java.lang.Class beansf = org.apache.axis.encoding.ser.BeanSerializerFactory.class;
            java.lang.Class beandf = org.apache.axis.encoding.ser.BeanDeserializerFactory.class;
            java.lang.Class enumsf = org.apache.axis.encoding.ser.EnumSerializerFactory.class;
            java.lang.Class enumdf = org.apache.axis.encoding.ser.EnumDeserializerFactory.class;
            java.lang.Class arraysf = org.apache.axis.encoding.ser.ArraySerializerFactory.class;
            java.lang.Class arraydf = org.apache.axis.encoding.ser.ArrayDeserializerFactory.class;
            java.lang.Class simplesf = org.apache.axis.encoding.ser.SimpleSerializerFactory.class;
            java.lang.Class simpledf = org.apache.axis.encoding.ser.SimpleDeserializerFactory.class;
            java.lang.Class simplelistsf = org.apache.axis.encoding.ser.SimpleListSerializerFactory.class;
            java.lang.Class simplelistdf = org.apache.axis.encoding.ser.SimpleListDeserializerFactory.class;
            qName = new javax.xml.namespace.QName("http://www.virtuemart.net/VM_Users/", "AddAuthGroupInput");
            cachedSerQNames.add(qName);
            cls = it.netgrid.virtuemart.soap.users.AddAuthGroupInput.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://www.virtuemart.net/VM_Users/", "AddManufacturerCatInput");
            cachedSerQNames.add(qName);
            cls = it.netgrid.virtuemart.soap.users.AddManufacturerCatInput.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://www.virtuemart.net/VM_Users/", "AddManufacturerInput");
            cachedSerQNames.add(qName);
            cls = it.netgrid.virtuemart.soap.users.AddManufacturerInput.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://www.virtuemart.net/VM_Users/", "AddMediaManufacturerInput");
            cachedSerQNames.add(qName);
            cls = it.netgrid.virtuemart.soap.users.AddMediaManufacturerInput.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://www.virtuemart.net/VM_Users/", "AddMediaVendorInput");
            cachedSerQNames.add(qName);
            cls = it.netgrid.virtuemart.soap.users.AddMediaVendorInput.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://www.virtuemart.net/VM_Users/", "AddShippingAdressInput");
            cachedSerQNames.add(qName);
            cls = it.netgrid.virtuemart.soap.users.AddShippingAdressInput.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://www.virtuemart.net/VM_Users/", "AddShopperGroupsInput");
            cachedSerQNames.add(qName);
            cls = it.netgrid.virtuemart.soap.users.AddShopperGroupsInput.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://www.virtuemart.net/VM_Users/", "AddStatesInput");
            cachedSerQNames.add(qName);
            cls = it.netgrid.virtuemart.soap.users.AddStatesInput.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://www.virtuemart.net/VM_Users/", "AddUserInput");
            cachedSerQNames.add(qName);
            cls = it.netgrid.virtuemart.soap.users.AddUserInput.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://www.virtuemart.net/VM_Users/", "AddVendorCatInput");
            cachedSerQNames.add(qName);
            cls = it.netgrid.virtuemart.soap.users.AddVendorCatInput.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://www.virtuemart.net/VM_Users/", "AddVendorInput");
            cachedSerQNames.add(qName);
            cls = it.netgrid.virtuemart.soap.users.AddVendorInput.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://www.virtuemart.net/VM_Users/", "AuthGroup");
            cachedSerQNames.add(qName);
            cls = it.netgrid.virtuemart.soap.users.AuthGroup.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://www.virtuemart.net/VM_Users/", "AuthGroupArray");
            cachedSerQNames.add(qName);
            cls = it.netgrid.virtuemart.soap.users.AuthGroup[].class;
            cachedSerClasses.add(cls);
            qName = new javax.xml.namespace.QName("http://www.virtuemart.net/VM_Users/", "AuthGroup");
            qName2 = new javax.xml.namespace.QName("", "AuthGroup");
            cachedSerFactories.add(new org.apache.axis.encoding.ser.ArraySerializerFactory(qName, qName2));
            cachedDeserFactories.add(new org.apache.axis.encoding.ser.ArrayDeserializerFactory());

            qName = new javax.xml.namespace.QName("http://www.virtuemart.net/VM_Users/", "AuthOutput");
            cachedSerQNames.add(qName);
            cls = it.netgrid.virtuemart.soap.users.AuthOutput.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://www.virtuemart.net/VM_Users/", "AvalaibleImage");
            cachedSerQNames.add(qName);
            cls = it.netgrid.virtuemart.soap.users.AvalaibleImage.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://www.virtuemart.net/VM_Users/", "AvalaibleImageArray");
            cachedSerQNames.add(qName);
            cls = it.netgrid.virtuemart.soap.users.AvalaibleImage[].class;
            cachedSerClasses.add(cls);
            qName = new javax.xml.namespace.QName("http://www.virtuemart.net/VM_Users/", "AvalaibleImage");
            qName2 = new javax.xml.namespace.QName("", "AvalaibleImage");
            cachedSerFactories.add(new org.apache.axis.encoding.ser.ArraySerializerFactory(qName, qName2));
            cachedDeserFactories.add(new org.apache.axis.encoding.ser.ArrayDeserializerFactory());

            qName = new javax.xml.namespace.QName("http://www.virtuemart.net/VM_Users/", "commonReturn");
            cachedSerQNames.add(qName);
            cls = it.netgrid.virtuemart.soap.users.CommonReturn.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://www.virtuemart.net/VM_Users/", "Country");
            cachedSerQNames.add(qName);
            cls = it.netgrid.virtuemart.soap.users.Country.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://www.virtuemart.net/VM_Users/", "CountryArray");
            cachedSerQNames.add(qName);
            cls = it.netgrid.virtuemart.soap.users.Country[].class;
            cachedSerClasses.add(cls);
            qName = new javax.xml.namespace.QName("http://www.virtuemart.net/VM_Users/", "Country");
            qName2 = new javax.xml.namespace.QName("", "Country");
            cachedSerFactories.add(new org.apache.axis.encoding.ser.ArraySerializerFactory(qName, qName2));
            cachedDeserFactories.add(new org.apache.axis.encoding.ser.ArrayDeserializerFactory());

            qName = new javax.xml.namespace.QName("http://www.virtuemart.net/VM_Users/", "DelAuthGroupInput");
            cachedSerQNames.add(qName);
            cls = it.netgrid.virtuemart.soap.users.DelAuthGroupInput.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://www.virtuemart.net/VM_Users/", "DeleteUserInput");
            cachedSerQNames.add(qName);
            cls = it.netgrid.virtuemart.soap.users.DeleteUserInput.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://www.virtuemart.net/VM_Users/", "DelInput");
            cachedSerQNames.add(qName);
            cls = it.netgrid.virtuemart.soap.users.DelInput.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://www.virtuemart.net/VM_Users/", "delMediaInput");
            cachedSerQNames.add(qName);
            cls = it.netgrid.virtuemart.soap.users.DelMediaInput.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://www.virtuemart.net/VM_Users/", "DelShippingAdressInput");
            cachedSerQNames.add(qName);
            cls = it.netgrid.virtuemart.soap.users.DelShippingAdressInput.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://www.virtuemart.net/VM_Users/", "EmailParams");
            cachedSerQNames.add(qName);
            cls = it.netgrid.virtuemart.soap.users.EmailParams.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://www.virtuemart.net/VM_Users/", "GenericGetInput");
            cachedSerQNames.add(qName);
            cls = it.netgrid.virtuemart.soap.users.GenericGetInput.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://www.virtuemart.net/VM_Users/", "GetAdditionalUserInfoInput");
            cachedSerQNames.add(qName);
            cls = it.netgrid.virtuemart.soap.users.GetAdditionalUserInfoInput.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://www.virtuemart.net/VM_Users/", "GetAvalaibleImageInput");
            cachedSerQNames.add(qName);
            cls = it.netgrid.virtuemart.soap.users.GetAvalaibleImageInput.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://www.virtuemart.net/VM_Users/", "GetMediaManufacturerInput");
            cachedSerQNames.add(qName);
            cls = it.netgrid.virtuemart.soap.users.GetMediaManufacturerInput.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://www.virtuemart.net/VM_Users/", "GetMediaVendorInput");
            cachedSerQNames.add(qName);
            cls = it.netgrid.virtuemart.soap.users.GetMediaVendorInput.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://www.virtuemart.net/VM_Users/", "GetSessionsInput");
            cachedSerQNames.add(qName);
            cls = it.netgrid.virtuemart.soap.users.GetSessionsInput.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://www.virtuemart.net/VM_Users/", "GetStatesInput");
            cachedSerQNames.add(qName);
            cls = it.netgrid.virtuemart.soap.users.GetStatesInput.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://www.virtuemart.net/VM_Users/", "getUserFromEmailOrUsernameInput");
            cachedSerQNames.add(qName);
            cls = it.netgrid.virtuemart.soap.users.GetUserFromEmailOrUsernameInput.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://www.virtuemart.net/VM_Users/", "GetUserInfoFromOrderIDInput");
            cachedSerQNames.add(qName);
            cls = it.netgrid.virtuemart.soap.users.GetUserInfoFromOrderIDInput.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://www.virtuemart.net/VM_Users/", "GetUsersInput");
            cachedSerQNames.add(qName);
            cls = it.netgrid.virtuemart.soap.users.GetUsersInput.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://www.virtuemart.net/VM_Users/", "GetVendorsInput");
            cachedSerQNames.add(qName);
            cls = it.netgrid.virtuemart.soap.users.GetVendorsInput.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://www.virtuemart.net/VM_Users/", "GetWaitingListInput");
            cachedSerQNames.add(qName);
            cls = it.netgrid.virtuemart.soap.users.GetWaitingListInput.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://www.virtuemart.net/VM_Users/", "ids");
            cachedSerQNames.add(qName);
            cls = java.lang.String[].class;
            cachedSerClasses.add(cls);
            qName = new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string");
            qName2 = new javax.xml.namespace.QName("", "id");
            cachedSerFactories.add(new org.apache.axis.encoding.ser.ArraySerializerFactory(qName, qName2));
            cachedDeserFactories.add(new org.apache.axis.encoding.ser.ArrayDeserializerFactory());

            qName = new javax.xml.namespace.QName("http://www.virtuemart.net/VM_Users/", "Image");
            cachedSerQNames.add(qName);
            cls = it.netgrid.virtuemart.soap.users.Image.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://www.virtuemart.net/VM_Users/", "Images");
            cachedSerQNames.add(qName);
            cls = it.netgrid.virtuemart.soap.users.Image[].class;
            cachedSerClasses.add(cls);
            qName = new javax.xml.namespace.QName("http://www.virtuemart.net/VM_Users/", "Image");
            qName2 = new javax.xml.namespace.QName("", "Image");
            cachedSerFactories.add(new org.apache.axis.encoding.ser.ArraySerializerFactory(qName, qName2));
            cachedDeserFactories.add(new org.apache.axis.encoding.ser.ArrayDeserializerFactory());

            qName = new javax.xml.namespace.QName("http://www.virtuemart.net/VM_Users/", "loginInfo");
            cachedSerQNames.add(qName);
            cls = it.netgrid.virtuemart.soap.users.LoginInfo.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://www.virtuemart.net/VM_Users/", "Manufacturer");
            cachedSerQNames.add(qName);
            cls = it.netgrid.virtuemart.soap.users.Manufacturer.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://www.virtuemart.net/VM_Users/", "ManufacturerArray");
            cachedSerQNames.add(qName);
            cls = it.netgrid.virtuemart.soap.users.Manufacturer[].class;
            cachedSerClasses.add(cls);
            qName = new javax.xml.namespace.QName("http://www.virtuemart.net/VM_Users/", "Manufacturer");
            qName2 = new javax.xml.namespace.QName("", "Manufacturer");
            cachedSerFactories.add(new org.apache.axis.encoding.ser.ArraySerializerFactory(qName, qName2));
            cachedDeserFactories.add(new org.apache.axis.encoding.ser.ArrayDeserializerFactory());

            qName = new javax.xml.namespace.QName("http://www.virtuemart.net/VM_Users/", "ManufacturerCat");
            cachedSerQNames.add(qName);
            cls = it.netgrid.virtuemart.soap.users.ManufacturerCat.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://www.virtuemart.net/VM_Users/", "ManufacturerCatArray");
            cachedSerQNames.add(qName);
            cls = it.netgrid.virtuemart.soap.users.ManufacturerCat[].class;
            cachedSerClasses.add(cls);
            qName = new javax.xml.namespace.QName("http://www.virtuemart.net/VM_Users/", "ManufacturerCat");
            qName2 = new javax.xml.namespace.QName("", "ManufacturerCat");
            cachedSerFactories.add(new org.apache.axis.encoding.ser.ArraySerializerFactory(qName, qName2));
            cachedDeserFactories.add(new org.apache.axis.encoding.ser.ArrayDeserializerFactory());

            qName = new javax.xml.namespace.QName("http://www.virtuemart.net/VM_Users/", "Media");
            cachedSerQNames.add(qName);
            cls = it.netgrid.virtuemart.soap.users.Media.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://www.virtuemart.net/VM_Users/", "Medias");
            cachedSerQNames.add(qName);
            cls = it.netgrid.virtuemart.soap.users.Media[].class;
            cachedSerClasses.add(cls);
            qName = new javax.xml.namespace.QName("http://www.virtuemart.net/VM_Users/", "Media");
            qName2 = new javax.xml.namespace.QName("", "Media");
            cachedSerFactories.add(new org.apache.axis.encoding.ser.ArraySerializerFactory(qName, qName2));
            cachedDeserFactories.add(new org.apache.axis.encoding.ser.ArrayDeserializerFactory());

            qName = new javax.xml.namespace.QName("http://www.virtuemart.net/VM_Users/", "NotifyWaitingListInput");
            cachedSerQNames.add(qName);
            cls = it.netgrid.virtuemart.soap.users.NotifyWaitingListInput.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://www.virtuemart.net/VM_Users/", "SendMailInput");
            cachedSerQNames.add(qName);
            cls = it.netgrid.virtuemart.soap.users.SendMailInput.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://www.virtuemart.net/VM_Users/", "Session");
            cachedSerQNames.add(qName);
            cls = it.netgrid.virtuemart.soap.users.Session.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://www.virtuemart.net/VM_Users/", "Sessions");
            cachedSerQNames.add(qName);
            cls = it.netgrid.virtuemart.soap.users.Session[].class;
            cachedSerClasses.add(cls);
            qName = new javax.xml.namespace.QName("http://www.virtuemart.net/VM_Users/", "Session");
            qName2 = new javax.xml.namespace.QName("", "Session");
            cachedSerFactories.add(new org.apache.axis.encoding.ser.ArraySerializerFactory(qName, qName2));
            cachedDeserFactories.add(new org.apache.axis.encoding.ser.ArrayDeserializerFactory());

            qName = new javax.xml.namespace.QName("http://www.virtuemart.net/VM_Users/", "ShopperGroup");
            cachedSerQNames.add(qName);
            cls = it.netgrid.virtuemart.soap.users.ShopperGroup.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://www.virtuemart.net/VM_Users/", "ShopperGroupArray");
            cachedSerQNames.add(qName);
            cls = it.netgrid.virtuemart.soap.users.ShopperGroup[].class;
            cachedSerClasses.add(cls);
            qName = new javax.xml.namespace.QName("http://www.virtuemart.net/VM_Users/", "ShopperGroup");
            qName2 = new javax.xml.namespace.QName("", "ShopperGroup");
            cachedSerFactories.add(new org.apache.axis.encoding.ser.ArraySerializerFactory(qName, qName2));
            cachedDeserFactories.add(new org.apache.axis.encoding.ser.ArrayDeserializerFactory());

            qName = new javax.xml.namespace.QName("http://www.virtuemart.net/VM_Users/", "ShopperGroups");
            cachedSerQNames.add(qName);
            cls = it.netgrid.virtuemart.soap.users.ShopperGroup[].class;
            cachedSerClasses.add(cls);
            qName = new javax.xml.namespace.QName("http://www.virtuemart.net/VM_Users/", "ShopperGroup");
            qName2 = new javax.xml.namespace.QName("", "shoppergroup");
            cachedSerFactories.add(new org.apache.axis.encoding.ser.ArraySerializerFactory(qName, qName2));
            cachedDeserFactories.add(new org.apache.axis.encoding.ser.ArrayDeserializerFactory());

            qName = new javax.xml.namespace.QName("http://www.virtuemart.net/VM_Users/", "State");
            cachedSerQNames.add(qName);
            cls = it.netgrid.virtuemart.soap.users.State.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://www.virtuemart.net/VM_Users/", "StateArray");
            cachedSerQNames.add(qName);
            cls = it.netgrid.virtuemart.soap.users.State[].class;
            cachedSerClasses.add(cls);
            qName = new javax.xml.namespace.QName("http://www.virtuemart.net/VM_Users/", "State");
            qName2 = new javax.xml.namespace.QName("", "State");
            cachedSerFactories.add(new org.apache.axis.encoding.ser.ArraySerializerFactory(qName, qName2));
            cachedDeserFactories.add(new org.apache.axis.encoding.ser.ArrayDeserializerFactory());

            qName = new javax.xml.namespace.QName("http://www.virtuemart.net/VM_Users/", "States");
            cachedSerQNames.add(qName);
            cls = it.netgrid.virtuemart.soap.users.State[].class;
            cachedSerClasses.add(cls);
            qName = new javax.xml.namespace.QName("http://www.virtuemart.net/VM_Users/", "State");
            qName2 = new javax.xml.namespace.QName("", "State");
            cachedSerFactories.add(new org.apache.axis.encoding.ser.ArraySerializerFactory(qName, qName2));
            cachedDeserFactories.add(new org.apache.axis.encoding.ser.ArrayDeserializerFactory());

            qName = new javax.xml.namespace.QName("http://www.virtuemart.net/VM_Users/", "User");
            cachedSerQNames.add(qName);
            cls = it.netgrid.virtuemart.soap.users.User.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://www.virtuemart.net/VM_Users/", "user_ids");
            cachedSerQNames.add(qName);
            cls = java.lang.String[].class;
            cachedSerClasses.add(cls);
            qName = new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string");
            qName2 = new javax.xml.namespace.QName("", "user_id");
            cachedSerFactories.add(new org.apache.axis.encoding.ser.ArraySerializerFactory(qName, qName2));
            cachedDeserFactories.add(new org.apache.axis.encoding.ser.ArrayDeserializerFactory());

            qName = new javax.xml.namespace.QName("http://www.virtuemart.net/VM_Users/", "Userfield");
            cachedSerQNames.add(qName);
            cls = it.netgrid.virtuemart.soap.users.Userfield.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://www.virtuemart.net/VM_Users/", "Userfields");
            cachedSerQNames.add(qName);
            cls = it.netgrid.virtuemart.soap.users.Userfield[].class;
            cachedSerClasses.add(cls);
            qName = new javax.xml.namespace.QName("http://www.virtuemart.net/VM_Users/", "Userfield");
            qName2 = new javax.xml.namespace.QName("", "Userfield");
            cachedSerFactories.add(new org.apache.axis.encoding.ser.ArraySerializerFactory(qName, qName2));
            cachedDeserFactories.add(new org.apache.axis.encoding.ser.ArrayDeserializerFactory());

            qName = new javax.xml.namespace.QName("http://www.virtuemart.net/VM_Users/", "UsersArray");
            cachedSerQNames.add(qName);
            cls = it.netgrid.virtuemart.soap.users.User[].class;
            cachedSerClasses.add(cls);
            qName = new javax.xml.namespace.QName("http://www.virtuemart.net/VM_Users/", "User");
            qName2 = new javax.xml.namespace.QName("", "User");
            cachedSerFactories.add(new org.apache.axis.encoding.ser.ArraySerializerFactory(qName, qName2));
            cachedDeserFactories.add(new org.apache.axis.encoding.ser.ArrayDeserializerFactory());

            qName = new javax.xml.namespace.QName("http://www.virtuemart.net/VM_Users/", "Vendor");
            cachedSerQNames.add(qName);
            cls = it.netgrid.virtuemart.soap.users.Vendor.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://www.virtuemart.net/VM_Users/", "VendorArray");
            cachedSerQNames.add(qName);
            cls = it.netgrid.virtuemart.soap.users.Vendor[].class;
            cachedSerClasses.add(cls);
            qName = new javax.xml.namespace.QName("http://www.virtuemart.net/VM_Users/", "Vendor");
            qName2 = new javax.xml.namespace.QName("", "Vendor");
            cachedSerFactories.add(new org.apache.axis.encoding.ser.ArraySerializerFactory(qName, qName2));
            cachedDeserFactories.add(new org.apache.axis.encoding.ser.ArrayDeserializerFactory());

            qName = new javax.xml.namespace.QName("http://www.virtuemart.net/VM_Users/", "VendorCategory");
            cachedSerQNames.add(qName);
            cls = it.netgrid.virtuemart.soap.users.VendorCategory.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://www.virtuemart.net/VM_Users/", "VendorCategoryArray");
            cachedSerQNames.add(qName);
            cls = it.netgrid.virtuemart.soap.users.VendorCategory[].class;
            cachedSerClasses.add(cls);
            qName = new javax.xml.namespace.QName("http://www.virtuemart.net/VM_Users/", "VendorCategory");
            qName2 = new javax.xml.namespace.QName("", "VendorCategory");
            cachedSerFactories.add(new org.apache.axis.encoding.ser.ArraySerializerFactory(qName, qName2));
            cachedDeserFactories.add(new org.apache.axis.encoding.ser.ArrayDeserializerFactory());

            qName = new javax.xml.namespace.QName("http://www.virtuemart.net/VM_Users/", "Version");
            cachedSerQNames.add(qName);
            cls = it.netgrid.virtuemart.soap.users.Version.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://www.virtuemart.net/VM_Users/", "WaitingList");
            cachedSerQNames.add(qName);
            cls = it.netgrid.virtuemart.soap.users.WaitingList.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://www.virtuemart.net/VM_Users/", "WaitingLists");
            cachedSerQNames.add(qName);
            cls = it.netgrid.virtuemart.soap.users.WaitingList[].class;
            cachedSerClasses.add(cls);
            qName = new javax.xml.namespace.QName("http://www.virtuemart.net/VM_Users/", "WaitingList");
            qName2 = new javax.xml.namespace.QName("", "WaitingList");
            cachedSerFactories.add(new org.apache.axis.encoding.ser.ArraySerializerFactory(qName, qName2));
            cachedDeserFactories.add(new org.apache.axis.encoding.ser.ArrayDeserializerFactory());

    }

    protected org.apache.axis.client.Call createCall() throws java.rmi.RemoteException {
        try {
            org.apache.axis.client.Call _call = super._createCall();
            if (super.maintainSessionSet) {
                _call.setMaintainSession(super.maintainSession);
            }
            if (super.cachedUsername != null) {
                _call.setUsername(super.cachedUsername);
            }
            if (super.cachedPassword != null) {
                _call.setPassword(super.cachedPassword);
            }
            if (super.cachedEndpoint != null) {
                _call.setTargetEndpointAddress(super.cachedEndpoint);
            }
            if (super.cachedTimeout != null) {
                _call.setTimeout(super.cachedTimeout);
            }
            if (super.cachedPortName != null) {
                _call.setPortName(super.cachedPortName);
            }
            java.util.Enumeration keys = super.cachedProperties.keys();
            while (keys.hasMoreElements()) {
                java.lang.String key = (java.lang.String) keys.nextElement();
                _call.setProperty(key, super.cachedProperties.get(key));
            }
            // All the type mapping information is registered
            // when the first call is made.
            // The type mapping information is actually registered in
            // the TypeMappingRegistry of the service, which
            // is the reason why registration is only needed for the first call.
            synchronized (this) {
                if (firstCall()) {
                    // must set encoding style before registering serializers
                    _call.setEncodingStyle(null);
                    for (int i = 0; i < cachedSerFactories.size(); ++i) {
                        java.lang.Class cls = (java.lang.Class) cachedSerClasses.get(i);
                        javax.xml.namespace.QName qName =
                                (javax.xml.namespace.QName) cachedSerQNames.get(i);
                        java.lang.Object x = cachedSerFactories.get(i);
                        if (x instanceof Class) {
                            java.lang.Class sf = (java.lang.Class)
                                 cachedSerFactories.get(i);
                            java.lang.Class df = (java.lang.Class)
                                 cachedDeserFactories.get(i);
                            _call.registerTypeMapping(cls, qName, sf, df, false);
                        }
                        else if (x instanceof javax.xml.rpc.encoding.SerializerFactory) {
                            org.apache.axis.encoding.SerializerFactory sf = (org.apache.axis.encoding.SerializerFactory)
                                 cachedSerFactories.get(i);
                            org.apache.axis.encoding.DeserializerFactory df = (org.apache.axis.encoding.DeserializerFactory)
                                 cachedDeserFactories.get(i);
                            _call.registerTypeMapping(cls, qName, sf, df, false);
                        }
                    }
                }
            }
            return _call;
        }
        catch (java.lang.Throwable _t) {
            throw new org.apache.axis.AxisFault("Failure trying to get the Call object", _t);
        }
    }

    public it.netgrid.virtuemart.soap.users.User[] getUsers(it.netgrid.virtuemart.soap.users.GetUsersInput parameters) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[0]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("http://www.virtuemart.net/VM_Users/GetUsers");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("", "GetUsers"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {parameters});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (it.netgrid.virtuemart.soap.users.User[]) _resp;
            } catch (java.lang.Exception _exception) {
                return (it.netgrid.virtuemart.soap.users.User[]) org.apache.axis.utils.JavaUtils.convert(_resp, it.netgrid.virtuemart.soap.users.User[].class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public it.netgrid.virtuemart.soap.users.AuthOutput authentification(it.netgrid.virtuemart.soap.users.LoginInfo parameters) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[1]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("http://www.virtuemart.net/VM_Users/Authentification");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("", "Authentification"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {parameters});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (it.netgrid.virtuemart.soap.users.AuthOutput) _resp;
            } catch (java.lang.Exception _exception) {
                return (it.netgrid.virtuemart.soap.users.AuthOutput) org.apache.axis.utils.JavaUtils.convert(_resp, it.netgrid.virtuemart.soap.users.AuthOutput.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public it.netgrid.virtuemart.soap.users.CommonReturn addUser(it.netgrid.virtuemart.soap.users.AddUserInput parameters) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[2]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("http://www.virtuemart.net/VM_Users/AddUser");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("", "AddUser"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {parameters});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (it.netgrid.virtuemart.soap.users.CommonReturn) _resp;
            } catch (java.lang.Exception _exception) {
                return (it.netgrid.virtuemart.soap.users.CommonReturn) org.apache.axis.utils.JavaUtils.convert(_resp, it.netgrid.virtuemart.soap.users.CommonReturn.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public it.netgrid.virtuemart.soap.users.CommonReturn deleteUser(it.netgrid.virtuemart.soap.users.DeleteUserInput parameters) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[3]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("http://www.virtuemart.net/VM_Users/DeleteUser");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("", "DeleteUser"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {parameters});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (it.netgrid.virtuemart.soap.users.CommonReturn) _resp;
            } catch (java.lang.Exception _exception) {
                return (it.netgrid.virtuemart.soap.users.CommonReturn) org.apache.axis.utils.JavaUtils.convert(_resp, it.netgrid.virtuemart.soap.users.CommonReturn.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public it.netgrid.virtuemart.soap.users.CommonReturn sendMail(it.netgrid.virtuemart.soap.users.SendMailInput parameters) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[4]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("http://www.virtuemart.net/VM_Users/SendMail");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("", "SendMail"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {parameters});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (it.netgrid.virtuemart.soap.users.CommonReturn) _resp;
            } catch (java.lang.Exception _exception) {
                return (it.netgrid.virtuemart.soap.users.CommonReturn) org.apache.axis.utils.JavaUtils.convert(_resp, it.netgrid.virtuemart.soap.users.CommonReturn.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public it.netgrid.virtuemart.soap.users.User[] getUserFromEmailOrUsername(it.netgrid.virtuemart.soap.users.GetUserFromEmailOrUsernameInput parameters) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[5]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("http://www.virtuemart.net/VM_Users/GetUserFromEmailOrUsername");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("", "GetUserFromEmailOrUsername"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {parameters});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (it.netgrid.virtuemart.soap.users.User[]) _resp;
            } catch (java.lang.Exception _exception) {
                return (it.netgrid.virtuemart.soap.users.User[]) org.apache.axis.utils.JavaUtils.convert(_resp, it.netgrid.virtuemart.soap.users.User[].class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public it.netgrid.virtuemart.soap.users.Country[] getAllCountryCode(it.netgrid.virtuemart.soap.users.LoginInfo parameters) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[6]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("http://www.virtuemart.net/VM_Users/GetAllCountryCode");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("", "GetAllCountryCode"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {parameters});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (it.netgrid.virtuemart.soap.users.Country[]) _resp;
            } catch (java.lang.Exception _exception) {
                return (it.netgrid.virtuemart.soap.users.Country[]) org.apache.axis.utils.JavaUtils.convert(_resp, it.netgrid.virtuemart.soap.users.Country[].class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public it.netgrid.virtuemart.soap.users.AuthGroup[] getAuthGroup(it.netgrid.virtuemart.soap.users.LoginInfo parameters) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[7]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("http://www.virtuemart.net/VM_Users/GetAuthGroup");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("", "GetAuthGroup"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {parameters});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (it.netgrid.virtuemart.soap.users.AuthGroup[]) _resp;
            } catch (java.lang.Exception _exception) {
                return (it.netgrid.virtuemart.soap.users.AuthGroup[]) org.apache.axis.utils.JavaUtils.convert(_resp, it.netgrid.virtuemart.soap.users.AuthGroup[].class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public it.netgrid.virtuemart.soap.users.CommonReturn addAuthGroup(it.netgrid.virtuemart.soap.users.AddAuthGroupInput parameters) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[8]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("http://www.virtuemart.net/VM_Users/AddAuthGroup");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("", "AddAuthGroup"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {parameters});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (it.netgrid.virtuemart.soap.users.CommonReturn) _resp;
            } catch (java.lang.Exception _exception) {
                return (it.netgrid.virtuemart.soap.users.CommonReturn) org.apache.axis.utils.JavaUtils.convert(_resp, it.netgrid.virtuemart.soap.users.CommonReturn.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public it.netgrid.virtuemart.soap.users.CommonReturn deleteAuthGroup(it.netgrid.virtuemart.soap.users.DelAuthGroupInput parameters) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[9]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("http://www.virtuemart.net/VM_Users/DeleteAuthGroup");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("", "DeleteAuthGroup"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {parameters});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (it.netgrid.virtuemart.soap.users.CommonReturn) _resp;
            } catch (java.lang.Exception _exception) {
                return (it.netgrid.virtuemart.soap.users.CommonReturn) org.apache.axis.utils.JavaUtils.convert(_resp, it.netgrid.virtuemart.soap.users.CommonReturn.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public it.netgrid.virtuemart.soap.users.State[] getAllStates(it.netgrid.virtuemart.soap.users.GetStatesInput parameters) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[10]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("http://www.virtuemart.net/VM_Users/GetAllStates");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("", "GetAllStates"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {parameters});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (it.netgrid.virtuemart.soap.users.State[]) _resp;
            } catch (java.lang.Exception _exception) {
                return (it.netgrid.virtuemart.soap.users.State[]) org.apache.axis.utils.JavaUtils.convert(_resp, it.netgrid.virtuemart.soap.users.State[].class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public it.netgrid.virtuemart.soap.users.CommonReturn addStates(it.netgrid.virtuemart.soap.users.AddStatesInput parameters) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[11]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("http://www.virtuemart.net/VM_Users/AddStates");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("", "AddStates"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {parameters});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (it.netgrid.virtuemart.soap.users.CommonReturn) _resp;
            } catch (java.lang.Exception _exception) {
                return (it.netgrid.virtuemart.soap.users.CommonReturn) org.apache.axis.utils.JavaUtils.convert(_resp, it.netgrid.virtuemart.soap.users.CommonReturn.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public it.netgrid.virtuemart.soap.users.CommonReturn deleteStates(it.netgrid.virtuemart.soap.users.DelInput parameters) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[12]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("http://www.virtuemart.net/VM_Users/DeleteStates");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("", "DeleteStates"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {parameters});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (it.netgrid.virtuemart.soap.users.CommonReturn) _resp;
            } catch (java.lang.Exception _exception) {
                return (it.netgrid.virtuemart.soap.users.CommonReturn) org.apache.axis.utils.JavaUtils.convert(_resp, it.netgrid.virtuemart.soap.users.CommonReturn.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public it.netgrid.virtuemart.soap.users.ShopperGroup[] getShopperGroup(it.netgrid.virtuemart.soap.users.LoginInfo parameters) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[13]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("http://www.virtuemart.net/VM_Users/GetShopperGroup");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("", "GetShopperGroup"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {parameters});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (it.netgrid.virtuemart.soap.users.ShopperGroup[]) _resp;
            } catch (java.lang.Exception _exception) {
                return (it.netgrid.virtuemart.soap.users.ShopperGroup[]) org.apache.axis.utils.JavaUtils.convert(_resp, it.netgrid.virtuemart.soap.users.ShopperGroup[].class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public it.netgrid.virtuemart.soap.users.CommonReturn addShopperGroup(it.netgrid.virtuemart.soap.users.AddShopperGroupsInput parameters) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[14]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("http://www.virtuemart.net/VM_Users/AddShopperGroup");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("", "AddShopperGroup"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {parameters});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (it.netgrid.virtuemart.soap.users.CommonReturn) _resp;
            } catch (java.lang.Exception _exception) {
                return (it.netgrid.virtuemart.soap.users.CommonReturn) org.apache.axis.utils.JavaUtils.convert(_resp, it.netgrid.virtuemart.soap.users.CommonReturn.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public it.netgrid.virtuemart.soap.users.CommonReturn updateShopperGroup(it.netgrid.virtuemart.soap.users.AddShopperGroupsInput parameters) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[15]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("http://www.virtuemart.net/VM_Users/UpdateShopperGroup");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("", "UpdateShopperGroup"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {parameters});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (it.netgrid.virtuemart.soap.users.CommonReturn) _resp;
            } catch (java.lang.Exception _exception) {
                return (it.netgrid.virtuemart.soap.users.CommonReturn) org.apache.axis.utils.JavaUtils.convert(_resp, it.netgrid.virtuemart.soap.users.CommonReturn.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public it.netgrid.virtuemart.soap.users.CommonReturn deleteShopperGroup(it.netgrid.virtuemart.soap.users.DelInput parameters) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[16]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("http://www.virtuemart.net/VM_Users/DeleteShopperGroup");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("", "DeleteShopperGroup"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {parameters});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (it.netgrid.virtuemart.soap.users.CommonReturn) _resp;
            } catch (java.lang.Exception _exception) {
                return (it.netgrid.virtuemart.soap.users.CommonReturn) org.apache.axis.utils.JavaUtils.convert(_resp, it.netgrid.virtuemart.soap.users.CommonReturn.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public it.netgrid.virtuemart.soap.users.CommonReturn updateUser(it.netgrid.virtuemart.soap.users.AddUserInput parameters) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[17]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("http://www.virtuemart.net/VM_Users/UpdateUser");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("", "UpdateUser"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {parameters});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (it.netgrid.virtuemart.soap.users.CommonReturn) _resp;
            } catch (java.lang.Exception _exception) {
                return (it.netgrid.virtuemart.soap.users.CommonReturn) org.apache.axis.utils.JavaUtils.convert(_resp, it.netgrid.virtuemart.soap.users.CommonReturn.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public it.netgrid.virtuemart.soap.users.Vendor[] getAllVendor(it.netgrid.virtuemart.soap.users.GetVendorsInput parameters) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[18]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("http://www.virtuemart.net/VM_Users/GetAllVendor");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("", "GetAllVendor"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {parameters});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (it.netgrid.virtuemart.soap.users.Vendor[]) _resp;
            } catch (java.lang.Exception _exception) {
                return (it.netgrid.virtuemart.soap.users.Vendor[]) org.apache.axis.utils.JavaUtils.convert(_resp, it.netgrid.virtuemart.soap.users.Vendor[].class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public it.netgrid.virtuemart.soap.users.CommonReturn addVendor(it.netgrid.virtuemart.soap.users.AddVendorInput parameters) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[19]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("http://www.virtuemart.net/VM_Users/AddVendor");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("", "AddVendor"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {parameters});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (it.netgrid.virtuemart.soap.users.CommonReturn) _resp;
            } catch (java.lang.Exception _exception) {
                return (it.netgrid.virtuemart.soap.users.CommonReturn) org.apache.axis.utils.JavaUtils.convert(_resp, it.netgrid.virtuemart.soap.users.CommonReturn.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public it.netgrid.virtuemart.soap.users.CommonReturn updateVendor(it.netgrid.virtuemart.soap.users.AddVendorInput parameters) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[20]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("http://www.virtuemart.net/VM_Users/UpdateVendor");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("", "UpdateVendor"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {parameters});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (it.netgrid.virtuemart.soap.users.CommonReturn) _resp;
            } catch (java.lang.Exception _exception) {
                return (it.netgrid.virtuemart.soap.users.CommonReturn) org.apache.axis.utils.JavaUtils.convert(_resp, it.netgrid.virtuemart.soap.users.CommonReturn.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public it.netgrid.virtuemart.soap.users.CommonReturn deleteVendor(it.netgrid.virtuemart.soap.users.DelInput parameters) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[21]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("http://www.virtuemart.net/VM_Users/DeleteVendor");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("", "DeleteVendor"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {parameters});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (it.netgrid.virtuemart.soap.users.CommonReturn) _resp;
            } catch (java.lang.Exception _exception) {
                return (it.netgrid.virtuemart.soap.users.CommonReturn) org.apache.axis.utils.JavaUtils.convert(_resp, it.netgrid.virtuemart.soap.users.CommonReturn.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public it.netgrid.virtuemart.soap.users.VendorCategory[] getAllVendorCategory(it.netgrid.virtuemart.soap.users.LoginInfo parameters) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[22]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("http://www.virtuemart.net/VM_Users/GetAllVendorCategory");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("", "GetAllVendorCategory"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {parameters});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (it.netgrid.virtuemart.soap.users.VendorCategory[]) _resp;
            } catch (java.lang.Exception _exception) {
                return (it.netgrid.virtuemart.soap.users.VendorCategory[]) org.apache.axis.utils.JavaUtils.convert(_resp, it.netgrid.virtuemart.soap.users.VendorCategory[].class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public it.netgrid.virtuemart.soap.users.CommonReturn addVendorCategory(it.netgrid.virtuemart.soap.users.AddVendorCatInput parameters) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[23]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("http://www.virtuemart.net/VM_Users/AddVendorCategory");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("", "AddVendorCategory"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {parameters});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (it.netgrid.virtuemart.soap.users.CommonReturn) _resp;
            } catch (java.lang.Exception _exception) {
                return (it.netgrid.virtuemart.soap.users.CommonReturn) org.apache.axis.utils.JavaUtils.convert(_resp, it.netgrid.virtuemart.soap.users.CommonReturn.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public it.netgrid.virtuemart.soap.users.CommonReturn updateVendorCategory(it.netgrid.virtuemart.soap.users.AddVendorCatInput parameters) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[24]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("http://www.virtuemart.net/VM_Users/UpdateVendorCategory");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("", "UpdateVendorCategory"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {parameters});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (it.netgrid.virtuemart.soap.users.CommonReturn) _resp;
            } catch (java.lang.Exception _exception) {
                return (it.netgrid.virtuemart.soap.users.CommonReturn) org.apache.axis.utils.JavaUtils.convert(_resp, it.netgrid.virtuemart.soap.users.CommonReturn.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public it.netgrid.virtuemart.soap.users.CommonReturn deleteVendorCategory(it.netgrid.virtuemart.soap.users.DelInput parameters) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[25]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("http://www.virtuemart.net/VM_Users/DeleteVendorCategory");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("", "DeleteVendorCategory"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {parameters});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (it.netgrid.virtuemart.soap.users.CommonReturn) _resp;
            } catch (java.lang.Exception _exception) {
                return (it.netgrid.virtuemart.soap.users.CommonReturn) org.apache.axis.utils.JavaUtils.convert(_resp, it.netgrid.virtuemart.soap.users.CommonReturn.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public it.netgrid.virtuemart.soap.users.Manufacturer[] getAllManufacturer(it.netgrid.virtuemart.soap.users.LoginInfo parameters) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[26]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("http://www.virtuemart.net/VM_Users/GetAllManufacturer");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("", "GetAllManufacturer"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {parameters});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (it.netgrid.virtuemart.soap.users.Manufacturer[]) _resp;
            } catch (java.lang.Exception _exception) {
                return (it.netgrid.virtuemart.soap.users.Manufacturer[]) org.apache.axis.utils.JavaUtils.convert(_resp, it.netgrid.virtuemart.soap.users.Manufacturer[].class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public it.netgrid.virtuemart.soap.users.CommonReturn addManufacturer(it.netgrid.virtuemart.soap.users.AddManufacturerInput parameters) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[27]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("http://www.virtuemart.net/VM_Users/AddManufacturer");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("", "AddManufacturer"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {parameters});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (it.netgrid.virtuemart.soap.users.CommonReturn) _resp;
            } catch (java.lang.Exception _exception) {
                return (it.netgrid.virtuemart.soap.users.CommonReturn) org.apache.axis.utils.JavaUtils.convert(_resp, it.netgrid.virtuemart.soap.users.CommonReturn.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public it.netgrid.virtuemart.soap.users.CommonReturn updateManufacturer(it.netgrid.virtuemart.soap.users.AddManufacturerInput parameters) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[28]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("http://www.virtuemart.net/VM_Users/UpdateManufacturer");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("", "UpdateManufacturer"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {parameters});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (it.netgrid.virtuemart.soap.users.CommonReturn) _resp;
            } catch (java.lang.Exception _exception) {
                return (it.netgrid.virtuemart.soap.users.CommonReturn) org.apache.axis.utils.JavaUtils.convert(_resp, it.netgrid.virtuemart.soap.users.CommonReturn.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public it.netgrid.virtuemart.soap.users.CommonReturn deleteManufacturer(it.netgrid.virtuemart.soap.users.DelInput parameters) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[29]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("http://www.virtuemart.net/VM_Users/DeleteManufacturer");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("", "DeleteManufacturer"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {parameters});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (it.netgrid.virtuemart.soap.users.CommonReturn) _resp;
            } catch (java.lang.Exception _exception) {
                return (it.netgrid.virtuemart.soap.users.CommonReturn) org.apache.axis.utils.JavaUtils.convert(_resp, it.netgrid.virtuemart.soap.users.CommonReturn.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public it.netgrid.virtuemart.soap.users.ManufacturerCat[] getAllManufacturerCat(it.netgrid.virtuemart.soap.users.LoginInfo parameters) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[30]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("http://www.virtuemart.net/VM_Users/GetAllManufacturerCat");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("", "GetAllManufacturerCat"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {parameters});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (it.netgrid.virtuemart.soap.users.ManufacturerCat[]) _resp;
            } catch (java.lang.Exception _exception) {
                return (it.netgrid.virtuemart.soap.users.ManufacturerCat[]) org.apache.axis.utils.JavaUtils.convert(_resp, it.netgrid.virtuemart.soap.users.ManufacturerCat[].class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public it.netgrid.virtuemart.soap.users.CommonReturn addManufacturerCat(it.netgrid.virtuemart.soap.users.AddManufacturerCatInput parameters) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[31]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("http://www.virtuemart.net/VM_Users/AddManufacturerCat");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("", "AddManufacturerCat"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {parameters});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (it.netgrid.virtuemart.soap.users.CommonReturn) _resp;
            } catch (java.lang.Exception _exception) {
                return (it.netgrid.virtuemart.soap.users.CommonReturn) org.apache.axis.utils.JavaUtils.convert(_resp, it.netgrid.virtuemart.soap.users.CommonReturn.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public it.netgrid.virtuemart.soap.users.CommonReturn updateManufacturerCat(it.netgrid.virtuemart.soap.users.AddManufacturerCatInput parameters) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[32]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("http://www.virtuemart.net/VM_Users/UpdateManufacturerCat");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("", "UpdateManufacturerCat"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {parameters});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (it.netgrid.virtuemart.soap.users.CommonReturn) _resp;
            } catch (java.lang.Exception _exception) {
                return (it.netgrid.virtuemart.soap.users.CommonReturn) org.apache.axis.utils.JavaUtils.convert(_resp, it.netgrid.virtuemart.soap.users.CommonReturn.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public it.netgrid.virtuemart.soap.users.CommonReturn deleteManufacturerCat(it.netgrid.virtuemart.soap.users.DelInput parameters) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[33]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("http://www.virtuemart.net/VM_Users/DeleteManufacturerCat");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("", "DeleteManufacturerCat"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {parameters});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (it.netgrid.virtuemart.soap.users.CommonReturn) _resp;
            } catch (java.lang.Exception _exception) {
                return (it.netgrid.virtuemart.soap.users.CommonReturn) org.apache.axis.utils.JavaUtils.convert(_resp, it.netgrid.virtuemart.soap.users.CommonReturn.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public it.netgrid.virtuemart.soap.users.AvalaibleImage[] getAvailableVendorImages(it.netgrid.virtuemart.soap.users.GetAvalaibleImageInput parameters) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[34]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("http://www.virtuemart.net/VM_Users/GetAvailableVendorImages");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("", "GetAvailableVendorImages"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {parameters});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (it.netgrid.virtuemart.soap.users.AvalaibleImage[]) _resp;
            } catch (java.lang.Exception _exception) {
                return (it.netgrid.virtuemart.soap.users.AvalaibleImage[]) org.apache.axis.utils.JavaUtils.convert(_resp, it.netgrid.virtuemart.soap.users.AvalaibleImage[].class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public it.netgrid.virtuemart.soap.users.Version getVersions(it.netgrid.virtuemart.soap.users.LoginInfo parameters) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[35]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("http://www.virtuemart.net/VM_Users/GetVersions");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("", "GetVersions"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {parameters});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (it.netgrid.virtuemart.soap.users.Version) _resp;
            } catch (java.lang.Exception _exception) {
                return (it.netgrid.virtuemart.soap.users.Version) org.apache.axis.utils.JavaUtils.convert(_resp, it.netgrid.virtuemart.soap.users.Version.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public it.netgrid.virtuemart.soap.users.User[] getAdditionalUserInfo(it.netgrid.virtuemart.soap.users.GetAdditionalUserInfoInput parameters) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[36]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("http://www.virtuemart.net/VM_Users/GetAdditionalUserInfo");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("", "GetAdditionalUserInfo"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {parameters});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (it.netgrid.virtuemart.soap.users.User[]) _resp;
            } catch (java.lang.Exception _exception) {
                return (it.netgrid.virtuemart.soap.users.User[]) org.apache.axis.utils.JavaUtils.convert(_resp, it.netgrid.virtuemart.soap.users.User[].class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public it.netgrid.virtuemart.soap.users.Session[] getSessions(it.netgrid.virtuemart.soap.users.GetSessionsInput parameters) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[37]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("http://www.virtuemart.net/VM_Users/GetSessions");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("", "GetSessions"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {parameters});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (it.netgrid.virtuemart.soap.users.Session[]) _resp;
            } catch (java.lang.Exception _exception) {
                return (it.netgrid.virtuemart.soap.users.Session[]) org.apache.axis.utils.JavaUtils.convert(_resp, it.netgrid.virtuemart.soap.users.Session[].class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public it.netgrid.virtuemart.soap.users.WaitingList[] getWaitingList(it.netgrid.virtuemart.soap.users.GetWaitingListInput parameters) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[38]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("http://www.virtuemart.net/VM_Users/GetWaitingList");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("", "GetWaitingList"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {parameters});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (it.netgrid.virtuemart.soap.users.WaitingList[]) _resp;
            } catch (java.lang.Exception _exception) {
                return (it.netgrid.virtuemart.soap.users.WaitingList[]) org.apache.axis.utils.JavaUtils.convert(_resp, it.netgrid.virtuemart.soap.users.WaitingList[].class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public it.netgrid.virtuemart.soap.users.CommonReturn notifyWaitingList(it.netgrid.virtuemart.soap.users.NotifyWaitingListInput parameters) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[39]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("http://www.virtuemart.net/VM_Users/NotifyWaitingList");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("", "NotifyWaitingList"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {parameters});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (it.netgrid.virtuemart.soap.users.CommonReturn) _resp;
            } catch (java.lang.Exception _exception) {
                return (it.netgrid.virtuemart.soap.users.CommonReturn) org.apache.axis.utils.JavaUtils.convert(_resp, it.netgrid.virtuemart.soap.users.CommonReturn.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public it.netgrid.virtuemart.soap.users.User[] getUserInfoFromOrderID(it.netgrid.virtuemart.soap.users.GetUserInfoFromOrderIDInput parameters) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[40]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("http://www.virtuemart.net/VM_Users/GetUserInfoFromOrderID");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("", "GetUserInfoFromOrderID"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {parameters});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (it.netgrid.virtuemart.soap.users.User[]) _resp;
            } catch (java.lang.Exception _exception) {
                return (it.netgrid.virtuemart.soap.users.User[]) org.apache.axis.utils.JavaUtils.convert(_resp, it.netgrid.virtuemart.soap.users.User[].class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public it.netgrid.virtuemart.soap.users.Userfield[] getUserfields(it.netgrid.virtuemart.soap.users.GenericGetInput parameters) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[41]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("http://www.virtuemart.net/VM_Users/GetUserfields");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("", "GetUserfields"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {parameters});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (it.netgrid.virtuemart.soap.users.Userfield[]) _resp;
            } catch (java.lang.Exception _exception) {
                return (it.netgrid.virtuemart.soap.users.Userfield[]) org.apache.axis.utils.JavaUtils.convert(_resp, it.netgrid.virtuemart.soap.users.Userfield[].class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public it.netgrid.virtuemart.soap.users.Media[] getMediaVendor(it.netgrid.virtuemart.soap.users.GetMediaVendorInput parameters) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[42]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("http://www.virtuemart.net/VM_Users/GetMediaVendor");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("", "GetMediaVendor"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {parameters});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (it.netgrid.virtuemart.soap.users.Media[]) _resp;
            } catch (java.lang.Exception _exception) {
                return (it.netgrid.virtuemart.soap.users.Media[]) org.apache.axis.utils.JavaUtils.convert(_resp, it.netgrid.virtuemart.soap.users.Media[].class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public it.netgrid.virtuemart.soap.users.Media[] getMediaManufacturer(it.netgrid.virtuemart.soap.users.GetMediaManufacturerInput parameters) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[43]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("http://www.virtuemart.net/VM_Users/GetMediaManufacturer");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("", "GetMediaManufacturer"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {parameters});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (it.netgrid.virtuemart.soap.users.Media[]) _resp;
            } catch (java.lang.Exception _exception) {
                return (it.netgrid.virtuemart.soap.users.Media[]) org.apache.axis.utils.JavaUtils.convert(_resp, it.netgrid.virtuemart.soap.users.Media[].class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public it.netgrid.virtuemart.soap.users.CommonReturn addMediaVendor(it.netgrid.virtuemart.soap.users.AddMediaVendorInput parameters) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[44]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("http://www.virtuemart.net/VM_Users/AddMediaVendor");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("", "AddMediaVendor"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {parameters});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (it.netgrid.virtuemart.soap.users.CommonReturn) _resp;
            } catch (java.lang.Exception _exception) {
                return (it.netgrid.virtuemart.soap.users.CommonReturn) org.apache.axis.utils.JavaUtils.convert(_resp, it.netgrid.virtuemart.soap.users.CommonReturn.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public it.netgrid.virtuemart.soap.users.CommonReturn addMediaManufacturer(it.netgrid.virtuemart.soap.users.AddMediaManufacturerInput parameters) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[45]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("http://www.virtuemart.net/VM_Users/AddMediaManufacturer");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("", "AddMediaManufacturer"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {parameters});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (it.netgrid.virtuemart.soap.users.CommonReturn) _resp;
            } catch (java.lang.Exception _exception) {
                return (it.netgrid.virtuemart.soap.users.CommonReturn) org.apache.axis.utils.JavaUtils.convert(_resp, it.netgrid.virtuemart.soap.users.CommonReturn.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public it.netgrid.virtuemart.soap.users.CommonReturn deleteMediaVendor(it.netgrid.virtuemart.soap.users.DelMediaInput parameters) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[46]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("http://www.virtuemart.net/VM_Users/DeleteMediaVendor");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("", "DeleteMediaVendor"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {parameters});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (it.netgrid.virtuemart.soap.users.CommonReturn) _resp;
            } catch (java.lang.Exception _exception) {
                return (it.netgrid.virtuemart.soap.users.CommonReturn) org.apache.axis.utils.JavaUtils.convert(_resp, it.netgrid.virtuemart.soap.users.CommonReturn.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public it.netgrid.virtuemart.soap.users.CommonReturn deleteMediaManufacturer(it.netgrid.virtuemart.soap.users.DelMediaInput parameters) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[47]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("http://www.virtuemart.net/VM_Users/DeleteMediaManufacturer");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("", "DeleteMediaManufacturer"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {parameters});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (it.netgrid.virtuemart.soap.users.CommonReturn) _resp;
            } catch (java.lang.Exception _exception) {
                return (it.netgrid.virtuemart.soap.users.CommonReturn) org.apache.axis.utils.JavaUtils.convert(_resp, it.netgrid.virtuemart.soap.users.CommonReturn.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public it.netgrid.virtuemart.soap.users.CommonReturn addShippingAdress(it.netgrid.virtuemart.soap.users.AddShippingAdressInput parameters) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[48]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("http://www.virtuemart.net/VM_Users/AddShippingAdress");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("", "AddShippingAdress"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {parameters});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (it.netgrid.virtuemart.soap.users.CommonReturn) _resp;
            } catch (java.lang.Exception _exception) {
                return (it.netgrid.virtuemart.soap.users.CommonReturn) org.apache.axis.utils.JavaUtils.convert(_resp, it.netgrid.virtuemart.soap.users.CommonReturn.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public it.netgrid.virtuemart.soap.users.CommonReturn updateShippingAdress(it.netgrid.virtuemart.soap.users.AddShippingAdressInput parameters) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[49]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("http://www.virtuemart.net/VM_Users/UpdateShippingAdress");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("", "UpdateShippingAdress"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {parameters});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (it.netgrid.virtuemart.soap.users.CommonReturn) _resp;
            } catch (java.lang.Exception _exception) {
                return (it.netgrid.virtuemart.soap.users.CommonReturn) org.apache.axis.utils.JavaUtils.convert(_resp, it.netgrid.virtuemart.soap.users.CommonReturn.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public it.netgrid.virtuemart.soap.users.CommonReturn deleteShippingAdress(it.netgrid.virtuemart.soap.users.DelShippingAdressInput parameters) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[50]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("http://www.virtuemart.net/VM_Users/DeleteShippingAdress");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("", "DeleteShippingAdress"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {parameters});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (it.netgrid.virtuemart.soap.users.CommonReturn) _resp;
            } catch (java.lang.Exception _exception) {
                return (it.netgrid.virtuemart.soap.users.CommonReturn) org.apache.axis.utils.JavaUtils.convert(_resp, it.netgrid.virtuemart.soap.users.CommonReturn.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

}
