/**
 * EmailParams.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package it.netgrid.virtuemart.soap.users;

public class EmailParams  implements java.io.Serializable {
    private java.lang.String from_mail;

    private java.lang.String from_name;

    private java.lang.String recipient;

    private java.lang.String subject;

    private java.lang.String body;

    private java.lang.String altbody;

    private java.lang.String mode;

    private java.lang.String cc;

    private java.lang.String bcc;

    private it.netgrid.virtuemart.soap.users.Image[] images;

    private java.lang.String attachment;

    private java.lang.String replyto;

    private java.lang.String removeAttachmentOnFinish;

    private byte[] attachValue;

    public EmailParams() {
    }

    public EmailParams(
           java.lang.String from_mail,
           java.lang.String from_name,
           java.lang.String recipient,
           java.lang.String subject,
           java.lang.String body,
           java.lang.String altbody,
           java.lang.String mode,
           java.lang.String cc,
           java.lang.String bcc,
           it.netgrid.virtuemart.soap.users.Image[] images,
           java.lang.String attachment,
           java.lang.String replyto,
           java.lang.String removeAttachmentOnFinish,
           byte[] attachValue) {
           this.from_mail = from_mail;
           this.from_name = from_name;
           this.recipient = recipient;
           this.subject = subject;
           this.body = body;
           this.altbody = altbody;
           this.mode = mode;
           this.cc = cc;
           this.bcc = bcc;
           this.images = images;
           this.attachment = attachment;
           this.replyto = replyto;
           this.removeAttachmentOnFinish = removeAttachmentOnFinish;
           this.attachValue = attachValue;
    }


    /**
     * Gets the from_mail value for this EmailParams.
     * 
     * @return from_mail
     */
    public java.lang.String getFrom_mail() {
        return from_mail;
    }


    /**
     * Sets the from_mail value for this EmailParams.
     * 
     * @param from_mail
     */
    public void setFrom_mail(java.lang.String from_mail) {
        this.from_mail = from_mail;
    }


    /**
     * Gets the from_name value for this EmailParams.
     * 
     * @return from_name
     */
    public java.lang.String getFrom_name() {
        return from_name;
    }


    /**
     * Sets the from_name value for this EmailParams.
     * 
     * @param from_name
     */
    public void setFrom_name(java.lang.String from_name) {
        this.from_name = from_name;
    }


    /**
     * Gets the recipient value for this EmailParams.
     * 
     * @return recipient
     */
    public java.lang.String getRecipient() {
        return recipient;
    }


    /**
     * Sets the recipient value for this EmailParams.
     * 
     * @param recipient
     */
    public void setRecipient(java.lang.String recipient) {
        this.recipient = recipient;
    }


    /**
     * Gets the subject value for this EmailParams.
     * 
     * @return subject
     */
    public java.lang.String getSubject() {
        return subject;
    }


    /**
     * Sets the subject value for this EmailParams.
     * 
     * @param subject
     */
    public void setSubject(java.lang.String subject) {
        this.subject = subject;
    }


    /**
     * Gets the body value for this EmailParams.
     * 
     * @return body
     */
    public java.lang.String getBody() {
        return body;
    }


    /**
     * Sets the body value for this EmailParams.
     * 
     * @param body
     */
    public void setBody(java.lang.String body) {
        this.body = body;
    }


    /**
     * Gets the altbody value for this EmailParams.
     * 
     * @return altbody
     */
    public java.lang.String getAltbody() {
        return altbody;
    }


    /**
     * Sets the altbody value for this EmailParams.
     * 
     * @param altbody
     */
    public void setAltbody(java.lang.String altbody) {
        this.altbody = altbody;
    }


    /**
     * Gets the mode value for this EmailParams.
     * 
     * @return mode
     */
    public java.lang.String getMode() {
        return mode;
    }


    /**
     * Sets the mode value for this EmailParams.
     * 
     * @param mode
     */
    public void setMode(java.lang.String mode) {
        this.mode = mode;
    }


    /**
     * Gets the cc value for this EmailParams.
     * 
     * @return cc
     */
    public java.lang.String getCc() {
        return cc;
    }


    /**
     * Sets the cc value for this EmailParams.
     * 
     * @param cc
     */
    public void setCc(java.lang.String cc) {
        this.cc = cc;
    }


    /**
     * Gets the bcc value for this EmailParams.
     * 
     * @return bcc
     */
    public java.lang.String getBcc() {
        return bcc;
    }


    /**
     * Sets the bcc value for this EmailParams.
     * 
     * @param bcc
     */
    public void setBcc(java.lang.String bcc) {
        this.bcc = bcc;
    }


    /**
     * Gets the images value for this EmailParams.
     * 
     * @return images
     */
    public it.netgrid.virtuemart.soap.users.Image[] getImages() {
        return images;
    }


    /**
     * Sets the images value for this EmailParams.
     * 
     * @param images
     */
    public void setImages(it.netgrid.virtuemart.soap.users.Image[] images) {
        this.images = images;
    }


    /**
     * Gets the attachment value for this EmailParams.
     * 
     * @return attachment
     */
    public java.lang.String getAttachment() {
        return attachment;
    }


    /**
     * Sets the attachment value for this EmailParams.
     * 
     * @param attachment
     */
    public void setAttachment(java.lang.String attachment) {
        this.attachment = attachment;
    }


    /**
     * Gets the replyto value for this EmailParams.
     * 
     * @return replyto
     */
    public java.lang.String getReplyto() {
        return replyto;
    }


    /**
     * Sets the replyto value for this EmailParams.
     * 
     * @param replyto
     */
    public void setReplyto(java.lang.String replyto) {
        this.replyto = replyto;
    }


    /**
     * Gets the removeAttachmentOnFinish value for this EmailParams.
     * 
     * @return removeAttachmentOnFinish
     */
    public java.lang.String getRemoveAttachmentOnFinish() {
        return removeAttachmentOnFinish;
    }


    /**
     * Sets the removeAttachmentOnFinish value for this EmailParams.
     * 
     * @param removeAttachmentOnFinish
     */
    public void setRemoveAttachmentOnFinish(java.lang.String removeAttachmentOnFinish) {
        this.removeAttachmentOnFinish = removeAttachmentOnFinish;
    }


    /**
     * Gets the attachValue value for this EmailParams.
     * 
     * @return attachValue
     */
    public byte[] getAttachValue() {
        return attachValue;
    }


    /**
     * Sets the attachValue value for this EmailParams.
     * 
     * @param attachValue
     */
    public void setAttachValue(byte[] attachValue) {
        this.attachValue = attachValue;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof EmailParams)) return false;
        EmailParams other = (EmailParams) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.from_mail==null && other.getFrom_mail()==null) || 
             (this.from_mail!=null &&
              this.from_mail.equals(other.getFrom_mail()))) &&
            ((this.from_name==null && other.getFrom_name()==null) || 
             (this.from_name!=null &&
              this.from_name.equals(other.getFrom_name()))) &&
            ((this.recipient==null && other.getRecipient()==null) || 
             (this.recipient!=null &&
              this.recipient.equals(other.getRecipient()))) &&
            ((this.subject==null && other.getSubject()==null) || 
             (this.subject!=null &&
              this.subject.equals(other.getSubject()))) &&
            ((this.body==null && other.getBody()==null) || 
             (this.body!=null &&
              this.body.equals(other.getBody()))) &&
            ((this.altbody==null && other.getAltbody()==null) || 
             (this.altbody!=null &&
              this.altbody.equals(other.getAltbody()))) &&
            ((this.mode==null && other.getMode()==null) || 
             (this.mode!=null &&
              this.mode.equals(other.getMode()))) &&
            ((this.cc==null && other.getCc()==null) || 
             (this.cc!=null &&
              this.cc.equals(other.getCc()))) &&
            ((this.bcc==null && other.getBcc()==null) || 
             (this.bcc!=null &&
              this.bcc.equals(other.getBcc()))) &&
            ((this.images==null && other.getImages()==null) || 
             (this.images!=null &&
              java.util.Arrays.equals(this.images, other.getImages()))) &&
            ((this.attachment==null && other.getAttachment()==null) || 
             (this.attachment!=null &&
              this.attachment.equals(other.getAttachment()))) &&
            ((this.replyto==null && other.getReplyto()==null) || 
             (this.replyto!=null &&
              this.replyto.equals(other.getReplyto()))) &&
            ((this.removeAttachmentOnFinish==null && other.getRemoveAttachmentOnFinish()==null) || 
             (this.removeAttachmentOnFinish!=null &&
              this.removeAttachmentOnFinish.equals(other.getRemoveAttachmentOnFinish()))) &&
            ((this.attachValue==null && other.getAttachValue()==null) || 
             (this.attachValue!=null &&
              java.util.Arrays.equals(this.attachValue, other.getAttachValue())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getFrom_mail() != null) {
            _hashCode += getFrom_mail().hashCode();
        }
        if (getFrom_name() != null) {
            _hashCode += getFrom_name().hashCode();
        }
        if (getRecipient() != null) {
            _hashCode += getRecipient().hashCode();
        }
        if (getSubject() != null) {
            _hashCode += getSubject().hashCode();
        }
        if (getBody() != null) {
            _hashCode += getBody().hashCode();
        }
        if (getAltbody() != null) {
            _hashCode += getAltbody().hashCode();
        }
        if (getMode() != null) {
            _hashCode += getMode().hashCode();
        }
        if (getCc() != null) {
            _hashCode += getCc().hashCode();
        }
        if (getBcc() != null) {
            _hashCode += getBcc().hashCode();
        }
        if (getImages() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getImages());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getImages(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        if (getAttachment() != null) {
            _hashCode += getAttachment().hashCode();
        }
        if (getReplyto() != null) {
            _hashCode += getReplyto().hashCode();
        }
        if (getRemoveAttachmentOnFinish() != null) {
            _hashCode += getRemoveAttachmentOnFinish().hashCode();
        }
        if (getAttachValue() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getAttachValue());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getAttachValue(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(EmailParams.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://www.virtuemart.net/VM_Users/", "EmailParams"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("from_mail");
        elemField.setXmlName(new javax.xml.namespace.QName("", "from_mail"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("from_name");
        elemField.setXmlName(new javax.xml.namespace.QName("", "from_name"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("recipient");
        elemField.setXmlName(new javax.xml.namespace.QName("", "recipient"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("subject");
        elemField.setXmlName(new javax.xml.namespace.QName("", "subject"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("body");
        elemField.setXmlName(new javax.xml.namespace.QName("", "body"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("altbody");
        elemField.setXmlName(new javax.xml.namespace.QName("", "altbody"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("mode");
        elemField.setXmlName(new javax.xml.namespace.QName("", "mode"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("cc");
        elemField.setXmlName(new javax.xml.namespace.QName("", "cc"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("bcc");
        elemField.setXmlName(new javax.xml.namespace.QName("", "bcc"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("images");
        elemField.setXmlName(new javax.xml.namespace.QName("", "images"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.virtuemart.net/VM_Users/", "Image"));
        elemField.setNillable(true);
        elemField.setItemQName(new javax.xml.namespace.QName("", "Image"));
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("attachment");
        elemField.setXmlName(new javax.xml.namespace.QName("", "attachment"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("replyto");
        elemField.setXmlName(new javax.xml.namespace.QName("", "replyto"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("removeAttachmentOnFinish");
        elemField.setXmlName(new javax.xml.namespace.QName("", "removeAttachmentOnFinish"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("attachValue");
        elemField.setXmlName(new javax.xml.namespace.QName("", "attachValue"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "base64Binary"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
