/**
 * VM_Users_Service.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package it.netgrid.virtuemart.soap.users;

public interface VM_Users_Service extends javax.xml.rpc.Service {
    public java.lang.String getVM_UsersSOAPAddress();

    public it.netgrid.virtuemart.soap.users.VM_Users_PortType getVM_UsersSOAP() throws javax.xml.rpc.ServiceException;

    public it.netgrid.virtuemart.soap.users.VM_Users_PortType getVM_UsersSOAP(java.net.URL portAddress) throws javax.xml.rpc.ServiceException;
}
