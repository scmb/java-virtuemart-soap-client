/**
 * VM_Users_PortType.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package it.netgrid.virtuemart.soap.users;

public interface VM_Users_PortType extends java.rmi.Remote {
    public it.netgrid.virtuemart.soap.users.User[] getUsers(it.netgrid.virtuemart.soap.users.GetUsersInput parameters) throws java.rmi.RemoteException;
    public it.netgrid.virtuemart.soap.users.AuthOutput authentification(it.netgrid.virtuemart.soap.users.LoginInfo parameters) throws java.rmi.RemoteException;
    public it.netgrid.virtuemart.soap.users.CommonReturn addUser(it.netgrid.virtuemart.soap.users.AddUserInput parameters) throws java.rmi.RemoteException;
    public it.netgrid.virtuemart.soap.users.CommonReturn deleteUser(it.netgrid.virtuemart.soap.users.DeleteUserInput parameters) throws java.rmi.RemoteException;
    public it.netgrid.virtuemart.soap.users.CommonReturn sendMail(it.netgrid.virtuemart.soap.users.SendMailInput parameters) throws java.rmi.RemoteException;
    public it.netgrid.virtuemart.soap.users.User[] getUserFromEmailOrUsername(it.netgrid.virtuemart.soap.users.GetUserFromEmailOrUsernameInput parameters) throws java.rmi.RemoteException;
    public it.netgrid.virtuemart.soap.users.Country[] getAllCountryCode(it.netgrid.virtuemart.soap.users.LoginInfo parameters) throws java.rmi.RemoteException;
    public it.netgrid.virtuemart.soap.users.AuthGroup[] getAuthGroup(it.netgrid.virtuemart.soap.users.LoginInfo parameters) throws java.rmi.RemoteException;
    public it.netgrid.virtuemart.soap.users.CommonReturn addAuthGroup(it.netgrid.virtuemart.soap.users.AddAuthGroupInput parameters) throws java.rmi.RemoteException;
    public it.netgrid.virtuemart.soap.users.CommonReturn deleteAuthGroup(it.netgrid.virtuemart.soap.users.DelAuthGroupInput parameters) throws java.rmi.RemoteException;
    public it.netgrid.virtuemart.soap.users.State[] getAllStates(it.netgrid.virtuemart.soap.users.GetStatesInput parameters) throws java.rmi.RemoteException;
    public it.netgrid.virtuemart.soap.users.CommonReturn addStates(it.netgrid.virtuemart.soap.users.AddStatesInput parameters) throws java.rmi.RemoteException;
    public it.netgrid.virtuemart.soap.users.CommonReturn deleteStates(it.netgrid.virtuemart.soap.users.DelInput parameters) throws java.rmi.RemoteException;
    public it.netgrid.virtuemart.soap.users.ShopperGroup[] getShopperGroup(it.netgrid.virtuemart.soap.users.LoginInfo parameters) throws java.rmi.RemoteException;
    public it.netgrid.virtuemart.soap.users.CommonReturn addShopperGroup(it.netgrid.virtuemart.soap.users.AddShopperGroupsInput parameters) throws java.rmi.RemoteException;
    public it.netgrid.virtuemart.soap.users.CommonReturn updateShopperGroup(it.netgrid.virtuemart.soap.users.AddShopperGroupsInput parameters) throws java.rmi.RemoteException;
    public it.netgrid.virtuemart.soap.users.CommonReturn deleteShopperGroup(it.netgrid.virtuemart.soap.users.DelInput parameters) throws java.rmi.RemoteException;
    public it.netgrid.virtuemart.soap.users.CommonReturn updateUser(it.netgrid.virtuemart.soap.users.AddUserInput parameters) throws java.rmi.RemoteException;
    public it.netgrid.virtuemart.soap.users.CommonReturn addVendor(it.netgrid.virtuemart.soap.users.AddVendorInput parameters) throws java.rmi.RemoteException;
    public it.netgrid.virtuemart.soap.users.Vendor[] getAllVendor(it.netgrid.virtuemart.soap.users.GetVendorsInput parameters) throws java.rmi.RemoteException;
    public it.netgrid.virtuemart.soap.users.CommonReturn updateVendor(it.netgrid.virtuemart.soap.users.AddVendorInput parameters) throws java.rmi.RemoteException;
    public it.netgrid.virtuemart.soap.users.CommonReturn deleteVendor(it.netgrid.virtuemart.soap.users.DelInput parameters) throws java.rmi.RemoteException;
    public it.netgrid.virtuemart.soap.users.VendorCategory[] getAllVendorCategory(it.netgrid.virtuemart.soap.users.LoginInfo parameters) throws java.rmi.RemoteException;
    public it.netgrid.virtuemart.soap.users.CommonReturn updateVendorCategory(it.netgrid.virtuemart.soap.users.AddVendorCatInput parameters) throws java.rmi.RemoteException;
    public it.netgrid.virtuemart.soap.users.CommonReturn addVendorCategory(it.netgrid.virtuemart.soap.users.AddVendorCatInput parameters) throws java.rmi.RemoteException;
    public it.netgrid.virtuemart.soap.users.CommonReturn deleteVendorCategory(it.netgrid.virtuemart.soap.users.DelInput parameters) throws java.rmi.RemoteException;
    public it.netgrid.virtuemart.soap.users.Manufacturer[] getAllManufacturer(it.netgrid.virtuemart.soap.users.LoginInfo parameters) throws java.rmi.RemoteException;
    public it.netgrid.virtuemart.soap.users.CommonReturn addManufacturer(it.netgrid.virtuemart.soap.users.AddManufacturerInput parameters) throws java.rmi.RemoteException;
    public it.netgrid.virtuemart.soap.users.CommonReturn updateManufacturer(it.netgrid.virtuemart.soap.users.AddManufacturerInput parameters) throws java.rmi.RemoteException;
    public it.netgrid.virtuemart.soap.users.CommonReturn deleteManufacturer(it.netgrid.virtuemart.soap.users.DelInput parameters) throws java.rmi.RemoteException;
    public it.netgrid.virtuemart.soap.users.ManufacturerCat[] getAllManufacturerCat(it.netgrid.virtuemart.soap.users.LoginInfo parameters) throws java.rmi.RemoteException;
    public it.netgrid.virtuemart.soap.users.CommonReturn addManufacturerCat(it.netgrid.virtuemart.soap.users.AddManufacturerCatInput parameters) throws java.rmi.RemoteException;
    public it.netgrid.virtuemart.soap.users.CommonReturn updateManufacturerCat(it.netgrid.virtuemart.soap.users.AddManufacturerCatInput parameters) throws java.rmi.RemoteException;
    public it.netgrid.virtuemart.soap.users.CommonReturn deleteManufacturerCat(it.netgrid.virtuemart.soap.users.DelInput parameters) throws java.rmi.RemoteException;
    public it.netgrid.virtuemart.soap.users.AvalaibleImage[] getAvailableVendorImages(it.netgrid.virtuemart.soap.users.GetAvalaibleImageInput parameters) throws java.rmi.RemoteException;
    public it.netgrid.virtuemart.soap.users.Version getVersions(it.netgrid.virtuemart.soap.users.LoginInfo parameters) throws java.rmi.RemoteException;
    public it.netgrid.virtuemart.soap.users.User[] getAdditionalUserInfo(it.netgrid.virtuemart.soap.users.GetAdditionalUserInfoInput parameters) throws java.rmi.RemoteException;
    public it.netgrid.virtuemart.soap.users.Session[] getSessions(it.netgrid.virtuemart.soap.users.GetSessionsInput parameters) throws java.rmi.RemoteException;
    public it.netgrid.virtuemart.soap.users.WaitingList[] getWaitingList(it.netgrid.virtuemart.soap.users.GetWaitingListInput parameters) throws java.rmi.RemoteException;
    public it.netgrid.virtuemart.soap.users.CommonReturn notifyWaitingList(it.netgrid.virtuemart.soap.users.NotifyWaitingListInput parameters) throws java.rmi.RemoteException;
    public it.netgrid.virtuemart.soap.users.User[] getUserInfoFromOrderID(it.netgrid.virtuemart.soap.users.GetUserInfoFromOrderIDInput parameters) throws java.rmi.RemoteException;
    public it.netgrid.virtuemart.soap.users.Userfield[] getUserfields(it.netgrid.virtuemart.soap.users.GenericGetInput parameters) throws java.rmi.RemoteException;
    public it.netgrid.virtuemart.soap.users.Media[] getMediaVendor(it.netgrid.virtuemart.soap.users.GetMediaVendorInput parameters) throws java.rmi.RemoteException;
    public it.netgrid.virtuemart.soap.users.Media[] getMediaManufacturer(it.netgrid.virtuemart.soap.users.GetMediaManufacturerInput parameters) throws java.rmi.RemoteException;
    public it.netgrid.virtuemart.soap.users.CommonReturn addMediaVendor(it.netgrid.virtuemart.soap.users.AddMediaVendorInput parameters) throws java.rmi.RemoteException;
    public it.netgrid.virtuemart.soap.users.CommonReturn addMediaManufacturer(it.netgrid.virtuemart.soap.users.AddMediaManufacturerInput parameters) throws java.rmi.RemoteException;
    public it.netgrid.virtuemart.soap.users.CommonReturn deleteMediaVendor(it.netgrid.virtuemart.soap.users.DelMediaInput parameters) throws java.rmi.RemoteException;
    public it.netgrid.virtuemart.soap.users.CommonReturn deleteMediaManufacturer(it.netgrid.virtuemart.soap.users.DelMediaInput parameters) throws java.rmi.RemoteException;
    public it.netgrid.virtuemart.soap.users.CommonReturn addShippingAdress(it.netgrid.virtuemart.soap.users.AddShippingAdressInput parameters) throws java.rmi.RemoteException;
    public it.netgrid.virtuemart.soap.users.CommonReturn updateShippingAdress(it.netgrid.virtuemart.soap.users.AddShippingAdressInput parameters) throws java.rmi.RemoteException;
    public it.netgrid.virtuemart.soap.users.CommonReturn deleteShippingAdress(it.netgrid.virtuemart.soap.users.DelShippingAdressInput parameters) throws java.rmi.RemoteException;
}
