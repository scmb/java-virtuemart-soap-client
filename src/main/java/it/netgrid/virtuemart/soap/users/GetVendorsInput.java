/**
 * GetVendorsInput.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package it.netgrid.virtuemart.soap.users;

public class GetVendorsInput  implements java.io.Serializable {
    private it.netgrid.virtuemart.soap.users.LoginInfo loginInfo;

    private java.lang.String include_user_info;

    public GetVendorsInput() {
    }

    public GetVendorsInput(
           it.netgrid.virtuemart.soap.users.LoginInfo loginInfo,
           java.lang.String include_user_info) {
           this.loginInfo = loginInfo;
           this.include_user_info = include_user_info;
    }


    /**
     * Gets the loginInfo value for this GetVendorsInput.
     * 
     * @return loginInfo
     */
    public it.netgrid.virtuemart.soap.users.LoginInfo getLoginInfo() {
        return loginInfo;
    }


    /**
     * Sets the loginInfo value for this GetVendorsInput.
     * 
     * @param loginInfo
     */
    public void setLoginInfo(it.netgrid.virtuemart.soap.users.LoginInfo loginInfo) {
        this.loginInfo = loginInfo;
    }


    /**
     * Gets the include_user_info value for this GetVendorsInput.
     * 
     * @return include_user_info
     */
    public java.lang.String getInclude_user_info() {
        return include_user_info;
    }


    /**
     * Sets the include_user_info value for this GetVendorsInput.
     * 
     * @param include_user_info
     */
    public void setInclude_user_info(java.lang.String include_user_info) {
        this.include_user_info = include_user_info;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof GetVendorsInput)) return false;
        GetVendorsInput other = (GetVendorsInput) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.loginInfo==null && other.getLoginInfo()==null) || 
             (this.loginInfo!=null &&
              this.loginInfo.equals(other.getLoginInfo()))) &&
            ((this.include_user_info==null && other.getInclude_user_info()==null) || 
             (this.include_user_info!=null &&
              this.include_user_info.equals(other.getInclude_user_info())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getLoginInfo() != null) {
            _hashCode += getLoginInfo().hashCode();
        }
        if (getInclude_user_info() != null) {
            _hashCode += getInclude_user_info().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(GetVendorsInput.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://www.virtuemart.net/VM_Users/", "GetVendorsInput"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("loginInfo");
        elemField.setXmlName(new javax.xml.namespace.QName("", "loginInfo"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.virtuemart.net/VM_Users/", "loginInfo"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("include_user_info");
        elemField.setXmlName(new javax.xml.namespace.QName("", "include_user_info"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
