package it.netgrid.virtuemart.soap.users;

public class VM_UsersProxy implements it.netgrid.virtuemart.soap.users.VM_Users_PortType {
  private String _endpoint = null;
  private it.netgrid.virtuemart.soap.users.VM_Users_PortType vM_Users_PortType = null;
  
  public VM_UsersProxy() {
    _initVM_UsersProxy();
  }
  
  public VM_UsersProxy(String endpoint) {
    _endpoint = endpoint;
    _initVM_UsersProxy();
  }
  
  private void _initVM_UsersProxy() {
    try {
      vM_Users_PortType = (new it.netgrid.virtuemart.soap.users.VM_Users_ServiceLocator()).getVM_UsersSOAP();
      if (vM_Users_PortType != null) {
        if (_endpoint != null)
          ((javax.xml.rpc.Stub)vM_Users_PortType)._setProperty("javax.xml.rpc.service.endpoint.address", _endpoint);
        else
          _endpoint = (String)((javax.xml.rpc.Stub)vM_Users_PortType)._getProperty("javax.xml.rpc.service.endpoint.address");
      }
      
    }
    catch (javax.xml.rpc.ServiceException serviceException) {}
  }
  
  public String getEndpoint() {
    return _endpoint;
  }
  
  public void setEndpoint(String endpoint) {
    _endpoint = endpoint;
    if (vM_Users_PortType != null)
      ((javax.xml.rpc.Stub)vM_Users_PortType)._setProperty("javax.xml.rpc.service.endpoint.address", _endpoint);
    
  }
  
  public it.netgrid.virtuemart.soap.users.VM_Users_PortType getVM_Users_PortType() {
    if (vM_Users_PortType == null)
      _initVM_UsersProxy();
    return vM_Users_PortType;
  }
  
  public it.netgrid.virtuemart.soap.users.User[] getUsers(it.netgrid.virtuemart.soap.users.GetUsersInput parameters) throws java.rmi.RemoteException{
    if (vM_Users_PortType == null)
      _initVM_UsersProxy();
    return vM_Users_PortType.getUsers(parameters);
  }
  
  public it.netgrid.virtuemart.soap.users.AuthOutput authentification(it.netgrid.virtuemart.soap.users.LoginInfo parameters) throws java.rmi.RemoteException{
    if (vM_Users_PortType == null)
      _initVM_UsersProxy();
    return vM_Users_PortType.authentification(parameters);
  }
  
  public it.netgrid.virtuemart.soap.users.CommonReturn addUser(it.netgrid.virtuemart.soap.users.AddUserInput parameters) throws java.rmi.RemoteException{
    if (vM_Users_PortType == null)
      _initVM_UsersProxy();
    return vM_Users_PortType.addUser(parameters);
  }
  
  public it.netgrid.virtuemart.soap.users.CommonReturn deleteUser(it.netgrid.virtuemart.soap.users.DeleteUserInput parameters) throws java.rmi.RemoteException{
    if (vM_Users_PortType == null)
      _initVM_UsersProxy();
    return vM_Users_PortType.deleteUser(parameters);
  }
  
  public it.netgrid.virtuemart.soap.users.CommonReturn sendMail(it.netgrid.virtuemart.soap.users.SendMailInput parameters) throws java.rmi.RemoteException{
    if (vM_Users_PortType == null)
      _initVM_UsersProxy();
    return vM_Users_PortType.sendMail(parameters);
  }
  
  public it.netgrid.virtuemart.soap.users.User[] getUserFromEmailOrUsername(it.netgrid.virtuemart.soap.users.GetUserFromEmailOrUsernameInput parameters) throws java.rmi.RemoteException{
    if (vM_Users_PortType == null)
      _initVM_UsersProxy();
    return vM_Users_PortType.getUserFromEmailOrUsername(parameters);
  }
  
  public it.netgrid.virtuemart.soap.users.Country[] getAllCountryCode(it.netgrid.virtuemart.soap.users.LoginInfo parameters) throws java.rmi.RemoteException{
    if (vM_Users_PortType == null)
      _initVM_UsersProxy();
    return vM_Users_PortType.getAllCountryCode(parameters);
  }
  
  public it.netgrid.virtuemart.soap.users.AuthGroup[] getAuthGroup(it.netgrid.virtuemart.soap.users.LoginInfo parameters) throws java.rmi.RemoteException{
    if (vM_Users_PortType == null)
      _initVM_UsersProxy();
    return vM_Users_PortType.getAuthGroup(parameters);
  }
  
  public it.netgrid.virtuemart.soap.users.CommonReturn addAuthGroup(it.netgrid.virtuemart.soap.users.AddAuthGroupInput parameters) throws java.rmi.RemoteException{
    if (vM_Users_PortType == null)
      _initVM_UsersProxy();
    return vM_Users_PortType.addAuthGroup(parameters);
  }
  
  public it.netgrid.virtuemart.soap.users.CommonReturn deleteAuthGroup(it.netgrid.virtuemart.soap.users.DelAuthGroupInput parameters) throws java.rmi.RemoteException{
    if (vM_Users_PortType == null)
      _initVM_UsersProxy();
    return vM_Users_PortType.deleteAuthGroup(parameters);
  }
  
  public it.netgrid.virtuemart.soap.users.State[] getAllStates(it.netgrid.virtuemart.soap.users.GetStatesInput parameters) throws java.rmi.RemoteException{
    if (vM_Users_PortType == null)
      _initVM_UsersProxy();
    return vM_Users_PortType.getAllStates(parameters);
  }
  
  public it.netgrid.virtuemart.soap.users.CommonReturn addStates(it.netgrid.virtuemart.soap.users.AddStatesInput parameters) throws java.rmi.RemoteException{
    if (vM_Users_PortType == null)
      _initVM_UsersProxy();
    return vM_Users_PortType.addStates(parameters);
  }
  
  public it.netgrid.virtuemart.soap.users.CommonReturn deleteStates(it.netgrid.virtuemart.soap.users.DelInput parameters) throws java.rmi.RemoteException{
    if (vM_Users_PortType == null)
      _initVM_UsersProxy();
    return vM_Users_PortType.deleteStates(parameters);
  }
  
  public it.netgrid.virtuemart.soap.users.ShopperGroup[] getShopperGroup(it.netgrid.virtuemart.soap.users.LoginInfo parameters) throws java.rmi.RemoteException{
    if (vM_Users_PortType == null)
      _initVM_UsersProxy();
    return vM_Users_PortType.getShopperGroup(parameters);
  }
  
  public it.netgrid.virtuemart.soap.users.CommonReturn addShopperGroup(it.netgrid.virtuemart.soap.users.AddShopperGroupsInput parameters) throws java.rmi.RemoteException{
    if (vM_Users_PortType == null)
      _initVM_UsersProxy();
    return vM_Users_PortType.addShopperGroup(parameters);
  }
  
  public it.netgrid.virtuemart.soap.users.CommonReturn updateShopperGroup(it.netgrid.virtuemart.soap.users.AddShopperGroupsInput parameters) throws java.rmi.RemoteException{
    if (vM_Users_PortType == null)
      _initVM_UsersProxy();
    return vM_Users_PortType.updateShopperGroup(parameters);
  }
  
  public it.netgrid.virtuemart.soap.users.CommonReturn deleteShopperGroup(it.netgrid.virtuemart.soap.users.DelInput parameters) throws java.rmi.RemoteException{
    if (vM_Users_PortType == null)
      _initVM_UsersProxy();
    return vM_Users_PortType.deleteShopperGroup(parameters);
  }
  
  public it.netgrid.virtuemart.soap.users.CommonReturn updateUser(it.netgrid.virtuemart.soap.users.AddUserInput parameters) throws java.rmi.RemoteException{
    if (vM_Users_PortType == null)
      _initVM_UsersProxy();
    return vM_Users_PortType.updateUser(parameters);
  }
  
  public it.netgrid.virtuemart.soap.users.CommonReturn addVendor(it.netgrid.virtuemart.soap.users.AddVendorInput parameters) throws java.rmi.RemoteException{
    if (vM_Users_PortType == null)
      _initVM_UsersProxy();
    return vM_Users_PortType.addVendor(parameters);
  }
  
  public it.netgrid.virtuemart.soap.users.Vendor[] getAllVendor(it.netgrid.virtuemart.soap.users.GetVendorsInput parameters) throws java.rmi.RemoteException{
    if (vM_Users_PortType == null)
      _initVM_UsersProxy();
    return vM_Users_PortType.getAllVendor(parameters);
  }
  
  public it.netgrid.virtuemart.soap.users.CommonReturn updateVendor(it.netgrid.virtuemart.soap.users.AddVendorInput parameters) throws java.rmi.RemoteException{
    if (vM_Users_PortType == null)
      _initVM_UsersProxy();
    return vM_Users_PortType.updateVendor(parameters);
  }
  
  public it.netgrid.virtuemart.soap.users.CommonReturn deleteVendor(it.netgrid.virtuemart.soap.users.DelInput parameters) throws java.rmi.RemoteException{
    if (vM_Users_PortType == null)
      _initVM_UsersProxy();
    return vM_Users_PortType.deleteVendor(parameters);
  }
  
  public it.netgrid.virtuemart.soap.users.VendorCategory[] getAllVendorCategory(it.netgrid.virtuemart.soap.users.LoginInfo parameters) throws java.rmi.RemoteException{
    if (vM_Users_PortType == null)
      _initVM_UsersProxy();
    return vM_Users_PortType.getAllVendorCategory(parameters);
  }
  
  public it.netgrid.virtuemart.soap.users.CommonReturn updateVendorCategory(it.netgrid.virtuemart.soap.users.AddVendorCatInput parameters) throws java.rmi.RemoteException{
    if (vM_Users_PortType == null)
      _initVM_UsersProxy();
    return vM_Users_PortType.updateVendorCategory(parameters);
  }
  
  public it.netgrid.virtuemart.soap.users.CommonReturn addVendorCategory(it.netgrid.virtuemart.soap.users.AddVendorCatInput parameters) throws java.rmi.RemoteException{
    if (vM_Users_PortType == null)
      _initVM_UsersProxy();
    return vM_Users_PortType.addVendorCategory(parameters);
  }
  
  public it.netgrid.virtuemart.soap.users.CommonReturn deleteVendorCategory(it.netgrid.virtuemart.soap.users.DelInput parameters) throws java.rmi.RemoteException{
    if (vM_Users_PortType == null)
      _initVM_UsersProxy();
    return vM_Users_PortType.deleteVendorCategory(parameters);
  }
  
  public it.netgrid.virtuemart.soap.users.Manufacturer[] getAllManufacturer(it.netgrid.virtuemart.soap.users.LoginInfo parameters) throws java.rmi.RemoteException{
    if (vM_Users_PortType == null)
      _initVM_UsersProxy();
    return vM_Users_PortType.getAllManufacturer(parameters);
  }
  
  public it.netgrid.virtuemart.soap.users.CommonReturn addManufacturer(it.netgrid.virtuemart.soap.users.AddManufacturerInput parameters) throws java.rmi.RemoteException{
    if (vM_Users_PortType == null)
      _initVM_UsersProxy();
    return vM_Users_PortType.addManufacturer(parameters);
  }
  
  public it.netgrid.virtuemart.soap.users.CommonReturn updateManufacturer(it.netgrid.virtuemart.soap.users.AddManufacturerInput parameters) throws java.rmi.RemoteException{
    if (vM_Users_PortType == null)
      _initVM_UsersProxy();
    return vM_Users_PortType.updateManufacturer(parameters);
  }
  
  public it.netgrid.virtuemart.soap.users.CommonReturn deleteManufacturer(it.netgrid.virtuemart.soap.users.DelInput parameters) throws java.rmi.RemoteException{
    if (vM_Users_PortType == null)
      _initVM_UsersProxy();
    return vM_Users_PortType.deleteManufacturer(parameters);
  }
  
  public it.netgrid.virtuemart.soap.users.ManufacturerCat[] getAllManufacturerCat(it.netgrid.virtuemart.soap.users.LoginInfo parameters) throws java.rmi.RemoteException{
    if (vM_Users_PortType == null)
      _initVM_UsersProxy();
    return vM_Users_PortType.getAllManufacturerCat(parameters);
  }
  
  public it.netgrid.virtuemart.soap.users.CommonReturn addManufacturerCat(it.netgrid.virtuemart.soap.users.AddManufacturerCatInput parameters) throws java.rmi.RemoteException{
    if (vM_Users_PortType == null)
      _initVM_UsersProxy();
    return vM_Users_PortType.addManufacturerCat(parameters);
  }
  
  public it.netgrid.virtuemart.soap.users.CommonReturn updateManufacturerCat(it.netgrid.virtuemart.soap.users.AddManufacturerCatInput parameters) throws java.rmi.RemoteException{
    if (vM_Users_PortType == null)
      _initVM_UsersProxy();
    return vM_Users_PortType.updateManufacturerCat(parameters);
  }
  
  public it.netgrid.virtuemart.soap.users.CommonReturn deleteManufacturerCat(it.netgrid.virtuemart.soap.users.DelInput parameters) throws java.rmi.RemoteException{
    if (vM_Users_PortType == null)
      _initVM_UsersProxy();
    return vM_Users_PortType.deleteManufacturerCat(parameters);
  }
  
  public it.netgrid.virtuemart.soap.users.AvalaibleImage[] getAvailableVendorImages(it.netgrid.virtuemart.soap.users.GetAvalaibleImageInput parameters) throws java.rmi.RemoteException{
    if (vM_Users_PortType == null)
      _initVM_UsersProxy();
    return vM_Users_PortType.getAvailableVendorImages(parameters);
  }
  
  public it.netgrid.virtuemart.soap.users.Version getVersions(it.netgrid.virtuemart.soap.users.LoginInfo parameters) throws java.rmi.RemoteException{
    if (vM_Users_PortType == null)
      _initVM_UsersProxy();
    return vM_Users_PortType.getVersions(parameters);
  }
  
  public it.netgrid.virtuemart.soap.users.User[] getAdditionalUserInfo(it.netgrid.virtuemart.soap.users.GetAdditionalUserInfoInput parameters) throws java.rmi.RemoteException{
    if (vM_Users_PortType == null)
      _initVM_UsersProxy();
    return vM_Users_PortType.getAdditionalUserInfo(parameters);
  }
  
  public it.netgrid.virtuemart.soap.users.Session[] getSessions(it.netgrid.virtuemart.soap.users.GetSessionsInput parameters) throws java.rmi.RemoteException{
    if (vM_Users_PortType == null)
      _initVM_UsersProxy();
    return vM_Users_PortType.getSessions(parameters);
  }
  
  public it.netgrid.virtuemart.soap.users.WaitingList[] getWaitingList(it.netgrid.virtuemart.soap.users.GetWaitingListInput parameters) throws java.rmi.RemoteException{
    if (vM_Users_PortType == null)
      _initVM_UsersProxy();
    return vM_Users_PortType.getWaitingList(parameters);
  }
  
  public it.netgrid.virtuemart.soap.users.CommonReturn notifyWaitingList(it.netgrid.virtuemart.soap.users.NotifyWaitingListInput parameters) throws java.rmi.RemoteException{
    if (vM_Users_PortType == null)
      _initVM_UsersProxy();
    return vM_Users_PortType.notifyWaitingList(parameters);
  }
  
  public it.netgrid.virtuemart.soap.users.User[] getUserInfoFromOrderID(it.netgrid.virtuemart.soap.users.GetUserInfoFromOrderIDInput parameters) throws java.rmi.RemoteException{
    if (vM_Users_PortType == null)
      _initVM_UsersProxy();
    return vM_Users_PortType.getUserInfoFromOrderID(parameters);
  }
  
  public it.netgrid.virtuemart.soap.users.Userfield[] getUserfields(it.netgrid.virtuemart.soap.users.GenericGetInput parameters) throws java.rmi.RemoteException{
    if (vM_Users_PortType == null)
      _initVM_UsersProxy();
    return vM_Users_PortType.getUserfields(parameters);
  }
  
  public it.netgrid.virtuemart.soap.users.Media[] getMediaVendor(it.netgrid.virtuemart.soap.users.GetMediaVendorInput parameters) throws java.rmi.RemoteException{
    if (vM_Users_PortType == null)
      _initVM_UsersProxy();
    return vM_Users_PortType.getMediaVendor(parameters);
  }
  
  public it.netgrid.virtuemart.soap.users.Media[] getMediaManufacturer(it.netgrid.virtuemart.soap.users.GetMediaManufacturerInput parameters) throws java.rmi.RemoteException{
    if (vM_Users_PortType == null)
      _initVM_UsersProxy();
    return vM_Users_PortType.getMediaManufacturer(parameters);
  }
  
  public it.netgrid.virtuemart.soap.users.CommonReturn addMediaVendor(it.netgrid.virtuemart.soap.users.AddMediaVendorInput parameters) throws java.rmi.RemoteException{
    if (vM_Users_PortType == null)
      _initVM_UsersProxy();
    return vM_Users_PortType.addMediaVendor(parameters);
  }
  
  public it.netgrid.virtuemart.soap.users.CommonReturn addMediaManufacturer(it.netgrid.virtuemart.soap.users.AddMediaManufacturerInput parameters) throws java.rmi.RemoteException{
    if (vM_Users_PortType == null)
      _initVM_UsersProxy();
    return vM_Users_PortType.addMediaManufacturer(parameters);
  }
  
  public it.netgrid.virtuemart.soap.users.CommonReturn deleteMediaVendor(it.netgrid.virtuemart.soap.users.DelMediaInput parameters) throws java.rmi.RemoteException{
    if (vM_Users_PortType == null)
      _initVM_UsersProxy();
    return vM_Users_PortType.deleteMediaVendor(parameters);
  }
  
  public it.netgrid.virtuemart.soap.users.CommonReturn deleteMediaManufacturer(it.netgrid.virtuemart.soap.users.DelMediaInput parameters) throws java.rmi.RemoteException{
    if (vM_Users_PortType == null)
      _initVM_UsersProxy();
    return vM_Users_PortType.deleteMediaManufacturer(parameters);
  }
  
  public it.netgrid.virtuemart.soap.users.CommonReturn addShippingAdress(it.netgrid.virtuemart.soap.users.AddShippingAdressInput parameters) throws java.rmi.RemoteException{
    if (vM_Users_PortType == null)
      _initVM_UsersProxy();
    return vM_Users_PortType.addShippingAdress(parameters);
  }
  
  public it.netgrid.virtuemart.soap.users.CommonReturn updateShippingAdress(it.netgrid.virtuemart.soap.users.AddShippingAdressInput parameters) throws java.rmi.RemoteException{
    if (vM_Users_PortType == null)
      _initVM_UsersProxy();
    return vM_Users_PortType.updateShippingAdress(parameters);
  }
  
  public it.netgrid.virtuemart.soap.users.CommonReturn deleteShippingAdress(it.netgrid.virtuemart.soap.users.DelShippingAdressInput parameters) throws java.rmi.RemoteException{
    if (vM_Users_PortType == null)
      _initVM_UsersProxy();
    return vM_Users_PortType.deleteShippingAdress(parameters);
  }
  
  
}