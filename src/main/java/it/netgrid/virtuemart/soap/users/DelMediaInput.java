/**
 * DelMediaInput.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package it.netgrid.virtuemart.soap.users;

public class DelMediaInput  implements java.io.Serializable {
    private it.netgrid.virtuemart.soap.users.LoginInfo loginInfo;

    private java.lang.String vendor_id;

    private java.lang.String manufacturer_id;

    private java.lang.String media_id;

    public DelMediaInput() {
    }

    public DelMediaInput(
           it.netgrid.virtuemart.soap.users.LoginInfo loginInfo,
           java.lang.String vendor_id,
           java.lang.String manufacturer_id,
           java.lang.String media_id) {
           this.loginInfo = loginInfo;
           this.vendor_id = vendor_id;
           this.manufacturer_id = manufacturer_id;
           this.media_id = media_id;
    }


    /**
     * Gets the loginInfo value for this DelMediaInput.
     * 
     * @return loginInfo
     */
    public it.netgrid.virtuemart.soap.users.LoginInfo getLoginInfo() {
        return loginInfo;
    }


    /**
     * Sets the loginInfo value for this DelMediaInput.
     * 
     * @param loginInfo
     */
    public void setLoginInfo(it.netgrid.virtuemart.soap.users.LoginInfo loginInfo) {
        this.loginInfo = loginInfo;
    }


    /**
     * Gets the vendor_id value for this DelMediaInput.
     * 
     * @return vendor_id
     */
    public java.lang.String getVendor_id() {
        return vendor_id;
    }


    /**
     * Sets the vendor_id value for this DelMediaInput.
     * 
     * @param vendor_id
     */
    public void setVendor_id(java.lang.String vendor_id) {
        this.vendor_id = vendor_id;
    }


    /**
     * Gets the manufacturer_id value for this DelMediaInput.
     * 
     * @return manufacturer_id
     */
    public java.lang.String getManufacturer_id() {
        return manufacturer_id;
    }


    /**
     * Sets the manufacturer_id value for this DelMediaInput.
     * 
     * @param manufacturer_id
     */
    public void setManufacturer_id(java.lang.String manufacturer_id) {
        this.manufacturer_id = manufacturer_id;
    }


    /**
     * Gets the media_id value for this DelMediaInput.
     * 
     * @return media_id
     */
    public java.lang.String getMedia_id() {
        return media_id;
    }


    /**
     * Sets the media_id value for this DelMediaInput.
     * 
     * @param media_id
     */
    public void setMedia_id(java.lang.String media_id) {
        this.media_id = media_id;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof DelMediaInput)) return false;
        DelMediaInput other = (DelMediaInput) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.loginInfo==null && other.getLoginInfo()==null) || 
             (this.loginInfo!=null &&
              this.loginInfo.equals(other.getLoginInfo()))) &&
            ((this.vendor_id==null && other.getVendor_id()==null) || 
             (this.vendor_id!=null &&
              this.vendor_id.equals(other.getVendor_id()))) &&
            ((this.manufacturer_id==null && other.getManufacturer_id()==null) || 
             (this.manufacturer_id!=null &&
              this.manufacturer_id.equals(other.getManufacturer_id()))) &&
            ((this.media_id==null && other.getMedia_id()==null) || 
             (this.media_id!=null &&
              this.media_id.equals(other.getMedia_id())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getLoginInfo() != null) {
            _hashCode += getLoginInfo().hashCode();
        }
        if (getVendor_id() != null) {
            _hashCode += getVendor_id().hashCode();
        }
        if (getManufacturer_id() != null) {
            _hashCode += getManufacturer_id().hashCode();
        }
        if (getMedia_id() != null) {
            _hashCode += getMedia_id().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(DelMediaInput.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://www.virtuemart.net/VM_Users/", "delMediaInput"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("loginInfo");
        elemField.setXmlName(new javax.xml.namespace.QName("", "loginInfo"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.virtuemart.net/VM_Users/", "loginInfo"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("vendor_id");
        elemField.setXmlName(new javax.xml.namespace.QName("", "vendor_id"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("manufacturer_id");
        elemField.setXmlName(new javax.xml.namespace.QName("", "manufacturer_id"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("media_id");
        elemField.setXmlName(new javax.xml.namespace.QName("", "media_id"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
