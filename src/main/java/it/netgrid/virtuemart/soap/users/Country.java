/**
 * Country.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package it.netgrid.virtuemart.soap.users;

public class Country  implements java.io.Serializable {
    private java.lang.String country_id;

    private java.lang.String virtuemart_worldzone_id;

    private java.lang.String country_name;

    private java.lang.String country_3_code;

    private java.lang.String country_2_code;

    private java.lang.String published;

    public Country() {
    }

    public Country(
           java.lang.String country_id,
           java.lang.String virtuemart_worldzone_id,
           java.lang.String country_name,
           java.lang.String country_3_code,
           java.lang.String country_2_code,
           java.lang.String published) {
           this.country_id = country_id;
           this.virtuemart_worldzone_id = virtuemart_worldzone_id;
           this.country_name = country_name;
           this.country_3_code = country_3_code;
           this.country_2_code = country_2_code;
           this.published = published;
    }


    /**
     * Gets the country_id value for this Country.
     * 
     * @return country_id
     */
    public java.lang.String getCountry_id() {
        return country_id;
    }


    /**
     * Sets the country_id value for this Country.
     * 
     * @param country_id
     */
    public void setCountry_id(java.lang.String country_id) {
        this.country_id = country_id;
    }


    /**
     * Gets the virtuemart_worldzone_id value for this Country.
     * 
     * @return virtuemart_worldzone_id
     */
    public java.lang.String getVirtuemart_worldzone_id() {
        return virtuemart_worldzone_id;
    }


    /**
     * Sets the virtuemart_worldzone_id value for this Country.
     * 
     * @param virtuemart_worldzone_id
     */
    public void setVirtuemart_worldzone_id(java.lang.String virtuemart_worldzone_id) {
        this.virtuemart_worldzone_id = virtuemart_worldzone_id;
    }


    /**
     * Gets the country_name value for this Country.
     * 
     * @return country_name
     */
    public java.lang.String getCountry_name() {
        return country_name;
    }


    /**
     * Sets the country_name value for this Country.
     * 
     * @param country_name
     */
    public void setCountry_name(java.lang.String country_name) {
        this.country_name = country_name;
    }


    /**
     * Gets the country_3_code value for this Country.
     * 
     * @return country_3_code
     */
    public java.lang.String getCountry_3_code() {
        return country_3_code;
    }


    /**
     * Sets the country_3_code value for this Country.
     * 
     * @param country_3_code
     */
    public void setCountry_3_code(java.lang.String country_3_code) {
        this.country_3_code = country_3_code;
    }


    /**
     * Gets the country_2_code value for this Country.
     * 
     * @return country_2_code
     */
    public java.lang.String getCountry_2_code() {
        return country_2_code;
    }


    /**
     * Sets the country_2_code value for this Country.
     * 
     * @param country_2_code
     */
    public void setCountry_2_code(java.lang.String country_2_code) {
        this.country_2_code = country_2_code;
    }


    /**
     * Gets the published value for this Country.
     * 
     * @return published
     */
    public java.lang.String getPublished() {
        return published;
    }


    /**
     * Sets the published value for this Country.
     * 
     * @param published
     */
    public void setPublished(java.lang.String published) {
        this.published = published;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof Country)) return false;
        Country other = (Country) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.country_id==null && other.getCountry_id()==null) || 
             (this.country_id!=null &&
              this.country_id.equals(other.getCountry_id()))) &&
            ((this.virtuemart_worldzone_id==null && other.getVirtuemart_worldzone_id()==null) || 
             (this.virtuemart_worldzone_id!=null &&
              this.virtuemart_worldzone_id.equals(other.getVirtuemart_worldzone_id()))) &&
            ((this.country_name==null && other.getCountry_name()==null) || 
             (this.country_name!=null &&
              this.country_name.equals(other.getCountry_name()))) &&
            ((this.country_3_code==null && other.getCountry_3_code()==null) || 
             (this.country_3_code!=null &&
              this.country_3_code.equals(other.getCountry_3_code()))) &&
            ((this.country_2_code==null && other.getCountry_2_code()==null) || 
             (this.country_2_code!=null &&
              this.country_2_code.equals(other.getCountry_2_code()))) &&
            ((this.published==null && other.getPublished()==null) || 
             (this.published!=null &&
              this.published.equals(other.getPublished())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getCountry_id() != null) {
            _hashCode += getCountry_id().hashCode();
        }
        if (getVirtuemart_worldzone_id() != null) {
            _hashCode += getVirtuemart_worldzone_id().hashCode();
        }
        if (getCountry_name() != null) {
            _hashCode += getCountry_name().hashCode();
        }
        if (getCountry_3_code() != null) {
            _hashCode += getCountry_3_code().hashCode();
        }
        if (getCountry_2_code() != null) {
            _hashCode += getCountry_2_code().hashCode();
        }
        if (getPublished() != null) {
            _hashCode += getPublished().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(Country.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://www.virtuemart.net/VM_Users/", "Country"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("country_id");
        elemField.setXmlName(new javax.xml.namespace.QName("", "country_id"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("virtuemart_worldzone_id");
        elemField.setXmlName(new javax.xml.namespace.QName("", "virtuemart_worldzone_id"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("country_name");
        elemField.setXmlName(new javax.xml.namespace.QName("", "country_name"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("country_3_code");
        elemField.setXmlName(new javax.xml.namespace.QName("", "country_3_code"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("country_2_code");
        elemField.setXmlName(new javax.xml.namespace.QName("", "country_2_code"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("published");
        elemField.setXmlName(new javax.xml.namespace.QName("", "published"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
