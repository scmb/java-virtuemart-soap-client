/**
 * GetWaitingListInput.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package it.netgrid.virtuemart.soap.users;

public class GetWaitingListInput  implements java.io.Serializable {
    private it.netgrid.virtuemart.soap.users.LoginInfo loginInfo;

    private java.lang.String waiting_list_id;

    private java.lang.String product_id;

    private java.lang.String user_id;

    private java.lang.String notify_email;

    private java.lang.String notified;

    private java.lang.String notify_date;

    private java.lang.String limite_start;

    private java.lang.String limite_end;

    public GetWaitingListInput() {
    }

    public GetWaitingListInput(
           it.netgrid.virtuemart.soap.users.LoginInfo loginInfo,
           java.lang.String waiting_list_id,
           java.lang.String product_id,
           java.lang.String user_id,
           java.lang.String notify_email,
           java.lang.String notified,
           java.lang.String notify_date,
           java.lang.String limite_start,
           java.lang.String limite_end) {
           this.loginInfo = loginInfo;
           this.waiting_list_id = waiting_list_id;
           this.product_id = product_id;
           this.user_id = user_id;
           this.notify_email = notify_email;
           this.notified = notified;
           this.notify_date = notify_date;
           this.limite_start = limite_start;
           this.limite_end = limite_end;
    }


    /**
     * Gets the loginInfo value for this GetWaitingListInput.
     * 
     * @return loginInfo
     */
    public it.netgrid.virtuemart.soap.users.LoginInfo getLoginInfo() {
        return loginInfo;
    }


    /**
     * Sets the loginInfo value for this GetWaitingListInput.
     * 
     * @param loginInfo
     */
    public void setLoginInfo(it.netgrid.virtuemart.soap.users.LoginInfo loginInfo) {
        this.loginInfo = loginInfo;
    }


    /**
     * Gets the waiting_list_id value for this GetWaitingListInput.
     * 
     * @return waiting_list_id
     */
    public java.lang.String getWaiting_list_id() {
        return waiting_list_id;
    }


    /**
     * Sets the waiting_list_id value for this GetWaitingListInput.
     * 
     * @param waiting_list_id
     */
    public void setWaiting_list_id(java.lang.String waiting_list_id) {
        this.waiting_list_id = waiting_list_id;
    }


    /**
     * Gets the product_id value for this GetWaitingListInput.
     * 
     * @return product_id
     */
    public java.lang.String getProduct_id() {
        return product_id;
    }


    /**
     * Sets the product_id value for this GetWaitingListInput.
     * 
     * @param product_id
     */
    public void setProduct_id(java.lang.String product_id) {
        this.product_id = product_id;
    }


    /**
     * Gets the user_id value for this GetWaitingListInput.
     * 
     * @return user_id
     */
    public java.lang.String getUser_id() {
        return user_id;
    }


    /**
     * Sets the user_id value for this GetWaitingListInput.
     * 
     * @param user_id
     */
    public void setUser_id(java.lang.String user_id) {
        this.user_id = user_id;
    }


    /**
     * Gets the notify_email value for this GetWaitingListInput.
     * 
     * @return notify_email
     */
    public java.lang.String getNotify_email() {
        return notify_email;
    }


    /**
     * Sets the notify_email value for this GetWaitingListInput.
     * 
     * @param notify_email
     */
    public void setNotify_email(java.lang.String notify_email) {
        this.notify_email = notify_email;
    }


    /**
     * Gets the notified value for this GetWaitingListInput.
     * 
     * @return notified
     */
    public java.lang.String getNotified() {
        return notified;
    }


    /**
     * Sets the notified value for this GetWaitingListInput.
     * 
     * @param notified
     */
    public void setNotified(java.lang.String notified) {
        this.notified = notified;
    }


    /**
     * Gets the notify_date value for this GetWaitingListInput.
     * 
     * @return notify_date
     */
    public java.lang.String getNotify_date() {
        return notify_date;
    }


    /**
     * Sets the notify_date value for this GetWaitingListInput.
     * 
     * @param notify_date
     */
    public void setNotify_date(java.lang.String notify_date) {
        this.notify_date = notify_date;
    }


    /**
     * Gets the limite_start value for this GetWaitingListInput.
     * 
     * @return limite_start
     */
    public java.lang.String getLimite_start() {
        return limite_start;
    }


    /**
     * Sets the limite_start value for this GetWaitingListInput.
     * 
     * @param limite_start
     */
    public void setLimite_start(java.lang.String limite_start) {
        this.limite_start = limite_start;
    }


    /**
     * Gets the limite_end value for this GetWaitingListInput.
     * 
     * @return limite_end
     */
    public java.lang.String getLimite_end() {
        return limite_end;
    }


    /**
     * Sets the limite_end value for this GetWaitingListInput.
     * 
     * @param limite_end
     */
    public void setLimite_end(java.lang.String limite_end) {
        this.limite_end = limite_end;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof GetWaitingListInput)) return false;
        GetWaitingListInput other = (GetWaitingListInput) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.loginInfo==null && other.getLoginInfo()==null) || 
             (this.loginInfo!=null &&
              this.loginInfo.equals(other.getLoginInfo()))) &&
            ((this.waiting_list_id==null && other.getWaiting_list_id()==null) || 
             (this.waiting_list_id!=null &&
              this.waiting_list_id.equals(other.getWaiting_list_id()))) &&
            ((this.product_id==null && other.getProduct_id()==null) || 
             (this.product_id!=null &&
              this.product_id.equals(other.getProduct_id()))) &&
            ((this.user_id==null && other.getUser_id()==null) || 
             (this.user_id!=null &&
              this.user_id.equals(other.getUser_id()))) &&
            ((this.notify_email==null && other.getNotify_email()==null) || 
             (this.notify_email!=null &&
              this.notify_email.equals(other.getNotify_email()))) &&
            ((this.notified==null && other.getNotified()==null) || 
             (this.notified!=null &&
              this.notified.equals(other.getNotified()))) &&
            ((this.notify_date==null && other.getNotify_date()==null) || 
             (this.notify_date!=null &&
              this.notify_date.equals(other.getNotify_date()))) &&
            ((this.limite_start==null && other.getLimite_start()==null) || 
             (this.limite_start!=null &&
              this.limite_start.equals(other.getLimite_start()))) &&
            ((this.limite_end==null && other.getLimite_end()==null) || 
             (this.limite_end!=null &&
              this.limite_end.equals(other.getLimite_end())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getLoginInfo() != null) {
            _hashCode += getLoginInfo().hashCode();
        }
        if (getWaiting_list_id() != null) {
            _hashCode += getWaiting_list_id().hashCode();
        }
        if (getProduct_id() != null) {
            _hashCode += getProduct_id().hashCode();
        }
        if (getUser_id() != null) {
            _hashCode += getUser_id().hashCode();
        }
        if (getNotify_email() != null) {
            _hashCode += getNotify_email().hashCode();
        }
        if (getNotified() != null) {
            _hashCode += getNotified().hashCode();
        }
        if (getNotify_date() != null) {
            _hashCode += getNotify_date().hashCode();
        }
        if (getLimite_start() != null) {
            _hashCode += getLimite_start().hashCode();
        }
        if (getLimite_end() != null) {
            _hashCode += getLimite_end().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(GetWaitingListInput.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://www.virtuemart.net/VM_Users/", "GetWaitingListInput"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("loginInfo");
        elemField.setXmlName(new javax.xml.namespace.QName("", "loginInfo"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.virtuemart.net/VM_Users/", "loginInfo"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("waiting_list_id");
        elemField.setXmlName(new javax.xml.namespace.QName("", "waiting_list_id"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("product_id");
        elemField.setXmlName(new javax.xml.namespace.QName("", "product_id"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("user_id");
        elemField.setXmlName(new javax.xml.namespace.QName("", "user_id"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("notify_email");
        elemField.setXmlName(new javax.xml.namespace.QName("", "notify_email"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("notified");
        elemField.setXmlName(new javax.xml.namespace.QName("", "notified"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("notify_date");
        elemField.setXmlName(new javax.xml.namespace.QName("", "notify_date"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("limite_start");
        elemField.setXmlName(new javax.xml.namespace.QName("", "limite_start"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("limite_end");
        elemField.setXmlName(new javax.xml.namespace.QName("", "limite_end"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
