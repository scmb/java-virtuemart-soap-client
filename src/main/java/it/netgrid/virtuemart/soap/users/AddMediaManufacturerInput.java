/**
 * AddMediaManufacturerInput.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package it.netgrid.virtuemart.soap.users;

public class AddMediaManufacturerInput  implements java.io.Serializable {
    private it.netgrid.virtuemart.soap.users.LoginInfo loginInfo;

    private java.lang.String manufacturer_id;

    private it.netgrid.virtuemart.soap.users.Media media;

    private java.lang.String filePath;

    private java.lang.String fileThumbPath;

    public AddMediaManufacturerInput() {
    }

    public AddMediaManufacturerInput(
           it.netgrid.virtuemart.soap.users.LoginInfo loginInfo,
           java.lang.String manufacturer_id,
           it.netgrid.virtuemart.soap.users.Media media,
           java.lang.String filePath,
           java.lang.String fileThumbPath) {
           this.loginInfo = loginInfo;
           this.manufacturer_id = manufacturer_id;
           this.media = media;
           this.filePath = filePath;
           this.fileThumbPath = fileThumbPath;
    }


    /**
     * Gets the loginInfo value for this AddMediaManufacturerInput.
     * 
     * @return loginInfo
     */
    public it.netgrid.virtuemart.soap.users.LoginInfo getLoginInfo() {
        return loginInfo;
    }


    /**
     * Sets the loginInfo value for this AddMediaManufacturerInput.
     * 
     * @param loginInfo
     */
    public void setLoginInfo(it.netgrid.virtuemart.soap.users.LoginInfo loginInfo) {
        this.loginInfo = loginInfo;
    }


    /**
     * Gets the manufacturer_id value for this AddMediaManufacturerInput.
     * 
     * @return manufacturer_id
     */
    public java.lang.String getManufacturer_id() {
        return manufacturer_id;
    }


    /**
     * Sets the manufacturer_id value for this AddMediaManufacturerInput.
     * 
     * @param manufacturer_id
     */
    public void setManufacturer_id(java.lang.String manufacturer_id) {
        this.manufacturer_id = manufacturer_id;
    }


    /**
     * Gets the media value for this AddMediaManufacturerInput.
     * 
     * @return media
     */
    public it.netgrid.virtuemart.soap.users.Media getMedia() {
        return media;
    }


    /**
     * Sets the media value for this AddMediaManufacturerInput.
     * 
     * @param media
     */
    public void setMedia(it.netgrid.virtuemart.soap.users.Media media) {
        this.media = media;
    }


    /**
     * Gets the filePath value for this AddMediaManufacturerInput.
     * 
     * @return filePath
     */
    public java.lang.String getFilePath() {
        return filePath;
    }


    /**
     * Sets the filePath value for this AddMediaManufacturerInput.
     * 
     * @param filePath
     */
    public void setFilePath(java.lang.String filePath) {
        this.filePath = filePath;
    }


    /**
     * Gets the fileThumbPath value for this AddMediaManufacturerInput.
     * 
     * @return fileThumbPath
     */
    public java.lang.String getFileThumbPath() {
        return fileThumbPath;
    }


    /**
     * Sets the fileThumbPath value for this AddMediaManufacturerInput.
     * 
     * @param fileThumbPath
     */
    public void setFileThumbPath(java.lang.String fileThumbPath) {
        this.fileThumbPath = fileThumbPath;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof AddMediaManufacturerInput)) return false;
        AddMediaManufacturerInput other = (AddMediaManufacturerInput) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.loginInfo==null && other.getLoginInfo()==null) || 
             (this.loginInfo!=null &&
              this.loginInfo.equals(other.getLoginInfo()))) &&
            ((this.manufacturer_id==null && other.getManufacturer_id()==null) || 
             (this.manufacturer_id!=null &&
              this.manufacturer_id.equals(other.getManufacturer_id()))) &&
            ((this.media==null && other.getMedia()==null) || 
             (this.media!=null &&
              this.media.equals(other.getMedia()))) &&
            ((this.filePath==null && other.getFilePath()==null) || 
             (this.filePath!=null &&
              this.filePath.equals(other.getFilePath()))) &&
            ((this.fileThumbPath==null && other.getFileThumbPath()==null) || 
             (this.fileThumbPath!=null &&
              this.fileThumbPath.equals(other.getFileThumbPath())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getLoginInfo() != null) {
            _hashCode += getLoginInfo().hashCode();
        }
        if (getManufacturer_id() != null) {
            _hashCode += getManufacturer_id().hashCode();
        }
        if (getMedia() != null) {
            _hashCode += getMedia().hashCode();
        }
        if (getFilePath() != null) {
            _hashCode += getFilePath().hashCode();
        }
        if (getFileThumbPath() != null) {
            _hashCode += getFileThumbPath().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(AddMediaManufacturerInput.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://www.virtuemart.net/VM_Users/", "AddMediaManufacturerInput"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("loginInfo");
        elemField.setXmlName(new javax.xml.namespace.QName("", "loginInfo"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.virtuemart.net/VM_Users/", "loginInfo"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("manufacturer_id");
        elemField.setXmlName(new javax.xml.namespace.QName("", "manufacturer_id"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("media");
        elemField.setXmlName(new javax.xml.namespace.QName("", "media"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.virtuemart.net/VM_Users/", "Media"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("filePath");
        elemField.setXmlName(new javax.xml.namespace.QName("", "filePath"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("fileThumbPath");
        elemField.setXmlName(new javax.xml.namespace.QName("", "fileThumbPath"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
