/**
 * ShopperGroup.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package it.netgrid.virtuemart.soap.users;

public class ShopperGroup  implements java.io.Serializable {
    private java.lang.String shopper_group_id;

    private java.lang.String vendor_id;

    private java.lang.String shopper_group_name;

    private java.lang.String shopper_group_desc;

    private java.lang.String custom_price_display;

    private java.lang.String price_display;

    private java.lang.String _default;

    private java.lang.String ordering;

    private java.lang.String shared;

    private java.lang.String published;

    public ShopperGroup() {
    }

    public ShopperGroup(
           java.lang.String shopper_group_id,
           java.lang.String vendor_id,
           java.lang.String shopper_group_name,
           java.lang.String shopper_group_desc,
           java.lang.String custom_price_display,
           java.lang.String price_display,
           java.lang.String _default,
           java.lang.String ordering,
           java.lang.String shared,
           java.lang.String published) {
           this.shopper_group_id = shopper_group_id;
           this.vendor_id = vendor_id;
           this.shopper_group_name = shopper_group_name;
           this.shopper_group_desc = shopper_group_desc;
           this.custom_price_display = custom_price_display;
           this.price_display = price_display;
           this._default = _default;
           this.ordering = ordering;
           this.shared = shared;
           this.published = published;
    }


    /**
     * Gets the shopper_group_id value for this ShopperGroup.
     * 
     * @return shopper_group_id
     */
    public java.lang.String getShopper_group_id() {
        return shopper_group_id;
    }


    /**
     * Sets the shopper_group_id value for this ShopperGroup.
     * 
     * @param shopper_group_id
     */
    public void setShopper_group_id(java.lang.String shopper_group_id) {
        this.shopper_group_id = shopper_group_id;
    }


    /**
     * Gets the vendor_id value for this ShopperGroup.
     * 
     * @return vendor_id
     */
    public java.lang.String getVendor_id() {
        return vendor_id;
    }


    /**
     * Sets the vendor_id value for this ShopperGroup.
     * 
     * @param vendor_id
     */
    public void setVendor_id(java.lang.String vendor_id) {
        this.vendor_id = vendor_id;
    }


    /**
     * Gets the shopper_group_name value for this ShopperGroup.
     * 
     * @return shopper_group_name
     */
    public java.lang.String getShopper_group_name() {
        return shopper_group_name;
    }


    /**
     * Sets the shopper_group_name value for this ShopperGroup.
     * 
     * @param shopper_group_name
     */
    public void setShopper_group_name(java.lang.String shopper_group_name) {
        this.shopper_group_name = shopper_group_name;
    }


    /**
     * Gets the shopper_group_desc value for this ShopperGroup.
     * 
     * @return shopper_group_desc
     */
    public java.lang.String getShopper_group_desc() {
        return shopper_group_desc;
    }


    /**
     * Sets the shopper_group_desc value for this ShopperGroup.
     * 
     * @param shopper_group_desc
     */
    public void setShopper_group_desc(java.lang.String shopper_group_desc) {
        this.shopper_group_desc = shopper_group_desc;
    }


    /**
     * Gets the custom_price_display value for this ShopperGroup.
     * 
     * @return custom_price_display
     */
    public java.lang.String getCustom_price_display() {
        return custom_price_display;
    }


    /**
     * Sets the custom_price_display value for this ShopperGroup.
     * 
     * @param custom_price_display
     */
    public void setCustom_price_display(java.lang.String custom_price_display) {
        this.custom_price_display = custom_price_display;
    }


    /**
     * Gets the price_display value for this ShopperGroup.
     * 
     * @return price_display
     */
    public java.lang.String getPrice_display() {
        return price_display;
    }


    /**
     * Sets the price_display value for this ShopperGroup.
     * 
     * @param price_display
     */
    public void setPrice_display(java.lang.String price_display) {
        this.price_display = price_display;
    }


    /**
     * Gets the _default value for this ShopperGroup.
     * 
     * @return _default
     */
    public java.lang.String get_default() {
        return _default;
    }


    /**
     * Sets the _default value for this ShopperGroup.
     * 
     * @param _default
     */
    public void set_default(java.lang.String _default) {
        this._default = _default;
    }


    /**
     * Gets the ordering value for this ShopperGroup.
     * 
     * @return ordering
     */
    public java.lang.String getOrdering() {
        return ordering;
    }


    /**
     * Sets the ordering value for this ShopperGroup.
     * 
     * @param ordering
     */
    public void setOrdering(java.lang.String ordering) {
        this.ordering = ordering;
    }


    /**
     * Gets the shared value for this ShopperGroup.
     * 
     * @return shared
     */
    public java.lang.String getShared() {
        return shared;
    }


    /**
     * Sets the shared value for this ShopperGroup.
     * 
     * @param shared
     */
    public void setShared(java.lang.String shared) {
        this.shared = shared;
    }


    /**
     * Gets the published value for this ShopperGroup.
     * 
     * @return published
     */
    public java.lang.String getPublished() {
        return published;
    }


    /**
     * Sets the published value for this ShopperGroup.
     * 
     * @param published
     */
    public void setPublished(java.lang.String published) {
        this.published = published;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof ShopperGroup)) return false;
        ShopperGroup other = (ShopperGroup) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.shopper_group_id==null && other.getShopper_group_id()==null) || 
             (this.shopper_group_id!=null &&
              this.shopper_group_id.equals(other.getShopper_group_id()))) &&
            ((this.vendor_id==null && other.getVendor_id()==null) || 
             (this.vendor_id!=null &&
              this.vendor_id.equals(other.getVendor_id()))) &&
            ((this.shopper_group_name==null && other.getShopper_group_name()==null) || 
             (this.shopper_group_name!=null &&
              this.shopper_group_name.equals(other.getShopper_group_name()))) &&
            ((this.shopper_group_desc==null && other.getShopper_group_desc()==null) || 
             (this.shopper_group_desc!=null &&
              this.shopper_group_desc.equals(other.getShopper_group_desc()))) &&
            ((this.custom_price_display==null && other.getCustom_price_display()==null) || 
             (this.custom_price_display!=null &&
              this.custom_price_display.equals(other.getCustom_price_display()))) &&
            ((this.price_display==null && other.getPrice_display()==null) || 
             (this.price_display!=null &&
              this.price_display.equals(other.getPrice_display()))) &&
            ((this._default==null && other.get_default()==null) || 
             (this._default!=null &&
              this._default.equals(other.get_default()))) &&
            ((this.ordering==null && other.getOrdering()==null) || 
             (this.ordering!=null &&
              this.ordering.equals(other.getOrdering()))) &&
            ((this.shared==null && other.getShared()==null) || 
             (this.shared!=null &&
              this.shared.equals(other.getShared()))) &&
            ((this.published==null && other.getPublished()==null) || 
             (this.published!=null &&
              this.published.equals(other.getPublished())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getShopper_group_id() != null) {
            _hashCode += getShopper_group_id().hashCode();
        }
        if (getVendor_id() != null) {
            _hashCode += getVendor_id().hashCode();
        }
        if (getShopper_group_name() != null) {
            _hashCode += getShopper_group_name().hashCode();
        }
        if (getShopper_group_desc() != null) {
            _hashCode += getShopper_group_desc().hashCode();
        }
        if (getCustom_price_display() != null) {
            _hashCode += getCustom_price_display().hashCode();
        }
        if (getPrice_display() != null) {
            _hashCode += getPrice_display().hashCode();
        }
        if (get_default() != null) {
            _hashCode += get_default().hashCode();
        }
        if (getOrdering() != null) {
            _hashCode += getOrdering().hashCode();
        }
        if (getShared() != null) {
            _hashCode += getShared().hashCode();
        }
        if (getPublished() != null) {
            _hashCode += getPublished().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(ShopperGroup.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://www.virtuemart.net/VM_Users/", "ShopperGroup"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("shopper_group_id");
        elemField.setXmlName(new javax.xml.namespace.QName("", "shopper_group_id"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("vendor_id");
        elemField.setXmlName(new javax.xml.namespace.QName("", "vendor_id"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("shopper_group_name");
        elemField.setXmlName(new javax.xml.namespace.QName("", "shopper_group_name"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("shopper_group_desc");
        elemField.setXmlName(new javax.xml.namespace.QName("", "shopper_group_desc"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("custom_price_display");
        elemField.setXmlName(new javax.xml.namespace.QName("", "custom_price_display"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("price_display");
        elemField.setXmlName(new javax.xml.namespace.QName("", "price_display"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("_default");
        elemField.setXmlName(new javax.xml.namespace.QName("", "default"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("ordering");
        elemField.setXmlName(new javax.xml.namespace.QName("", "ordering"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("shared");
        elemField.setXmlName(new javax.xml.namespace.QName("", "shared"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("published");
        elemField.setXmlName(new javax.xml.namespace.QName("", "published"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
