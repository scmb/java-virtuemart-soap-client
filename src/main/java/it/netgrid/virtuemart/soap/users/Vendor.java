/**
 * Vendor.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package it.netgrid.virtuemart.soap.users;

public class Vendor  implements java.io.Serializable {
    private java.lang.String vendor_id;

    private java.lang.String vendor_name;

    private java.lang.String vendor_phone;

    private java.lang.String vendor_store_name;

    private java.lang.String vendor_store_desc;

    private java.lang.String vendor_currency;

    private java.lang.String vendor_image_path;

    private java.lang.String vendor_terms_of_service;

    private java.lang.String vendor_url;

    private java.lang.String slug;

    private java.lang.String vendor_freeshipping;

    private java.lang.String vendor_accepted_currencies;

    private java.lang.String vendor_address_format;

    private java.lang.String vendor_date_format;

    private java.lang.String vendor_params;

    private java.lang.String img_uri;

    private java.lang.String img_thumb_uri;

    private it.netgrid.virtuemart.soap.users.User userInfo;

    public Vendor() {
    }

    public Vendor(
           java.lang.String vendor_id,
           java.lang.String vendor_name,
           java.lang.String vendor_phone,
           java.lang.String vendor_store_name,
           java.lang.String vendor_store_desc,
           java.lang.String vendor_currency,
           java.lang.String vendor_image_path,
           java.lang.String vendor_terms_of_service,
           java.lang.String vendor_url,
           java.lang.String slug,
           java.lang.String vendor_freeshipping,
           java.lang.String vendor_accepted_currencies,
           java.lang.String vendor_address_format,
           java.lang.String vendor_date_format,
           java.lang.String vendor_params,
           java.lang.String img_uri,
           java.lang.String img_thumb_uri,
           it.netgrid.virtuemart.soap.users.User userInfo) {
           this.vendor_id = vendor_id;
           this.vendor_name = vendor_name;
           this.vendor_phone = vendor_phone;
           this.vendor_store_name = vendor_store_name;
           this.vendor_store_desc = vendor_store_desc;
           this.vendor_currency = vendor_currency;
           this.vendor_image_path = vendor_image_path;
           this.vendor_terms_of_service = vendor_terms_of_service;
           this.vendor_url = vendor_url;
           this.slug = slug;
           this.vendor_freeshipping = vendor_freeshipping;
           this.vendor_accepted_currencies = vendor_accepted_currencies;
           this.vendor_address_format = vendor_address_format;
           this.vendor_date_format = vendor_date_format;
           this.vendor_params = vendor_params;
           this.img_uri = img_uri;
           this.img_thumb_uri = img_thumb_uri;
           this.userInfo = userInfo;
    }


    /**
     * Gets the vendor_id value for this Vendor.
     * 
     * @return vendor_id
     */
    public java.lang.String getVendor_id() {
        return vendor_id;
    }


    /**
     * Sets the vendor_id value for this Vendor.
     * 
     * @param vendor_id
     */
    public void setVendor_id(java.lang.String vendor_id) {
        this.vendor_id = vendor_id;
    }


    /**
     * Gets the vendor_name value for this Vendor.
     * 
     * @return vendor_name
     */
    public java.lang.String getVendor_name() {
        return vendor_name;
    }


    /**
     * Sets the vendor_name value for this Vendor.
     * 
     * @param vendor_name
     */
    public void setVendor_name(java.lang.String vendor_name) {
        this.vendor_name = vendor_name;
    }


    /**
     * Gets the vendor_phone value for this Vendor.
     * 
     * @return vendor_phone
     */
    public java.lang.String getVendor_phone() {
        return vendor_phone;
    }


    /**
     * Sets the vendor_phone value for this Vendor.
     * 
     * @param vendor_phone
     */
    public void setVendor_phone(java.lang.String vendor_phone) {
        this.vendor_phone = vendor_phone;
    }


    /**
     * Gets the vendor_store_name value for this Vendor.
     * 
     * @return vendor_store_name
     */
    public java.lang.String getVendor_store_name() {
        return vendor_store_name;
    }


    /**
     * Sets the vendor_store_name value for this Vendor.
     * 
     * @param vendor_store_name
     */
    public void setVendor_store_name(java.lang.String vendor_store_name) {
        this.vendor_store_name = vendor_store_name;
    }


    /**
     * Gets the vendor_store_desc value for this Vendor.
     * 
     * @return vendor_store_desc
     */
    public java.lang.String getVendor_store_desc() {
        return vendor_store_desc;
    }


    /**
     * Sets the vendor_store_desc value for this Vendor.
     * 
     * @param vendor_store_desc
     */
    public void setVendor_store_desc(java.lang.String vendor_store_desc) {
        this.vendor_store_desc = vendor_store_desc;
    }


    /**
     * Gets the vendor_currency value for this Vendor.
     * 
     * @return vendor_currency
     */
    public java.lang.String getVendor_currency() {
        return vendor_currency;
    }


    /**
     * Sets the vendor_currency value for this Vendor.
     * 
     * @param vendor_currency
     */
    public void setVendor_currency(java.lang.String vendor_currency) {
        this.vendor_currency = vendor_currency;
    }


    /**
     * Gets the vendor_image_path value for this Vendor.
     * 
     * @return vendor_image_path
     */
    public java.lang.String getVendor_image_path() {
        return vendor_image_path;
    }


    /**
     * Sets the vendor_image_path value for this Vendor.
     * 
     * @param vendor_image_path
     */
    public void setVendor_image_path(java.lang.String vendor_image_path) {
        this.vendor_image_path = vendor_image_path;
    }


    /**
     * Gets the vendor_terms_of_service value for this Vendor.
     * 
     * @return vendor_terms_of_service
     */
    public java.lang.String getVendor_terms_of_service() {
        return vendor_terms_of_service;
    }


    /**
     * Sets the vendor_terms_of_service value for this Vendor.
     * 
     * @param vendor_terms_of_service
     */
    public void setVendor_terms_of_service(java.lang.String vendor_terms_of_service) {
        this.vendor_terms_of_service = vendor_terms_of_service;
    }


    /**
     * Gets the vendor_url value for this Vendor.
     * 
     * @return vendor_url
     */
    public java.lang.String getVendor_url() {
        return vendor_url;
    }


    /**
     * Sets the vendor_url value for this Vendor.
     * 
     * @param vendor_url
     */
    public void setVendor_url(java.lang.String vendor_url) {
        this.vendor_url = vendor_url;
    }


    /**
     * Gets the slug value for this Vendor.
     * 
     * @return slug
     */
    public java.lang.String getSlug() {
        return slug;
    }


    /**
     * Sets the slug value for this Vendor.
     * 
     * @param slug
     */
    public void setSlug(java.lang.String slug) {
        this.slug = slug;
    }


    /**
     * Gets the vendor_freeshipping value for this Vendor.
     * 
     * @return vendor_freeshipping
     */
    public java.lang.String getVendor_freeshipping() {
        return vendor_freeshipping;
    }


    /**
     * Sets the vendor_freeshipping value for this Vendor.
     * 
     * @param vendor_freeshipping
     */
    public void setVendor_freeshipping(java.lang.String vendor_freeshipping) {
        this.vendor_freeshipping = vendor_freeshipping;
    }


    /**
     * Gets the vendor_accepted_currencies value for this Vendor.
     * 
     * @return vendor_accepted_currencies
     */
    public java.lang.String getVendor_accepted_currencies() {
        return vendor_accepted_currencies;
    }


    /**
     * Sets the vendor_accepted_currencies value for this Vendor.
     * 
     * @param vendor_accepted_currencies
     */
    public void setVendor_accepted_currencies(java.lang.String vendor_accepted_currencies) {
        this.vendor_accepted_currencies = vendor_accepted_currencies;
    }


    /**
     * Gets the vendor_address_format value for this Vendor.
     * 
     * @return vendor_address_format
     */
    public java.lang.String getVendor_address_format() {
        return vendor_address_format;
    }


    /**
     * Sets the vendor_address_format value for this Vendor.
     * 
     * @param vendor_address_format
     */
    public void setVendor_address_format(java.lang.String vendor_address_format) {
        this.vendor_address_format = vendor_address_format;
    }


    /**
     * Gets the vendor_date_format value for this Vendor.
     * 
     * @return vendor_date_format
     */
    public java.lang.String getVendor_date_format() {
        return vendor_date_format;
    }


    /**
     * Sets the vendor_date_format value for this Vendor.
     * 
     * @param vendor_date_format
     */
    public void setVendor_date_format(java.lang.String vendor_date_format) {
        this.vendor_date_format = vendor_date_format;
    }


    /**
     * Gets the vendor_params value for this Vendor.
     * 
     * @return vendor_params
     */
    public java.lang.String getVendor_params() {
        return vendor_params;
    }


    /**
     * Sets the vendor_params value for this Vendor.
     * 
     * @param vendor_params
     */
    public void setVendor_params(java.lang.String vendor_params) {
        this.vendor_params = vendor_params;
    }


    /**
     * Gets the img_uri value for this Vendor.
     * 
     * @return img_uri
     */
    public java.lang.String getImg_uri() {
        return img_uri;
    }


    /**
     * Sets the img_uri value for this Vendor.
     * 
     * @param img_uri
     */
    public void setImg_uri(java.lang.String img_uri) {
        this.img_uri = img_uri;
    }


    /**
     * Gets the img_thumb_uri value for this Vendor.
     * 
     * @return img_thumb_uri
     */
    public java.lang.String getImg_thumb_uri() {
        return img_thumb_uri;
    }


    /**
     * Sets the img_thumb_uri value for this Vendor.
     * 
     * @param img_thumb_uri
     */
    public void setImg_thumb_uri(java.lang.String img_thumb_uri) {
        this.img_thumb_uri = img_thumb_uri;
    }


    /**
     * Gets the userInfo value for this Vendor.
     * 
     * @return userInfo
     */
    public it.netgrid.virtuemart.soap.users.User getUserInfo() {
        return userInfo;
    }


    /**
     * Sets the userInfo value for this Vendor.
     * 
     * @param userInfo
     */
    public void setUserInfo(it.netgrid.virtuemart.soap.users.User userInfo) {
        this.userInfo = userInfo;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof Vendor)) return false;
        Vendor other = (Vendor) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.vendor_id==null && other.getVendor_id()==null) || 
             (this.vendor_id!=null &&
              this.vendor_id.equals(other.getVendor_id()))) &&
            ((this.vendor_name==null && other.getVendor_name()==null) || 
             (this.vendor_name!=null &&
              this.vendor_name.equals(other.getVendor_name()))) &&
            ((this.vendor_phone==null && other.getVendor_phone()==null) || 
             (this.vendor_phone!=null &&
              this.vendor_phone.equals(other.getVendor_phone()))) &&
            ((this.vendor_store_name==null && other.getVendor_store_name()==null) || 
             (this.vendor_store_name!=null &&
              this.vendor_store_name.equals(other.getVendor_store_name()))) &&
            ((this.vendor_store_desc==null && other.getVendor_store_desc()==null) || 
             (this.vendor_store_desc!=null &&
              this.vendor_store_desc.equals(other.getVendor_store_desc()))) &&
            ((this.vendor_currency==null && other.getVendor_currency()==null) || 
             (this.vendor_currency!=null &&
              this.vendor_currency.equals(other.getVendor_currency()))) &&
            ((this.vendor_image_path==null && other.getVendor_image_path()==null) || 
             (this.vendor_image_path!=null &&
              this.vendor_image_path.equals(other.getVendor_image_path()))) &&
            ((this.vendor_terms_of_service==null && other.getVendor_terms_of_service()==null) || 
             (this.vendor_terms_of_service!=null &&
              this.vendor_terms_of_service.equals(other.getVendor_terms_of_service()))) &&
            ((this.vendor_url==null && other.getVendor_url()==null) || 
             (this.vendor_url!=null &&
              this.vendor_url.equals(other.getVendor_url()))) &&
            ((this.slug==null && other.getSlug()==null) || 
             (this.slug!=null &&
              this.slug.equals(other.getSlug()))) &&
            ((this.vendor_freeshipping==null && other.getVendor_freeshipping()==null) || 
             (this.vendor_freeshipping!=null &&
              this.vendor_freeshipping.equals(other.getVendor_freeshipping()))) &&
            ((this.vendor_accepted_currencies==null && other.getVendor_accepted_currencies()==null) || 
             (this.vendor_accepted_currencies!=null &&
              this.vendor_accepted_currencies.equals(other.getVendor_accepted_currencies()))) &&
            ((this.vendor_address_format==null && other.getVendor_address_format()==null) || 
             (this.vendor_address_format!=null &&
              this.vendor_address_format.equals(other.getVendor_address_format()))) &&
            ((this.vendor_date_format==null && other.getVendor_date_format()==null) || 
             (this.vendor_date_format!=null &&
              this.vendor_date_format.equals(other.getVendor_date_format()))) &&
            ((this.vendor_params==null && other.getVendor_params()==null) || 
             (this.vendor_params!=null &&
              this.vendor_params.equals(other.getVendor_params()))) &&
            ((this.img_uri==null && other.getImg_uri()==null) || 
             (this.img_uri!=null &&
              this.img_uri.equals(other.getImg_uri()))) &&
            ((this.img_thumb_uri==null && other.getImg_thumb_uri()==null) || 
             (this.img_thumb_uri!=null &&
              this.img_thumb_uri.equals(other.getImg_thumb_uri()))) &&
            ((this.userInfo==null && other.getUserInfo()==null) || 
             (this.userInfo!=null &&
              this.userInfo.equals(other.getUserInfo())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getVendor_id() != null) {
            _hashCode += getVendor_id().hashCode();
        }
        if (getVendor_name() != null) {
            _hashCode += getVendor_name().hashCode();
        }
        if (getVendor_phone() != null) {
            _hashCode += getVendor_phone().hashCode();
        }
        if (getVendor_store_name() != null) {
            _hashCode += getVendor_store_name().hashCode();
        }
        if (getVendor_store_desc() != null) {
            _hashCode += getVendor_store_desc().hashCode();
        }
        if (getVendor_currency() != null) {
            _hashCode += getVendor_currency().hashCode();
        }
        if (getVendor_image_path() != null) {
            _hashCode += getVendor_image_path().hashCode();
        }
        if (getVendor_terms_of_service() != null) {
            _hashCode += getVendor_terms_of_service().hashCode();
        }
        if (getVendor_url() != null) {
            _hashCode += getVendor_url().hashCode();
        }
        if (getSlug() != null) {
            _hashCode += getSlug().hashCode();
        }
        if (getVendor_freeshipping() != null) {
            _hashCode += getVendor_freeshipping().hashCode();
        }
        if (getVendor_accepted_currencies() != null) {
            _hashCode += getVendor_accepted_currencies().hashCode();
        }
        if (getVendor_address_format() != null) {
            _hashCode += getVendor_address_format().hashCode();
        }
        if (getVendor_date_format() != null) {
            _hashCode += getVendor_date_format().hashCode();
        }
        if (getVendor_params() != null) {
            _hashCode += getVendor_params().hashCode();
        }
        if (getImg_uri() != null) {
            _hashCode += getImg_uri().hashCode();
        }
        if (getImg_thumb_uri() != null) {
            _hashCode += getImg_thumb_uri().hashCode();
        }
        if (getUserInfo() != null) {
            _hashCode += getUserInfo().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(Vendor.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://www.virtuemart.net/VM_Users/", "Vendor"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("vendor_id");
        elemField.setXmlName(new javax.xml.namespace.QName("", "vendor_id"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("vendor_name");
        elemField.setXmlName(new javax.xml.namespace.QName("", "vendor_name"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("vendor_phone");
        elemField.setXmlName(new javax.xml.namespace.QName("", "vendor_phone"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("vendor_store_name");
        elemField.setXmlName(new javax.xml.namespace.QName("", "vendor_store_name"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("vendor_store_desc");
        elemField.setXmlName(new javax.xml.namespace.QName("", "vendor_store_desc"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("vendor_currency");
        elemField.setXmlName(new javax.xml.namespace.QName("", "vendor_currency"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("vendor_image_path");
        elemField.setXmlName(new javax.xml.namespace.QName("", "vendor_image_path"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("vendor_terms_of_service");
        elemField.setXmlName(new javax.xml.namespace.QName("", "vendor_terms_of_service"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("vendor_url");
        elemField.setXmlName(new javax.xml.namespace.QName("", "vendor_url"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("slug");
        elemField.setXmlName(new javax.xml.namespace.QName("", "slug"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("vendor_freeshipping");
        elemField.setXmlName(new javax.xml.namespace.QName("", "vendor_freeshipping"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("vendor_accepted_currencies");
        elemField.setXmlName(new javax.xml.namespace.QName("", "vendor_accepted_currencies"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("vendor_address_format");
        elemField.setXmlName(new javax.xml.namespace.QName("", "vendor_address_format"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("vendor_date_format");
        elemField.setXmlName(new javax.xml.namespace.QName("", "vendor_date_format"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("vendor_params");
        elemField.setXmlName(new javax.xml.namespace.QName("", "vendor_params"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("img_uri");
        elemField.setXmlName(new javax.xml.namespace.QName("", "img_uri"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("img_thumb_uri");
        elemField.setXmlName(new javax.xml.namespace.QName("", "img_thumb_uri"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("userInfo");
        elemField.setXmlName(new javax.xml.namespace.QName("", "userInfo"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.virtuemart.net/VM_Users/", "User"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
