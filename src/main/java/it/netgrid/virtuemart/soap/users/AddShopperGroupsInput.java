/**
 * AddShopperGroupsInput.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package it.netgrid.virtuemart.soap.users;

public class AddShopperGroupsInput  implements java.io.Serializable {
    private it.netgrid.virtuemart.soap.users.LoginInfo loginInfo;

    private it.netgrid.virtuemart.soap.users.ShopperGroup[] shoppergroups;

    public AddShopperGroupsInput() {
    }

    public AddShopperGroupsInput(
           it.netgrid.virtuemart.soap.users.LoginInfo loginInfo,
           it.netgrid.virtuemart.soap.users.ShopperGroup[] shoppergroups) {
           this.loginInfo = loginInfo;
           this.shoppergroups = shoppergroups;
    }


    /**
     * Gets the loginInfo value for this AddShopperGroupsInput.
     * 
     * @return loginInfo
     */
    public it.netgrid.virtuemart.soap.users.LoginInfo getLoginInfo() {
        return loginInfo;
    }


    /**
     * Sets the loginInfo value for this AddShopperGroupsInput.
     * 
     * @param loginInfo
     */
    public void setLoginInfo(it.netgrid.virtuemart.soap.users.LoginInfo loginInfo) {
        this.loginInfo = loginInfo;
    }


    /**
     * Gets the shoppergroups value for this AddShopperGroupsInput.
     * 
     * @return shoppergroups
     */
    public it.netgrid.virtuemart.soap.users.ShopperGroup[] getShoppergroups() {
        return shoppergroups;
    }


    /**
     * Sets the shoppergroups value for this AddShopperGroupsInput.
     * 
     * @param shoppergroups
     */
    public void setShoppergroups(it.netgrid.virtuemart.soap.users.ShopperGroup[] shoppergroups) {
        this.shoppergroups = shoppergroups;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof AddShopperGroupsInput)) return false;
        AddShopperGroupsInput other = (AddShopperGroupsInput) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.loginInfo==null && other.getLoginInfo()==null) || 
             (this.loginInfo!=null &&
              this.loginInfo.equals(other.getLoginInfo()))) &&
            ((this.shoppergroups==null && other.getShoppergroups()==null) || 
             (this.shoppergroups!=null &&
              java.util.Arrays.equals(this.shoppergroups, other.getShoppergroups())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getLoginInfo() != null) {
            _hashCode += getLoginInfo().hashCode();
        }
        if (getShoppergroups() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getShoppergroups());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getShoppergroups(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(AddShopperGroupsInput.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://www.virtuemart.net/VM_Users/", "AddShopperGroupsInput"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("loginInfo");
        elemField.setXmlName(new javax.xml.namespace.QName("", "loginInfo"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.virtuemart.net/VM_Users/", "loginInfo"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("shoppergroups");
        elemField.setXmlName(new javax.xml.namespace.QName("", "shoppergroups"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.virtuemart.net/VM_Users/", "ShopperGroup"));
        elemField.setNillable(true);
        elemField.setItemQName(new javax.xml.namespace.QName("", "shoppergroup"));
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
