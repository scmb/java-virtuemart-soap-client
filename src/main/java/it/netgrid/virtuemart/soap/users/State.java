/**
 * State.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package it.netgrid.virtuemart.soap.users;

public class State  implements java.io.Serializable {
    private java.lang.String state_id;

    private java.lang.String virtuemart_vendor_id;

    private java.lang.String virtuemart_country_id;

    private java.lang.String virtuemart_worldzone_id;

    private java.lang.String state_name;

    private java.lang.String state_3_code;

    private java.lang.String state_2_code;

    private java.lang.String published;

    public State() {
    }

    public State(
           java.lang.String state_id,
           java.lang.String virtuemart_vendor_id,
           java.lang.String virtuemart_country_id,
           java.lang.String virtuemart_worldzone_id,
           java.lang.String state_name,
           java.lang.String state_3_code,
           java.lang.String state_2_code,
           java.lang.String published) {
           this.state_id = state_id;
           this.virtuemart_vendor_id = virtuemart_vendor_id;
           this.virtuemart_country_id = virtuemart_country_id;
           this.virtuemart_worldzone_id = virtuemart_worldzone_id;
           this.state_name = state_name;
           this.state_3_code = state_3_code;
           this.state_2_code = state_2_code;
           this.published = published;
    }


    /**
     * Gets the state_id value for this State.
     * 
     * @return state_id
     */
    public java.lang.String getState_id() {
        return state_id;
    }


    /**
     * Sets the state_id value for this State.
     * 
     * @param state_id
     */
    public void setState_id(java.lang.String state_id) {
        this.state_id = state_id;
    }


    /**
     * Gets the virtuemart_vendor_id value for this State.
     * 
     * @return virtuemart_vendor_id
     */
    public java.lang.String getVirtuemart_vendor_id() {
        return virtuemart_vendor_id;
    }


    /**
     * Sets the virtuemart_vendor_id value for this State.
     * 
     * @param virtuemart_vendor_id
     */
    public void setVirtuemart_vendor_id(java.lang.String virtuemart_vendor_id) {
        this.virtuemart_vendor_id = virtuemart_vendor_id;
    }


    /**
     * Gets the virtuemart_country_id value for this State.
     * 
     * @return virtuemart_country_id
     */
    public java.lang.String getVirtuemart_country_id() {
        return virtuemart_country_id;
    }


    /**
     * Sets the virtuemart_country_id value for this State.
     * 
     * @param virtuemart_country_id
     */
    public void setVirtuemart_country_id(java.lang.String virtuemart_country_id) {
        this.virtuemart_country_id = virtuemart_country_id;
    }


    /**
     * Gets the virtuemart_worldzone_id value for this State.
     * 
     * @return virtuemart_worldzone_id
     */
    public java.lang.String getVirtuemart_worldzone_id() {
        return virtuemart_worldzone_id;
    }


    /**
     * Sets the virtuemart_worldzone_id value for this State.
     * 
     * @param virtuemart_worldzone_id
     */
    public void setVirtuemart_worldzone_id(java.lang.String virtuemart_worldzone_id) {
        this.virtuemart_worldzone_id = virtuemart_worldzone_id;
    }


    /**
     * Gets the state_name value for this State.
     * 
     * @return state_name
     */
    public java.lang.String getState_name() {
        return state_name;
    }


    /**
     * Sets the state_name value for this State.
     * 
     * @param state_name
     */
    public void setState_name(java.lang.String state_name) {
        this.state_name = state_name;
    }


    /**
     * Gets the state_3_code value for this State.
     * 
     * @return state_3_code
     */
    public java.lang.String getState_3_code() {
        return state_3_code;
    }


    /**
     * Sets the state_3_code value for this State.
     * 
     * @param state_3_code
     */
    public void setState_3_code(java.lang.String state_3_code) {
        this.state_3_code = state_3_code;
    }


    /**
     * Gets the state_2_code value for this State.
     * 
     * @return state_2_code
     */
    public java.lang.String getState_2_code() {
        return state_2_code;
    }


    /**
     * Sets the state_2_code value for this State.
     * 
     * @param state_2_code
     */
    public void setState_2_code(java.lang.String state_2_code) {
        this.state_2_code = state_2_code;
    }


    /**
     * Gets the published value for this State.
     * 
     * @return published
     */
    public java.lang.String getPublished() {
        return published;
    }


    /**
     * Sets the published value for this State.
     * 
     * @param published
     */
    public void setPublished(java.lang.String published) {
        this.published = published;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof State)) return false;
        State other = (State) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.state_id==null && other.getState_id()==null) || 
             (this.state_id!=null &&
              this.state_id.equals(other.getState_id()))) &&
            ((this.virtuemart_vendor_id==null && other.getVirtuemart_vendor_id()==null) || 
             (this.virtuemart_vendor_id!=null &&
              this.virtuemart_vendor_id.equals(other.getVirtuemart_vendor_id()))) &&
            ((this.virtuemart_country_id==null && other.getVirtuemart_country_id()==null) || 
             (this.virtuemart_country_id!=null &&
              this.virtuemart_country_id.equals(other.getVirtuemart_country_id()))) &&
            ((this.virtuemart_worldzone_id==null && other.getVirtuemart_worldzone_id()==null) || 
             (this.virtuemart_worldzone_id!=null &&
              this.virtuemart_worldzone_id.equals(other.getVirtuemart_worldzone_id()))) &&
            ((this.state_name==null && other.getState_name()==null) || 
             (this.state_name!=null &&
              this.state_name.equals(other.getState_name()))) &&
            ((this.state_3_code==null && other.getState_3_code()==null) || 
             (this.state_3_code!=null &&
              this.state_3_code.equals(other.getState_3_code()))) &&
            ((this.state_2_code==null && other.getState_2_code()==null) || 
             (this.state_2_code!=null &&
              this.state_2_code.equals(other.getState_2_code()))) &&
            ((this.published==null && other.getPublished()==null) || 
             (this.published!=null &&
              this.published.equals(other.getPublished())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getState_id() != null) {
            _hashCode += getState_id().hashCode();
        }
        if (getVirtuemart_vendor_id() != null) {
            _hashCode += getVirtuemart_vendor_id().hashCode();
        }
        if (getVirtuemart_country_id() != null) {
            _hashCode += getVirtuemart_country_id().hashCode();
        }
        if (getVirtuemart_worldzone_id() != null) {
            _hashCode += getVirtuemart_worldzone_id().hashCode();
        }
        if (getState_name() != null) {
            _hashCode += getState_name().hashCode();
        }
        if (getState_3_code() != null) {
            _hashCode += getState_3_code().hashCode();
        }
        if (getState_2_code() != null) {
            _hashCode += getState_2_code().hashCode();
        }
        if (getPublished() != null) {
            _hashCode += getPublished().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(State.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://www.virtuemart.net/VM_Users/", "State"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("state_id");
        elemField.setXmlName(new javax.xml.namespace.QName("", "state_id"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("virtuemart_vendor_id");
        elemField.setXmlName(new javax.xml.namespace.QName("", "virtuemart_vendor_id"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("virtuemart_country_id");
        elemField.setXmlName(new javax.xml.namespace.QName("", "virtuemart_country_id"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("virtuemart_worldzone_id");
        elemField.setXmlName(new javax.xml.namespace.QName("", "virtuemart_worldzone_id"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("state_name");
        elemField.setXmlName(new javax.xml.namespace.QName("", "state_name"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("state_3_code");
        elemField.setXmlName(new javax.xml.namespace.QName("", "state_3_code"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("state_2_code");
        elemField.setXmlName(new javax.xml.namespace.QName("", "state_2_code"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("published");
        elemField.setXmlName(new javax.xml.namespace.QName("", "published"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
