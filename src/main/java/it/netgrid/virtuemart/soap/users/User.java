/**
 * User.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package it.netgrid.virtuemart.soap.users;

public class User  implements java.io.Serializable {
    private java.lang.String user_id;

    private java.lang.String email;

    private java.lang.String username;

    private java.lang.String password;

    private java.lang.String userinfo_id;

    private java.lang.String address_type;

    private java.lang.String address_type_name;

    private java.lang.String name;

    private java.lang.String company;

    private java.lang.String title;

    private java.lang.String last_name;

    private java.lang.String first_name;

    private java.lang.String middle_name;

    private java.lang.String phone_1;

    private java.lang.String phone_2;

    private java.lang.String fax;

    private java.lang.String address_1;

    private java.lang.String address_2;

    private java.lang.String city;

    private java.lang.String virtuemart_state_id;

    private java.lang.String virtuemart_country_id;

    private java.lang.String zip;

    private java.lang.String extra_field_1;

    private java.lang.String extra_field_2;

    private java.lang.String extra_field_3;

    private java.lang.String extra_field_4;

    private java.lang.String extra_field_5;

    private java.lang.String created_on;

    private java.lang.String modified_on;

    private java.lang.String user_is_vendor;

    private java.lang.String customer_number;

    private java.lang.String perms;

    private java.lang.String virtuemart_paymentmethod_id;

    private java.lang.String virtuemart_shippingcarrier_id;

    private java.lang.String agreed;

    private java.lang.String shoppergroup_id;

    private java.lang.String extra_fields_data;

    public User() {
    }

    public User(
           java.lang.String user_id,
           java.lang.String email,
           java.lang.String username,
           java.lang.String password,
           java.lang.String userinfo_id,
           java.lang.String address_type,
           java.lang.String address_type_name,
           java.lang.String name,
           java.lang.String company,
           java.lang.String title,
           java.lang.String last_name,
           java.lang.String first_name,
           java.lang.String middle_name,
           java.lang.String phone_1,
           java.lang.String phone_2,
           java.lang.String fax,
           java.lang.String address_1,
           java.lang.String address_2,
           java.lang.String city,
           java.lang.String virtuemart_state_id,
           java.lang.String virtuemart_country_id,
           java.lang.String zip,
           java.lang.String extra_field_1,
           java.lang.String extra_field_2,
           java.lang.String extra_field_3,
           java.lang.String extra_field_4,
           java.lang.String extra_field_5,
           java.lang.String created_on,
           java.lang.String modified_on,
           java.lang.String user_is_vendor,
           java.lang.String customer_number,
           java.lang.String perms,
           java.lang.String virtuemart_paymentmethod_id,
           java.lang.String virtuemart_shippingcarrier_id,
           java.lang.String agreed,
           java.lang.String shoppergroup_id,
           java.lang.String extra_fields_data) {
           this.user_id = user_id;
           this.email = email;
           this.username = username;
           this.password = password;
           this.userinfo_id = userinfo_id;
           this.address_type = address_type;
           this.address_type_name = address_type_name;
           this.name = name;
           this.company = company;
           this.title = title;
           this.last_name = last_name;
           this.first_name = first_name;
           this.middle_name = middle_name;
           this.phone_1 = phone_1;
           this.phone_2 = phone_2;
           this.fax = fax;
           this.address_1 = address_1;
           this.address_2 = address_2;
           this.city = city;
           this.virtuemart_state_id = virtuemart_state_id;
           this.virtuemart_country_id = virtuemart_country_id;
           this.zip = zip;
           this.extra_field_1 = extra_field_1;
           this.extra_field_2 = extra_field_2;
           this.extra_field_3 = extra_field_3;
           this.extra_field_4 = extra_field_4;
           this.extra_field_5 = extra_field_5;
           this.created_on = created_on;
           this.modified_on = modified_on;
           this.user_is_vendor = user_is_vendor;
           this.customer_number = customer_number;
           this.perms = perms;
           this.virtuemart_paymentmethod_id = virtuemart_paymentmethod_id;
           this.virtuemart_shippingcarrier_id = virtuemart_shippingcarrier_id;
           this.agreed = agreed;
           this.shoppergroup_id = shoppergroup_id;
           this.extra_fields_data = extra_fields_data;
    }


    /**
     * Gets the user_id value for this User.
     * 
     * @return user_id
     */
    public java.lang.String getUser_id() {
        return user_id;
    }


    /**
     * Sets the user_id value for this User.
     * 
     * @param user_id
     */
    public void setUser_id(java.lang.String user_id) {
        this.user_id = user_id;
    }


    /**
     * Gets the email value for this User.
     * 
     * @return email
     */
    public java.lang.String getEmail() {
        return email;
    }


    /**
     * Sets the email value for this User.
     * 
     * @param email
     */
    public void setEmail(java.lang.String email) {
        this.email = email;
    }


    /**
     * Gets the username value for this User.
     * 
     * @return username
     */
    public java.lang.String getUsername() {
        return username;
    }


    /**
     * Sets the username value for this User.
     * 
     * @param username
     */
    public void setUsername(java.lang.String username) {
        this.username = username;
    }


    /**
     * Gets the password value for this User.
     * 
     * @return password
     */
    public java.lang.String getPassword() {
        return password;
    }


    /**
     * Sets the password value for this User.
     * 
     * @param password
     */
    public void setPassword(java.lang.String password) {
        this.password = password;
    }


    /**
     * Gets the userinfo_id value for this User.
     * 
     * @return userinfo_id
     */
    public java.lang.String getUserinfo_id() {
        return userinfo_id;
    }


    /**
     * Sets the userinfo_id value for this User.
     * 
     * @param userinfo_id
     */
    public void setUserinfo_id(java.lang.String userinfo_id) {
        this.userinfo_id = userinfo_id;
    }


    /**
     * Gets the address_type value for this User.
     * 
     * @return address_type
     */
    public java.lang.String getAddress_type() {
        return address_type;
    }


    /**
     * Sets the address_type value for this User.
     * 
     * @param address_type
     */
    public void setAddress_type(java.lang.String address_type) {
        this.address_type = address_type;
    }


    /**
     * Gets the address_type_name value for this User.
     * 
     * @return address_type_name
     */
    public java.lang.String getAddress_type_name() {
        return address_type_name;
    }


    /**
     * Sets the address_type_name value for this User.
     * 
     * @param address_type_name
     */
    public void setAddress_type_name(java.lang.String address_type_name) {
        this.address_type_name = address_type_name;
    }


    /**
     * Gets the name value for this User.
     * 
     * @return name
     */
    public java.lang.String getName() {
        return name;
    }


    /**
     * Sets the name value for this User.
     * 
     * @param name
     */
    public void setName(java.lang.String name) {
        this.name = name;
    }


    /**
     * Gets the company value for this User.
     * 
     * @return company
     */
    public java.lang.String getCompany() {
        return company;
    }


    /**
     * Sets the company value for this User.
     * 
     * @param company
     */
    public void setCompany(java.lang.String company) {
        this.company = company;
    }


    /**
     * Gets the title value for this User.
     * 
     * @return title
     */
    public java.lang.String getTitle() {
        return title;
    }


    /**
     * Sets the title value for this User.
     * 
     * @param title
     */
    public void setTitle(java.lang.String title) {
        this.title = title;
    }


    /**
     * Gets the last_name value for this User.
     * 
     * @return last_name
     */
    public java.lang.String getLast_name() {
        return last_name;
    }


    /**
     * Sets the last_name value for this User.
     * 
     * @param last_name
     */
    public void setLast_name(java.lang.String last_name) {
        this.last_name = last_name;
    }


    /**
     * Gets the first_name value for this User.
     * 
     * @return first_name
     */
    public java.lang.String getFirst_name() {
        return first_name;
    }


    /**
     * Sets the first_name value for this User.
     * 
     * @param first_name
     */
    public void setFirst_name(java.lang.String first_name) {
        this.first_name = first_name;
    }


    /**
     * Gets the middle_name value for this User.
     * 
     * @return middle_name
     */
    public java.lang.String getMiddle_name() {
        return middle_name;
    }


    /**
     * Sets the middle_name value for this User.
     * 
     * @param middle_name
     */
    public void setMiddle_name(java.lang.String middle_name) {
        this.middle_name = middle_name;
    }


    /**
     * Gets the phone_1 value for this User.
     * 
     * @return phone_1
     */
    public java.lang.String getPhone_1() {
        return phone_1;
    }


    /**
     * Sets the phone_1 value for this User.
     * 
     * @param phone_1
     */
    public void setPhone_1(java.lang.String phone_1) {
        this.phone_1 = phone_1;
    }


    /**
     * Gets the phone_2 value for this User.
     * 
     * @return phone_2
     */
    public java.lang.String getPhone_2() {
        return phone_2;
    }


    /**
     * Sets the phone_2 value for this User.
     * 
     * @param phone_2
     */
    public void setPhone_2(java.lang.String phone_2) {
        this.phone_2 = phone_2;
    }


    /**
     * Gets the fax value for this User.
     * 
     * @return fax
     */
    public java.lang.String getFax() {
        return fax;
    }


    /**
     * Sets the fax value for this User.
     * 
     * @param fax
     */
    public void setFax(java.lang.String fax) {
        this.fax = fax;
    }


    /**
     * Gets the address_1 value for this User.
     * 
     * @return address_1
     */
    public java.lang.String getAddress_1() {
        return address_1;
    }


    /**
     * Sets the address_1 value for this User.
     * 
     * @param address_1
     */
    public void setAddress_1(java.lang.String address_1) {
        this.address_1 = address_1;
    }


    /**
     * Gets the address_2 value for this User.
     * 
     * @return address_2
     */
    public java.lang.String getAddress_2() {
        return address_2;
    }


    /**
     * Sets the address_2 value for this User.
     * 
     * @param address_2
     */
    public void setAddress_2(java.lang.String address_2) {
        this.address_2 = address_2;
    }


    /**
     * Gets the city value for this User.
     * 
     * @return city
     */
    public java.lang.String getCity() {
        return city;
    }


    /**
     * Sets the city value for this User.
     * 
     * @param city
     */
    public void setCity(java.lang.String city) {
        this.city = city;
    }


    /**
     * Gets the virtuemart_state_id value for this User.
     * 
     * @return virtuemart_state_id
     */
    public java.lang.String getVirtuemart_state_id() {
        return virtuemart_state_id;
    }


    /**
     * Sets the virtuemart_state_id value for this User.
     * 
     * @param virtuemart_state_id
     */
    public void setVirtuemart_state_id(java.lang.String virtuemart_state_id) {
        this.virtuemart_state_id = virtuemart_state_id;
    }


    /**
     * Gets the virtuemart_country_id value for this User.
     * 
     * @return virtuemart_country_id
     */
    public java.lang.String getVirtuemart_country_id() {
        return virtuemart_country_id;
    }


    /**
     * Sets the virtuemart_country_id value for this User.
     * 
     * @param virtuemart_country_id
     */
    public void setVirtuemart_country_id(java.lang.String virtuemart_country_id) {
        this.virtuemart_country_id = virtuemart_country_id;
    }


    /**
     * Gets the zip value for this User.
     * 
     * @return zip
     */
    public java.lang.String getZip() {
        return zip;
    }


    /**
     * Sets the zip value for this User.
     * 
     * @param zip
     */
    public void setZip(java.lang.String zip) {
        this.zip = zip;
    }


    /**
     * Gets the extra_field_1 value for this User.
     * 
     * @return extra_field_1
     */
    public java.lang.String getExtra_field_1() {
        return extra_field_1;
    }


    /**
     * Sets the extra_field_1 value for this User.
     * 
     * @param extra_field_1
     */
    public void setExtra_field_1(java.lang.String extra_field_1) {
        this.extra_field_1 = extra_field_1;
    }


    /**
     * Gets the extra_field_2 value for this User.
     * 
     * @return extra_field_2
     */
    public java.lang.String getExtra_field_2() {
        return extra_field_2;
    }


    /**
     * Sets the extra_field_2 value for this User.
     * 
     * @param extra_field_2
     */
    public void setExtra_field_2(java.lang.String extra_field_2) {
        this.extra_field_2 = extra_field_2;
    }


    /**
     * Gets the extra_field_3 value for this User.
     * 
     * @return extra_field_3
     */
    public java.lang.String getExtra_field_3() {
        return extra_field_3;
    }


    /**
     * Sets the extra_field_3 value for this User.
     * 
     * @param extra_field_3
     */
    public void setExtra_field_3(java.lang.String extra_field_3) {
        this.extra_field_3 = extra_field_3;
    }


    /**
     * Gets the extra_field_4 value for this User.
     * 
     * @return extra_field_4
     */
    public java.lang.String getExtra_field_4() {
        return extra_field_4;
    }


    /**
     * Sets the extra_field_4 value for this User.
     * 
     * @param extra_field_4
     */
    public void setExtra_field_4(java.lang.String extra_field_4) {
        this.extra_field_4 = extra_field_4;
    }


    /**
     * Gets the extra_field_5 value for this User.
     * 
     * @return extra_field_5
     */
    public java.lang.String getExtra_field_5() {
        return extra_field_5;
    }


    /**
     * Sets the extra_field_5 value for this User.
     * 
     * @param extra_field_5
     */
    public void setExtra_field_5(java.lang.String extra_field_5) {
        this.extra_field_5 = extra_field_5;
    }


    /**
     * Gets the created_on value for this User.
     * 
     * @return created_on
     */
    public java.lang.String getCreated_on() {
        return created_on;
    }


    /**
     * Sets the created_on value for this User.
     * 
     * @param created_on
     */
    public void setCreated_on(java.lang.String created_on) {
        this.created_on = created_on;
    }


    /**
     * Gets the modified_on value for this User.
     * 
     * @return modified_on
     */
    public java.lang.String getModified_on() {
        return modified_on;
    }


    /**
     * Sets the modified_on value for this User.
     * 
     * @param modified_on
     */
    public void setModified_on(java.lang.String modified_on) {
        this.modified_on = modified_on;
    }


    /**
     * Gets the user_is_vendor value for this User.
     * 
     * @return user_is_vendor
     */
    public java.lang.String getUser_is_vendor() {
        return user_is_vendor;
    }


    /**
     * Sets the user_is_vendor value for this User.
     * 
     * @param user_is_vendor
     */
    public void setUser_is_vendor(java.lang.String user_is_vendor) {
        this.user_is_vendor = user_is_vendor;
    }


    /**
     * Gets the customer_number value for this User.
     * 
     * @return customer_number
     */
    public java.lang.String getCustomer_number() {
        return customer_number;
    }


    /**
     * Sets the customer_number value for this User.
     * 
     * @param customer_number
     */
    public void setCustomer_number(java.lang.String customer_number) {
        this.customer_number = customer_number;
    }


    /**
     * Gets the perms value for this User.
     * 
     * @return perms
     */
    public java.lang.String getPerms() {
        return perms;
    }


    /**
     * Sets the perms value for this User.
     * 
     * @param perms
     */
    public void setPerms(java.lang.String perms) {
        this.perms = perms;
    }


    /**
     * Gets the virtuemart_paymentmethod_id value for this User.
     * 
     * @return virtuemart_paymentmethod_id
     */
    public java.lang.String getVirtuemart_paymentmethod_id() {
        return virtuemart_paymentmethod_id;
    }


    /**
     * Sets the virtuemart_paymentmethod_id value for this User.
     * 
     * @param virtuemart_paymentmethod_id
     */
    public void setVirtuemart_paymentmethod_id(java.lang.String virtuemart_paymentmethod_id) {
        this.virtuemart_paymentmethod_id = virtuemart_paymentmethod_id;
    }


    /**
     * Gets the virtuemart_shippingcarrier_id value for this User.
     * 
     * @return virtuemart_shippingcarrier_id
     */
    public java.lang.String getVirtuemart_shippingcarrier_id() {
        return virtuemart_shippingcarrier_id;
    }


    /**
     * Sets the virtuemart_shippingcarrier_id value for this User.
     * 
     * @param virtuemart_shippingcarrier_id
     */
    public void setVirtuemart_shippingcarrier_id(java.lang.String virtuemart_shippingcarrier_id) {
        this.virtuemart_shippingcarrier_id = virtuemart_shippingcarrier_id;
    }


    /**
     * Gets the agreed value for this User.
     * 
     * @return agreed
     */
    public java.lang.String getAgreed() {
        return agreed;
    }


    /**
     * Sets the agreed value for this User.
     * 
     * @param agreed
     */
    public void setAgreed(java.lang.String agreed) {
        this.agreed = agreed;
    }


    /**
     * Gets the shoppergroup_id value for this User.
     * 
     * @return shoppergroup_id
     */
    public java.lang.String getShoppergroup_id() {
        return shoppergroup_id;
    }


    /**
     * Sets the shoppergroup_id value for this User.
     * 
     * @param shoppergroup_id
     */
    public void setShoppergroup_id(java.lang.String shoppergroup_id) {
        this.shoppergroup_id = shoppergroup_id;
    }


    /**
     * Gets the extra_fields_data value for this User.
     * 
     * @return extra_fields_data
     */
    public java.lang.String getExtra_fields_data() {
        return extra_fields_data;
    }


    /**
     * Sets the extra_fields_data value for this User.
     * 
     * @param extra_fields_data
     */
    public void setExtra_fields_data(java.lang.String extra_fields_data) {
        this.extra_fields_data = extra_fields_data;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof User)) return false;
        User other = (User) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.user_id==null && other.getUser_id()==null) || 
             (this.user_id!=null &&
              this.user_id.equals(other.getUser_id()))) &&
            ((this.email==null && other.getEmail()==null) || 
             (this.email!=null &&
              this.email.equals(other.getEmail()))) &&
            ((this.username==null && other.getUsername()==null) || 
             (this.username!=null &&
              this.username.equals(other.getUsername()))) &&
            ((this.password==null && other.getPassword()==null) || 
             (this.password!=null &&
              this.password.equals(other.getPassword()))) &&
            ((this.userinfo_id==null && other.getUserinfo_id()==null) || 
             (this.userinfo_id!=null &&
              this.userinfo_id.equals(other.getUserinfo_id()))) &&
            ((this.address_type==null && other.getAddress_type()==null) || 
             (this.address_type!=null &&
              this.address_type.equals(other.getAddress_type()))) &&
            ((this.address_type_name==null && other.getAddress_type_name()==null) || 
             (this.address_type_name!=null &&
              this.address_type_name.equals(other.getAddress_type_name()))) &&
            ((this.name==null && other.getName()==null) || 
             (this.name!=null &&
              this.name.equals(other.getName()))) &&
            ((this.company==null && other.getCompany()==null) || 
             (this.company!=null &&
              this.company.equals(other.getCompany()))) &&
            ((this.title==null && other.getTitle()==null) || 
             (this.title!=null &&
              this.title.equals(other.getTitle()))) &&
            ((this.last_name==null && other.getLast_name()==null) || 
             (this.last_name!=null &&
              this.last_name.equals(other.getLast_name()))) &&
            ((this.first_name==null && other.getFirst_name()==null) || 
             (this.first_name!=null &&
              this.first_name.equals(other.getFirst_name()))) &&
            ((this.middle_name==null && other.getMiddle_name()==null) || 
             (this.middle_name!=null &&
              this.middle_name.equals(other.getMiddle_name()))) &&
            ((this.phone_1==null && other.getPhone_1()==null) || 
             (this.phone_1!=null &&
              this.phone_1.equals(other.getPhone_1()))) &&
            ((this.phone_2==null && other.getPhone_2()==null) || 
             (this.phone_2!=null &&
              this.phone_2.equals(other.getPhone_2()))) &&
            ((this.fax==null && other.getFax()==null) || 
             (this.fax!=null &&
              this.fax.equals(other.getFax()))) &&
            ((this.address_1==null && other.getAddress_1()==null) || 
             (this.address_1!=null &&
              this.address_1.equals(other.getAddress_1()))) &&
            ((this.address_2==null && other.getAddress_2()==null) || 
             (this.address_2!=null &&
              this.address_2.equals(other.getAddress_2()))) &&
            ((this.city==null && other.getCity()==null) || 
             (this.city!=null &&
              this.city.equals(other.getCity()))) &&
            ((this.virtuemart_state_id==null && other.getVirtuemart_state_id()==null) || 
             (this.virtuemart_state_id!=null &&
              this.virtuemart_state_id.equals(other.getVirtuemart_state_id()))) &&
            ((this.virtuemart_country_id==null && other.getVirtuemart_country_id()==null) || 
             (this.virtuemart_country_id!=null &&
              this.virtuemart_country_id.equals(other.getVirtuemart_country_id()))) &&
            ((this.zip==null && other.getZip()==null) || 
             (this.zip!=null &&
              this.zip.equals(other.getZip()))) &&
            ((this.extra_field_1==null && other.getExtra_field_1()==null) || 
             (this.extra_field_1!=null &&
              this.extra_field_1.equals(other.getExtra_field_1()))) &&
            ((this.extra_field_2==null && other.getExtra_field_2()==null) || 
             (this.extra_field_2!=null &&
              this.extra_field_2.equals(other.getExtra_field_2()))) &&
            ((this.extra_field_3==null && other.getExtra_field_3()==null) || 
             (this.extra_field_3!=null &&
              this.extra_field_3.equals(other.getExtra_field_3()))) &&
            ((this.extra_field_4==null && other.getExtra_field_4()==null) || 
             (this.extra_field_4!=null &&
              this.extra_field_4.equals(other.getExtra_field_4()))) &&
            ((this.extra_field_5==null && other.getExtra_field_5()==null) || 
             (this.extra_field_5!=null &&
              this.extra_field_5.equals(other.getExtra_field_5()))) &&
            ((this.created_on==null && other.getCreated_on()==null) || 
             (this.created_on!=null &&
              this.created_on.equals(other.getCreated_on()))) &&
            ((this.modified_on==null && other.getModified_on()==null) || 
             (this.modified_on!=null &&
              this.modified_on.equals(other.getModified_on()))) &&
            ((this.user_is_vendor==null && other.getUser_is_vendor()==null) || 
             (this.user_is_vendor!=null &&
              this.user_is_vendor.equals(other.getUser_is_vendor()))) &&
            ((this.customer_number==null && other.getCustomer_number()==null) || 
             (this.customer_number!=null &&
              this.customer_number.equals(other.getCustomer_number()))) &&
            ((this.perms==null && other.getPerms()==null) || 
             (this.perms!=null &&
              this.perms.equals(other.getPerms()))) &&
            ((this.virtuemart_paymentmethod_id==null && other.getVirtuemart_paymentmethod_id()==null) || 
             (this.virtuemart_paymentmethod_id!=null &&
              this.virtuemart_paymentmethod_id.equals(other.getVirtuemart_paymentmethod_id()))) &&
            ((this.virtuemart_shippingcarrier_id==null && other.getVirtuemart_shippingcarrier_id()==null) || 
             (this.virtuemart_shippingcarrier_id!=null &&
              this.virtuemart_shippingcarrier_id.equals(other.getVirtuemart_shippingcarrier_id()))) &&
            ((this.agreed==null && other.getAgreed()==null) || 
             (this.agreed!=null &&
              this.agreed.equals(other.getAgreed()))) &&
            ((this.shoppergroup_id==null && other.getShoppergroup_id()==null) || 
             (this.shoppergroup_id!=null &&
              this.shoppergroup_id.equals(other.getShoppergroup_id()))) &&
            ((this.extra_fields_data==null && other.getExtra_fields_data()==null) || 
             (this.extra_fields_data!=null &&
              this.extra_fields_data.equals(other.getExtra_fields_data())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getUser_id() != null) {
            _hashCode += getUser_id().hashCode();
        }
        if (getEmail() != null) {
            _hashCode += getEmail().hashCode();
        }
        if (getUsername() != null) {
            _hashCode += getUsername().hashCode();
        }
        if (getPassword() != null) {
            _hashCode += getPassword().hashCode();
        }
        if (getUserinfo_id() != null) {
            _hashCode += getUserinfo_id().hashCode();
        }
        if (getAddress_type() != null) {
            _hashCode += getAddress_type().hashCode();
        }
        if (getAddress_type_name() != null) {
            _hashCode += getAddress_type_name().hashCode();
        }
        if (getName() != null) {
            _hashCode += getName().hashCode();
        }
        if (getCompany() != null) {
            _hashCode += getCompany().hashCode();
        }
        if (getTitle() != null) {
            _hashCode += getTitle().hashCode();
        }
        if (getLast_name() != null) {
            _hashCode += getLast_name().hashCode();
        }
        if (getFirst_name() != null) {
            _hashCode += getFirst_name().hashCode();
        }
        if (getMiddle_name() != null) {
            _hashCode += getMiddle_name().hashCode();
        }
        if (getPhone_1() != null) {
            _hashCode += getPhone_1().hashCode();
        }
        if (getPhone_2() != null) {
            _hashCode += getPhone_2().hashCode();
        }
        if (getFax() != null) {
            _hashCode += getFax().hashCode();
        }
        if (getAddress_1() != null) {
            _hashCode += getAddress_1().hashCode();
        }
        if (getAddress_2() != null) {
            _hashCode += getAddress_2().hashCode();
        }
        if (getCity() != null) {
            _hashCode += getCity().hashCode();
        }
        if (getVirtuemart_state_id() != null) {
            _hashCode += getVirtuemart_state_id().hashCode();
        }
        if (getVirtuemart_country_id() != null) {
            _hashCode += getVirtuemart_country_id().hashCode();
        }
        if (getZip() != null) {
            _hashCode += getZip().hashCode();
        }
        if (getExtra_field_1() != null) {
            _hashCode += getExtra_field_1().hashCode();
        }
        if (getExtra_field_2() != null) {
            _hashCode += getExtra_field_2().hashCode();
        }
        if (getExtra_field_3() != null) {
            _hashCode += getExtra_field_3().hashCode();
        }
        if (getExtra_field_4() != null) {
            _hashCode += getExtra_field_4().hashCode();
        }
        if (getExtra_field_5() != null) {
            _hashCode += getExtra_field_5().hashCode();
        }
        if (getCreated_on() != null) {
            _hashCode += getCreated_on().hashCode();
        }
        if (getModified_on() != null) {
            _hashCode += getModified_on().hashCode();
        }
        if (getUser_is_vendor() != null) {
            _hashCode += getUser_is_vendor().hashCode();
        }
        if (getCustomer_number() != null) {
            _hashCode += getCustomer_number().hashCode();
        }
        if (getPerms() != null) {
            _hashCode += getPerms().hashCode();
        }
        if (getVirtuemart_paymentmethod_id() != null) {
            _hashCode += getVirtuemart_paymentmethod_id().hashCode();
        }
        if (getVirtuemart_shippingcarrier_id() != null) {
            _hashCode += getVirtuemart_shippingcarrier_id().hashCode();
        }
        if (getAgreed() != null) {
            _hashCode += getAgreed().hashCode();
        }
        if (getShoppergroup_id() != null) {
            _hashCode += getShoppergroup_id().hashCode();
        }
        if (getExtra_fields_data() != null) {
            _hashCode += getExtra_fields_data().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(User.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://www.virtuemart.net/VM_Users/", "User"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("user_id");
        elemField.setXmlName(new javax.xml.namespace.QName("", "user_id"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("email");
        elemField.setXmlName(new javax.xml.namespace.QName("", "email"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("username");
        elemField.setXmlName(new javax.xml.namespace.QName("", "username"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("password");
        elemField.setXmlName(new javax.xml.namespace.QName("", "password"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("userinfo_id");
        elemField.setXmlName(new javax.xml.namespace.QName("", "userinfo_id"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("address_type");
        elemField.setXmlName(new javax.xml.namespace.QName("", "address_type"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("address_type_name");
        elemField.setXmlName(new javax.xml.namespace.QName("", "address_type_name"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("name");
        elemField.setXmlName(new javax.xml.namespace.QName("", "name"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("company");
        elemField.setXmlName(new javax.xml.namespace.QName("", "company"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("title");
        elemField.setXmlName(new javax.xml.namespace.QName("", "title"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("last_name");
        elemField.setXmlName(new javax.xml.namespace.QName("", "last_name"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("first_name");
        elemField.setXmlName(new javax.xml.namespace.QName("", "first_name"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("middle_name");
        elemField.setXmlName(new javax.xml.namespace.QName("", "middle_name"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("phone_1");
        elemField.setXmlName(new javax.xml.namespace.QName("", "phone_1"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("phone_2");
        elemField.setXmlName(new javax.xml.namespace.QName("", "phone_2"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("fax");
        elemField.setXmlName(new javax.xml.namespace.QName("", "fax"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("address_1");
        elemField.setXmlName(new javax.xml.namespace.QName("", "address_1"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("address_2");
        elemField.setXmlName(new javax.xml.namespace.QName("", "address_2"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("city");
        elemField.setXmlName(new javax.xml.namespace.QName("", "city"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("virtuemart_state_id");
        elemField.setXmlName(new javax.xml.namespace.QName("", "virtuemart_state_id"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("virtuemart_country_id");
        elemField.setXmlName(new javax.xml.namespace.QName("", "virtuemart_country_id"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("zip");
        elemField.setXmlName(new javax.xml.namespace.QName("", "zip"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("extra_field_1");
        elemField.setXmlName(new javax.xml.namespace.QName("", "extra_field_1"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("extra_field_2");
        elemField.setXmlName(new javax.xml.namespace.QName("", "extra_field_2"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("extra_field_3");
        elemField.setXmlName(new javax.xml.namespace.QName("", "extra_field_3"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("extra_field_4");
        elemField.setXmlName(new javax.xml.namespace.QName("", "extra_field_4"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("extra_field_5");
        elemField.setXmlName(new javax.xml.namespace.QName("", "extra_field_5"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("created_on");
        elemField.setXmlName(new javax.xml.namespace.QName("", "created_on"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("modified_on");
        elemField.setXmlName(new javax.xml.namespace.QName("", "modified_on"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("user_is_vendor");
        elemField.setXmlName(new javax.xml.namespace.QName("", "user_is_vendor"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("customer_number");
        elemField.setXmlName(new javax.xml.namespace.QName("", "customer_number"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("perms");
        elemField.setXmlName(new javax.xml.namespace.QName("", "perms"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("virtuemart_paymentmethod_id");
        elemField.setXmlName(new javax.xml.namespace.QName("", "virtuemart_paymentmethod_id"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("virtuemart_shippingcarrier_id");
        elemField.setXmlName(new javax.xml.namespace.QName("", "virtuemart_shippingcarrier_id"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("agreed");
        elemField.setXmlName(new javax.xml.namespace.QName("", "agreed"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("shoppergroup_id");
        elemField.setXmlName(new javax.xml.namespace.QName("", "shoppergroup_id"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("extra_fields_data");
        elemField.setXmlName(new javax.xml.namespace.QName("", "extra_fields_data"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
