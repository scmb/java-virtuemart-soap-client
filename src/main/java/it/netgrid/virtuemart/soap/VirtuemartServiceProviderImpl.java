package it.netgrid.virtuemart.soap;

import javax.xml.namespace.QName;
import javax.xml.rpc.ServiceException;

import it.netgrid.virtuemart.soap.categories.VM_Categories_PortType;
import it.netgrid.virtuemart.soap.categories.VM_Categories_ServiceLocator;
import it.netgrid.virtuemart.soap.custom.VM_Custom_PortType;
import it.netgrid.virtuemart.soap.custom.VM_Custom_ServiceLocator;
import it.netgrid.virtuemart.soap.orders.VM_Order_PortType;
import it.netgrid.virtuemart.soap.orders.VM_Order_ServiceLocator;
import it.netgrid.virtuemart.soap.products.VM_Product_PortType;
import it.netgrid.virtuemart.soap.products.VM_Product_ServiceLocator;
import it.netgrid.virtuemart.soap.sqlqueries.VM_SQLQueries_PortType;
import it.netgrid.virtuemart.soap.sqlqueries.VM_SQLQueries_ServiceLocator;
import it.netgrid.virtuemart.soap.users.VM_Users_PortType;
import it.netgrid.virtuemart.soap.users.VM_Users_ServiceLocator;

public class VirtuemartServiceProviderImpl implements VirtuemartServiceProvider {
	
	private final VirtuemartServiceConfig config;
	private static final QName CATEGORIES_SERVICE_NAME = new QName("http://www.virtuemart.net/VM_Categories/", "VM_Categories"); 
	private static final QName PRODUCT_SERVICE_NAME = new QName("http://www.virtuemart.net/VM_Product/", "VM_Product"); 
	private static final QName CUSTOM_SERVICE_NAME = new QName("http://www.virtuemart.net/VM_Categories/", "VM_Custom"); 
	private static final QName ORDERS_SERVICE_NAME = new QName("http://www.virtuemart.net/VM_Order/", "VM_Order"); 
	private static final QName SQLQUERIES_SERVICE_NAME = new QName("http://www.virtuemart.net/VM_SQLQueries/", "VM_SQLQueries");
	private static final QName USERS_SERVICE_NAME = new QName("http://www.virtuemart.net/VM_Custom/", "VM_Users");
	
	private static final String CATEGORIES_SERVICE = "/administrator/components/com_vm_soa/services/VM_CategoriesService.php";
	private static final String PRODUCT_SERVICE = "/administrator/components/com_vm_soa/services/VM_ProductService.php";
	private static final String CUSTOM_SERVICE = "/administrator/components/com_vm_soa/services/VM_CustomizedService.php";
	private static final String ORDERS_SERVICE = "/administrator/components/com_vm_soa/services/VM_OrdersService.php";
	private static final String SQLQUERIES_SERVICE = "/administrator/components/com_vm_soa/services/VM_SQLQueriesService.php";
	private static final String USERS_SERVICE = "/administrator/components/com_vm_soa/services/VM_UsersService.php";
	
	private static final String CATEGORIES_WSDL = "/administrator/components/com_vm_soa/services/VM_CategoriesWSDL.php";
	private static final String PRODUCT_WSDL = "/administrator/components/com_vm_soa/services/VM_ProductWSDL.php";
	private static final String CUSTOM_WSDL = "/administrator/components/com_vm_soa/services/VM_CustomizedWSDL.php";
	private static final String ORDERS_WSDL = "/administrator/components/com_vm_soa/services/VM_OrderWSDL.php";
	private static final String SQLQUERIES_WSDL = "/administrator/components/com_vm_soa/services/VM_SQLQueriesWSDL.php";
	private static final String USERS_WSDL = "/administrator/components/com_vm_soa/services/VM_UsersWSDL.php";
	
	
	public VirtuemartServiceProviderImpl() {
		this.config = this.getDefaultServiceConfig();
	}
	
	public VirtuemartServiceProviderImpl(VirtuemartServiceConfig config) {
		if(config != null) {
			this.config = config;
		} else {
			this.config = this.getDefaultServiceConfig();
		}
	}
	
	public VM_Categories_PortType getCategoriesSOAP() {
		try {
			VM_Categories_ServiceLocator locator = new VM_Categories_ServiceLocator(this.getVmCategoriesWSDL(), CATEGORIES_SERVICE_NAME);
			locator.setVM_CategoriesSOAPEndpointAddress(this.getVmCategoriesService());
			return (VM_Categories_PortType)locator.getPort(VM_Categories_PortType.class);
		} catch (ServiceException e) {
			e.printStackTrace();
		}
		return null;
	}
	
	public VM_Product_PortType getProductSOAP() {
		try {
			VM_Product_ServiceLocator locator = new VM_Product_ServiceLocator(this.getVmProductsWSDL(), PRODUCT_SERVICE_NAME);
			locator.setVM_ProductSOAPEndpointAddress(this.getVmProductsService());
			return (VM_Product_PortType)locator.getPort(VM_Product_PortType.class);
		} catch (ServiceException e) {
			e.printStackTrace();
		}
		return null;
	}
	
	public VM_Custom_PortType getCustomSOAP() {
		try {
			VM_Custom_ServiceLocator locator = new VM_Custom_ServiceLocator(this.getVmCustomWSDL(), CUSTOM_SERVICE_NAME);
			locator.setVM_CustomSOAPEndpointAddress(this.getVmCustomService());
			return (VM_Custom_PortType)locator.getPort(VM_Custom_PortType.class);
		} catch (ServiceException e) {
			e.printStackTrace();
		}
		return null;
	}
	
	public VM_Order_PortType getOrdersSOAP() {
		try {
			VM_Order_ServiceLocator locator = new VM_Order_ServiceLocator(this.getVmOrdersWSDL(), ORDERS_SERVICE_NAME);
			locator.setVM_OrderSOAPEndpointAddress(this.getVmOrdersService());
			return (VM_Order_PortType)locator.getPort(VM_Order_PortType.class);
		} catch (ServiceException e) {
			e.printStackTrace();
		}
		return null;
	}
	
	public VM_SQLQueries_PortType getSQLQueriesSOAP() {
		try {
			VM_SQLQueries_ServiceLocator locator = new VM_SQLQueries_ServiceLocator(this.getVmSqlQueriesWSDL(), SQLQUERIES_SERVICE_NAME);
			locator.setVM_SQLQueriesSOAPEndpointAddress(this.getVmSqlQueriesService());
			return (VM_SQLQueries_PortType)locator.getPort(VM_SQLQueries_PortType.class);
		} catch (ServiceException e) {
			e.printStackTrace();
		}
		return null;
	}
	
	public VM_Users_PortType getUsersSOAP() {
		try {
			VM_Users_ServiceLocator locator = new VM_Users_ServiceLocator(this.getVmUsersWSDL(), USERS_SERVICE_NAME);
			locator.setVM_UsersSOAPEndpointAddress(this.getVmUsersService());
			return (VM_Users_PortType)locator.getPort(VM_Users_PortType.class);
		} catch (ServiceException e) {
			e.printStackTrace();
		}
		return null;
	}
	
	private String getVmOrdersWSDL() {
		return String.format("%s%s", this.config.getBaseUrl(), ORDERS_WSDL);
	}
	
	private String getVmUsersWSDL() {
		return String.format("%s%s", this.config.getBaseUrl(), USERS_WSDL);
	}
	
	private String getVmProductsWSDL() {
		return String.format("%s%s", this.config.getBaseUrl(), PRODUCT_WSDL);
	}
	
	private String getVmCategoriesWSDL() {
		return String.format("%s%s", this.config.getBaseUrl(), CATEGORIES_WSDL);
	}
	
	private String getVmSqlQueriesWSDL() {
		return String.format("%s%s", this.config.getBaseUrl(), SQLQUERIES_WSDL);
	}
	
	private String getVmCustomWSDL() {
		return String.format("%s%s", this.config.getBaseUrl(), CUSTOM_WSDL);
	}
	
	
	
	private String getVmOrdersService() {
		return String.format("%s%s", this.config.getBaseUrl(), ORDERS_SERVICE);
	}
	
	private String getVmUsersService() {
		return String.format("%s%s", this.config.getBaseUrl(), USERS_SERVICE);
	}
	
	private String getVmProductsService() {
		return String.format("%s%s", this.config.getBaseUrl(), PRODUCT_SERVICE);
	}
	
	private String getVmCategoriesService() {
		return String.format("%s%s", this.config.getBaseUrl(), CATEGORIES_SERVICE);
	}
	
	private String getVmSqlQueriesService() {
		return String.format("%s%s", this.config.getBaseUrl(), SQLQUERIES_SERVICE);
	}
	
	private String getVmCustomService() {
		return String.format("%s%s", this.config.getBaseUrl(), CUSTOM_SERVICE);
	}	
	
	private VirtuemartServiceConfig getDefaultServiceConfig() {
		return new VirtuemartServiceConfig() {
			
			public String getBaseUrl() {
				return "http://localhost/";
			}
			
			public String getVmUsername() {
				return "admin";
			}
			
			public String getVmPassword() {
				return "admin";
			}
		};
	}
}
