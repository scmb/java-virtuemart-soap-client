/**
 * Worldzone.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package it.netgrid.virtuemart.soap.products;

public class Worldzone  implements java.io.Serializable {
    private java.lang.String virtuemart_worldzone_id;

    private java.lang.String virtuemart_vendor_id;

    private java.lang.String zone_name;

    private java.lang.String zone_cost;

    private java.lang.String zone_limit;

    private java.lang.String zone_description;

    private java.lang.String zone_tax_rate;

    private java.lang.String ordering;

    private java.lang.String shared;

    private java.lang.String published;

    public Worldzone() {
    }

    public Worldzone(
           java.lang.String virtuemart_worldzone_id,
           java.lang.String virtuemart_vendor_id,
           java.lang.String zone_name,
           java.lang.String zone_cost,
           java.lang.String zone_limit,
           java.lang.String zone_description,
           java.lang.String zone_tax_rate,
           java.lang.String ordering,
           java.lang.String shared,
           java.lang.String published) {
           this.virtuemart_worldzone_id = virtuemart_worldzone_id;
           this.virtuemart_vendor_id = virtuemart_vendor_id;
           this.zone_name = zone_name;
           this.zone_cost = zone_cost;
           this.zone_limit = zone_limit;
           this.zone_description = zone_description;
           this.zone_tax_rate = zone_tax_rate;
           this.ordering = ordering;
           this.shared = shared;
           this.published = published;
    }


    /**
     * Gets the virtuemart_worldzone_id value for this Worldzone.
     * 
     * @return virtuemart_worldzone_id
     */
    public java.lang.String getVirtuemart_worldzone_id() {
        return virtuemart_worldzone_id;
    }


    /**
     * Sets the virtuemart_worldzone_id value for this Worldzone.
     * 
     * @param virtuemart_worldzone_id
     */
    public void setVirtuemart_worldzone_id(java.lang.String virtuemart_worldzone_id) {
        this.virtuemart_worldzone_id = virtuemart_worldzone_id;
    }


    /**
     * Gets the virtuemart_vendor_id value for this Worldzone.
     * 
     * @return virtuemart_vendor_id
     */
    public java.lang.String getVirtuemart_vendor_id() {
        return virtuemart_vendor_id;
    }


    /**
     * Sets the virtuemart_vendor_id value for this Worldzone.
     * 
     * @param virtuemart_vendor_id
     */
    public void setVirtuemart_vendor_id(java.lang.String virtuemart_vendor_id) {
        this.virtuemart_vendor_id = virtuemart_vendor_id;
    }


    /**
     * Gets the zone_name value for this Worldzone.
     * 
     * @return zone_name
     */
    public java.lang.String getZone_name() {
        return zone_name;
    }


    /**
     * Sets the zone_name value for this Worldzone.
     * 
     * @param zone_name
     */
    public void setZone_name(java.lang.String zone_name) {
        this.zone_name = zone_name;
    }


    /**
     * Gets the zone_cost value for this Worldzone.
     * 
     * @return zone_cost
     */
    public java.lang.String getZone_cost() {
        return zone_cost;
    }


    /**
     * Sets the zone_cost value for this Worldzone.
     * 
     * @param zone_cost
     */
    public void setZone_cost(java.lang.String zone_cost) {
        this.zone_cost = zone_cost;
    }


    /**
     * Gets the zone_limit value for this Worldzone.
     * 
     * @return zone_limit
     */
    public java.lang.String getZone_limit() {
        return zone_limit;
    }


    /**
     * Sets the zone_limit value for this Worldzone.
     * 
     * @param zone_limit
     */
    public void setZone_limit(java.lang.String zone_limit) {
        this.zone_limit = zone_limit;
    }


    /**
     * Gets the zone_description value for this Worldzone.
     * 
     * @return zone_description
     */
    public java.lang.String getZone_description() {
        return zone_description;
    }


    /**
     * Sets the zone_description value for this Worldzone.
     * 
     * @param zone_description
     */
    public void setZone_description(java.lang.String zone_description) {
        this.zone_description = zone_description;
    }


    /**
     * Gets the zone_tax_rate value for this Worldzone.
     * 
     * @return zone_tax_rate
     */
    public java.lang.String getZone_tax_rate() {
        return zone_tax_rate;
    }


    /**
     * Sets the zone_tax_rate value for this Worldzone.
     * 
     * @param zone_tax_rate
     */
    public void setZone_tax_rate(java.lang.String zone_tax_rate) {
        this.zone_tax_rate = zone_tax_rate;
    }


    /**
     * Gets the ordering value for this Worldzone.
     * 
     * @return ordering
     */
    public java.lang.String getOrdering() {
        return ordering;
    }


    /**
     * Sets the ordering value for this Worldzone.
     * 
     * @param ordering
     */
    public void setOrdering(java.lang.String ordering) {
        this.ordering = ordering;
    }


    /**
     * Gets the shared value for this Worldzone.
     * 
     * @return shared
     */
    public java.lang.String getShared() {
        return shared;
    }


    /**
     * Sets the shared value for this Worldzone.
     * 
     * @param shared
     */
    public void setShared(java.lang.String shared) {
        this.shared = shared;
    }


    /**
     * Gets the published value for this Worldzone.
     * 
     * @return published
     */
    public java.lang.String getPublished() {
        return published;
    }


    /**
     * Sets the published value for this Worldzone.
     * 
     * @param published
     */
    public void setPublished(java.lang.String published) {
        this.published = published;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof Worldzone)) return false;
        Worldzone other = (Worldzone) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.virtuemart_worldzone_id==null && other.getVirtuemart_worldzone_id()==null) || 
             (this.virtuemart_worldzone_id!=null &&
              this.virtuemart_worldzone_id.equals(other.getVirtuemart_worldzone_id()))) &&
            ((this.virtuemart_vendor_id==null && other.getVirtuemart_vendor_id()==null) || 
             (this.virtuemart_vendor_id!=null &&
              this.virtuemart_vendor_id.equals(other.getVirtuemart_vendor_id()))) &&
            ((this.zone_name==null && other.getZone_name()==null) || 
             (this.zone_name!=null &&
              this.zone_name.equals(other.getZone_name()))) &&
            ((this.zone_cost==null && other.getZone_cost()==null) || 
             (this.zone_cost!=null &&
              this.zone_cost.equals(other.getZone_cost()))) &&
            ((this.zone_limit==null && other.getZone_limit()==null) || 
             (this.zone_limit!=null &&
              this.zone_limit.equals(other.getZone_limit()))) &&
            ((this.zone_description==null && other.getZone_description()==null) || 
             (this.zone_description!=null &&
              this.zone_description.equals(other.getZone_description()))) &&
            ((this.zone_tax_rate==null && other.getZone_tax_rate()==null) || 
             (this.zone_tax_rate!=null &&
              this.zone_tax_rate.equals(other.getZone_tax_rate()))) &&
            ((this.ordering==null && other.getOrdering()==null) || 
             (this.ordering!=null &&
              this.ordering.equals(other.getOrdering()))) &&
            ((this.shared==null && other.getShared()==null) || 
             (this.shared!=null &&
              this.shared.equals(other.getShared()))) &&
            ((this.published==null && other.getPublished()==null) || 
             (this.published!=null &&
              this.published.equals(other.getPublished())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getVirtuemart_worldzone_id() != null) {
            _hashCode += getVirtuemart_worldzone_id().hashCode();
        }
        if (getVirtuemart_vendor_id() != null) {
            _hashCode += getVirtuemart_vendor_id().hashCode();
        }
        if (getZone_name() != null) {
            _hashCode += getZone_name().hashCode();
        }
        if (getZone_cost() != null) {
            _hashCode += getZone_cost().hashCode();
        }
        if (getZone_limit() != null) {
            _hashCode += getZone_limit().hashCode();
        }
        if (getZone_description() != null) {
            _hashCode += getZone_description().hashCode();
        }
        if (getZone_tax_rate() != null) {
            _hashCode += getZone_tax_rate().hashCode();
        }
        if (getOrdering() != null) {
            _hashCode += getOrdering().hashCode();
        }
        if (getShared() != null) {
            _hashCode += getShared().hashCode();
        }
        if (getPublished() != null) {
            _hashCode += getPublished().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(Worldzone.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://www.virtuemart.net/VM_Product/", "Worldzone"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("virtuemart_worldzone_id");
        elemField.setXmlName(new javax.xml.namespace.QName("", "virtuemart_worldzone_id"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("virtuemart_vendor_id");
        elemField.setXmlName(new javax.xml.namespace.QName("", "virtuemart_vendor_id"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("zone_name");
        elemField.setXmlName(new javax.xml.namespace.QName("", "zone_name"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("zone_cost");
        elemField.setXmlName(new javax.xml.namespace.QName("", "zone_cost"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("zone_limit");
        elemField.setXmlName(new javax.xml.namespace.QName("", "zone_limit"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("zone_description");
        elemField.setXmlName(new javax.xml.namespace.QName("", "zone_description"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("zone_tax_rate");
        elemField.setXmlName(new javax.xml.namespace.QName("", "zone_tax_rate"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("ordering");
        elemField.setXmlName(new javax.xml.namespace.QName("", "ordering"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("shared");
        elemField.setXmlName(new javax.xml.namespace.QName("", "shared"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("published");
        elemField.setXmlName(new javax.xml.namespace.QName("", "published"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
