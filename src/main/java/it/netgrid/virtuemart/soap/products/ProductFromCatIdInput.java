/**
 * ProductFromCatIdInput.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package it.netgrid.virtuemart.soap.products;

public class ProductFromCatIdInput  implements java.io.Serializable {
    private it.netgrid.virtuemart.soap.products.LoginInfo loginInfo;

    private java.lang.String catgory_id;

    private java.lang.String product_publish;

    private java.lang.String with_childs;

    private java.lang.String include_prices;

    private java.lang.String limite_start;

    private java.lang.String limite_end;

    public ProductFromCatIdInput() {
    }

    public ProductFromCatIdInput(
           it.netgrid.virtuemart.soap.products.LoginInfo loginInfo,
           java.lang.String catgory_id,
           java.lang.String product_publish,
           java.lang.String with_childs,
           java.lang.String include_prices,
           java.lang.String limite_start,
           java.lang.String limite_end) {
           this.loginInfo = loginInfo;
           this.catgory_id = catgory_id;
           this.product_publish = product_publish;
           this.with_childs = with_childs;
           this.include_prices = include_prices;
           this.limite_start = limite_start;
           this.limite_end = limite_end;
    }


    /**
     * Gets the loginInfo value for this ProductFromCatIdInput.
     * 
     * @return loginInfo
     */
    public it.netgrid.virtuemart.soap.products.LoginInfo getLoginInfo() {
        return loginInfo;
    }


    /**
     * Sets the loginInfo value for this ProductFromCatIdInput.
     * 
     * @param loginInfo
     */
    public void setLoginInfo(it.netgrid.virtuemart.soap.products.LoginInfo loginInfo) {
        this.loginInfo = loginInfo;
    }


    /**
     * Gets the catgory_id value for this ProductFromCatIdInput.
     * 
     * @return catgory_id
     */
    public java.lang.String getCatgory_id() {
        return catgory_id;
    }


    /**
     * Sets the catgory_id value for this ProductFromCatIdInput.
     * 
     * @param catgory_id
     */
    public void setCatgory_id(java.lang.String catgory_id) {
        this.catgory_id = catgory_id;
    }


    /**
     * Gets the product_publish value for this ProductFromCatIdInput.
     * 
     * @return product_publish
     */
    public java.lang.String getProduct_publish() {
        return product_publish;
    }


    /**
     * Sets the product_publish value for this ProductFromCatIdInput.
     * 
     * @param product_publish
     */
    public void setProduct_publish(java.lang.String product_publish) {
        this.product_publish = product_publish;
    }


    /**
     * Gets the with_childs value for this ProductFromCatIdInput.
     * 
     * @return with_childs
     */
    public java.lang.String getWith_childs() {
        return with_childs;
    }


    /**
     * Sets the with_childs value for this ProductFromCatIdInput.
     * 
     * @param with_childs
     */
    public void setWith_childs(java.lang.String with_childs) {
        this.with_childs = with_childs;
    }


    /**
     * Gets the include_prices value for this ProductFromCatIdInput.
     * 
     * @return include_prices
     */
    public java.lang.String getInclude_prices() {
        return include_prices;
    }


    /**
     * Sets the include_prices value for this ProductFromCatIdInput.
     * 
     * @param include_prices
     */
    public void setInclude_prices(java.lang.String include_prices) {
        this.include_prices = include_prices;
    }


    /**
     * Gets the limite_start value for this ProductFromCatIdInput.
     * 
     * @return limite_start
     */
    public java.lang.String getLimite_start() {
        return limite_start;
    }


    /**
     * Sets the limite_start value for this ProductFromCatIdInput.
     * 
     * @param limite_start
     */
    public void setLimite_start(java.lang.String limite_start) {
        this.limite_start = limite_start;
    }


    /**
     * Gets the limite_end value for this ProductFromCatIdInput.
     * 
     * @return limite_end
     */
    public java.lang.String getLimite_end() {
        return limite_end;
    }


    /**
     * Sets the limite_end value for this ProductFromCatIdInput.
     * 
     * @param limite_end
     */
    public void setLimite_end(java.lang.String limite_end) {
        this.limite_end = limite_end;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof ProductFromCatIdInput)) return false;
        ProductFromCatIdInput other = (ProductFromCatIdInput) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.loginInfo==null && other.getLoginInfo()==null) || 
             (this.loginInfo!=null &&
              this.loginInfo.equals(other.getLoginInfo()))) &&
            ((this.catgory_id==null && other.getCatgory_id()==null) || 
             (this.catgory_id!=null &&
              this.catgory_id.equals(other.getCatgory_id()))) &&
            ((this.product_publish==null && other.getProduct_publish()==null) || 
             (this.product_publish!=null &&
              this.product_publish.equals(other.getProduct_publish()))) &&
            ((this.with_childs==null && other.getWith_childs()==null) || 
             (this.with_childs!=null &&
              this.with_childs.equals(other.getWith_childs()))) &&
            ((this.include_prices==null && other.getInclude_prices()==null) || 
             (this.include_prices!=null &&
              this.include_prices.equals(other.getInclude_prices()))) &&
            ((this.limite_start==null && other.getLimite_start()==null) || 
             (this.limite_start!=null &&
              this.limite_start.equals(other.getLimite_start()))) &&
            ((this.limite_end==null && other.getLimite_end()==null) || 
             (this.limite_end!=null &&
              this.limite_end.equals(other.getLimite_end())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getLoginInfo() != null) {
            _hashCode += getLoginInfo().hashCode();
        }
        if (getCatgory_id() != null) {
            _hashCode += getCatgory_id().hashCode();
        }
        if (getProduct_publish() != null) {
            _hashCode += getProduct_publish().hashCode();
        }
        if (getWith_childs() != null) {
            _hashCode += getWith_childs().hashCode();
        }
        if (getInclude_prices() != null) {
            _hashCode += getInclude_prices().hashCode();
        }
        if (getLimite_start() != null) {
            _hashCode += getLimite_start().hashCode();
        }
        if (getLimite_end() != null) {
            _hashCode += getLimite_end().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(ProductFromCatIdInput.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://www.virtuemart.net/VM_Product/", "ProductFromCatIdInput"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("loginInfo");
        elemField.setXmlName(new javax.xml.namespace.QName("", "loginInfo"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.virtuemart.net/VM_Product/", "loginInfo"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("catgory_id");
        elemField.setXmlName(new javax.xml.namespace.QName("", "catgory_id"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("product_publish");
        elemField.setXmlName(new javax.xml.namespace.QName("", "product_publish"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("with_childs");
        elemField.setXmlName(new javax.xml.namespace.QName("", "with_childs"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("include_prices");
        elemField.setXmlName(new javax.xml.namespace.QName("", "include_prices"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("limite_start");
        elemField.setXmlName(new javax.xml.namespace.QName("", "limite_start"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("limite_end");
        elemField.setXmlName(new javax.xml.namespace.QName("", "limite_end"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
