/**
 * ProductFile.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package it.netgrid.virtuemart.soap.products;

public class ProductFile  implements java.io.Serializable {
    private java.lang.String file_id;

    private java.lang.String file_product_id;

    private java.lang.String file_name;

    private java.lang.String file_title;

    private java.lang.String file_description;

    private java.lang.String file_extension;

    private java.lang.String file_mimetype;

    private java.lang.String file_url;

    private java.lang.String file_published;

    private java.lang.String file_is_image;

    private java.lang.String file_image_height;

    private java.lang.String file_image_width;

    private java.lang.String file_image_thumb_height;

    private java.lang.String file_image_thumb_width;

    public ProductFile() {
    }

    public ProductFile(
           java.lang.String file_id,
           java.lang.String file_product_id,
           java.lang.String file_name,
           java.lang.String file_title,
           java.lang.String file_description,
           java.lang.String file_extension,
           java.lang.String file_mimetype,
           java.lang.String file_url,
           java.lang.String file_published,
           java.lang.String file_is_image,
           java.lang.String file_image_height,
           java.lang.String file_image_width,
           java.lang.String file_image_thumb_height,
           java.lang.String file_image_thumb_width) {
           this.file_id = file_id;
           this.file_product_id = file_product_id;
           this.file_name = file_name;
           this.file_title = file_title;
           this.file_description = file_description;
           this.file_extension = file_extension;
           this.file_mimetype = file_mimetype;
           this.file_url = file_url;
           this.file_published = file_published;
           this.file_is_image = file_is_image;
           this.file_image_height = file_image_height;
           this.file_image_width = file_image_width;
           this.file_image_thumb_height = file_image_thumb_height;
           this.file_image_thumb_width = file_image_thumb_width;
    }


    /**
     * Gets the file_id value for this ProductFile.
     * 
     * @return file_id
     */
    public java.lang.String getFile_id() {
        return file_id;
    }


    /**
     * Sets the file_id value for this ProductFile.
     * 
     * @param file_id
     */
    public void setFile_id(java.lang.String file_id) {
        this.file_id = file_id;
    }


    /**
     * Gets the file_product_id value for this ProductFile.
     * 
     * @return file_product_id
     */
    public java.lang.String getFile_product_id() {
        return file_product_id;
    }


    /**
     * Sets the file_product_id value for this ProductFile.
     * 
     * @param file_product_id
     */
    public void setFile_product_id(java.lang.String file_product_id) {
        this.file_product_id = file_product_id;
    }


    /**
     * Gets the file_name value for this ProductFile.
     * 
     * @return file_name
     */
    public java.lang.String getFile_name() {
        return file_name;
    }


    /**
     * Sets the file_name value for this ProductFile.
     * 
     * @param file_name
     */
    public void setFile_name(java.lang.String file_name) {
        this.file_name = file_name;
    }


    /**
     * Gets the file_title value for this ProductFile.
     * 
     * @return file_title
     */
    public java.lang.String getFile_title() {
        return file_title;
    }


    /**
     * Sets the file_title value for this ProductFile.
     * 
     * @param file_title
     */
    public void setFile_title(java.lang.String file_title) {
        this.file_title = file_title;
    }


    /**
     * Gets the file_description value for this ProductFile.
     * 
     * @return file_description
     */
    public java.lang.String getFile_description() {
        return file_description;
    }


    /**
     * Sets the file_description value for this ProductFile.
     * 
     * @param file_description
     */
    public void setFile_description(java.lang.String file_description) {
        this.file_description = file_description;
    }


    /**
     * Gets the file_extension value for this ProductFile.
     * 
     * @return file_extension
     */
    public java.lang.String getFile_extension() {
        return file_extension;
    }


    /**
     * Sets the file_extension value for this ProductFile.
     * 
     * @param file_extension
     */
    public void setFile_extension(java.lang.String file_extension) {
        this.file_extension = file_extension;
    }


    /**
     * Gets the file_mimetype value for this ProductFile.
     * 
     * @return file_mimetype
     */
    public java.lang.String getFile_mimetype() {
        return file_mimetype;
    }


    /**
     * Sets the file_mimetype value for this ProductFile.
     * 
     * @param file_mimetype
     */
    public void setFile_mimetype(java.lang.String file_mimetype) {
        this.file_mimetype = file_mimetype;
    }


    /**
     * Gets the file_url value for this ProductFile.
     * 
     * @return file_url
     */
    public java.lang.String getFile_url() {
        return file_url;
    }


    /**
     * Sets the file_url value for this ProductFile.
     * 
     * @param file_url
     */
    public void setFile_url(java.lang.String file_url) {
        this.file_url = file_url;
    }


    /**
     * Gets the file_published value for this ProductFile.
     * 
     * @return file_published
     */
    public java.lang.String getFile_published() {
        return file_published;
    }


    /**
     * Sets the file_published value for this ProductFile.
     * 
     * @param file_published
     */
    public void setFile_published(java.lang.String file_published) {
        this.file_published = file_published;
    }


    /**
     * Gets the file_is_image value for this ProductFile.
     * 
     * @return file_is_image
     */
    public java.lang.String getFile_is_image() {
        return file_is_image;
    }


    /**
     * Sets the file_is_image value for this ProductFile.
     * 
     * @param file_is_image
     */
    public void setFile_is_image(java.lang.String file_is_image) {
        this.file_is_image = file_is_image;
    }


    /**
     * Gets the file_image_height value for this ProductFile.
     * 
     * @return file_image_height
     */
    public java.lang.String getFile_image_height() {
        return file_image_height;
    }


    /**
     * Sets the file_image_height value for this ProductFile.
     * 
     * @param file_image_height
     */
    public void setFile_image_height(java.lang.String file_image_height) {
        this.file_image_height = file_image_height;
    }


    /**
     * Gets the file_image_width value for this ProductFile.
     * 
     * @return file_image_width
     */
    public java.lang.String getFile_image_width() {
        return file_image_width;
    }


    /**
     * Sets the file_image_width value for this ProductFile.
     * 
     * @param file_image_width
     */
    public void setFile_image_width(java.lang.String file_image_width) {
        this.file_image_width = file_image_width;
    }


    /**
     * Gets the file_image_thumb_height value for this ProductFile.
     * 
     * @return file_image_thumb_height
     */
    public java.lang.String getFile_image_thumb_height() {
        return file_image_thumb_height;
    }


    /**
     * Sets the file_image_thumb_height value for this ProductFile.
     * 
     * @param file_image_thumb_height
     */
    public void setFile_image_thumb_height(java.lang.String file_image_thumb_height) {
        this.file_image_thumb_height = file_image_thumb_height;
    }


    /**
     * Gets the file_image_thumb_width value for this ProductFile.
     * 
     * @return file_image_thumb_width
     */
    public java.lang.String getFile_image_thumb_width() {
        return file_image_thumb_width;
    }


    /**
     * Sets the file_image_thumb_width value for this ProductFile.
     * 
     * @param file_image_thumb_width
     */
    public void setFile_image_thumb_width(java.lang.String file_image_thumb_width) {
        this.file_image_thumb_width = file_image_thumb_width;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof ProductFile)) return false;
        ProductFile other = (ProductFile) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.file_id==null && other.getFile_id()==null) || 
             (this.file_id!=null &&
              this.file_id.equals(other.getFile_id()))) &&
            ((this.file_product_id==null && other.getFile_product_id()==null) || 
             (this.file_product_id!=null &&
              this.file_product_id.equals(other.getFile_product_id()))) &&
            ((this.file_name==null && other.getFile_name()==null) || 
             (this.file_name!=null &&
              this.file_name.equals(other.getFile_name()))) &&
            ((this.file_title==null && other.getFile_title()==null) || 
             (this.file_title!=null &&
              this.file_title.equals(other.getFile_title()))) &&
            ((this.file_description==null && other.getFile_description()==null) || 
             (this.file_description!=null &&
              this.file_description.equals(other.getFile_description()))) &&
            ((this.file_extension==null && other.getFile_extension()==null) || 
             (this.file_extension!=null &&
              this.file_extension.equals(other.getFile_extension()))) &&
            ((this.file_mimetype==null && other.getFile_mimetype()==null) || 
             (this.file_mimetype!=null &&
              this.file_mimetype.equals(other.getFile_mimetype()))) &&
            ((this.file_url==null && other.getFile_url()==null) || 
             (this.file_url!=null &&
              this.file_url.equals(other.getFile_url()))) &&
            ((this.file_published==null && other.getFile_published()==null) || 
             (this.file_published!=null &&
              this.file_published.equals(other.getFile_published()))) &&
            ((this.file_is_image==null && other.getFile_is_image()==null) || 
             (this.file_is_image!=null &&
              this.file_is_image.equals(other.getFile_is_image()))) &&
            ((this.file_image_height==null && other.getFile_image_height()==null) || 
             (this.file_image_height!=null &&
              this.file_image_height.equals(other.getFile_image_height()))) &&
            ((this.file_image_width==null && other.getFile_image_width()==null) || 
             (this.file_image_width!=null &&
              this.file_image_width.equals(other.getFile_image_width()))) &&
            ((this.file_image_thumb_height==null && other.getFile_image_thumb_height()==null) || 
             (this.file_image_thumb_height!=null &&
              this.file_image_thumb_height.equals(other.getFile_image_thumb_height()))) &&
            ((this.file_image_thumb_width==null && other.getFile_image_thumb_width()==null) || 
             (this.file_image_thumb_width!=null &&
              this.file_image_thumb_width.equals(other.getFile_image_thumb_width())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getFile_id() != null) {
            _hashCode += getFile_id().hashCode();
        }
        if (getFile_product_id() != null) {
            _hashCode += getFile_product_id().hashCode();
        }
        if (getFile_name() != null) {
            _hashCode += getFile_name().hashCode();
        }
        if (getFile_title() != null) {
            _hashCode += getFile_title().hashCode();
        }
        if (getFile_description() != null) {
            _hashCode += getFile_description().hashCode();
        }
        if (getFile_extension() != null) {
            _hashCode += getFile_extension().hashCode();
        }
        if (getFile_mimetype() != null) {
            _hashCode += getFile_mimetype().hashCode();
        }
        if (getFile_url() != null) {
            _hashCode += getFile_url().hashCode();
        }
        if (getFile_published() != null) {
            _hashCode += getFile_published().hashCode();
        }
        if (getFile_is_image() != null) {
            _hashCode += getFile_is_image().hashCode();
        }
        if (getFile_image_height() != null) {
            _hashCode += getFile_image_height().hashCode();
        }
        if (getFile_image_width() != null) {
            _hashCode += getFile_image_width().hashCode();
        }
        if (getFile_image_thumb_height() != null) {
            _hashCode += getFile_image_thumb_height().hashCode();
        }
        if (getFile_image_thumb_width() != null) {
            _hashCode += getFile_image_thumb_width().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(ProductFile.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://www.virtuemart.net/VM_Product/", "ProductFile"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("file_id");
        elemField.setXmlName(new javax.xml.namespace.QName("", "file_id"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("file_product_id");
        elemField.setXmlName(new javax.xml.namespace.QName("", "file_product_id"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("file_name");
        elemField.setXmlName(new javax.xml.namespace.QName("", "file_name"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("file_title");
        elemField.setXmlName(new javax.xml.namespace.QName("", "file_title"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("file_description");
        elemField.setXmlName(new javax.xml.namespace.QName("", "file_description"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("file_extension");
        elemField.setXmlName(new javax.xml.namespace.QName("", "file_extension"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("file_mimetype");
        elemField.setXmlName(new javax.xml.namespace.QName("", "file_mimetype"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("file_url");
        elemField.setXmlName(new javax.xml.namespace.QName("", "file_url"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("file_published");
        elemField.setXmlName(new javax.xml.namespace.QName("", "file_published"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("file_is_image");
        elemField.setXmlName(new javax.xml.namespace.QName("", "file_is_image"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("file_image_height");
        elemField.setXmlName(new javax.xml.namespace.QName("", "file_image_height"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("file_image_width");
        elemField.setXmlName(new javax.xml.namespace.QName("", "file_image_width"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("file_image_thumb_height");
        elemField.setXmlName(new javax.xml.namespace.QName("", "file_image_thumb_height"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("file_image_thumb_width");
        elemField.setXmlName(new javax.xml.namespace.QName("", "file_image_thumb_width"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
