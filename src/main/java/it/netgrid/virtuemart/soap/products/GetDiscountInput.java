/**
 * GetDiscountInput.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package it.netgrid.virtuemart.soap.products;

public class GetDiscountInput  implements java.io.Serializable {
    private it.netgrid.virtuemart.soap.products.LoginInfo loginInfo;

    private java.lang.String discount_id;

    private java.lang.String calc_name;

    private java.lang.String calc_kind;

    public GetDiscountInput() {
    }

    public GetDiscountInput(
           it.netgrid.virtuemart.soap.products.LoginInfo loginInfo,
           java.lang.String discount_id,
           java.lang.String calc_name,
           java.lang.String calc_kind) {
           this.loginInfo = loginInfo;
           this.discount_id = discount_id;
           this.calc_name = calc_name;
           this.calc_kind = calc_kind;
    }


    /**
     * Gets the loginInfo value for this GetDiscountInput.
     * 
     * @return loginInfo
     */
    public it.netgrid.virtuemart.soap.products.LoginInfo getLoginInfo() {
        return loginInfo;
    }


    /**
     * Sets the loginInfo value for this GetDiscountInput.
     * 
     * @param loginInfo
     */
    public void setLoginInfo(it.netgrid.virtuemart.soap.products.LoginInfo loginInfo) {
        this.loginInfo = loginInfo;
    }


    /**
     * Gets the discount_id value for this GetDiscountInput.
     * 
     * @return discount_id
     */
    public java.lang.String getDiscount_id() {
        return discount_id;
    }


    /**
     * Sets the discount_id value for this GetDiscountInput.
     * 
     * @param discount_id
     */
    public void setDiscount_id(java.lang.String discount_id) {
        this.discount_id = discount_id;
    }


    /**
     * Gets the calc_name value for this GetDiscountInput.
     * 
     * @return calc_name
     */
    public java.lang.String getCalc_name() {
        return calc_name;
    }


    /**
     * Sets the calc_name value for this GetDiscountInput.
     * 
     * @param calc_name
     */
    public void setCalc_name(java.lang.String calc_name) {
        this.calc_name = calc_name;
    }


    /**
     * Gets the calc_kind value for this GetDiscountInput.
     * 
     * @return calc_kind
     */
    public java.lang.String getCalc_kind() {
        return calc_kind;
    }


    /**
     * Sets the calc_kind value for this GetDiscountInput.
     * 
     * @param calc_kind
     */
    public void setCalc_kind(java.lang.String calc_kind) {
        this.calc_kind = calc_kind;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof GetDiscountInput)) return false;
        GetDiscountInput other = (GetDiscountInput) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.loginInfo==null && other.getLoginInfo()==null) || 
             (this.loginInfo!=null &&
              this.loginInfo.equals(other.getLoginInfo()))) &&
            ((this.discount_id==null && other.getDiscount_id()==null) || 
             (this.discount_id!=null &&
              this.discount_id.equals(other.getDiscount_id()))) &&
            ((this.calc_name==null && other.getCalc_name()==null) || 
             (this.calc_name!=null &&
              this.calc_name.equals(other.getCalc_name()))) &&
            ((this.calc_kind==null && other.getCalc_kind()==null) || 
             (this.calc_kind!=null &&
              this.calc_kind.equals(other.getCalc_kind())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getLoginInfo() != null) {
            _hashCode += getLoginInfo().hashCode();
        }
        if (getDiscount_id() != null) {
            _hashCode += getDiscount_id().hashCode();
        }
        if (getCalc_name() != null) {
            _hashCode += getCalc_name().hashCode();
        }
        if (getCalc_kind() != null) {
            _hashCode += getCalc_kind().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(GetDiscountInput.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://www.virtuemart.net/VM_Product/", "GetDiscountInput"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("loginInfo");
        elemField.setXmlName(new javax.xml.namespace.QName("", "loginInfo"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.virtuemart.net/VM_Product/", "loginInfo"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("discount_id");
        elemField.setXmlName(new javax.xml.namespace.QName("", "discount_id"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("calc_name");
        elemField.setXmlName(new javax.xml.namespace.QName("", "calc_name"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("calc_kind");
        elemField.setXmlName(new javax.xml.namespace.QName("", "calc_kind"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
