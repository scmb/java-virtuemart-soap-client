package it.netgrid.virtuemart.soap.products;

public class VM_ProductProxy implements it.netgrid.virtuemart.soap.products.VM_Product_PortType {
  private String _endpoint = null;
  private it.netgrid.virtuemart.soap.products.VM_Product_PortType vM_Product_PortType = null;
  
  public VM_ProductProxy() {
    _initVM_ProductProxy();
  }
  
  public VM_ProductProxy(String endpoint) {
    _endpoint = endpoint;
    _initVM_ProductProxy();
  }
  
  private void _initVM_ProductProxy() {
    try {
      vM_Product_PortType = (new it.netgrid.virtuemart.soap.products.VM_Product_ServiceLocator()).getVM_ProductSOAP();
      if (vM_Product_PortType != null) {
        if (_endpoint != null)
          ((javax.xml.rpc.Stub)vM_Product_PortType)._setProperty("javax.xml.rpc.service.endpoint.address", _endpoint);
        else
          _endpoint = (String)((javax.xml.rpc.Stub)vM_Product_PortType)._getProperty("javax.xml.rpc.service.endpoint.address");
      }
      
    }
    catch (javax.xml.rpc.ServiceException serviceException) {}
  }
  
  public String getEndpoint() {
    return _endpoint;
  }
  
  public void setEndpoint(String endpoint) {
    _endpoint = endpoint;
    if (vM_Product_PortType != null)
      ((javax.xml.rpc.Stub)vM_Product_PortType)._setProperty("javax.xml.rpc.service.endpoint.address", _endpoint);
    
  }
  
  public it.netgrid.virtuemart.soap.products.VM_Product_PortType getVM_Product_PortType() {
    if (vM_Product_PortType == null)
      _initVM_ProductProxy();
    return vM_Product_PortType;
  }
  
  public it.netgrid.virtuemart.soap.products.Product[] getProductsFromCategory(it.netgrid.virtuemart.soap.products.ProductFromCatIdInput parameters) throws java.rmi.RemoteException{
    if (vM_Product_PortType == null)
      _initVM_ProductProxy();
    return vM_Product_PortType.getProductsFromCategory(parameters);
  }
  
  public it.netgrid.virtuemart.soap.products.Product[] getChildsProduct(it.netgrid.virtuemart.soap.products.ProductFromIdInput parameters) throws java.rmi.RemoteException{
    if (vM_Product_PortType == null)
      _initVM_ProductProxy();
    return vM_Product_PortType.getChildsProduct(parameters);
  }
  
  public it.netgrid.virtuemart.soap.products.Product getProductFromId(it.netgrid.virtuemart.soap.products.ProductFromIdInput parameters) throws java.rmi.RemoteException{
    if (vM_Product_PortType == null)
      _initVM_ProductProxy();
    return vM_Product_PortType.getProductFromId(parameters);
  }
  
  public it.netgrid.virtuemart.soap.products.Product getProductFromSKU(it.netgrid.virtuemart.soap.products.ProductFromSKUInput parameters) throws java.rmi.RemoteException{
    if (vM_Product_PortType == null)
      _initVM_ProductProxy();
    return vM_Product_PortType.getProductFromSKU(parameters);
  }
  
  public it.netgrid.virtuemart.soap.products.CommonReturn updateProduct(it.netgrid.virtuemart.soap.products.UpdateProductInput parameters) throws java.rmi.RemoteException{
    if (vM_Product_PortType == null)
      _initVM_ProductProxy();
    return vM_Product_PortType.updateProduct(parameters);
  }
  
  public it.netgrid.virtuemart.soap.products.OrderItemInfo[] getProductsFromOrderId(it.netgrid.virtuemart.soap.products.ProductsFromOrderIdInput parameters) throws java.rmi.RemoteException{
    if (vM_Product_PortType == null)
      _initVM_ProductProxy();
    return vM_Product_PortType.getProductsFromOrderId(parameters);
  }
  
  public it.netgrid.virtuemart.soap.products.CommonReturn addProduct(it.netgrid.virtuemart.soap.products.UpdateProductInput parameters) throws java.rmi.RemoteException{
    if (vM_Product_PortType == null)
      _initVM_ProductProxy();
    return vM_Product_PortType.addProduct(parameters);
  }
  
  public it.netgrid.virtuemart.soap.products.CommonReturn deleteProduct(it.netgrid.virtuemart.soap.products.ProductFromIdInput parameters) throws java.rmi.RemoteException{
    if (vM_Product_PortType == null)
      _initVM_ProductProxy();
    return vM_Product_PortType.deleteProduct(parameters);
  }
  
  public it.netgrid.virtuemart.soap.products.Currency[] getAllCurrency(it.netgrid.virtuemart.soap.products.GetAllCurrencyInput parameters) throws java.rmi.RemoteException{
    if (vM_Product_PortType == null)
      _initVM_ProductProxy();
    return vM_Product_PortType.getAllCurrency(parameters);
  }
  
  public it.netgrid.virtuemart.soap.products.Tax[] getAllTax(it.netgrid.virtuemart.soap.products.LoginInfo parameters) throws java.rmi.RemoteException{
    if (vM_Product_PortType == null)
      _initVM_ProductProxy();
    return vM_Product_PortType.getAllTax(parameters);
  }
  
  public it.netgrid.virtuemart.soap.products.CommonReturn addTax(it.netgrid.virtuemart.soap.products.TaxesInput parameters) throws java.rmi.RemoteException{
    if (vM_Product_PortType == null)
      _initVM_ProductProxy();
    return vM_Product_PortType.addTax(parameters);
  }
  
  public it.netgrid.virtuemart.soap.products.CommonReturn updateTax(it.netgrid.virtuemart.soap.products.TaxesInput parameters) throws java.rmi.RemoteException{
    if (vM_Product_PortType == null)
      _initVM_ProductProxy();
    return vM_Product_PortType.updateTax(parameters);
  }
  
  public it.netgrid.virtuemart.soap.products.CommonReturn deleteTax(it.netgrid.virtuemart.soap.products.DelInput parameters) throws java.rmi.RemoteException{
    if (vM_Product_PortType == null)
      _initVM_ProductProxy();
    return vM_Product_PortType.deleteTax(parameters);
  }
  
  public it.netgrid.virtuemart.soap.products.Product[] getAllProducts(it.netgrid.virtuemart.soap.products.GetAllProductsInput parameters) throws java.rmi.RemoteException{
    if (vM_Product_PortType == null)
      _initVM_ProductProxy();
    return vM_Product_PortType.getAllProducts(parameters);
  }
  
  public it.netgrid.virtuemart.soap.products.AvalaibleImage[] getAvailableImages(it.netgrid.virtuemart.soap.products.GetAvalaibleImageInput parameters) throws java.rmi.RemoteException{
    if (vM_Product_PortType == null)
      _initVM_ProductProxy();
    return vM_Product_PortType.getAvailableImages(parameters);
  }
  
  public it.netgrid.virtuemart.soap.products.ProductPrice[] getProductPrices(it.netgrid.virtuemart.soap.products.ProductPriceInput parameters) throws java.rmi.RemoteException{
    if (vM_Product_PortType == null)
      _initVM_ProductProxy();
    return vM_Product_PortType.getProductPrices(parameters);
  }
  
  public it.netgrid.virtuemart.soap.products.CommonReturn addProductPrices(it.netgrid.virtuemart.soap.products.AddProductPricesInput parameters) throws java.rmi.RemoteException{
    if (vM_Product_PortType == null)
      _initVM_ProductProxy();
    return vM_Product_PortType.addProductPrices(parameters);
  }
  
  public it.netgrid.virtuemart.soap.products.CommonReturn updateProductPrices(it.netgrid.virtuemart.soap.products.AddProductPricesInput parameters) throws java.rmi.RemoteException{
    if (vM_Product_PortType == null)
      _initVM_ProductProxy();
    return vM_Product_PortType.updateProductPrices(parameters);
  }
  
  public it.netgrid.virtuemart.soap.products.CommonReturn deleteProductPrices(it.netgrid.virtuemart.soap.products.DelInput parameters) throws java.rmi.RemoteException{
    if (vM_Product_PortType == null)
      _initVM_ProductProxy();
    return vM_Product_PortType.deleteProductPrices(parameters);
  }
  
  public it.netgrid.virtuemart.soap.products.Discount[] getDiscount(it.netgrid.virtuemart.soap.products.GetDiscountInput parameters) throws java.rmi.RemoteException{
    if (vM_Product_PortType == null)
      _initVM_ProductProxy();
    return vM_Product_PortType.getDiscount(parameters);
  }
  
  public it.netgrid.virtuemart.soap.products.CommonReturn addDiscount(it.netgrid.virtuemart.soap.products.AddDiscountInput parameters) throws java.rmi.RemoteException{
    if (vM_Product_PortType == null)
      _initVM_ProductProxy();
    return vM_Product_PortType.addDiscount(parameters);
  }
  
  public it.netgrid.virtuemart.soap.products.CommonReturn updateDiscount(it.netgrid.virtuemart.soap.products.AddDiscountInput parameters) throws java.rmi.RemoteException{
    if (vM_Product_PortType == null)
      _initVM_ProductProxy();
    return vM_Product_PortType.updateDiscount(parameters);
  }
  
  public it.netgrid.virtuemart.soap.products.CommonReturn deleteDiscount(it.netgrid.virtuemart.soap.products.DelInput parameters) throws java.rmi.RemoteException{
    if (vM_Product_PortType == null)
      _initVM_ProductProxy();
    return vM_Product_PortType.deleteDiscount(parameters);
  }
  
  public it.netgrid.virtuemart.soap.products.CommonReturn updateProductDiscount(it.netgrid.virtuemart.soap.products.UpdateProductDiscountInput parameters) throws java.rmi.RemoteException{
    if (vM_Product_PortType == null)
      _initVM_ProductProxy();
    return vM_Product_PortType.updateProductDiscount(parameters);
  }
  
  public it.netgrid.virtuemart.soap.products.ProductFile[] getProductFile(it.netgrid.virtuemart.soap.products.GetProductFileInput parameters) throws java.rmi.RemoteException{
    if (vM_Product_PortType == null)
      _initVM_ProductProxy();
    return vM_Product_PortType.getProductFile(parameters);
  }
  
  public it.netgrid.virtuemart.soap.products.CommonReturn addProductFile(it.netgrid.virtuemart.soap.products.AddProductFileInput parameters) throws java.rmi.RemoteException{
    if (vM_Product_PortType == null)
      _initVM_ProductProxy();
    return vM_Product_PortType.addProductFile(parameters);
  }
  
  public it.netgrid.virtuemart.soap.products.CommonReturn updateProductFile(it.netgrid.virtuemart.soap.products.AddProductFileInput parameters) throws java.rmi.RemoteException{
    if (vM_Product_PortType == null)
      _initVM_ProductProxy();
    return vM_Product_PortType.updateProductFile(parameters);
  }
  
  public it.netgrid.virtuemart.soap.products.CommonReturn deleteProductFile(it.netgrid.virtuemart.soap.products.DelInput parameters) throws java.rmi.RemoteException{
    if (vM_Product_PortType == null)
      _initVM_ProductProxy();
    return vM_Product_PortType.deleteProductFile(parameters);
  }
  
  public it.netgrid.virtuemart.soap.products.AvalaibleFile[] getAvailableFiles(it.netgrid.virtuemart.soap.products.LoginInfo parameters) throws java.rmi.RemoteException{
    if (vM_Product_PortType == null)
      _initVM_ProductProxy();
    return vM_Product_PortType.getAvailableFiles(parameters);
  }
  
  public it.netgrid.virtuemart.soap.products.Product[] searchProducts(it.netgrid.virtuemart.soap.products.SearchProductsInput parameters) throws java.rmi.RemoteException{
    if (vM_Product_PortType == null)
      _initVM_ProductProxy();
    return vM_Product_PortType.searchProducts(parameters);
  }
  
  public it.netgrid.virtuemart.soap.products.ProductVote[] getProductVote(it.netgrid.virtuemart.soap.products.ProductVoteInput parameters) throws java.rmi.RemoteException{
    if (vM_Product_PortType == null)
      _initVM_ProductProxy();
    return vM_Product_PortType.getProductVote(parameters);
  }
  
  public it.netgrid.virtuemart.soap.products.ProductReview[] getProductReviews(it.netgrid.virtuemart.soap.products.ProductReviewInput parameters) throws java.rmi.RemoteException{
    if (vM_Product_PortType == null)
      _initVM_ProductProxy();
    return vM_Product_PortType.getProductReviews(parameters);
  }
  
  public it.netgrid.virtuemart.soap.products.CommonReturn publishReviews(it.netgrid.virtuemart.soap.products.PublishReviewsInput parameters) throws java.rmi.RemoteException{
    if (vM_Product_PortType == null)
      _initVM_ProductProxy();
    return vM_Product_PortType.publishReviews(parameters);
  }
  
  public it.netgrid.virtuemart.soap.products.Product[] getRelatedProducts(it.netgrid.virtuemart.soap.products.GetRelatedProductsInput parameters) throws java.rmi.RemoteException{
    if (vM_Product_PortType == null)
      _initVM_ProductProxy();
    return vM_Product_PortType.getRelatedProducts(parameters);
  }
  
  public it.netgrid.virtuemart.soap.products.CommonReturn setRelatedProducts(it.netgrid.virtuemart.soap.products.SetRelatedProductsInput parameters) throws java.rmi.RemoteException{
    if (vM_Product_PortType == null)
      _initVM_ProductProxy();
    return vM_Product_PortType.setRelatedProducts(parameters);
  }
  
  public it.netgrid.virtuemart.soap.products.Media[] getMediaProduct(it.netgrid.virtuemart.soap.products.GetMediaProductsInput parameters) throws java.rmi.RemoteException{
    if (vM_Product_PortType == null)
      _initVM_ProductProxy();
    return vM_Product_PortType.getMediaProduct(parameters);
  }
  
  public it.netgrid.virtuemart.soap.products.CommonReturn addMediaProduct(it.netgrid.virtuemart.soap.products.AddMediaProductInput parameters) throws java.rmi.RemoteException{
    if (vM_Product_PortType == null)
      _initVM_ProductProxy();
    return vM_Product_PortType.addMediaProduct(parameters);
  }
  
  public it.netgrid.virtuemart.soap.products.Custom[] getCustomsList(it.netgrid.virtuemart.soap.products.GetCustomsListInput parameters) throws java.rmi.RemoteException{
    if (vM_Product_PortType == null)
      _initVM_ProductProxy();
    return vM_Product_PortType.getCustomsList(parameters);
  }
  
  public it.netgrid.virtuemart.soap.products.CustomField[] getCustomsFields(it.netgrid.virtuemart.soap.products.GetCustomsFieldsInput parameters) throws java.rmi.RemoteException{
    if (vM_Product_PortType == null)
      _initVM_ProductProxy();
    return vM_Product_PortType.getCustomsFields(parameters);
  }
  
  public it.netgrid.virtuemart.soap.products.CommonReturn deleteMediaProduct(it.netgrid.virtuemart.soap.products.DelMediaInput parameters) throws java.rmi.RemoteException{
    if (vM_Product_PortType == null)
      _initVM_ProductProxy();
    return vM_Product_PortType.deleteMediaProduct(parameters);
  }
  
  public it.netgrid.virtuemart.soap.products.CommonReturn addCustomField(it.netgrid.virtuemart.soap.products.AddCustomFieldInput parameters) throws java.rmi.RemoteException{
    if (vM_Product_PortType == null)
      _initVM_ProductProxy();
    return vM_Product_PortType.addCustomField(parameters);
  }
  
  public it.netgrid.virtuemart.soap.products.CommonReturn updateCustomField(it.netgrid.virtuemart.soap.products.AddCustomFieldInput parameters) throws java.rmi.RemoteException{
    if (vM_Product_PortType == null)
      _initVM_ProductProxy();
    return vM_Product_PortType.updateCustomField(parameters);
  }
  
  public it.netgrid.virtuemart.soap.products.CommonReturn deleteCustomField(it.netgrid.virtuemart.soap.products.AddCustomFieldInput parameters) throws java.rmi.RemoteException{
    if (vM_Product_PortType == null)
      _initVM_ProductProxy();
    return vM_Product_PortType.deleteCustomField(parameters);
  }
  
  public it.netgrid.virtuemart.soap.products.Worldzone[] getWorldZones(it.netgrid.virtuemart.soap.products.GetWorldzonesInput parameters) throws java.rmi.RemoteException{
    if (vM_Product_PortType == null)
      _initVM_ProductProxy();
    return vM_Product_PortType.getWorldZones(parameters);
  }
  
  public it.netgrid.virtuemart.soap.products.CommonReturn addWorldZones(it.netgrid.virtuemart.soap.products.GetWorldzonesInput parameters) throws java.rmi.RemoteException{
    if (vM_Product_PortType == null)
      _initVM_ProductProxy();
    return vM_Product_PortType.addWorldZones(parameters);
  }
  
  public it.netgrid.virtuemart.soap.products.CommonReturn updateWorldZones(it.netgrid.virtuemart.soap.products.GetWorldzonesInput parameters) throws java.rmi.RemoteException{
    if (vM_Product_PortType == null)
      _initVM_ProductProxy();
    return vM_Product_PortType.updateWorldZones(parameters);
  }
  
  public it.netgrid.virtuemart.soap.products.CommonReturn deleteWorldZones(it.netgrid.virtuemart.soap.products.GetWorldzonesInput parameters) throws java.rmi.RemoteException{
    if (vM_Product_PortType == null)
      _initVM_ProductProxy();
    return vM_Product_PortType.deleteWorldZones(parameters);
  }
  
  public it.netgrid.virtuemart.soap.products.CommonReturn addCustomsList(it.netgrid.virtuemart.soap.products.AddCustomListInput parameters) throws java.rmi.RemoteException{
    if (vM_Product_PortType == null)
      _initVM_ProductProxy();
    return vM_Product_PortType.addCustomsList(parameters);
  }
  
  public it.netgrid.virtuemart.soap.products.CommonReturn updateCustomsList(it.netgrid.virtuemart.soap.products.AddCustomListInput parameters) throws java.rmi.RemoteException{
    if (vM_Product_PortType == null)
      _initVM_ProductProxy();
    return vM_Product_PortType.updateCustomsList(parameters);
  }
  
  public it.netgrid.virtuemart.soap.products.CommonReturn deleteCustomsList(it.netgrid.virtuemart.soap.products.AddCustomListInput parameters) throws java.rmi.RemoteException{
    if (vM_Product_PortType == null)
      _initVM_ProductProxy();
    return vM_Product_PortType.deleteCustomsList(parameters);
  }
  
  
}