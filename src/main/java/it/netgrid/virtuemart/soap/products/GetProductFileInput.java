/**
 * GetProductFileInput.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package it.netgrid.virtuemart.soap.products;

public class GetProductFileInput  implements java.io.Serializable {
    private it.netgrid.virtuemart.soap.products.LoginInfo loginInfo;

    private java.lang.String file_id;

    private java.lang.String file_product_id;

    private java.lang.String file_name;

    private java.lang.String file_published;

    private java.lang.String file_extension;

    private java.lang.String file_is_image;

    public GetProductFileInput() {
    }

    public GetProductFileInput(
           it.netgrid.virtuemart.soap.products.LoginInfo loginInfo,
           java.lang.String file_id,
           java.lang.String file_product_id,
           java.lang.String file_name,
           java.lang.String file_published,
           java.lang.String file_extension,
           java.lang.String file_is_image) {
           this.loginInfo = loginInfo;
           this.file_id = file_id;
           this.file_product_id = file_product_id;
           this.file_name = file_name;
           this.file_published = file_published;
           this.file_extension = file_extension;
           this.file_is_image = file_is_image;
    }


    /**
     * Gets the loginInfo value for this GetProductFileInput.
     * 
     * @return loginInfo
     */
    public it.netgrid.virtuemart.soap.products.LoginInfo getLoginInfo() {
        return loginInfo;
    }


    /**
     * Sets the loginInfo value for this GetProductFileInput.
     * 
     * @param loginInfo
     */
    public void setLoginInfo(it.netgrid.virtuemart.soap.products.LoginInfo loginInfo) {
        this.loginInfo = loginInfo;
    }


    /**
     * Gets the file_id value for this GetProductFileInput.
     * 
     * @return file_id
     */
    public java.lang.String getFile_id() {
        return file_id;
    }


    /**
     * Sets the file_id value for this GetProductFileInput.
     * 
     * @param file_id
     */
    public void setFile_id(java.lang.String file_id) {
        this.file_id = file_id;
    }


    /**
     * Gets the file_product_id value for this GetProductFileInput.
     * 
     * @return file_product_id
     */
    public java.lang.String getFile_product_id() {
        return file_product_id;
    }


    /**
     * Sets the file_product_id value for this GetProductFileInput.
     * 
     * @param file_product_id
     */
    public void setFile_product_id(java.lang.String file_product_id) {
        this.file_product_id = file_product_id;
    }


    /**
     * Gets the file_name value for this GetProductFileInput.
     * 
     * @return file_name
     */
    public java.lang.String getFile_name() {
        return file_name;
    }


    /**
     * Sets the file_name value for this GetProductFileInput.
     * 
     * @param file_name
     */
    public void setFile_name(java.lang.String file_name) {
        this.file_name = file_name;
    }


    /**
     * Gets the file_published value for this GetProductFileInput.
     * 
     * @return file_published
     */
    public java.lang.String getFile_published() {
        return file_published;
    }


    /**
     * Sets the file_published value for this GetProductFileInput.
     * 
     * @param file_published
     */
    public void setFile_published(java.lang.String file_published) {
        this.file_published = file_published;
    }


    /**
     * Gets the file_extension value for this GetProductFileInput.
     * 
     * @return file_extension
     */
    public java.lang.String getFile_extension() {
        return file_extension;
    }


    /**
     * Sets the file_extension value for this GetProductFileInput.
     * 
     * @param file_extension
     */
    public void setFile_extension(java.lang.String file_extension) {
        this.file_extension = file_extension;
    }


    /**
     * Gets the file_is_image value for this GetProductFileInput.
     * 
     * @return file_is_image
     */
    public java.lang.String getFile_is_image() {
        return file_is_image;
    }


    /**
     * Sets the file_is_image value for this GetProductFileInput.
     * 
     * @param file_is_image
     */
    public void setFile_is_image(java.lang.String file_is_image) {
        this.file_is_image = file_is_image;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof GetProductFileInput)) return false;
        GetProductFileInput other = (GetProductFileInput) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.loginInfo==null && other.getLoginInfo()==null) || 
             (this.loginInfo!=null &&
              this.loginInfo.equals(other.getLoginInfo()))) &&
            ((this.file_id==null && other.getFile_id()==null) || 
             (this.file_id!=null &&
              this.file_id.equals(other.getFile_id()))) &&
            ((this.file_product_id==null && other.getFile_product_id()==null) || 
             (this.file_product_id!=null &&
              this.file_product_id.equals(other.getFile_product_id()))) &&
            ((this.file_name==null && other.getFile_name()==null) || 
             (this.file_name!=null &&
              this.file_name.equals(other.getFile_name()))) &&
            ((this.file_published==null && other.getFile_published()==null) || 
             (this.file_published!=null &&
              this.file_published.equals(other.getFile_published()))) &&
            ((this.file_extension==null && other.getFile_extension()==null) || 
             (this.file_extension!=null &&
              this.file_extension.equals(other.getFile_extension()))) &&
            ((this.file_is_image==null && other.getFile_is_image()==null) || 
             (this.file_is_image!=null &&
              this.file_is_image.equals(other.getFile_is_image())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getLoginInfo() != null) {
            _hashCode += getLoginInfo().hashCode();
        }
        if (getFile_id() != null) {
            _hashCode += getFile_id().hashCode();
        }
        if (getFile_product_id() != null) {
            _hashCode += getFile_product_id().hashCode();
        }
        if (getFile_name() != null) {
            _hashCode += getFile_name().hashCode();
        }
        if (getFile_published() != null) {
            _hashCode += getFile_published().hashCode();
        }
        if (getFile_extension() != null) {
            _hashCode += getFile_extension().hashCode();
        }
        if (getFile_is_image() != null) {
            _hashCode += getFile_is_image().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(GetProductFileInput.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://www.virtuemart.net/VM_Product/", "GetProductFileInput"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("loginInfo");
        elemField.setXmlName(new javax.xml.namespace.QName("", "loginInfo"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.virtuemart.net/VM_Product/", "loginInfo"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("file_id");
        elemField.setXmlName(new javax.xml.namespace.QName("", "file_id"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("file_product_id");
        elemField.setXmlName(new javax.xml.namespace.QName("", "file_product_id"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("file_name");
        elemField.setXmlName(new javax.xml.namespace.QName("", "file_name"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("file_published");
        elemField.setXmlName(new javax.xml.namespace.QName("", "file_published"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("file_extension");
        elemField.setXmlName(new javax.xml.namespace.QName("", "file_extension"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("file_is_image");
        elemField.setXmlName(new javax.xml.namespace.QName("", "file_is_image"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
