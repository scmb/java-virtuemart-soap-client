/**
 * Custom.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package it.netgrid.virtuemart.soap.products;

public class Custom  implements java.io.Serializable {
    private java.lang.String virtuemart_custom_id;

    private java.lang.String custom_parent_id;

    private java.lang.String admin_only;

    private java.lang.String custom_title;

    private java.lang.String custom_tip;

    private java.lang.String custom_value;

    private java.lang.String custom_field_desc;

    private java.lang.String field_type;

    private java.lang.String is_list;

    private java.lang.String is_hidden;

    private java.lang.String is_cart_attribute;

    private java.lang.String published;

    private java.lang.String virtuemart_vendor_id;

    private java.lang.String custom_jplugin_id;

    private java.lang.String custom_element;

    private java.lang.String layout_pos;

    private java.lang.String custom_params;

    public Custom() {
    }

    public Custom(
           java.lang.String virtuemart_custom_id,
           java.lang.String custom_parent_id,
           java.lang.String admin_only,
           java.lang.String custom_title,
           java.lang.String custom_tip,
           java.lang.String custom_value,
           java.lang.String custom_field_desc,
           java.lang.String field_type,
           java.lang.String is_list,
           java.lang.String is_hidden,
           java.lang.String is_cart_attribute,
           java.lang.String published,
           java.lang.String virtuemart_vendor_id,
           java.lang.String custom_jplugin_id,
           java.lang.String custom_element,
           java.lang.String layout_pos,
           java.lang.String custom_params) {
           this.virtuemart_custom_id = virtuemart_custom_id;
           this.custom_parent_id = custom_parent_id;
           this.admin_only = admin_only;
           this.custom_title = custom_title;
           this.custom_tip = custom_tip;
           this.custom_value = custom_value;
           this.custom_field_desc = custom_field_desc;
           this.field_type = field_type;
           this.is_list = is_list;
           this.is_hidden = is_hidden;
           this.is_cart_attribute = is_cart_attribute;
           this.published = published;
           this.virtuemart_vendor_id = virtuemart_vendor_id;
           this.custom_jplugin_id = custom_jplugin_id;
           this.custom_element = custom_element;
           this.layout_pos = layout_pos;
           this.custom_params = custom_params;
    }


    /**
     * Gets the virtuemart_custom_id value for this Custom.
     * 
     * @return virtuemart_custom_id
     */
    public java.lang.String getVirtuemart_custom_id() {
        return virtuemart_custom_id;
    }


    /**
     * Sets the virtuemart_custom_id value for this Custom.
     * 
     * @param virtuemart_custom_id
     */
    public void setVirtuemart_custom_id(java.lang.String virtuemart_custom_id) {
        this.virtuemart_custom_id = virtuemart_custom_id;
    }


    /**
     * Gets the custom_parent_id value for this Custom.
     * 
     * @return custom_parent_id
     */
    public java.lang.String getCustom_parent_id() {
        return custom_parent_id;
    }


    /**
     * Sets the custom_parent_id value for this Custom.
     * 
     * @param custom_parent_id
     */
    public void setCustom_parent_id(java.lang.String custom_parent_id) {
        this.custom_parent_id = custom_parent_id;
    }


    /**
     * Gets the admin_only value for this Custom.
     * 
     * @return admin_only
     */
    public java.lang.String getAdmin_only() {
        return admin_only;
    }


    /**
     * Sets the admin_only value for this Custom.
     * 
     * @param admin_only
     */
    public void setAdmin_only(java.lang.String admin_only) {
        this.admin_only = admin_only;
    }


    /**
     * Gets the custom_title value for this Custom.
     * 
     * @return custom_title
     */
    public java.lang.String getCustom_title() {
        return custom_title;
    }


    /**
     * Sets the custom_title value for this Custom.
     * 
     * @param custom_title
     */
    public void setCustom_title(java.lang.String custom_title) {
        this.custom_title = custom_title;
    }


    /**
     * Gets the custom_tip value for this Custom.
     * 
     * @return custom_tip
     */
    public java.lang.String getCustom_tip() {
        return custom_tip;
    }


    /**
     * Sets the custom_tip value for this Custom.
     * 
     * @param custom_tip
     */
    public void setCustom_tip(java.lang.String custom_tip) {
        this.custom_tip = custom_tip;
    }


    /**
     * Gets the custom_value value for this Custom.
     * 
     * @return custom_value
     */
    public java.lang.String getCustom_value() {
        return custom_value;
    }


    /**
     * Sets the custom_value value for this Custom.
     * 
     * @param custom_value
     */
    public void setCustom_value(java.lang.String custom_value) {
        this.custom_value = custom_value;
    }


    /**
     * Gets the custom_field_desc value for this Custom.
     * 
     * @return custom_field_desc
     */
    public java.lang.String getCustom_field_desc() {
        return custom_field_desc;
    }


    /**
     * Sets the custom_field_desc value for this Custom.
     * 
     * @param custom_field_desc
     */
    public void setCustom_field_desc(java.lang.String custom_field_desc) {
        this.custom_field_desc = custom_field_desc;
    }


    /**
     * Gets the field_type value for this Custom.
     * 
     * @return field_type
     */
    public java.lang.String getField_type() {
        return field_type;
    }


    /**
     * Sets the field_type value for this Custom.
     * 
     * @param field_type
     */
    public void setField_type(java.lang.String field_type) {
        this.field_type = field_type;
    }


    /**
     * Gets the is_list value for this Custom.
     * 
     * @return is_list
     */
    public java.lang.String getIs_list() {
        return is_list;
    }


    /**
     * Sets the is_list value for this Custom.
     * 
     * @param is_list
     */
    public void setIs_list(java.lang.String is_list) {
        this.is_list = is_list;
    }


    /**
     * Gets the is_hidden value for this Custom.
     * 
     * @return is_hidden
     */
    public java.lang.String getIs_hidden() {
        return is_hidden;
    }


    /**
     * Sets the is_hidden value for this Custom.
     * 
     * @param is_hidden
     */
    public void setIs_hidden(java.lang.String is_hidden) {
        this.is_hidden = is_hidden;
    }


    /**
     * Gets the is_cart_attribute value for this Custom.
     * 
     * @return is_cart_attribute
     */
    public java.lang.String getIs_cart_attribute() {
        return is_cart_attribute;
    }


    /**
     * Sets the is_cart_attribute value for this Custom.
     * 
     * @param is_cart_attribute
     */
    public void setIs_cart_attribute(java.lang.String is_cart_attribute) {
        this.is_cart_attribute = is_cart_attribute;
    }


    /**
     * Gets the published value for this Custom.
     * 
     * @return published
     */
    public java.lang.String getPublished() {
        return published;
    }


    /**
     * Sets the published value for this Custom.
     * 
     * @param published
     */
    public void setPublished(java.lang.String published) {
        this.published = published;
    }


    /**
     * Gets the virtuemart_vendor_id value for this Custom.
     * 
     * @return virtuemart_vendor_id
     */
    public java.lang.String getVirtuemart_vendor_id() {
        return virtuemart_vendor_id;
    }


    /**
     * Sets the virtuemart_vendor_id value for this Custom.
     * 
     * @param virtuemart_vendor_id
     */
    public void setVirtuemart_vendor_id(java.lang.String virtuemart_vendor_id) {
        this.virtuemart_vendor_id = virtuemart_vendor_id;
    }


    /**
     * Gets the custom_jplugin_id value for this Custom.
     * 
     * @return custom_jplugin_id
     */
    public java.lang.String getCustom_jplugin_id() {
        return custom_jplugin_id;
    }


    /**
     * Sets the custom_jplugin_id value for this Custom.
     * 
     * @param custom_jplugin_id
     */
    public void setCustom_jplugin_id(java.lang.String custom_jplugin_id) {
        this.custom_jplugin_id = custom_jplugin_id;
    }


    /**
     * Gets the custom_element value for this Custom.
     * 
     * @return custom_element
     */
    public java.lang.String getCustom_element() {
        return custom_element;
    }


    /**
     * Sets the custom_element value for this Custom.
     * 
     * @param custom_element
     */
    public void setCustom_element(java.lang.String custom_element) {
        this.custom_element = custom_element;
    }


    /**
     * Gets the layout_pos value for this Custom.
     * 
     * @return layout_pos
     */
    public java.lang.String getLayout_pos() {
        return layout_pos;
    }


    /**
     * Sets the layout_pos value for this Custom.
     * 
     * @param layout_pos
     */
    public void setLayout_pos(java.lang.String layout_pos) {
        this.layout_pos = layout_pos;
    }


    /**
     * Gets the custom_params value for this Custom.
     * 
     * @return custom_params
     */
    public java.lang.String getCustom_params() {
        return custom_params;
    }


    /**
     * Sets the custom_params value for this Custom.
     * 
     * @param custom_params
     */
    public void setCustom_params(java.lang.String custom_params) {
        this.custom_params = custom_params;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof Custom)) return false;
        Custom other = (Custom) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.virtuemart_custom_id==null && other.getVirtuemart_custom_id()==null) || 
             (this.virtuemart_custom_id!=null &&
              this.virtuemart_custom_id.equals(other.getVirtuemart_custom_id()))) &&
            ((this.custom_parent_id==null && other.getCustom_parent_id()==null) || 
             (this.custom_parent_id!=null &&
              this.custom_parent_id.equals(other.getCustom_parent_id()))) &&
            ((this.admin_only==null && other.getAdmin_only()==null) || 
             (this.admin_only!=null &&
              this.admin_only.equals(other.getAdmin_only()))) &&
            ((this.custom_title==null && other.getCustom_title()==null) || 
             (this.custom_title!=null &&
              this.custom_title.equals(other.getCustom_title()))) &&
            ((this.custom_tip==null && other.getCustom_tip()==null) || 
             (this.custom_tip!=null &&
              this.custom_tip.equals(other.getCustom_tip()))) &&
            ((this.custom_value==null && other.getCustom_value()==null) || 
             (this.custom_value!=null &&
              this.custom_value.equals(other.getCustom_value()))) &&
            ((this.custom_field_desc==null && other.getCustom_field_desc()==null) || 
             (this.custom_field_desc!=null &&
              this.custom_field_desc.equals(other.getCustom_field_desc()))) &&
            ((this.field_type==null && other.getField_type()==null) || 
             (this.field_type!=null &&
              this.field_type.equals(other.getField_type()))) &&
            ((this.is_list==null && other.getIs_list()==null) || 
             (this.is_list!=null &&
              this.is_list.equals(other.getIs_list()))) &&
            ((this.is_hidden==null && other.getIs_hidden()==null) || 
             (this.is_hidden!=null &&
              this.is_hidden.equals(other.getIs_hidden()))) &&
            ((this.is_cart_attribute==null && other.getIs_cart_attribute()==null) || 
             (this.is_cart_attribute!=null &&
              this.is_cart_attribute.equals(other.getIs_cart_attribute()))) &&
            ((this.published==null && other.getPublished()==null) || 
             (this.published!=null &&
              this.published.equals(other.getPublished()))) &&
            ((this.virtuemart_vendor_id==null && other.getVirtuemart_vendor_id()==null) || 
             (this.virtuemart_vendor_id!=null &&
              this.virtuemart_vendor_id.equals(other.getVirtuemart_vendor_id()))) &&
            ((this.custom_jplugin_id==null && other.getCustom_jplugin_id()==null) || 
             (this.custom_jplugin_id!=null &&
              this.custom_jplugin_id.equals(other.getCustom_jplugin_id()))) &&
            ((this.custom_element==null && other.getCustom_element()==null) || 
             (this.custom_element!=null &&
              this.custom_element.equals(other.getCustom_element()))) &&
            ((this.layout_pos==null && other.getLayout_pos()==null) || 
             (this.layout_pos!=null &&
              this.layout_pos.equals(other.getLayout_pos()))) &&
            ((this.custom_params==null && other.getCustom_params()==null) || 
             (this.custom_params!=null &&
              this.custom_params.equals(other.getCustom_params())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getVirtuemart_custom_id() != null) {
            _hashCode += getVirtuemart_custom_id().hashCode();
        }
        if (getCustom_parent_id() != null) {
            _hashCode += getCustom_parent_id().hashCode();
        }
        if (getAdmin_only() != null) {
            _hashCode += getAdmin_only().hashCode();
        }
        if (getCustom_title() != null) {
            _hashCode += getCustom_title().hashCode();
        }
        if (getCustom_tip() != null) {
            _hashCode += getCustom_tip().hashCode();
        }
        if (getCustom_value() != null) {
            _hashCode += getCustom_value().hashCode();
        }
        if (getCustom_field_desc() != null) {
            _hashCode += getCustom_field_desc().hashCode();
        }
        if (getField_type() != null) {
            _hashCode += getField_type().hashCode();
        }
        if (getIs_list() != null) {
            _hashCode += getIs_list().hashCode();
        }
        if (getIs_hidden() != null) {
            _hashCode += getIs_hidden().hashCode();
        }
        if (getIs_cart_attribute() != null) {
            _hashCode += getIs_cart_attribute().hashCode();
        }
        if (getPublished() != null) {
            _hashCode += getPublished().hashCode();
        }
        if (getVirtuemart_vendor_id() != null) {
            _hashCode += getVirtuemart_vendor_id().hashCode();
        }
        if (getCustom_jplugin_id() != null) {
            _hashCode += getCustom_jplugin_id().hashCode();
        }
        if (getCustom_element() != null) {
            _hashCode += getCustom_element().hashCode();
        }
        if (getLayout_pos() != null) {
            _hashCode += getLayout_pos().hashCode();
        }
        if (getCustom_params() != null) {
            _hashCode += getCustom_params().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(Custom.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://www.virtuemart.net/VM_Product/", "Custom"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("virtuemart_custom_id");
        elemField.setXmlName(new javax.xml.namespace.QName("", "virtuemart_custom_id"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("custom_parent_id");
        elemField.setXmlName(new javax.xml.namespace.QName("", "custom_parent_id"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("admin_only");
        elemField.setXmlName(new javax.xml.namespace.QName("", "admin_only"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("custom_title");
        elemField.setXmlName(new javax.xml.namespace.QName("", "custom_title"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("custom_tip");
        elemField.setXmlName(new javax.xml.namespace.QName("", "custom_tip"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("custom_value");
        elemField.setXmlName(new javax.xml.namespace.QName("", "custom_value"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("custom_field_desc");
        elemField.setXmlName(new javax.xml.namespace.QName("", "custom_field_desc"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("field_type");
        elemField.setXmlName(new javax.xml.namespace.QName("", "field_type"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("is_list");
        elemField.setXmlName(new javax.xml.namespace.QName("", "is_list"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("is_hidden");
        elemField.setXmlName(new javax.xml.namespace.QName("", "is_hidden"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("is_cart_attribute");
        elemField.setXmlName(new javax.xml.namespace.QName("", "is_cart_attribute"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("published");
        elemField.setXmlName(new javax.xml.namespace.QName("", "published"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("virtuemart_vendor_id");
        elemField.setXmlName(new javax.xml.namespace.QName("", "virtuemart_vendor_id"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("custom_jplugin_id");
        elemField.setXmlName(new javax.xml.namespace.QName("", "custom_jplugin_id"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("custom_element");
        elemField.setXmlName(new javax.xml.namespace.QName("", "custom_element"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("layout_pos");
        elemField.setXmlName(new javax.xml.namespace.QName("", "layout_pos"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("custom_params");
        elemField.setXmlName(new javax.xml.namespace.QName("", "custom_params"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
