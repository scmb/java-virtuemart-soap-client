/**
 * Product.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package it.netgrid.virtuemart.soap.products;

public class Product  implements java.io.Serializable {
    private java.lang.String product_id;

    private java.lang.String virtuemart_vendor_id;

    private java.lang.String product_parent_id;

    private java.lang.String product_sku;

    private java.lang.String product_name;

    private java.lang.String slug;

    private java.lang.String product_s_desc;

    private java.lang.String product_desc;

    private java.lang.String product_weight;

    private java.lang.String product_weight_uom;

    private java.lang.String product_length;

    private java.lang.String product_width;

    private java.lang.String product_height;

    private java.lang.String product_lwh_uom;

    private java.lang.String product_url;

    private java.lang.String product_in_stock;

    private java.lang.String low_stock_notification;

    private java.lang.String product_available_date;

    private java.lang.String product_availability;

    private java.lang.String product_special;

    private java.lang.String ship_code_id;

    private java.lang.String product_sales;

    private java.lang.String product_unit;

    private java.lang.String product_packaging;

    private java.lang.String product_ordered;

    private java.lang.String hits;

    private java.lang.String intnotes;

    private java.lang.String metadesc;

    private java.lang.String metakey;

    private java.lang.String metarobot;

    private java.lang.String metaauthor;

    private java.lang.String layout;

    private java.lang.String published;

    private java.lang.String product_categories;

    private java.lang.String manufacturer_id;

    private java.lang.String product_params;

    private java.lang.String img_uri;

    private java.lang.String img_thumb_uri;

    private java.lang.String shared;

    private java.lang.String ordering;

    private java.lang.String customtitle;

    private java.lang.String shopper_group_ids;

    private it.netgrid.virtuemart.soap.products.ProductPrice[] prices;

    private java.lang.String product_gtin;

    private java.lang.String product_mpn;

    public Product() {
    }

    public Product(
           java.lang.String product_id,
           java.lang.String virtuemart_vendor_id,
           java.lang.String product_parent_id,
           java.lang.String product_sku,
           java.lang.String product_name,
           java.lang.String slug,
           java.lang.String product_s_desc,
           java.lang.String product_desc,
           java.lang.String product_weight,
           java.lang.String product_weight_uom,
           java.lang.String product_length,
           java.lang.String product_width,
           java.lang.String product_height,
           java.lang.String product_lwh_uom,
           java.lang.String product_url,
           java.lang.String product_in_stock,
           java.lang.String low_stock_notification,
           java.lang.String product_available_date,
           java.lang.String product_availability,
           java.lang.String product_special,
           java.lang.String ship_code_id,
           java.lang.String product_sales,
           java.lang.String product_unit,
           java.lang.String product_packaging,
           java.lang.String product_ordered,
           java.lang.String hits,
           java.lang.String intnotes,
           java.lang.String metadesc,
           java.lang.String metakey,
           java.lang.String metarobot,
           java.lang.String metaauthor,
           java.lang.String layout,
           java.lang.String published,
           java.lang.String product_categories,
           java.lang.String manufacturer_id,
           java.lang.String product_params,
           java.lang.String img_uri,
           java.lang.String img_thumb_uri,
           java.lang.String shared,
           java.lang.String ordering,
           java.lang.String customtitle,
           java.lang.String shopper_group_ids,
           it.netgrid.virtuemart.soap.products.ProductPrice[] prices,
           java.lang.String product_gtin,
           java.lang.String product_mpn) {
           this.product_id = product_id;
           this.virtuemart_vendor_id = virtuemart_vendor_id;
           this.product_parent_id = product_parent_id;
           this.product_sku = product_sku;
           this.product_name = product_name;
           this.slug = slug;
           this.product_s_desc = product_s_desc;
           this.product_desc = product_desc;
           this.product_weight = product_weight;
           this.product_weight_uom = product_weight_uom;
           this.product_length = product_length;
           this.product_width = product_width;
           this.product_height = product_height;
           this.product_lwh_uom = product_lwh_uom;
           this.product_url = product_url;
           this.product_in_stock = product_in_stock;
           this.low_stock_notification = low_stock_notification;
           this.product_available_date = product_available_date;
           this.product_availability = product_availability;
           this.product_special = product_special;
           this.ship_code_id = ship_code_id;
           this.product_sales = product_sales;
           this.product_unit = product_unit;
           this.product_packaging = product_packaging;
           this.product_ordered = product_ordered;
           this.hits = hits;
           this.intnotes = intnotes;
           this.metadesc = metadesc;
           this.metakey = metakey;
           this.metarobot = metarobot;
           this.metaauthor = metaauthor;
           this.layout = layout;
           this.published = published;
           this.product_categories = product_categories;
           this.manufacturer_id = manufacturer_id;
           this.product_params = product_params;
           this.img_uri = img_uri;
           this.img_thumb_uri = img_thumb_uri;
           this.shared = shared;
           this.ordering = ordering;
           this.customtitle = customtitle;
           this.shopper_group_ids = shopper_group_ids;
           this.prices = prices;
           this.product_gtin = product_gtin;
           this.product_mpn = product_mpn;
    }


    /**
     * Gets the product_id value for this Product.
     * 
     * @return product_id
     */
    public java.lang.String getProduct_id() {
        return product_id;
    }


    /**
     * Sets the product_id value for this Product.
     * 
     * @param product_id
     */
    public void setProduct_id(java.lang.String product_id) {
        this.product_id = product_id;
    }


    /**
     * Gets the virtuemart_vendor_id value for this Product.
     * 
     * @return virtuemart_vendor_id
     */
    public java.lang.String getVirtuemart_vendor_id() {
        return virtuemart_vendor_id;
    }


    /**
     * Sets the virtuemart_vendor_id value for this Product.
     * 
     * @param virtuemart_vendor_id
     */
    public void setVirtuemart_vendor_id(java.lang.String virtuemart_vendor_id) {
        this.virtuemart_vendor_id = virtuemart_vendor_id;
    }


    /**
     * Gets the product_parent_id value for this Product.
     * 
     * @return product_parent_id
     */
    public java.lang.String getProduct_parent_id() {
        return product_parent_id;
    }


    /**
     * Sets the product_parent_id value for this Product.
     * 
     * @param product_parent_id
     */
    public void setProduct_parent_id(java.lang.String product_parent_id) {
        this.product_parent_id = product_parent_id;
    }


    /**
     * Gets the product_sku value for this Product.
     * 
     * @return product_sku
     */
    public java.lang.String getProduct_sku() {
        return product_sku;
    }


    /**
     * Sets the product_sku value for this Product.
     * 
     * @param product_sku
     */
    public void setProduct_sku(java.lang.String product_sku) {
        this.product_sku = product_sku;
    }


    /**
     * Gets the product_name value for this Product.
     * 
     * @return product_name
     */
    public java.lang.String getProduct_name() {
        return product_name;
    }


    /**
     * Sets the product_name value for this Product.
     * 
     * @param product_name
     */
    public void setProduct_name(java.lang.String product_name) {
        this.product_name = product_name;
    }


    /**
     * Gets the slug value for this Product.
     * 
     * @return slug
     */
    public java.lang.String getSlug() {
        return slug;
    }


    /**
     * Sets the slug value for this Product.
     * 
     * @param slug
     */
    public void setSlug(java.lang.String slug) {
        this.slug = slug;
    }


    /**
     * Gets the product_s_desc value for this Product.
     * 
     * @return product_s_desc
     */
    public java.lang.String getProduct_s_desc() {
        return product_s_desc;
    }


    /**
     * Sets the product_s_desc value for this Product.
     * 
     * @param product_s_desc
     */
    public void setProduct_s_desc(java.lang.String product_s_desc) {
        this.product_s_desc = product_s_desc;
    }


    /**
     * Gets the product_desc value for this Product.
     * 
     * @return product_desc
     */
    public java.lang.String getProduct_desc() {
        return product_desc;
    }


    /**
     * Sets the product_desc value for this Product.
     * 
     * @param product_desc
     */
    public void setProduct_desc(java.lang.String product_desc) {
        this.product_desc = product_desc;
    }


    /**
     * Gets the product_weight value for this Product.
     * 
     * @return product_weight
     */
    public java.lang.String getProduct_weight() {
        return product_weight;
    }


    /**
     * Sets the product_weight value for this Product.
     * 
     * @param product_weight
     */
    public void setProduct_weight(java.lang.String product_weight) {
        this.product_weight = product_weight;
    }


    /**
     * Gets the product_weight_uom value for this Product.
     * 
     * @return product_weight_uom
     */
    public java.lang.String getProduct_weight_uom() {
        return product_weight_uom;
    }


    /**
     * Sets the product_weight_uom value for this Product.
     * 
     * @param product_weight_uom
     */
    public void setProduct_weight_uom(java.lang.String product_weight_uom) {
        this.product_weight_uom = product_weight_uom;
    }


    /**
     * Gets the product_length value for this Product.
     * 
     * @return product_length
     */
    public java.lang.String getProduct_length() {
        return product_length;
    }


    /**
     * Sets the product_length value for this Product.
     * 
     * @param product_length
     */
    public void setProduct_length(java.lang.String product_length) {
        this.product_length = product_length;
    }


    /**
     * Gets the product_width value for this Product.
     * 
     * @return product_width
     */
    public java.lang.String getProduct_width() {
        return product_width;
    }


    /**
     * Sets the product_width value for this Product.
     * 
     * @param product_width
     */
    public void setProduct_width(java.lang.String product_width) {
        this.product_width = product_width;
    }


    /**
     * Gets the product_height value for this Product.
     * 
     * @return product_height
     */
    public java.lang.String getProduct_height() {
        return product_height;
    }


    /**
     * Sets the product_height value for this Product.
     * 
     * @param product_height
     */
    public void setProduct_height(java.lang.String product_height) {
        this.product_height = product_height;
    }


    /**
     * Gets the product_lwh_uom value for this Product.
     * 
     * @return product_lwh_uom
     */
    public java.lang.String getProduct_lwh_uom() {
        return product_lwh_uom;
    }


    /**
     * Sets the product_lwh_uom value for this Product.
     * 
     * @param product_lwh_uom
     */
    public void setProduct_lwh_uom(java.lang.String product_lwh_uom) {
        this.product_lwh_uom = product_lwh_uom;
    }


    /**
     * Gets the product_url value for this Product.
     * 
     * @return product_url
     */
    public java.lang.String getProduct_url() {
        return product_url;
    }


    /**
     * Sets the product_url value for this Product.
     * 
     * @param product_url
     */
    public void setProduct_url(java.lang.String product_url) {
        this.product_url = product_url;
    }


    /**
     * Gets the product_in_stock value for this Product.
     * 
     * @return product_in_stock
     */
    public java.lang.String getProduct_in_stock() {
        return product_in_stock;
    }


    /**
     * Sets the product_in_stock value for this Product.
     * 
     * @param product_in_stock
     */
    public void setProduct_in_stock(java.lang.String product_in_stock) {
        this.product_in_stock = product_in_stock;
    }


    /**
     * Gets the low_stock_notification value for this Product.
     * 
     * @return low_stock_notification
     */
    public java.lang.String getLow_stock_notification() {
        return low_stock_notification;
    }


    /**
     * Sets the low_stock_notification value for this Product.
     * 
     * @param low_stock_notification
     */
    public void setLow_stock_notification(java.lang.String low_stock_notification) {
        this.low_stock_notification = low_stock_notification;
    }


    /**
     * Gets the product_available_date value for this Product.
     * 
     * @return product_available_date
     */
    public java.lang.String getProduct_available_date() {
        return product_available_date;
    }


    /**
     * Sets the product_available_date value for this Product.
     * 
     * @param product_available_date
     */
    public void setProduct_available_date(java.lang.String product_available_date) {
        this.product_available_date = product_available_date;
    }


    /**
     * Gets the product_availability value for this Product.
     * 
     * @return product_availability
     */
    public java.lang.String getProduct_availability() {
        return product_availability;
    }


    /**
     * Sets the product_availability value for this Product.
     * 
     * @param product_availability
     */
    public void setProduct_availability(java.lang.String product_availability) {
        this.product_availability = product_availability;
    }


    /**
     * Gets the product_special value for this Product.
     * 
     * @return product_special
     */
    public java.lang.String getProduct_special() {
        return product_special;
    }


    /**
     * Sets the product_special value for this Product.
     * 
     * @param product_special
     */
    public void setProduct_special(java.lang.String product_special) {
        this.product_special = product_special;
    }


    /**
     * Gets the ship_code_id value for this Product.
     * 
     * @return ship_code_id
     */
    public java.lang.String getShip_code_id() {
        return ship_code_id;
    }


    /**
     * Sets the ship_code_id value for this Product.
     * 
     * @param ship_code_id
     */
    public void setShip_code_id(java.lang.String ship_code_id) {
        this.ship_code_id = ship_code_id;
    }


    /**
     * Gets the product_sales value for this Product.
     * 
     * @return product_sales
     */
    public java.lang.String getProduct_sales() {
        return product_sales;
    }


    /**
     * Sets the product_sales value for this Product.
     * 
     * @param product_sales
     */
    public void setProduct_sales(java.lang.String product_sales) {
        this.product_sales = product_sales;
    }


    /**
     * Gets the product_unit value for this Product.
     * 
     * @return product_unit
     */
    public java.lang.String getProduct_unit() {
        return product_unit;
    }


    /**
     * Sets the product_unit value for this Product.
     * 
     * @param product_unit
     */
    public void setProduct_unit(java.lang.String product_unit) {
        this.product_unit = product_unit;
    }


    /**
     * Gets the product_packaging value for this Product.
     * 
     * @return product_packaging
     */
    public java.lang.String getProduct_packaging() {
        return product_packaging;
    }


    /**
     * Sets the product_packaging value for this Product.
     * 
     * @param product_packaging
     */
    public void setProduct_packaging(java.lang.String product_packaging) {
        this.product_packaging = product_packaging;
    }


    /**
     * Gets the product_ordered value for this Product.
     * 
     * @return product_ordered
     */
    public java.lang.String getProduct_ordered() {
        return product_ordered;
    }


    /**
     * Sets the product_ordered value for this Product.
     * 
     * @param product_ordered
     */
    public void setProduct_ordered(java.lang.String product_ordered) {
        this.product_ordered = product_ordered;
    }


    /**
     * Gets the hits value for this Product.
     * 
     * @return hits
     */
    public java.lang.String getHits() {
        return hits;
    }


    /**
     * Sets the hits value for this Product.
     * 
     * @param hits
     */
    public void setHits(java.lang.String hits) {
        this.hits = hits;
    }


    /**
     * Gets the intnotes value for this Product.
     * 
     * @return intnotes
     */
    public java.lang.String getIntnotes() {
        return intnotes;
    }


    /**
     * Sets the intnotes value for this Product.
     * 
     * @param intnotes
     */
    public void setIntnotes(java.lang.String intnotes) {
        this.intnotes = intnotes;
    }


    /**
     * Gets the metadesc value for this Product.
     * 
     * @return metadesc
     */
    public java.lang.String getMetadesc() {
        return metadesc;
    }


    /**
     * Sets the metadesc value for this Product.
     * 
     * @param metadesc
     */
    public void setMetadesc(java.lang.String metadesc) {
        this.metadesc = metadesc;
    }


    /**
     * Gets the metakey value for this Product.
     * 
     * @return metakey
     */
    public java.lang.String getMetakey() {
        return metakey;
    }


    /**
     * Sets the metakey value for this Product.
     * 
     * @param metakey
     */
    public void setMetakey(java.lang.String metakey) {
        this.metakey = metakey;
    }


    /**
     * Gets the metarobot value for this Product.
     * 
     * @return metarobot
     */
    public java.lang.String getMetarobot() {
        return metarobot;
    }


    /**
     * Sets the metarobot value for this Product.
     * 
     * @param metarobot
     */
    public void setMetarobot(java.lang.String metarobot) {
        this.metarobot = metarobot;
    }


    /**
     * Gets the metaauthor value for this Product.
     * 
     * @return metaauthor
     */
    public java.lang.String getMetaauthor() {
        return metaauthor;
    }


    /**
     * Sets the metaauthor value for this Product.
     * 
     * @param metaauthor
     */
    public void setMetaauthor(java.lang.String metaauthor) {
        this.metaauthor = metaauthor;
    }


    /**
     * Gets the layout value for this Product.
     * 
     * @return layout
     */
    public java.lang.String getLayout() {
        return layout;
    }


    /**
     * Sets the layout value for this Product.
     * 
     * @param layout
     */
    public void setLayout(java.lang.String layout) {
        this.layout = layout;
    }


    /**
     * Gets the published value for this Product.
     * 
     * @return published
     */
    public java.lang.String getPublished() {
        return published;
    }


    /**
     * Sets the published value for this Product.
     * 
     * @param published
     */
    public void setPublished(java.lang.String published) {
        this.published = published;
    }


    /**
     * Gets the product_categories value for this Product.
     * 
     * @return product_categories
     */
    public java.lang.String getProduct_categories() {
        return product_categories;
    }


    /**
     * Sets the product_categories value for this Product.
     * 
     * @param product_categories
     */
    public void setProduct_categories(java.lang.String product_categories) {
        this.product_categories = product_categories;
    }


    /**
     * Gets the manufacturer_id value for this Product.
     * 
     * @return manufacturer_id
     */
    public java.lang.String getManufacturer_id() {
        return manufacturer_id;
    }


    /**
     * Sets the manufacturer_id value for this Product.
     * 
     * @param manufacturer_id
     */
    public void setManufacturer_id(java.lang.String manufacturer_id) {
        this.manufacturer_id = manufacturer_id;
    }


    /**
     * Gets the product_params value for this Product.
     * 
     * @return product_params
     */
    public java.lang.String getProduct_params() {
        return product_params;
    }


    /**
     * Sets the product_params value for this Product.
     * 
     * @param product_params
     */
    public void setProduct_params(java.lang.String product_params) {
        this.product_params = product_params;
    }


    /**
     * Gets the img_uri value for this Product.
     * 
     * @return img_uri
     */
    public java.lang.String getImg_uri() {
        return img_uri;
    }


    /**
     * Sets the img_uri value for this Product.
     * 
     * @param img_uri
     */
    public void setImg_uri(java.lang.String img_uri) {
        this.img_uri = img_uri;
    }


    /**
     * Gets the img_thumb_uri value for this Product.
     * 
     * @return img_thumb_uri
     */
    public java.lang.String getImg_thumb_uri() {
        return img_thumb_uri;
    }


    /**
     * Sets the img_thumb_uri value for this Product.
     * 
     * @param img_thumb_uri
     */
    public void setImg_thumb_uri(java.lang.String img_thumb_uri) {
        this.img_thumb_uri = img_thumb_uri;
    }


    /**
     * Gets the shared value for this Product.
     * 
     * @return shared
     */
    public java.lang.String getShared() {
        return shared;
    }


    /**
     * Sets the shared value for this Product.
     * 
     * @param shared
     */
    public void setShared(java.lang.String shared) {
        this.shared = shared;
    }


    /**
     * Gets the ordering value for this Product.
     * 
     * @return ordering
     */
    public java.lang.String getOrdering() {
        return ordering;
    }


    /**
     * Sets the ordering value for this Product.
     * 
     * @param ordering
     */
    public void setOrdering(java.lang.String ordering) {
        this.ordering = ordering;
    }


    /**
     * Gets the customtitle value for this Product.
     * 
     * @return customtitle
     */
    public java.lang.String getCustomtitle() {
        return customtitle;
    }


    /**
     * Sets the customtitle value for this Product.
     * 
     * @param customtitle
     */
    public void setCustomtitle(java.lang.String customtitle) {
        this.customtitle = customtitle;
    }


    /**
     * Gets the shopper_group_ids value for this Product.
     * 
     * @return shopper_group_ids
     */
    public java.lang.String getShopper_group_ids() {
        return shopper_group_ids;
    }


    /**
     * Sets the shopper_group_ids value for this Product.
     * 
     * @param shopper_group_ids
     */
    public void setShopper_group_ids(java.lang.String shopper_group_ids) {
        this.shopper_group_ids = shopper_group_ids;
    }


    /**
     * Gets the prices value for this Product.
     * 
     * @return prices
     */
    public it.netgrid.virtuemart.soap.products.ProductPrice[] getPrices() {
        return prices;
    }


    /**
     * Sets the prices value for this Product.
     * 
     * @param prices
     */
    public void setPrices(it.netgrid.virtuemart.soap.products.ProductPrice[] prices) {
        this.prices = prices;
    }


    /**
     * Gets the product_gtin value for this Product.
     * 
     * @return product_gtin
     */
    public java.lang.String getProduct_gtin() {
        return product_gtin;
    }


    /**
     * Sets the product_gtin value for this Product.
     * 
     * @param product_gtin
     */
    public void setProduct_gtin(java.lang.String product_gtin) {
        this.product_gtin = product_gtin;
    }


    /**
     * Gets the product_mpn value for this Product.
     * 
     * @return product_mpn
     */
    public java.lang.String getProduct_mpn() {
        return product_mpn;
    }


    /**
     * Sets the product_mpn value for this Product.
     * 
     * @param product_mpn
     */
    public void setProduct_mpn(java.lang.String product_mpn) {
        this.product_mpn = product_mpn;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof Product)) return false;
        Product other = (Product) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.product_id==null && other.getProduct_id()==null) || 
             (this.product_id!=null &&
              this.product_id.equals(other.getProduct_id()))) &&
            ((this.virtuemart_vendor_id==null && other.getVirtuemart_vendor_id()==null) || 
             (this.virtuemart_vendor_id!=null &&
              this.virtuemart_vendor_id.equals(other.getVirtuemart_vendor_id()))) &&
            ((this.product_parent_id==null && other.getProduct_parent_id()==null) || 
             (this.product_parent_id!=null &&
              this.product_parent_id.equals(other.getProduct_parent_id()))) &&
            ((this.product_sku==null && other.getProduct_sku()==null) || 
             (this.product_sku!=null &&
              this.product_sku.equals(other.getProduct_sku()))) &&
            ((this.product_name==null && other.getProduct_name()==null) || 
             (this.product_name!=null &&
              this.product_name.equals(other.getProduct_name()))) &&
            ((this.slug==null && other.getSlug()==null) || 
             (this.slug!=null &&
              this.slug.equals(other.getSlug()))) &&
            ((this.product_s_desc==null && other.getProduct_s_desc()==null) || 
             (this.product_s_desc!=null &&
              this.product_s_desc.equals(other.getProduct_s_desc()))) &&
            ((this.product_desc==null && other.getProduct_desc()==null) || 
             (this.product_desc!=null &&
              this.product_desc.equals(other.getProduct_desc()))) &&
            ((this.product_weight==null && other.getProduct_weight()==null) || 
             (this.product_weight!=null &&
              this.product_weight.equals(other.getProduct_weight()))) &&
            ((this.product_weight_uom==null && other.getProduct_weight_uom()==null) || 
             (this.product_weight_uom!=null &&
              this.product_weight_uom.equals(other.getProduct_weight_uom()))) &&
            ((this.product_length==null && other.getProduct_length()==null) || 
             (this.product_length!=null &&
              this.product_length.equals(other.getProduct_length()))) &&
            ((this.product_width==null && other.getProduct_width()==null) || 
             (this.product_width!=null &&
              this.product_width.equals(other.getProduct_width()))) &&
            ((this.product_height==null && other.getProduct_height()==null) || 
             (this.product_height!=null &&
              this.product_height.equals(other.getProduct_height()))) &&
            ((this.product_lwh_uom==null && other.getProduct_lwh_uom()==null) || 
             (this.product_lwh_uom!=null &&
              this.product_lwh_uom.equals(other.getProduct_lwh_uom()))) &&
            ((this.product_url==null && other.getProduct_url()==null) || 
             (this.product_url!=null &&
              this.product_url.equals(other.getProduct_url()))) &&
            ((this.product_in_stock==null && other.getProduct_in_stock()==null) || 
             (this.product_in_stock!=null &&
              this.product_in_stock.equals(other.getProduct_in_stock()))) &&
            ((this.low_stock_notification==null && other.getLow_stock_notification()==null) || 
             (this.low_stock_notification!=null &&
              this.low_stock_notification.equals(other.getLow_stock_notification()))) &&
            ((this.product_available_date==null && other.getProduct_available_date()==null) || 
             (this.product_available_date!=null &&
              this.product_available_date.equals(other.getProduct_available_date()))) &&
            ((this.product_availability==null && other.getProduct_availability()==null) || 
             (this.product_availability!=null &&
              this.product_availability.equals(other.getProduct_availability()))) &&
            ((this.product_special==null && other.getProduct_special()==null) || 
             (this.product_special!=null &&
              this.product_special.equals(other.getProduct_special()))) &&
            ((this.ship_code_id==null && other.getShip_code_id()==null) || 
             (this.ship_code_id!=null &&
              this.ship_code_id.equals(other.getShip_code_id()))) &&
            ((this.product_sales==null && other.getProduct_sales()==null) || 
             (this.product_sales!=null &&
              this.product_sales.equals(other.getProduct_sales()))) &&
            ((this.product_unit==null && other.getProduct_unit()==null) || 
             (this.product_unit!=null &&
              this.product_unit.equals(other.getProduct_unit()))) &&
            ((this.product_packaging==null && other.getProduct_packaging()==null) || 
             (this.product_packaging!=null &&
              this.product_packaging.equals(other.getProduct_packaging()))) &&
            ((this.product_ordered==null && other.getProduct_ordered()==null) || 
             (this.product_ordered!=null &&
              this.product_ordered.equals(other.getProduct_ordered()))) &&
            ((this.hits==null && other.getHits()==null) || 
             (this.hits!=null &&
              this.hits.equals(other.getHits()))) &&
            ((this.intnotes==null && other.getIntnotes()==null) || 
             (this.intnotes!=null &&
              this.intnotes.equals(other.getIntnotes()))) &&
            ((this.metadesc==null && other.getMetadesc()==null) || 
             (this.metadesc!=null &&
              this.metadesc.equals(other.getMetadesc()))) &&
            ((this.metakey==null && other.getMetakey()==null) || 
             (this.metakey!=null &&
              this.metakey.equals(other.getMetakey()))) &&
            ((this.metarobot==null && other.getMetarobot()==null) || 
             (this.metarobot!=null &&
              this.metarobot.equals(other.getMetarobot()))) &&
            ((this.metaauthor==null && other.getMetaauthor()==null) || 
             (this.metaauthor!=null &&
              this.metaauthor.equals(other.getMetaauthor()))) &&
            ((this.layout==null && other.getLayout()==null) || 
             (this.layout!=null &&
              this.layout.equals(other.getLayout()))) &&
            ((this.published==null && other.getPublished()==null) || 
             (this.published!=null &&
              this.published.equals(other.getPublished()))) &&
            ((this.product_categories==null && other.getProduct_categories()==null) || 
             (this.product_categories!=null &&
              this.product_categories.equals(other.getProduct_categories()))) &&
            ((this.manufacturer_id==null && other.getManufacturer_id()==null) || 
             (this.manufacturer_id!=null &&
              this.manufacturer_id.equals(other.getManufacturer_id()))) &&
            ((this.product_params==null && other.getProduct_params()==null) || 
             (this.product_params!=null &&
              this.product_params.equals(other.getProduct_params()))) &&
            ((this.img_uri==null && other.getImg_uri()==null) || 
             (this.img_uri!=null &&
              this.img_uri.equals(other.getImg_uri()))) &&
            ((this.img_thumb_uri==null && other.getImg_thumb_uri()==null) || 
             (this.img_thumb_uri!=null &&
              this.img_thumb_uri.equals(other.getImg_thumb_uri()))) &&
            ((this.shared==null && other.getShared()==null) || 
             (this.shared!=null &&
              this.shared.equals(other.getShared()))) &&
            ((this.ordering==null && other.getOrdering()==null) || 
             (this.ordering!=null &&
              this.ordering.equals(other.getOrdering()))) &&
            ((this.customtitle==null && other.getCustomtitle()==null) || 
             (this.customtitle!=null &&
              this.customtitle.equals(other.getCustomtitle()))) &&
            ((this.shopper_group_ids==null && other.getShopper_group_ids()==null) || 
             (this.shopper_group_ids!=null &&
              this.shopper_group_ids.equals(other.getShopper_group_ids()))) &&
            ((this.prices==null && other.getPrices()==null) || 
             (this.prices!=null &&
              java.util.Arrays.equals(this.prices, other.getPrices()))) &&
            ((this.product_gtin==null && other.getProduct_gtin()==null) || 
             (this.product_gtin!=null &&
              this.product_gtin.equals(other.getProduct_gtin()))) &&
            ((this.product_mpn==null && other.getProduct_mpn()==null) || 
             (this.product_mpn!=null &&
              this.product_mpn.equals(other.getProduct_mpn())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getProduct_id() != null) {
            _hashCode += getProduct_id().hashCode();
        }
        if (getVirtuemart_vendor_id() != null) {
            _hashCode += getVirtuemart_vendor_id().hashCode();
        }
        if (getProduct_parent_id() != null) {
            _hashCode += getProduct_parent_id().hashCode();
        }
        if (getProduct_sku() != null) {
            _hashCode += getProduct_sku().hashCode();
        }
        if (getProduct_name() != null) {
            _hashCode += getProduct_name().hashCode();
        }
        if (getSlug() != null) {
            _hashCode += getSlug().hashCode();
        }
        if (getProduct_s_desc() != null) {
            _hashCode += getProduct_s_desc().hashCode();
        }
        if (getProduct_desc() != null) {
            _hashCode += getProduct_desc().hashCode();
        }
        if (getProduct_weight() != null) {
            _hashCode += getProduct_weight().hashCode();
        }
        if (getProduct_weight_uom() != null) {
            _hashCode += getProduct_weight_uom().hashCode();
        }
        if (getProduct_length() != null) {
            _hashCode += getProduct_length().hashCode();
        }
        if (getProduct_width() != null) {
            _hashCode += getProduct_width().hashCode();
        }
        if (getProduct_height() != null) {
            _hashCode += getProduct_height().hashCode();
        }
        if (getProduct_lwh_uom() != null) {
            _hashCode += getProduct_lwh_uom().hashCode();
        }
        if (getProduct_url() != null) {
            _hashCode += getProduct_url().hashCode();
        }
        if (getProduct_in_stock() != null) {
            _hashCode += getProduct_in_stock().hashCode();
        }
        if (getLow_stock_notification() != null) {
            _hashCode += getLow_stock_notification().hashCode();
        }
        if (getProduct_available_date() != null) {
            _hashCode += getProduct_available_date().hashCode();
        }
        if (getProduct_availability() != null) {
            _hashCode += getProduct_availability().hashCode();
        }
        if (getProduct_special() != null) {
            _hashCode += getProduct_special().hashCode();
        }
        if (getShip_code_id() != null) {
            _hashCode += getShip_code_id().hashCode();
        }
        if (getProduct_sales() != null) {
            _hashCode += getProduct_sales().hashCode();
        }
        if (getProduct_unit() != null) {
            _hashCode += getProduct_unit().hashCode();
        }
        if (getProduct_packaging() != null) {
            _hashCode += getProduct_packaging().hashCode();
        }
        if (getProduct_ordered() != null) {
            _hashCode += getProduct_ordered().hashCode();
        }
        if (getHits() != null) {
            _hashCode += getHits().hashCode();
        }
        if (getIntnotes() != null) {
            _hashCode += getIntnotes().hashCode();
        }
        if (getMetadesc() != null) {
            _hashCode += getMetadesc().hashCode();
        }
        if (getMetakey() != null) {
            _hashCode += getMetakey().hashCode();
        }
        if (getMetarobot() != null) {
            _hashCode += getMetarobot().hashCode();
        }
        if (getMetaauthor() != null) {
            _hashCode += getMetaauthor().hashCode();
        }
        if (getLayout() != null) {
            _hashCode += getLayout().hashCode();
        }
        if (getPublished() != null) {
            _hashCode += getPublished().hashCode();
        }
        if (getProduct_categories() != null) {
            _hashCode += getProduct_categories().hashCode();
        }
        if (getManufacturer_id() != null) {
            _hashCode += getManufacturer_id().hashCode();
        }
        if (getProduct_params() != null) {
            _hashCode += getProduct_params().hashCode();
        }
        if (getImg_uri() != null) {
            _hashCode += getImg_uri().hashCode();
        }
        if (getImg_thumb_uri() != null) {
            _hashCode += getImg_thumb_uri().hashCode();
        }
        if (getShared() != null) {
            _hashCode += getShared().hashCode();
        }
        if (getOrdering() != null) {
            _hashCode += getOrdering().hashCode();
        }
        if (getCustomtitle() != null) {
            _hashCode += getCustomtitle().hashCode();
        }
        if (getShopper_group_ids() != null) {
            _hashCode += getShopper_group_ids().hashCode();
        }
        if (getPrices() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getPrices());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getPrices(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        if (getProduct_gtin() != null) {
            _hashCode += getProduct_gtin().hashCode();
        }
        if (getProduct_mpn() != null) {
            _hashCode += getProduct_mpn().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(Product.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://www.virtuemart.net/VM_Product/", "Product"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("product_id");
        elemField.setXmlName(new javax.xml.namespace.QName("", "product_id"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("virtuemart_vendor_id");
        elemField.setXmlName(new javax.xml.namespace.QName("", "virtuemart_vendor_id"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("product_parent_id");
        elemField.setXmlName(new javax.xml.namespace.QName("", "product_parent_id"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("product_sku");
        elemField.setXmlName(new javax.xml.namespace.QName("", "product_sku"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("product_name");
        elemField.setXmlName(new javax.xml.namespace.QName("", "product_name"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("slug");
        elemField.setXmlName(new javax.xml.namespace.QName("", "slug"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("product_s_desc");
        elemField.setXmlName(new javax.xml.namespace.QName("", "product_s_desc"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("product_desc");
        elemField.setXmlName(new javax.xml.namespace.QName("", "product_desc"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("product_weight");
        elemField.setXmlName(new javax.xml.namespace.QName("", "product_weight"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("product_weight_uom");
        elemField.setXmlName(new javax.xml.namespace.QName("", "product_weight_uom"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("product_length");
        elemField.setXmlName(new javax.xml.namespace.QName("", "product_length"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("product_width");
        elemField.setXmlName(new javax.xml.namespace.QName("", "product_width"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("product_height");
        elemField.setXmlName(new javax.xml.namespace.QName("", "product_height"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("product_lwh_uom");
        elemField.setXmlName(new javax.xml.namespace.QName("", "product_lwh_uom"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("product_url");
        elemField.setXmlName(new javax.xml.namespace.QName("", "product_url"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("product_in_stock");
        elemField.setXmlName(new javax.xml.namespace.QName("", "product_in_stock"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("low_stock_notification");
        elemField.setXmlName(new javax.xml.namespace.QName("", "low_stock_notification"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("product_available_date");
        elemField.setXmlName(new javax.xml.namespace.QName("", "product_available_date"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("product_availability");
        elemField.setXmlName(new javax.xml.namespace.QName("", "product_availability"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("product_special");
        elemField.setXmlName(new javax.xml.namespace.QName("", "product_special"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("ship_code_id");
        elemField.setXmlName(new javax.xml.namespace.QName("", "ship_code_id"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("product_sales");
        elemField.setXmlName(new javax.xml.namespace.QName("", "product_sales"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("product_unit");
        elemField.setXmlName(new javax.xml.namespace.QName("", "product_unit"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("product_packaging");
        elemField.setXmlName(new javax.xml.namespace.QName("", "product_packaging"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("product_ordered");
        elemField.setXmlName(new javax.xml.namespace.QName("", "product_ordered"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("hits");
        elemField.setXmlName(new javax.xml.namespace.QName("", "hits"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("intnotes");
        elemField.setXmlName(new javax.xml.namespace.QName("", "intnotes"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("metadesc");
        elemField.setXmlName(new javax.xml.namespace.QName("", "metadesc"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("metakey");
        elemField.setXmlName(new javax.xml.namespace.QName("", "metakey"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("metarobot");
        elemField.setXmlName(new javax.xml.namespace.QName("", "metarobot"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("metaauthor");
        elemField.setXmlName(new javax.xml.namespace.QName("", "metaauthor"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("layout");
        elemField.setXmlName(new javax.xml.namespace.QName("", "layout"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("published");
        elemField.setXmlName(new javax.xml.namespace.QName("", "published"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("product_categories");
        elemField.setXmlName(new javax.xml.namespace.QName("", "product_categories"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("manufacturer_id");
        elemField.setXmlName(new javax.xml.namespace.QName("", "manufacturer_id"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("product_params");
        elemField.setXmlName(new javax.xml.namespace.QName("", "product_params"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("img_uri");
        elemField.setXmlName(new javax.xml.namespace.QName("", "img_uri"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("img_thumb_uri");
        elemField.setXmlName(new javax.xml.namespace.QName("", "img_thumb_uri"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("shared");
        elemField.setXmlName(new javax.xml.namespace.QName("", "shared"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("ordering");
        elemField.setXmlName(new javax.xml.namespace.QName("", "ordering"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("customtitle");
        elemField.setXmlName(new javax.xml.namespace.QName("", "customtitle"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("shopper_group_ids");
        elemField.setXmlName(new javax.xml.namespace.QName("", "shopper_group_ids"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("prices");
        elemField.setXmlName(new javax.xml.namespace.QName("", "prices"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.virtuemart.net/VM_Product/", "ProductPrice"));
        elemField.setNillable(true);
        elemField.setItemQName(new javax.xml.namespace.QName("", "ProductPrice"));
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("product_gtin");
        elemField.setXmlName(new javax.xml.namespace.QName("", "product_gtin"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("product_mpn");
        elemField.setXmlName(new javax.xml.namespace.QName("", "product_mpn"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
