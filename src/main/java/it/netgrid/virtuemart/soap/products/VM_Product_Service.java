/**
 * VM_Product_Service.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package it.netgrid.virtuemart.soap.products;

public interface VM_Product_Service extends javax.xml.rpc.Service {
    public java.lang.String getVM_ProductSOAPAddress();

    public it.netgrid.virtuemart.soap.products.VM_Product_PortType getVM_ProductSOAP() throws javax.xml.rpc.ServiceException;

    public it.netgrid.virtuemart.soap.products.VM_Product_PortType getVM_ProductSOAP(java.net.URL portAddress) throws javax.xml.rpc.ServiceException;
}
