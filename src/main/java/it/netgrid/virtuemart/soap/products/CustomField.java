/**
 * CustomField.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package it.netgrid.virtuemart.soap.products;

public class CustomField  implements java.io.Serializable {
    private java.lang.String virtuemart_customfield_id;

    private java.lang.String virtuemart_product_id;

    private java.lang.String virtuemart_custom_id;

    private java.lang.String custom_value;

    private java.lang.String custom_price;

    private java.lang.String custom_param;

    private java.lang.String published;

    public CustomField() {
    }

    public CustomField(
           java.lang.String virtuemart_customfield_id,
           java.lang.String virtuemart_product_id,
           java.lang.String virtuemart_custom_id,
           java.lang.String custom_value,
           java.lang.String custom_price,
           java.lang.String custom_param,
           java.lang.String published) {
           this.virtuemart_customfield_id = virtuemart_customfield_id;
           this.virtuemart_product_id = virtuemart_product_id;
           this.virtuemart_custom_id = virtuemart_custom_id;
           this.custom_value = custom_value;
           this.custom_price = custom_price;
           this.custom_param = custom_param;
           this.published = published;
    }


    /**
     * Gets the virtuemart_customfield_id value for this CustomField.
     * 
     * @return virtuemart_customfield_id
     */
    public java.lang.String getVirtuemart_customfield_id() {
        return virtuemart_customfield_id;
    }


    /**
     * Sets the virtuemart_customfield_id value for this CustomField.
     * 
     * @param virtuemart_customfield_id
     */
    public void setVirtuemart_customfield_id(java.lang.String virtuemart_customfield_id) {
        this.virtuemart_customfield_id = virtuemart_customfield_id;
    }


    /**
     * Gets the virtuemart_product_id value for this CustomField.
     * 
     * @return virtuemart_product_id
     */
    public java.lang.String getVirtuemart_product_id() {
        return virtuemart_product_id;
    }


    /**
     * Sets the virtuemart_product_id value for this CustomField.
     * 
     * @param virtuemart_product_id
     */
    public void setVirtuemart_product_id(java.lang.String virtuemart_product_id) {
        this.virtuemart_product_id = virtuemart_product_id;
    }


    /**
     * Gets the virtuemart_custom_id value for this CustomField.
     * 
     * @return virtuemart_custom_id
     */
    public java.lang.String getVirtuemart_custom_id() {
        return virtuemart_custom_id;
    }


    /**
     * Sets the virtuemart_custom_id value for this CustomField.
     * 
     * @param virtuemart_custom_id
     */
    public void setVirtuemart_custom_id(java.lang.String virtuemart_custom_id) {
        this.virtuemart_custom_id = virtuemart_custom_id;
    }


    /**
     * Gets the custom_value value for this CustomField.
     * 
     * @return custom_value
     */
    public java.lang.String getCustom_value() {
        return custom_value;
    }


    /**
     * Sets the custom_value value for this CustomField.
     * 
     * @param custom_value
     */
    public void setCustom_value(java.lang.String custom_value) {
        this.custom_value = custom_value;
    }


    /**
     * Gets the custom_price value for this CustomField.
     * 
     * @return custom_price
     */
    public java.lang.String getCustom_price() {
        return custom_price;
    }


    /**
     * Sets the custom_price value for this CustomField.
     * 
     * @param custom_price
     */
    public void setCustom_price(java.lang.String custom_price) {
        this.custom_price = custom_price;
    }


    /**
     * Gets the custom_param value for this CustomField.
     * 
     * @return custom_param
     */
    public java.lang.String getCustom_param() {
        return custom_param;
    }


    /**
     * Sets the custom_param value for this CustomField.
     * 
     * @param custom_param
     */
    public void setCustom_param(java.lang.String custom_param) {
        this.custom_param = custom_param;
    }


    /**
     * Gets the published value for this CustomField.
     * 
     * @return published
     */
    public java.lang.String getPublished() {
        return published;
    }


    /**
     * Sets the published value for this CustomField.
     * 
     * @param published
     */
    public void setPublished(java.lang.String published) {
        this.published = published;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof CustomField)) return false;
        CustomField other = (CustomField) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.virtuemart_customfield_id==null && other.getVirtuemart_customfield_id()==null) || 
             (this.virtuemart_customfield_id!=null &&
              this.virtuemart_customfield_id.equals(other.getVirtuemart_customfield_id()))) &&
            ((this.virtuemart_product_id==null && other.getVirtuemart_product_id()==null) || 
             (this.virtuemart_product_id!=null &&
              this.virtuemart_product_id.equals(other.getVirtuemart_product_id()))) &&
            ((this.virtuemart_custom_id==null && other.getVirtuemart_custom_id()==null) || 
             (this.virtuemart_custom_id!=null &&
              this.virtuemart_custom_id.equals(other.getVirtuemart_custom_id()))) &&
            ((this.custom_value==null && other.getCustom_value()==null) || 
             (this.custom_value!=null &&
              this.custom_value.equals(other.getCustom_value()))) &&
            ((this.custom_price==null && other.getCustom_price()==null) || 
             (this.custom_price!=null &&
              this.custom_price.equals(other.getCustom_price()))) &&
            ((this.custom_param==null && other.getCustom_param()==null) || 
             (this.custom_param!=null &&
              this.custom_param.equals(other.getCustom_param()))) &&
            ((this.published==null && other.getPublished()==null) || 
             (this.published!=null &&
              this.published.equals(other.getPublished())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getVirtuemart_customfield_id() != null) {
            _hashCode += getVirtuemart_customfield_id().hashCode();
        }
        if (getVirtuemart_product_id() != null) {
            _hashCode += getVirtuemart_product_id().hashCode();
        }
        if (getVirtuemart_custom_id() != null) {
            _hashCode += getVirtuemart_custom_id().hashCode();
        }
        if (getCustom_value() != null) {
            _hashCode += getCustom_value().hashCode();
        }
        if (getCustom_price() != null) {
            _hashCode += getCustom_price().hashCode();
        }
        if (getCustom_param() != null) {
            _hashCode += getCustom_param().hashCode();
        }
        if (getPublished() != null) {
            _hashCode += getPublished().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(CustomField.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://www.virtuemart.net/VM_Product/", "CustomField"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("virtuemart_customfield_id");
        elemField.setXmlName(new javax.xml.namespace.QName("", "virtuemart_customfield_id"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("virtuemart_product_id");
        elemField.setXmlName(new javax.xml.namespace.QName("", "virtuemart_product_id"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("virtuemart_custom_id");
        elemField.setXmlName(new javax.xml.namespace.QName("", "virtuemart_custom_id"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("custom_value");
        elemField.setXmlName(new javax.xml.namespace.QName("", "custom_value"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("custom_price");
        elemField.setXmlName(new javax.xml.namespace.QName("", "custom_price"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("custom_param");
        elemField.setXmlName(new javax.xml.namespace.QName("", "custom_param"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("published");
        elemField.setXmlName(new javax.xml.namespace.QName("", "published"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
