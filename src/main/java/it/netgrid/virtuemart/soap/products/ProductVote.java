/**
 * ProductVote.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package it.netgrid.virtuemart.soap.products;

public class ProductVote  implements java.io.Serializable {
    private java.lang.String rating_id;

    private java.lang.String product_id;

    private java.lang.String product_name;

    private java.lang.String product_sku;

    private java.lang.String rates;

    private java.lang.String ratingcount;

    private java.lang.String rating;

    private java.lang.String published;

    public ProductVote() {
    }

    public ProductVote(
           java.lang.String rating_id,
           java.lang.String product_id,
           java.lang.String product_name,
           java.lang.String product_sku,
           java.lang.String rates,
           java.lang.String ratingcount,
           java.lang.String rating,
           java.lang.String published) {
           this.rating_id = rating_id;
           this.product_id = product_id;
           this.product_name = product_name;
           this.product_sku = product_sku;
           this.rates = rates;
           this.ratingcount = ratingcount;
           this.rating = rating;
           this.published = published;
    }


    /**
     * Gets the rating_id value for this ProductVote.
     * 
     * @return rating_id
     */
    public java.lang.String getRating_id() {
        return rating_id;
    }


    /**
     * Sets the rating_id value for this ProductVote.
     * 
     * @param rating_id
     */
    public void setRating_id(java.lang.String rating_id) {
        this.rating_id = rating_id;
    }


    /**
     * Gets the product_id value for this ProductVote.
     * 
     * @return product_id
     */
    public java.lang.String getProduct_id() {
        return product_id;
    }


    /**
     * Sets the product_id value for this ProductVote.
     * 
     * @param product_id
     */
    public void setProduct_id(java.lang.String product_id) {
        this.product_id = product_id;
    }


    /**
     * Gets the product_name value for this ProductVote.
     * 
     * @return product_name
     */
    public java.lang.String getProduct_name() {
        return product_name;
    }


    /**
     * Sets the product_name value for this ProductVote.
     * 
     * @param product_name
     */
    public void setProduct_name(java.lang.String product_name) {
        this.product_name = product_name;
    }


    /**
     * Gets the product_sku value for this ProductVote.
     * 
     * @return product_sku
     */
    public java.lang.String getProduct_sku() {
        return product_sku;
    }


    /**
     * Sets the product_sku value for this ProductVote.
     * 
     * @param product_sku
     */
    public void setProduct_sku(java.lang.String product_sku) {
        this.product_sku = product_sku;
    }


    /**
     * Gets the rates value for this ProductVote.
     * 
     * @return rates
     */
    public java.lang.String getRates() {
        return rates;
    }


    /**
     * Sets the rates value for this ProductVote.
     * 
     * @param rates
     */
    public void setRates(java.lang.String rates) {
        this.rates = rates;
    }


    /**
     * Gets the ratingcount value for this ProductVote.
     * 
     * @return ratingcount
     */
    public java.lang.String getRatingcount() {
        return ratingcount;
    }


    /**
     * Sets the ratingcount value for this ProductVote.
     * 
     * @param ratingcount
     */
    public void setRatingcount(java.lang.String ratingcount) {
        this.ratingcount = ratingcount;
    }


    /**
     * Gets the rating value for this ProductVote.
     * 
     * @return rating
     */
    public java.lang.String getRating() {
        return rating;
    }


    /**
     * Sets the rating value for this ProductVote.
     * 
     * @param rating
     */
    public void setRating(java.lang.String rating) {
        this.rating = rating;
    }


    /**
     * Gets the published value for this ProductVote.
     * 
     * @return published
     */
    public java.lang.String getPublished() {
        return published;
    }


    /**
     * Sets the published value for this ProductVote.
     * 
     * @param published
     */
    public void setPublished(java.lang.String published) {
        this.published = published;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof ProductVote)) return false;
        ProductVote other = (ProductVote) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.rating_id==null && other.getRating_id()==null) || 
             (this.rating_id!=null &&
              this.rating_id.equals(other.getRating_id()))) &&
            ((this.product_id==null && other.getProduct_id()==null) || 
             (this.product_id!=null &&
              this.product_id.equals(other.getProduct_id()))) &&
            ((this.product_name==null && other.getProduct_name()==null) || 
             (this.product_name!=null &&
              this.product_name.equals(other.getProduct_name()))) &&
            ((this.product_sku==null && other.getProduct_sku()==null) || 
             (this.product_sku!=null &&
              this.product_sku.equals(other.getProduct_sku()))) &&
            ((this.rates==null && other.getRates()==null) || 
             (this.rates!=null &&
              this.rates.equals(other.getRates()))) &&
            ((this.ratingcount==null && other.getRatingcount()==null) || 
             (this.ratingcount!=null &&
              this.ratingcount.equals(other.getRatingcount()))) &&
            ((this.rating==null && other.getRating()==null) || 
             (this.rating!=null &&
              this.rating.equals(other.getRating()))) &&
            ((this.published==null && other.getPublished()==null) || 
             (this.published!=null &&
              this.published.equals(other.getPublished())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getRating_id() != null) {
            _hashCode += getRating_id().hashCode();
        }
        if (getProduct_id() != null) {
            _hashCode += getProduct_id().hashCode();
        }
        if (getProduct_name() != null) {
            _hashCode += getProduct_name().hashCode();
        }
        if (getProduct_sku() != null) {
            _hashCode += getProduct_sku().hashCode();
        }
        if (getRates() != null) {
            _hashCode += getRates().hashCode();
        }
        if (getRatingcount() != null) {
            _hashCode += getRatingcount().hashCode();
        }
        if (getRating() != null) {
            _hashCode += getRating().hashCode();
        }
        if (getPublished() != null) {
            _hashCode += getPublished().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(ProductVote.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://www.virtuemart.net/VM_Product/", "ProductVote"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("rating_id");
        elemField.setXmlName(new javax.xml.namespace.QName("", "rating_id"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("product_id");
        elemField.setXmlName(new javax.xml.namespace.QName("", "product_id"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("product_name");
        elemField.setXmlName(new javax.xml.namespace.QName("", "product_name"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("product_sku");
        elemField.setXmlName(new javax.xml.namespace.QName("", "product_sku"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("rates");
        elemField.setXmlName(new javax.xml.namespace.QName("", "rates"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("ratingcount");
        elemField.setXmlName(new javax.xml.namespace.QName("", "ratingcount"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("rating");
        elemField.setXmlName(new javax.xml.namespace.QName("", "rating"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("published");
        elemField.setXmlName(new javax.xml.namespace.QName("", "published"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
