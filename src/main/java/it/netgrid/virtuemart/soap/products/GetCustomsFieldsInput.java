/**
 * GetCustomsFieldsInput.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package it.netgrid.virtuemart.soap.products;

public class GetCustomsFieldsInput  implements java.io.Serializable {
    private it.netgrid.virtuemart.soap.products.LoginInfo loginInfo;

    private java.lang.String virtuemart_customfield_id;

    private java.lang.String virtuemart_product_id;

    private java.lang.String virtuemart_custom_id;

    private java.lang.String published;

    public GetCustomsFieldsInput() {
    }

    public GetCustomsFieldsInput(
           it.netgrid.virtuemart.soap.products.LoginInfo loginInfo,
           java.lang.String virtuemart_customfield_id,
           java.lang.String virtuemart_product_id,
           java.lang.String virtuemart_custom_id,
           java.lang.String published) {
           this.loginInfo = loginInfo;
           this.virtuemart_customfield_id = virtuemart_customfield_id;
           this.virtuemart_product_id = virtuemart_product_id;
           this.virtuemart_custom_id = virtuemart_custom_id;
           this.published = published;
    }


    /**
     * Gets the loginInfo value for this GetCustomsFieldsInput.
     * 
     * @return loginInfo
     */
    public it.netgrid.virtuemart.soap.products.LoginInfo getLoginInfo() {
        return loginInfo;
    }


    /**
     * Sets the loginInfo value for this GetCustomsFieldsInput.
     * 
     * @param loginInfo
     */
    public void setLoginInfo(it.netgrid.virtuemart.soap.products.LoginInfo loginInfo) {
        this.loginInfo = loginInfo;
    }


    /**
     * Gets the virtuemart_customfield_id value for this GetCustomsFieldsInput.
     * 
     * @return virtuemart_customfield_id
     */
    public java.lang.String getVirtuemart_customfield_id() {
        return virtuemart_customfield_id;
    }


    /**
     * Sets the virtuemart_customfield_id value for this GetCustomsFieldsInput.
     * 
     * @param virtuemart_customfield_id
     */
    public void setVirtuemart_customfield_id(java.lang.String virtuemart_customfield_id) {
        this.virtuemart_customfield_id = virtuemart_customfield_id;
    }


    /**
     * Gets the virtuemart_product_id value for this GetCustomsFieldsInput.
     * 
     * @return virtuemart_product_id
     */
    public java.lang.String getVirtuemart_product_id() {
        return virtuemart_product_id;
    }


    /**
     * Sets the virtuemart_product_id value for this GetCustomsFieldsInput.
     * 
     * @param virtuemart_product_id
     */
    public void setVirtuemart_product_id(java.lang.String virtuemart_product_id) {
        this.virtuemart_product_id = virtuemart_product_id;
    }


    /**
     * Gets the virtuemart_custom_id value for this GetCustomsFieldsInput.
     * 
     * @return virtuemart_custom_id
     */
    public java.lang.String getVirtuemart_custom_id() {
        return virtuemart_custom_id;
    }


    /**
     * Sets the virtuemart_custom_id value for this GetCustomsFieldsInput.
     * 
     * @param virtuemart_custom_id
     */
    public void setVirtuemart_custom_id(java.lang.String virtuemart_custom_id) {
        this.virtuemart_custom_id = virtuemart_custom_id;
    }


    /**
     * Gets the published value for this GetCustomsFieldsInput.
     * 
     * @return published
     */
    public java.lang.String getPublished() {
        return published;
    }


    /**
     * Sets the published value for this GetCustomsFieldsInput.
     * 
     * @param published
     */
    public void setPublished(java.lang.String published) {
        this.published = published;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof GetCustomsFieldsInput)) return false;
        GetCustomsFieldsInput other = (GetCustomsFieldsInput) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.loginInfo==null && other.getLoginInfo()==null) || 
             (this.loginInfo!=null &&
              this.loginInfo.equals(other.getLoginInfo()))) &&
            ((this.virtuemart_customfield_id==null && other.getVirtuemart_customfield_id()==null) || 
             (this.virtuemart_customfield_id!=null &&
              this.virtuemart_customfield_id.equals(other.getVirtuemart_customfield_id()))) &&
            ((this.virtuemart_product_id==null && other.getVirtuemart_product_id()==null) || 
             (this.virtuemart_product_id!=null &&
              this.virtuemart_product_id.equals(other.getVirtuemart_product_id()))) &&
            ((this.virtuemart_custom_id==null && other.getVirtuemart_custom_id()==null) || 
             (this.virtuemart_custom_id!=null &&
              this.virtuemart_custom_id.equals(other.getVirtuemart_custom_id()))) &&
            ((this.published==null && other.getPublished()==null) || 
             (this.published!=null &&
              this.published.equals(other.getPublished())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getLoginInfo() != null) {
            _hashCode += getLoginInfo().hashCode();
        }
        if (getVirtuemart_customfield_id() != null) {
            _hashCode += getVirtuemart_customfield_id().hashCode();
        }
        if (getVirtuemart_product_id() != null) {
            _hashCode += getVirtuemart_product_id().hashCode();
        }
        if (getVirtuemart_custom_id() != null) {
            _hashCode += getVirtuemart_custom_id().hashCode();
        }
        if (getPublished() != null) {
            _hashCode += getPublished().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(GetCustomsFieldsInput.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://www.virtuemart.net/VM_Product/", "GetCustomsFieldsInput"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("loginInfo");
        elemField.setXmlName(new javax.xml.namespace.QName("", "loginInfo"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.virtuemart.net/VM_Product/", "loginInfo"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("virtuemart_customfield_id");
        elemField.setXmlName(new javax.xml.namespace.QName("", "virtuemart_customfield_id"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("virtuemart_product_id");
        elemField.setXmlName(new javax.xml.namespace.QName("", "virtuemart_product_id"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("virtuemart_custom_id");
        elemField.setXmlName(new javax.xml.namespace.QName("", "virtuemart_custom_id"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("published");
        elemField.setXmlName(new javax.xml.namespace.QName("", "published"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
