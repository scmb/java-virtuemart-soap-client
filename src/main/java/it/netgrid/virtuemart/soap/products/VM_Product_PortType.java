/**
 * VM_Product_PortType.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package it.netgrid.virtuemart.soap.products;

public interface VM_Product_PortType extends java.rmi.Remote {
    public it.netgrid.virtuemart.soap.products.Product[] getProductsFromCategory(it.netgrid.virtuemart.soap.products.ProductFromCatIdInput parameters) throws java.rmi.RemoteException;
    public it.netgrid.virtuemart.soap.products.Product[] getChildsProduct(it.netgrid.virtuemart.soap.products.ProductFromIdInput parameters) throws java.rmi.RemoteException;
    public it.netgrid.virtuemart.soap.products.Product getProductFromId(it.netgrid.virtuemart.soap.products.ProductFromIdInput parameters) throws java.rmi.RemoteException;
    public it.netgrid.virtuemart.soap.products.Product getProductFromSKU(it.netgrid.virtuemart.soap.products.ProductFromSKUInput parameters) throws java.rmi.RemoteException;
    public it.netgrid.virtuemart.soap.products.CommonReturn updateProduct(it.netgrid.virtuemart.soap.products.UpdateProductInput parameters) throws java.rmi.RemoteException;
    public it.netgrid.virtuemart.soap.products.OrderItemInfo[] getProductsFromOrderId(it.netgrid.virtuemart.soap.products.ProductsFromOrderIdInput parameters) throws java.rmi.RemoteException;
    public it.netgrid.virtuemart.soap.products.CommonReturn addProduct(it.netgrid.virtuemart.soap.products.UpdateProductInput parameters) throws java.rmi.RemoteException;
    public it.netgrid.virtuemart.soap.products.CommonReturn deleteProduct(it.netgrid.virtuemart.soap.products.ProductFromIdInput parameters) throws java.rmi.RemoteException;
    public it.netgrid.virtuemart.soap.products.Currency[] getAllCurrency(it.netgrid.virtuemart.soap.products.GetAllCurrencyInput parameters) throws java.rmi.RemoteException;
    public it.netgrid.virtuemart.soap.products.Tax[] getAllTax(it.netgrid.virtuemart.soap.products.LoginInfo parameters) throws java.rmi.RemoteException;
    public it.netgrid.virtuemart.soap.products.CommonReturn addTax(it.netgrid.virtuemart.soap.products.TaxesInput parameters) throws java.rmi.RemoteException;
    public it.netgrid.virtuemart.soap.products.CommonReturn updateTax(it.netgrid.virtuemart.soap.products.TaxesInput parameters) throws java.rmi.RemoteException;
    public it.netgrid.virtuemart.soap.products.CommonReturn deleteTax(it.netgrid.virtuemart.soap.products.DelInput parameters) throws java.rmi.RemoteException;
    public it.netgrid.virtuemart.soap.products.Product[] getAllProducts(it.netgrid.virtuemart.soap.products.GetAllProductsInput parameters) throws java.rmi.RemoteException;
    public it.netgrid.virtuemart.soap.products.AvalaibleImage[] getAvailableImages(it.netgrid.virtuemart.soap.products.GetAvalaibleImageInput parameters) throws java.rmi.RemoteException;
    public it.netgrid.virtuemart.soap.products.ProductPrice[] getProductPrices(it.netgrid.virtuemart.soap.products.ProductPriceInput parameters) throws java.rmi.RemoteException;
    public it.netgrid.virtuemart.soap.products.CommonReturn addProductPrices(it.netgrid.virtuemart.soap.products.AddProductPricesInput parameters) throws java.rmi.RemoteException;
    public it.netgrid.virtuemart.soap.products.CommonReturn updateProductPrices(it.netgrid.virtuemart.soap.products.AddProductPricesInput parameters) throws java.rmi.RemoteException;
    public it.netgrid.virtuemart.soap.products.CommonReturn deleteProductPrices(it.netgrid.virtuemart.soap.products.DelInput parameters) throws java.rmi.RemoteException;
    public it.netgrid.virtuemart.soap.products.Discount[] getDiscount(it.netgrid.virtuemart.soap.products.GetDiscountInput parameters) throws java.rmi.RemoteException;
    public it.netgrid.virtuemart.soap.products.CommonReturn addDiscount(it.netgrid.virtuemart.soap.products.AddDiscountInput parameters) throws java.rmi.RemoteException;
    public it.netgrid.virtuemart.soap.products.CommonReturn updateDiscount(it.netgrid.virtuemart.soap.products.AddDiscountInput parameters) throws java.rmi.RemoteException;
    public it.netgrid.virtuemart.soap.products.CommonReturn deleteDiscount(it.netgrid.virtuemart.soap.products.DelInput parameters) throws java.rmi.RemoteException;
    public it.netgrid.virtuemart.soap.products.CommonReturn updateProductDiscount(it.netgrid.virtuemart.soap.products.UpdateProductDiscountInput parameters) throws java.rmi.RemoteException;
    public it.netgrid.virtuemart.soap.products.ProductFile[] getProductFile(it.netgrid.virtuemart.soap.products.GetProductFileInput parameters) throws java.rmi.RemoteException;
    public it.netgrid.virtuemart.soap.products.CommonReturn addProductFile(it.netgrid.virtuemart.soap.products.AddProductFileInput parameters) throws java.rmi.RemoteException;
    public it.netgrid.virtuemart.soap.products.CommonReturn updateProductFile(it.netgrid.virtuemart.soap.products.AddProductFileInput parameters) throws java.rmi.RemoteException;
    public it.netgrid.virtuemart.soap.products.CommonReturn deleteProductFile(it.netgrid.virtuemart.soap.products.DelInput parameters) throws java.rmi.RemoteException;
    public it.netgrid.virtuemart.soap.products.AvalaibleFile[] getAvailableFiles(it.netgrid.virtuemart.soap.products.LoginInfo parameters) throws java.rmi.RemoteException;
    public it.netgrid.virtuemart.soap.products.Product[] searchProducts(it.netgrid.virtuemart.soap.products.SearchProductsInput parameters) throws java.rmi.RemoteException;
    public it.netgrid.virtuemart.soap.products.ProductVote[] getProductVote(it.netgrid.virtuemart.soap.products.ProductVoteInput parameters) throws java.rmi.RemoteException;
    public it.netgrid.virtuemart.soap.products.ProductReview[] getProductReviews(it.netgrid.virtuemart.soap.products.ProductReviewInput parameters) throws java.rmi.RemoteException;
    public it.netgrid.virtuemart.soap.products.CommonReturn publishReviews(it.netgrid.virtuemart.soap.products.PublishReviewsInput parameters) throws java.rmi.RemoteException;
    public it.netgrid.virtuemart.soap.products.Product[] getRelatedProducts(it.netgrid.virtuemart.soap.products.GetRelatedProductsInput parameters) throws java.rmi.RemoteException;
    public it.netgrid.virtuemart.soap.products.CommonReturn setRelatedProducts(it.netgrid.virtuemart.soap.products.SetRelatedProductsInput parameters) throws java.rmi.RemoteException;
    public it.netgrid.virtuemart.soap.products.Media[] getMediaProduct(it.netgrid.virtuemart.soap.products.GetMediaProductsInput parameters) throws java.rmi.RemoteException;
    public it.netgrid.virtuemart.soap.products.CommonReturn addMediaProduct(it.netgrid.virtuemart.soap.products.AddMediaProductInput parameters) throws java.rmi.RemoteException;
    public it.netgrid.virtuemart.soap.products.Custom[] getCustomsList(it.netgrid.virtuemart.soap.products.GetCustomsListInput parameters) throws java.rmi.RemoteException;
    public it.netgrid.virtuemart.soap.products.CustomField[] getCustomsFields(it.netgrid.virtuemart.soap.products.GetCustomsFieldsInput parameters) throws java.rmi.RemoteException;
    public it.netgrid.virtuemart.soap.products.CommonReturn deleteMediaProduct(it.netgrid.virtuemart.soap.products.DelMediaInput parameters) throws java.rmi.RemoteException;
    public it.netgrid.virtuemart.soap.products.CommonReturn addCustomField(it.netgrid.virtuemart.soap.products.AddCustomFieldInput parameters) throws java.rmi.RemoteException;
    public it.netgrid.virtuemart.soap.products.CommonReturn updateCustomField(it.netgrid.virtuemart.soap.products.AddCustomFieldInput parameters) throws java.rmi.RemoteException;
    public it.netgrid.virtuemart.soap.products.CommonReturn deleteCustomField(it.netgrid.virtuemart.soap.products.AddCustomFieldInput parameters) throws java.rmi.RemoteException;
    public it.netgrid.virtuemart.soap.products.Worldzone[] getWorldZones(it.netgrid.virtuemart.soap.products.GetWorldzonesInput parameters) throws java.rmi.RemoteException;
    public it.netgrid.virtuemart.soap.products.CommonReturn addWorldZones(it.netgrid.virtuemart.soap.products.GetWorldzonesInput parameters) throws java.rmi.RemoteException;
    public it.netgrid.virtuemart.soap.products.CommonReturn updateWorldZones(it.netgrid.virtuemart.soap.products.GetWorldzonesInput parameters) throws java.rmi.RemoteException;
    public it.netgrid.virtuemart.soap.products.CommonReturn deleteWorldZones(it.netgrid.virtuemart.soap.products.GetWorldzonesInput parameters) throws java.rmi.RemoteException;
    public it.netgrid.virtuemart.soap.products.CommonReturn addCustomsList(it.netgrid.virtuemart.soap.products.AddCustomListInput parameters) throws java.rmi.RemoteException;
    public it.netgrid.virtuemart.soap.products.CommonReturn updateCustomsList(it.netgrid.virtuemart.soap.products.AddCustomListInput parameters) throws java.rmi.RemoteException;
    public it.netgrid.virtuemart.soap.products.CommonReturn deleteCustomsList(it.netgrid.virtuemart.soap.products.AddCustomListInput parameters) throws java.rmi.RemoteException;
}
