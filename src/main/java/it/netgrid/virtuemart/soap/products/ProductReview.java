/**
 * ProductReview.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package it.netgrid.virtuemart.soap.products;

public class ProductReview  implements java.io.Serializable {
    private java.lang.String review_id;

    private java.lang.String product_id;

    private java.lang.String comment;

    private java.lang.String review_ok;

    private java.lang.String review_rates;

    private java.lang.String review_ratingcount;

    private java.lang.String review_rating;

    private java.lang.String lastip;

    private java.lang.String published;

    public ProductReview() {
    }

    public ProductReview(
           java.lang.String review_id,
           java.lang.String product_id,
           java.lang.String comment,
           java.lang.String review_ok,
           java.lang.String review_rates,
           java.lang.String review_ratingcount,
           java.lang.String review_rating,
           java.lang.String lastip,
           java.lang.String published) {
           this.review_id = review_id;
           this.product_id = product_id;
           this.comment = comment;
           this.review_ok = review_ok;
           this.review_rates = review_rates;
           this.review_ratingcount = review_ratingcount;
           this.review_rating = review_rating;
           this.lastip = lastip;
           this.published = published;
    }


    /**
     * Gets the review_id value for this ProductReview.
     * 
     * @return review_id
     */
    public java.lang.String getReview_id() {
        return review_id;
    }


    /**
     * Sets the review_id value for this ProductReview.
     * 
     * @param review_id
     */
    public void setReview_id(java.lang.String review_id) {
        this.review_id = review_id;
    }


    /**
     * Gets the product_id value for this ProductReview.
     * 
     * @return product_id
     */
    public java.lang.String getProduct_id() {
        return product_id;
    }


    /**
     * Sets the product_id value for this ProductReview.
     * 
     * @param product_id
     */
    public void setProduct_id(java.lang.String product_id) {
        this.product_id = product_id;
    }


    /**
     * Gets the comment value for this ProductReview.
     * 
     * @return comment
     */
    public java.lang.String getComment() {
        return comment;
    }


    /**
     * Sets the comment value for this ProductReview.
     * 
     * @param comment
     */
    public void setComment(java.lang.String comment) {
        this.comment = comment;
    }


    /**
     * Gets the review_ok value for this ProductReview.
     * 
     * @return review_ok
     */
    public java.lang.String getReview_ok() {
        return review_ok;
    }


    /**
     * Sets the review_ok value for this ProductReview.
     * 
     * @param review_ok
     */
    public void setReview_ok(java.lang.String review_ok) {
        this.review_ok = review_ok;
    }


    /**
     * Gets the review_rates value for this ProductReview.
     * 
     * @return review_rates
     */
    public java.lang.String getReview_rates() {
        return review_rates;
    }


    /**
     * Sets the review_rates value for this ProductReview.
     * 
     * @param review_rates
     */
    public void setReview_rates(java.lang.String review_rates) {
        this.review_rates = review_rates;
    }


    /**
     * Gets the review_ratingcount value for this ProductReview.
     * 
     * @return review_ratingcount
     */
    public java.lang.String getReview_ratingcount() {
        return review_ratingcount;
    }


    /**
     * Sets the review_ratingcount value for this ProductReview.
     * 
     * @param review_ratingcount
     */
    public void setReview_ratingcount(java.lang.String review_ratingcount) {
        this.review_ratingcount = review_ratingcount;
    }


    /**
     * Gets the review_rating value for this ProductReview.
     * 
     * @return review_rating
     */
    public java.lang.String getReview_rating() {
        return review_rating;
    }


    /**
     * Sets the review_rating value for this ProductReview.
     * 
     * @param review_rating
     */
    public void setReview_rating(java.lang.String review_rating) {
        this.review_rating = review_rating;
    }


    /**
     * Gets the lastip value for this ProductReview.
     * 
     * @return lastip
     */
    public java.lang.String getLastip() {
        return lastip;
    }


    /**
     * Sets the lastip value for this ProductReview.
     * 
     * @param lastip
     */
    public void setLastip(java.lang.String lastip) {
        this.lastip = lastip;
    }


    /**
     * Gets the published value for this ProductReview.
     * 
     * @return published
     */
    public java.lang.String getPublished() {
        return published;
    }


    /**
     * Sets the published value for this ProductReview.
     * 
     * @param published
     */
    public void setPublished(java.lang.String published) {
        this.published = published;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof ProductReview)) return false;
        ProductReview other = (ProductReview) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.review_id==null && other.getReview_id()==null) || 
             (this.review_id!=null &&
              this.review_id.equals(other.getReview_id()))) &&
            ((this.product_id==null && other.getProduct_id()==null) || 
             (this.product_id!=null &&
              this.product_id.equals(other.getProduct_id()))) &&
            ((this.comment==null && other.getComment()==null) || 
             (this.comment!=null &&
              this.comment.equals(other.getComment()))) &&
            ((this.review_ok==null && other.getReview_ok()==null) || 
             (this.review_ok!=null &&
              this.review_ok.equals(other.getReview_ok()))) &&
            ((this.review_rates==null && other.getReview_rates()==null) || 
             (this.review_rates!=null &&
              this.review_rates.equals(other.getReview_rates()))) &&
            ((this.review_ratingcount==null && other.getReview_ratingcount()==null) || 
             (this.review_ratingcount!=null &&
              this.review_ratingcount.equals(other.getReview_ratingcount()))) &&
            ((this.review_rating==null && other.getReview_rating()==null) || 
             (this.review_rating!=null &&
              this.review_rating.equals(other.getReview_rating()))) &&
            ((this.lastip==null && other.getLastip()==null) || 
             (this.lastip!=null &&
              this.lastip.equals(other.getLastip()))) &&
            ((this.published==null && other.getPublished()==null) || 
             (this.published!=null &&
              this.published.equals(other.getPublished())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getReview_id() != null) {
            _hashCode += getReview_id().hashCode();
        }
        if (getProduct_id() != null) {
            _hashCode += getProduct_id().hashCode();
        }
        if (getComment() != null) {
            _hashCode += getComment().hashCode();
        }
        if (getReview_ok() != null) {
            _hashCode += getReview_ok().hashCode();
        }
        if (getReview_rates() != null) {
            _hashCode += getReview_rates().hashCode();
        }
        if (getReview_ratingcount() != null) {
            _hashCode += getReview_ratingcount().hashCode();
        }
        if (getReview_rating() != null) {
            _hashCode += getReview_rating().hashCode();
        }
        if (getLastip() != null) {
            _hashCode += getLastip().hashCode();
        }
        if (getPublished() != null) {
            _hashCode += getPublished().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(ProductReview.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://www.virtuemart.net/VM_Product/", "ProductReview"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("review_id");
        elemField.setXmlName(new javax.xml.namespace.QName("", "review_id"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("product_id");
        elemField.setXmlName(new javax.xml.namespace.QName("", "product_id"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("comment");
        elemField.setXmlName(new javax.xml.namespace.QName("", "comment"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("review_ok");
        elemField.setXmlName(new javax.xml.namespace.QName("", "review_ok"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("review_rates");
        elemField.setXmlName(new javax.xml.namespace.QName("", "review_rates"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("review_ratingcount");
        elemField.setXmlName(new javax.xml.namespace.QName("", "review_ratingcount"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("review_rating");
        elemField.setXmlName(new javax.xml.namespace.QName("", "review_rating"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("lastip");
        elemField.setXmlName(new javax.xml.namespace.QName("", "lastip"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("published");
        elemField.setXmlName(new javax.xml.namespace.QName("", "published"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
