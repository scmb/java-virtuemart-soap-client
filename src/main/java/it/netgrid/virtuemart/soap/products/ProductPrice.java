/**
 * ProductPrice.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package it.netgrid.virtuemart.soap.products;

public class ProductPrice  implements java.io.Serializable {
    private java.lang.String product_price_id;

    private java.lang.String product_id;

    private java.lang.String product_price;

    private java.lang.String product_currency;

    private java.lang.String product_price_vdate;

    private java.lang.String product_price_edate;

    private java.lang.String created_on;

    private java.lang.String modified_on;

    private java.lang.String shopper_group_id;

    private java.lang.String price_quantity_start;

    private java.lang.String price_quantity_end;

    private java.lang.String override;

    private java.lang.String product_override_price;

    private java.lang.String product_tax_id;

    private java.lang.String product_discount_id;

    private java.lang.String product_final_price;

    private java.lang.String product_price_info;

    public ProductPrice() {
    }

    public ProductPrice(
           java.lang.String product_price_id,
           java.lang.String product_id,
           java.lang.String product_price,
           java.lang.String product_currency,
           java.lang.String product_price_vdate,
           java.lang.String product_price_edate,
           java.lang.String created_on,
           java.lang.String modified_on,
           java.lang.String shopper_group_id,
           java.lang.String price_quantity_start,
           java.lang.String price_quantity_end,
           java.lang.String override,
           java.lang.String product_override_price,
           java.lang.String product_tax_id,
           java.lang.String product_discount_id,
           java.lang.String product_final_price,
           java.lang.String product_price_info) {
           this.product_price_id = product_price_id;
           this.product_id = product_id;
           this.product_price = product_price;
           this.product_currency = product_currency;
           this.product_price_vdate = product_price_vdate;
           this.product_price_edate = product_price_edate;
           this.created_on = created_on;
           this.modified_on = modified_on;
           this.shopper_group_id = shopper_group_id;
           this.price_quantity_start = price_quantity_start;
           this.price_quantity_end = price_quantity_end;
           this.override = override;
           this.product_override_price = product_override_price;
           this.product_tax_id = product_tax_id;
           this.product_discount_id = product_discount_id;
           this.product_final_price = product_final_price;
           this.product_price_info = product_price_info;
    }


    /**
     * Gets the product_price_id value for this ProductPrice.
     * 
     * @return product_price_id
     */
    public java.lang.String getProduct_price_id() {
        return product_price_id;
    }


    /**
     * Sets the product_price_id value for this ProductPrice.
     * 
     * @param product_price_id
     */
    public void setProduct_price_id(java.lang.String product_price_id) {
        this.product_price_id = product_price_id;
    }


    /**
     * Gets the product_id value for this ProductPrice.
     * 
     * @return product_id
     */
    public java.lang.String getProduct_id() {
        return product_id;
    }


    /**
     * Sets the product_id value for this ProductPrice.
     * 
     * @param product_id
     */
    public void setProduct_id(java.lang.String product_id) {
        this.product_id = product_id;
    }


    /**
     * Gets the product_price value for this ProductPrice.
     * 
     * @return product_price
     */
    public java.lang.String getProduct_price() {
        return product_price;
    }


    /**
     * Sets the product_price value for this ProductPrice.
     * 
     * @param product_price
     */
    public void setProduct_price(java.lang.String product_price) {
        this.product_price = product_price;
    }


    /**
     * Gets the product_currency value for this ProductPrice.
     * 
     * @return product_currency
     */
    public java.lang.String getProduct_currency() {
        return product_currency;
    }


    /**
     * Sets the product_currency value for this ProductPrice.
     * 
     * @param product_currency
     */
    public void setProduct_currency(java.lang.String product_currency) {
        this.product_currency = product_currency;
    }


    /**
     * Gets the product_price_vdate value for this ProductPrice.
     * 
     * @return product_price_vdate
     */
    public java.lang.String getProduct_price_vdate() {
        return product_price_vdate;
    }


    /**
     * Sets the product_price_vdate value for this ProductPrice.
     * 
     * @param product_price_vdate
     */
    public void setProduct_price_vdate(java.lang.String product_price_vdate) {
        this.product_price_vdate = product_price_vdate;
    }


    /**
     * Gets the product_price_edate value for this ProductPrice.
     * 
     * @return product_price_edate
     */
    public java.lang.String getProduct_price_edate() {
        return product_price_edate;
    }


    /**
     * Sets the product_price_edate value for this ProductPrice.
     * 
     * @param product_price_edate
     */
    public void setProduct_price_edate(java.lang.String product_price_edate) {
        this.product_price_edate = product_price_edate;
    }


    /**
     * Gets the created_on value for this ProductPrice.
     * 
     * @return created_on
     */
    public java.lang.String getCreated_on() {
        return created_on;
    }


    /**
     * Sets the created_on value for this ProductPrice.
     * 
     * @param created_on
     */
    public void setCreated_on(java.lang.String created_on) {
        this.created_on = created_on;
    }


    /**
     * Gets the modified_on value for this ProductPrice.
     * 
     * @return modified_on
     */
    public java.lang.String getModified_on() {
        return modified_on;
    }


    /**
     * Sets the modified_on value for this ProductPrice.
     * 
     * @param modified_on
     */
    public void setModified_on(java.lang.String modified_on) {
        this.modified_on = modified_on;
    }


    /**
     * Gets the shopper_group_id value for this ProductPrice.
     * 
     * @return shopper_group_id
     */
    public java.lang.String getShopper_group_id() {
        return shopper_group_id;
    }


    /**
     * Sets the shopper_group_id value for this ProductPrice.
     * 
     * @param shopper_group_id
     */
    public void setShopper_group_id(java.lang.String shopper_group_id) {
        this.shopper_group_id = shopper_group_id;
    }


    /**
     * Gets the price_quantity_start value for this ProductPrice.
     * 
     * @return price_quantity_start
     */
    public java.lang.String getPrice_quantity_start() {
        return price_quantity_start;
    }


    /**
     * Sets the price_quantity_start value for this ProductPrice.
     * 
     * @param price_quantity_start
     */
    public void setPrice_quantity_start(java.lang.String price_quantity_start) {
        this.price_quantity_start = price_quantity_start;
    }


    /**
     * Gets the price_quantity_end value for this ProductPrice.
     * 
     * @return price_quantity_end
     */
    public java.lang.String getPrice_quantity_end() {
        return price_quantity_end;
    }


    /**
     * Sets the price_quantity_end value for this ProductPrice.
     * 
     * @param price_quantity_end
     */
    public void setPrice_quantity_end(java.lang.String price_quantity_end) {
        this.price_quantity_end = price_quantity_end;
    }


    /**
     * Gets the override value for this ProductPrice.
     * 
     * @return override
     */
    public java.lang.String getOverride() {
        return override;
    }


    /**
     * Sets the override value for this ProductPrice.
     * 
     * @param override
     */
    public void setOverride(java.lang.String override) {
        this.override = override;
    }


    /**
     * Gets the product_override_price value for this ProductPrice.
     * 
     * @return product_override_price
     */
    public java.lang.String getProduct_override_price() {
        return product_override_price;
    }


    /**
     * Sets the product_override_price value for this ProductPrice.
     * 
     * @param product_override_price
     */
    public void setProduct_override_price(java.lang.String product_override_price) {
        this.product_override_price = product_override_price;
    }


    /**
     * Gets the product_tax_id value for this ProductPrice.
     * 
     * @return product_tax_id
     */
    public java.lang.String getProduct_tax_id() {
        return product_tax_id;
    }


    /**
     * Sets the product_tax_id value for this ProductPrice.
     * 
     * @param product_tax_id
     */
    public void setProduct_tax_id(java.lang.String product_tax_id) {
        this.product_tax_id = product_tax_id;
    }


    /**
     * Gets the product_discount_id value for this ProductPrice.
     * 
     * @return product_discount_id
     */
    public java.lang.String getProduct_discount_id() {
        return product_discount_id;
    }


    /**
     * Sets the product_discount_id value for this ProductPrice.
     * 
     * @param product_discount_id
     */
    public void setProduct_discount_id(java.lang.String product_discount_id) {
        this.product_discount_id = product_discount_id;
    }


    /**
     * Gets the product_final_price value for this ProductPrice.
     * 
     * @return product_final_price
     */
    public java.lang.String getProduct_final_price() {
        return product_final_price;
    }


    /**
     * Sets the product_final_price value for this ProductPrice.
     * 
     * @param product_final_price
     */
    public void setProduct_final_price(java.lang.String product_final_price) {
        this.product_final_price = product_final_price;
    }


    /**
     * Gets the product_price_info value for this ProductPrice.
     * 
     * @return product_price_info
     */
    public java.lang.String getProduct_price_info() {
        return product_price_info;
    }


    /**
     * Sets the product_price_info value for this ProductPrice.
     * 
     * @param product_price_info
     */
    public void setProduct_price_info(java.lang.String product_price_info) {
        this.product_price_info = product_price_info;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof ProductPrice)) return false;
        ProductPrice other = (ProductPrice) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.product_price_id==null && other.getProduct_price_id()==null) || 
             (this.product_price_id!=null &&
              this.product_price_id.equals(other.getProduct_price_id()))) &&
            ((this.product_id==null && other.getProduct_id()==null) || 
             (this.product_id!=null &&
              this.product_id.equals(other.getProduct_id()))) &&
            ((this.product_price==null && other.getProduct_price()==null) || 
             (this.product_price!=null &&
              this.product_price.equals(other.getProduct_price()))) &&
            ((this.product_currency==null && other.getProduct_currency()==null) || 
             (this.product_currency!=null &&
              this.product_currency.equals(other.getProduct_currency()))) &&
            ((this.product_price_vdate==null && other.getProduct_price_vdate()==null) || 
             (this.product_price_vdate!=null &&
              this.product_price_vdate.equals(other.getProduct_price_vdate()))) &&
            ((this.product_price_edate==null && other.getProduct_price_edate()==null) || 
             (this.product_price_edate!=null &&
              this.product_price_edate.equals(other.getProduct_price_edate()))) &&
            ((this.created_on==null && other.getCreated_on()==null) || 
             (this.created_on!=null &&
              this.created_on.equals(other.getCreated_on()))) &&
            ((this.modified_on==null && other.getModified_on()==null) || 
             (this.modified_on!=null &&
              this.modified_on.equals(other.getModified_on()))) &&
            ((this.shopper_group_id==null && other.getShopper_group_id()==null) || 
             (this.shopper_group_id!=null &&
              this.shopper_group_id.equals(other.getShopper_group_id()))) &&
            ((this.price_quantity_start==null && other.getPrice_quantity_start()==null) || 
             (this.price_quantity_start!=null &&
              this.price_quantity_start.equals(other.getPrice_quantity_start()))) &&
            ((this.price_quantity_end==null && other.getPrice_quantity_end()==null) || 
             (this.price_quantity_end!=null &&
              this.price_quantity_end.equals(other.getPrice_quantity_end()))) &&
            ((this.override==null && other.getOverride()==null) || 
             (this.override!=null &&
              this.override.equals(other.getOverride()))) &&
            ((this.product_override_price==null && other.getProduct_override_price()==null) || 
             (this.product_override_price!=null &&
              this.product_override_price.equals(other.getProduct_override_price()))) &&
            ((this.product_tax_id==null && other.getProduct_tax_id()==null) || 
             (this.product_tax_id!=null &&
              this.product_tax_id.equals(other.getProduct_tax_id()))) &&
            ((this.product_discount_id==null && other.getProduct_discount_id()==null) || 
             (this.product_discount_id!=null &&
              this.product_discount_id.equals(other.getProduct_discount_id()))) &&
            ((this.product_final_price==null && other.getProduct_final_price()==null) || 
             (this.product_final_price!=null &&
              this.product_final_price.equals(other.getProduct_final_price()))) &&
            ((this.product_price_info==null && other.getProduct_price_info()==null) || 
             (this.product_price_info!=null &&
              this.product_price_info.equals(other.getProduct_price_info())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getProduct_price_id() != null) {
            _hashCode += getProduct_price_id().hashCode();
        }
        if (getProduct_id() != null) {
            _hashCode += getProduct_id().hashCode();
        }
        if (getProduct_price() != null) {
            _hashCode += getProduct_price().hashCode();
        }
        if (getProduct_currency() != null) {
            _hashCode += getProduct_currency().hashCode();
        }
        if (getProduct_price_vdate() != null) {
            _hashCode += getProduct_price_vdate().hashCode();
        }
        if (getProduct_price_edate() != null) {
            _hashCode += getProduct_price_edate().hashCode();
        }
        if (getCreated_on() != null) {
            _hashCode += getCreated_on().hashCode();
        }
        if (getModified_on() != null) {
            _hashCode += getModified_on().hashCode();
        }
        if (getShopper_group_id() != null) {
            _hashCode += getShopper_group_id().hashCode();
        }
        if (getPrice_quantity_start() != null) {
            _hashCode += getPrice_quantity_start().hashCode();
        }
        if (getPrice_quantity_end() != null) {
            _hashCode += getPrice_quantity_end().hashCode();
        }
        if (getOverride() != null) {
            _hashCode += getOverride().hashCode();
        }
        if (getProduct_override_price() != null) {
            _hashCode += getProduct_override_price().hashCode();
        }
        if (getProduct_tax_id() != null) {
            _hashCode += getProduct_tax_id().hashCode();
        }
        if (getProduct_discount_id() != null) {
            _hashCode += getProduct_discount_id().hashCode();
        }
        if (getProduct_final_price() != null) {
            _hashCode += getProduct_final_price().hashCode();
        }
        if (getProduct_price_info() != null) {
            _hashCode += getProduct_price_info().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(ProductPrice.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://www.virtuemart.net/VM_Product/", "ProductPrice"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("product_price_id");
        elemField.setXmlName(new javax.xml.namespace.QName("", "product_price_id"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("product_id");
        elemField.setXmlName(new javax.xml.namespace.QName("", "product_id"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("product_price");
        elemField.setXmlName(new javax.xml.namespace.QName("", "product_price"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("product_currency");
        elemField.setXmlName(new javax.xml.namespace.QName("", "product_currency"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("product_price_vdate");
        elemField.setXmlName(new javax.xml.namespace.QName("", "product_price_vdate"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("product_price_edate");
        elemField.setXmlName(new javax.xml.namespace.QName("", "product_price_edate"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("created_on");
        elemField.setXmlName(new javax.xml.namespace.QName("", "created_on"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("modified_on");
        elemField.setXmlName(new javax.xml.namespace.QName("", "modified_on"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("shopper_group_id");
        elemField.setXmlName(new javax.xml.namespace.QName("", "shopper_group_id"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("price_quantity_start");
        elemField.setXmlName(new javax.xml.namespace.QName("", "price_quantity_start"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("price_quantity_end");
        elemField.setXmlName(new javax.xml.namespace.QName("", "price_quantity_end"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("override");
        elemField.setXmlName(new javax.xml.namespace.QName("", "override"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("product_override_price");
        elemField.setXmlName(new javax.xml.namespace.QName("", "product_override_price"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("product_tax_id");
        elemField.setXmlName(new javax.xml.namespace.QName("", "product_tax_id"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("product_discount_id");
        elemField.setXmlName(new javax.xml.namespace.QName("", "product_discount_id"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("product_final_price");
        elemField.setXmlName(new javax.xml.namespace.QName("", "product_final_price"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("product_price_info");
        elemField.setXmlName(new javax.xml.namespace.QName("", "product_price_info"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
