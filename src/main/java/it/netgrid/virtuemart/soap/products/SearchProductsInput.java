/**
 * SearchProductsInput.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package it.netgrid.virtuemart.soap.products;

public class SearchProductsInput  implements java.io.Serializable {
    private it.netgrid.virtuemart.soap.products.LoginInfo loginInfo;

    private java.lang.String product_id;

    private java.lang.String product_sku;

    private java.lang.String product_name;

    private java.lang.String product_desc;

    private java.lang.String product_sdesc;

    private java.lang.String product_categories;

    private java.lang.String product_sales;

    private java.lang.String price;

    private java.lang.String quantity;

    private java.lang.String parent_product_id;

    private java.lang.String product_publish;

    private java.lang.String product_currency;

    private java.lang.String manufacturer_id;

    private java.lang.String vendor_id;

    private java.lang.String shopper_group_id;

    private java.lang.String product_weight;

    private java.lang.String product_weight_uom;

    private java.lang.String product_length;

    private java.lang.String product_width;

    private java.lang.String product_height;

    private java.lang.String product_lwh_uom;

    private java.lang.String product_unit;

    private java.lang.String product_packaging;

    private java.lang.String product_url;

    private java.lang.String product_special;

    private java.lang.String with_childs;

    private java.lang.String operator;

    private java.lang.String operator_more_less_equal;

    private java.lang.String limite_start;

    private java.lang.String limite_end;

    private java.lang.String include_prices;

    public SearchProductsInput() {
    }

    public SearchProductsInput(
           it.netgrid.virtuemart.soap.products.LoginInfo loginInfo,
           java.lang.String product_id,
           java.lang.String product_sku,
           java.lang.String product_name,
           java.lang.String product_desc,
           java.lang.String product_sdesc,
           java.lang.String product_categories,
           java.lang.String product_sales,
           java.lang.String price,
           java.lang.String quantity,
           java.lang.String parent_product_id,
           java.lang.String product_publish,
           java.lang.String product_currency,
           java.lang.String manufacturer_id,
           java.lang.String vendor_id,
           java.lang.String shopper_group_id,
           java.lang.String product_weight,
           java.lang.String product_weight_uom,
           java.lang.String product_length,
           java.lang.String product_width,
           java.lang.String product_height,
           java.lang.String product_lwh_uom,
           java.lang.String product_unit,
           java.lang.String product_packaging,
           java.lang.String product_url,
           java.lang.String product_special,
           java.lang.String with_childs,
           java.lang.String operator,
           java.lang.String operator_more_less_equal,
           java.lang.String limite_start,
           java.lang.String limite_end,
           java.lang.String include_prices) {
           this.loginInfo = loginInfo;
           this.product_id = product_id;
           this.product_sku = product_sku;
           this.product_name = product_name;
           this.product_desc = product_desc;
           this.product_sdesc = product_sdesc;
           this.product_categories = product_categories;
           this.product_sales = product_sales;
           this.price = price;
           this.quantity = quantity;
           this.parent_product_id = parent_product_id;
           this.product_publish = product_publish;
           this.product_currency = product_currency;
           this.manufacturer_id = manufacturer_id;
           this.vendor_id = vendor_id;
           this.shopper_group_id = shopper_group_id;
           this.product_weight = product_weight;
           this.product_weight_uom = product_weight_uom;
           this.product_length = product_length;
           this.product_width = product_width;
           this.product_height = product_height;
           this.product_lwh_uom = product_lwh_uom;
           this.product_unit = product_unit;
           this.product_packaging = product_packaging;
           this.product_url = product_url;
           this.product_special = product_special;
           this.with_childs = with_childs;
           this.operator = operator;
           this.operator_more_less_equal = operator_more_less_equal;
           this.limite_start = limite_start;
           this.limite_end = limite_end;
           this.include_prices = include_prices;
    }


    /**
     * Gets the loginInfo value for this SearchProductsInput.
     * 
     * @return loginInfo
     */
    public it.netgrid.virtuemart.soap.products.LoginInfo getLoginInfo() {
        return loginInfo;
    }


    /**
     * Sets the loginInfo value for this SearchProductsInput.
     * 
     * @param loginInfo
     */
    public void setLoginInfo(it.netgrid.virtuemart.soap.products.LoginInfo loginInfo) {
        this.loginInfo = loginInfo;
    }


    /**
     * Gets the product_id value for this SearchProductsInput.
     * 
     * @return product_id
     */
    public java.lang.String getProduct_id() {
        return product_id;
    }


    /**
     * Sets the product_id value for this SearchProductsInput.
     * 
     * @param product_id
     */
    public void setProduct_id(java.lang.String product_id) {
        this.product_id = product_id;
    }


    /**
     * Gets the product_sku value for this SearchProductsInput.
     * 
     * @return product_sku
     */
    public java.lang.String getProduct_sku() {
        return product_sku;
    }


    /**
     * Sets the product_sku value for this SearchProductsInput.
     * 
     * @param product_sku
     */
    public void setProduct_sku(java.lang.String product_sku) {
        this.product_sku = product_sku;
    }


    /**
     * Gets the product_name value for this SearchProductsInput.
     * 
     * @return product_name
     */
    public java.lang.String getProduct_name() {
        return product_name;
    }


    /**
     * Sets the product_name value for this SearchProductsInput.
     * 
     * @param product_name
     */
    public void setProduct_name(java.lang.String product_name) {
        this.product_name = product_name;
    }


    /**
     * Gets the product_desc value for this SearchProductsInput.
     * 
     * @return product_desc
     */
    public java.lang.String getProduct_desc() {
        return product_desc;
    }


    /**
     * Sets the product_desc value for this SearchProductsInput.
     * 
     * @param product_desc
     */
    public void setProduct_desc(java.lang.String product_desc) {
        this.product_desc = product_desc;
    }


    /**
     * Gets the product_sdesc value for this SearchProductsInput.
     * 
     * @return product_sdesc
     */
    public java.lang.String getProduct_sdesc() {
        return product_sdesc;
    }


    /**
     * Sets the product_sdesc value for this SearchProductsInput.
     * 
     * @param product_sdesc
     */
    public void setProduct_sdesc(java.lang.String product_sdesc) {
        this.product_sdesc = product_sdesc;
    }


    /**
     * Gets the product_categories value for this SearchProductsInput.
     * 
     * @return product_categories
     */
    public java.lang.String getProduct_categories() {
        return product_categories;
    }


    /**
     * Sets the product_categories value for this SearchProductsInput.
     * 
     * @param product_categories
     */
    public void setProduct_categories(java.lang.String product_categories) {
        this.product_categories = product_categories;
    }


    /**
     * Gets the product_sales value for this SearchProductsInput.
     * 
     * @return product_sales
     */
    public java.lang.String getProduct_sales() {
        return product_sales;
    }


    /**
     * Sets the product_sales value for this SearchProductsInput.
     * 
     * @param product_sales
     */
    public void setProduct_sales(java.lang.String product_sales) {
        this.product_sales = product_sales;
    }


    /**
     * Gets the price value for this SearchProductsInput.
     * 
     * @return price
     */
    public java.lang.String getPrice() {
        return price;
    }


    /**
     * Sets the price value for this SearchProductsInput.
     * 
     * @param price
     */
    public void setPrice(java.lang.String price) {
        this.price = price;
    }


    /**
     * Gets the quantity value for this SearchProductsInput.
     * 
     * @return quantity
     */
    public java.lang.String getQuantity() {
        return quantity;
    }


    /**
     * Sets the quantity value for this SearchProductsInput.
     * 
     * @param quantity
     */
    public void setQuantity(java.lang.String quantity) {
        this.quantity = quantity;
    }


    /**
     * Gets the parent_product_id value for this SearchProductsInput.
     * 
     * @return parent_product_id
     */
    public java.lang.String getParent_product_id() {
        return parent_product_id;
    }


    /**
     * Sets the parent_product_id value for this SearchProductsInput.
     * 
     * @param parent_product_id
     */
    public void setParent_product_id(java.lang.String parent_product_id) {
        this.parent_product_id = parent_product_id;
    }


    /**
     * Gets the product_publish value for this SearchProductsInput.
     * 
     * @return product_publish
     */
    public java.lang.String getProduct_publish() {
        return product_publish;
    }


    /**
     * Sets the product_publish value for this SearchProductsInput.
     * 
     * @param product_publish
     */
    public void setProduct_publish(java.lang.String product_publish) {
        this.product_publish = product_publish;
    }


    /**
     * Gets the product_currency value for this SearchProductsInput.
     * 
     * @return product_currency
     */
    public java.lang.String getProduct_currency() {
        return product_currency;
    }


    /**
     * Sets the product_currency value for this SearchProductsInput.
     * 
     * @param product_currency
     */
    public void setProduct_currency(java.lang.String product_currency) {
        this.product_currency = product_currency;
    }


    /**
     * Gets the manufacturer_id value for this SearchProductsInput.
     * 
     * @return manufacturer_id
     */
    public java.lang.String getManufacturer_id() {
        return manufacturer_id;
    }


    /**
     * Sets the manufacturer_id value for this SearchProductsInput.
     * 
     * @param manufacturer_id
     */
    public void setManufacturer_id(java.lang.String manufacturer_id) {
        this.manufacturer_id = manufacturer_id;
    }


    /**
     * Gets the vendor_id value for this SearchProductsInput.
     * 
     * @return vendor_id
     */
    public java.lang.String getVendor_id() {
        return vendor_id;
    }


    /**
     * Sets the vendor_id value for this SearchProductsInput.
     * 
     * @param vendor_id
     */
    public void setVendor_id(java.lang.String vendor_id) {
        this.vendor_id = vendor_id;
    }


    /**
     * Gets the shopper_group_id value for this SearchProductsInput.
     * 
     * @return shopper_group_id
     */
    public java.lang.String getShopper_group_id() {
        return shopper_group_id;
    }


    /**
     * Sets the shopper_group_id value for this SearchProductsInput.
     * 
     * @param shopper_group_id
     */
    public void setShopper_group_id(java.lang.String shopper_group_id) {
        this.shopper_group_id = shopper_group_id;
    }


    /**
     * Gets the product_weight value for this SearchProductsInput.
     * 
     * @return product_weight
     */
    public java.lang.String getProduct_weight() {
        return product_weight;
    }


    /**
     * Sets the product_weight value for this SearchProductsInput.
     * 
     * @param product_weight
     */
    public void setProduct_weight(java.lang.String product_weight) {
        this.product_weight = product_weight;
    }


    /**
     * Gets the product_weight_uom value for this SearchProductsInput.
     * 
     * @return product_weight_uom
     */
    public java.lang.String getProduct_weight_uom() {
        return product_weight_uom;
    }


    /**
     * Sets the product_weight_uom value for this SearchProductsInput.
     * 
     * @param product_weight_uom
     */
    public void setProduct_weight_uom(java.lang.String product_weight_uom) {
        this.product_weight_uom = product_weight_uom;
    }


    /**
     * Gets the product_length value for this SearchProductsInput.
     * 
     * @return product_length
     */
    public java.lang.String getProduct_length() {
        return product_length;
    }


    /**
     * Sets the product_length value for this SearchProductsInput.
     * 
     * @param product_length
     */
    public void setProduct_length(java.lang.String product_length) {
        this.product_length = product_length;
    }


    /**
     * Gets the product_width value for this SearchProductsInput.
     * 
     * @return product_width
     */
    public java.lang.String getProduct_width() {
        return product_width;
    }


    /**
     * Sets the product_width value for this SearchProductsInput.
     * 
     * @param product_width
     */
    public void setProduct_width(java.lang.String product_width) {
        this.product_width = product_width;
    }


    /**
     * Gets the product_height value for this SearchProductsInput.
     * 
     * @return product_height
     */
    public java.lang.String getProduct_height() {
        return product_height;
    }


    /**
     * Sets the product_height value for this SearchProductsInput.
     * 
     * @param product_height
     */
    public void setProduct_height(java.lang.String product_height) {
        this.product_height = product_height;
    }


    /**
     * Gets the product_lwh_uom value for this SearchProductsInput.
     * 
     * @return product_lwh_uom
     */
    public java.lang.String getProduct_lwh_uom() {
        return product_lwh_uom;
    }


    /**
     * Sets the product_lwh_uom value for this SearchProductsInput.
     * 
     * @param product_lwh_uom
     */
    public void setProduct_lwh_uom(java.lang.String product_lwh_uom) {
        this.product_lwh_uom = product_lwh_uom;
    }


    /**
     * Gets the product_unit value for this SearchProductsInput.
     * 
     * @return product_unit
     */
    public java.lang.String getProduct_unit() {
        return product_unit;
    }


    /**
     * Sets the product_unit value for this SearchProductsInput.
     * 
     * @param product_unit
     */
    public void setProduct_unit(java.lang.String product_unit) {
        this.product_unit = product_unit;
    }


    /**
     * Gets the product_packaging value for this SearchProductsInput.
     * 
     * @return product_packaging
     */
    public java.lang.String getProduct_packaging() {
        return product_packaging;
    }


    /**
     * Sets the product_packaging value for this SearchProductsInput.
     * 
     * @param product_packaging
     */
    public void setProduct_packaging(java.lang.String product_packaging) {
        this.product_packaging = product_packaging;
    }


    /**
     * Gets the product_url value for this SearchProductsInput.
     * 
     * @return product_url
     */
    public java.lang.String getProduct_url() {
        return product_url;
    }


    /**
     * Sets the product_url value for this SearchProductsInput.
     * 
     * @param product_url
     */
    public void setProduct_url(java.lang.String product_url) {
        this.product_url = product_url;
    }


    /**
     * Gets the product_special value for this SearchProductsInput.
     * 
     * @return product_special
     */
    public java.lang.String getProduct_special() {
        return product_special;
    }


    /**
     * Sets the product_special value for this SearchProductsInput.
     * 
     * @param product_special
     */
    public void setProduct_special(java.lang.String product_special) {
        this.product_special = product_special;
    }


    /**
     * Gets the with_childs value for this SearchProductsInput.
     * 
     * @return with_childs
     */
    public java.lang.String getWith_childs() {
        return with_childs;
    }


    /**
     * Sets the with_childs value for this SearchProductsInput.
     * 
     * @param with_childs
     */
    public void setWith_childs(java.lang.String with_childs) {
        this.with_childs = with_childs;
    }


    /**
     * Gets the operator value for this SearchProductsInput.
     * 
     * @return operator
     */
    public java.lang.String getOperator() {
        return operator;
    }


    /**
     * Sets the operator value for this SearchProductsInput.
     * 
     * @param operator
     */
    public void setOperator(java.lang.String operator) {
        this.operator = operator;
    }


    /**
     * Gets the operator_more_less_equal value for this SearchProductsInput.
     * 
     * @return operator_more_less_equal
     */
    public java.lang.String getOperator_more_less_equal() {
        return operator_more_less_equal;
    }


    /**
     * Sets the operator_more_less_equal value for this SearchProductsInput.
     * 
     * @param operator_more_less_equal
     */
    public void setOperator_more_less_equal(java.lang.String operator_more_less_equal) {
        this.operator_more_less_equal = operator_more_less_equal;
    }


    /**
     * Gets the limite_start value for this SearchProductsInput.
     * 
     * @return limite_start
     */
    public java.lang.String getLimite_start() {
        return limite_start;
    }


    /**
     * Sets the limite_start value for this SearchProductsInput.
     * 
     * @param limite_start
     */
    public void setLimite_start(java.lang.String limite_start) {
        this.limite_start = limite_start;
    }


    /**
     * Gets the limite_end value for this SearchProductsInput.
     * 
     * @return limite_end
     */
    public java.lang.String getLimite_end() {
        return limite_end;
    }


    /**
     * Sets the limite_end value for this SearchProductsInput.
     * 
     * @param limite_end
     */
    public void setLimite_end(java.lang.String limite_end) {
        this.limite_end = limite_end;
    }


    /**
     * Gets the include_prices value for this SearchProductsInput.
     * 
     * @return include_prices
     */
    public java.lang.String getInclude_prices() {
        return include_prices;
    }


    /**
     * Sets the include_prices value for this SearchProductsInput.
     * 
     * @param include_prices
     */
    public void setInclude_prices(java.lang.String include_prices) {
        this.include_prices = include_prices;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof SearchProductsInput)) return false;
        SearchProductsInput other = (SearchProductsInput) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.loginInfo==null && other.getLoginInfo()==null) || 
             (this.loginInfo!=null &&
              this.loginInfo.equals(other.getLoginInfo()))) &&
            ((this.product_id==null && other.getProduct_id()==null) || 
             (this.product_id!=null &&
              this.product_id.equals(other.getProduct_id()))) &&
            ((this.product_sku==null && other.getProduct_sku()==null) || 
             (this.product_sku!=null &&
              this.product_sku.equals(other.getProduct_sku()))) &&
            ((this.product_name==null && other.getProduct_name()==null) || 
             (this.product_name!=null &&
              this.product_name.equals(other.getProduct_name()))) &&
            ((this.product_desc==null && other.getProduct_desc()==null) || 
             (this.product_desc!=null &&
              this.product_desc.equals(other.getProduct_desc()))) &&
            ((this.product_sdesc==null && other.getProduct_sdesc()==null) || 
             (this.product_sdesc!=null &&
              this.product_sdesc.equals(other.getProduct_sdesc()))) &&
            ((this.product_categories==null && other.getProduct_categories()==null) || 
             (this.product_categories!=null &&
              this.product_categories.equals(other.getProduct_categories()))) &&
            ((this.product_sales==null && other.getProduct_sales()==null) || 
             (this.product_sales!=null &&
              this.product_sales.equals(other.getProduct_sales()))) &&
            ((this.price==null && other.getPrice()==null) || 
             (this.price!=null &&
              this.price.equals(other.getPrice()))) &&
            ((this.quantity==null && other.getQuantity()==null) || 
             (this.quantity!=null &&
              this.quantity.equals(other.getQuantity()))) &&
            ((this.parent_product_id==null && other.getParent_product_id()==null) || 
             (this.parent_product_id!=null &&
              this.parent_product_id.equals(other.getParent_product_id()))) &&
            ((this.product_publish==null && other.getProduct_publish()==null) || 
             (this.product_publish!=null &&
              this.product_publish.equals(other.getProduct_publish()))) &&
            ((this.product_currency==null && other.getProduct_currency()==null) || 
             (this.product_currency!=null &&
              this.product_currency.equals(other.getProduct_currency()))) &&
            ((this.manufacturer_id==null && other.getManufacturer_id()==null) || 
             (this.manufacturer_id!=null &&
              this.manufacturer_id.equals(other.getManufacturer_id()))) &&
            ((this.vendor_id==null && other.getVendor_id()==null) || 
             (this.vendor_id!=null &&
              this.vendor_id.equals(other.getVendor_id()))) &&
            ((this.shopper_group_id==null && other.getShopper_group_id()==null) || 
             (this.shopper_group_id!=null &&
              this.shopper_group_id.equals(other.getShopper_group_id()))) &&
            ((this.product_weight==null && other.getProduct_weight()==null) || 
             (this.product_weight!=null &&
              this.product_weight.equals(other.getProduct_weight()))) &&
            ((this.product_weight_uom==null && other.getProduct_weight_uom()==null) || 
             (this.product_weight_uom!=null &&
              this.product_weight_uom.equals(other.getProduct_weight_uom()))) &&
            ((this.product_length==null && other.getProduct_length()==null) || 
             (this.product_length!=null &&
              this.product_length.equals(other.getProduct_length()))) &&
            ((this.product_width==null && other.getProduct_width()==null) || 
             (this.product_width!=null &&
              this.product_width.equals(other.getProduct_width()))) &&
            ((this.product_height==null && other.getProduct_height()==null) || 
             (this.product_height!=null &&
              this.product_height.equals(other.getProduct_height()))) &&
            ((this.product_lwh_uom==null && other.getProduct_lwh_uom()==null) || 
             (this.product_lwh_uom!=null &&
              this.product_lwh_uom.equals(other.getProduct_lwh_uom()))) &&
            ((this.product_unit==null && other.getProduct_unit()==null) || 
             (this.product_unit!=null &&
              this.product_unit.equals(other.getProduct_unit()))) &&
            ((this.product_packaging==null && other.getProduct_packaging()==null) || 
             (this.product_packaging!=null &&
              this.product_packaging.equals(other.getProduct_packaging()))) &&
            ((this.product_url==null && other.getProduct_url()==null) || 
             (this.product_url!=null &&
              this.product_url.equals(other.getProduct_url()))) &&
            ((this.product_special==null && other.getProduct_special()==null) || 
             (this.product_special!=null &&
              this.product_special.equals(other.getProduct_special()))) &&
            ((this.with_childs==null && other.getWith_childs()==null) || 
             (this.with_childs!=null &&
              this.with_childs.equals(other.getWith_childs()))) &&
            ((this.operator==null && other.getOperator()==null) || 
             (this.operator!=null &&
              this.operator.equals(other.getOperator()))) &&
            ((this.operator_more_less_equal==null && other.getOperator_more_less_equal()==null) || 
             (this.operator_more_less_equal!=null &&
              this.operator_more_less_equal.equals(other.getOperator_more_less_equal()))) &&
            ((this.limite_start==null && other.getLimite_start()==null) || 
             (this.limite_start!=null &&
              this.limite_start.equals(other.getLimite_start()))) &&
            ((this.limite_end==null && other.getLimite_end()==null) || 
             (this.limite_end!=null &&
              this.limite_end.equals(other.getLimite_end()))) &&
            ((this.include_prices==null && other.getInclude_prices()==null) || 
             (this.include_prices!=null &&
              this.include_prices.equals(other.getInclude_prices())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getLoginInfo() != null) {
            _hashCode += getLoginInfo().hashCode();
        }
        if (getProduct_id() != null) {
            _hashCode += getProduct_id().hashCode();
        }
        if (getProduct_sku() != null) {
            _hashCode += getProduct_sku().hashCode();
        }
        if (getProduct_name() != null) {
            _hashCode += getProduct_name().hashCode();
        }
        if (getProduct_desc() != null) {
            _hashCode += getProduct_desc().hashCode();
        }
        if (getProduct_sdesc() != null) {
            _hashCode += getProduct_sdesc().hashCode();
        }
        if (getProduct_categories() != null) {
            _hashCode += getProduct_categories().hashCode();
        }
        if (getProduct_sales() != null) {
            _hashCode += getProduct_sales().hashCode();
        }
        if (getPrice() != null) {
            _hashCode += getPrice().hashCode();
        }
        if (getQuantity() != null) {
            _hashCode += getQuantity().hashCode();
        }
        if (getParent_product_id() != null) {
            _hashCode += getParent_product_id().hashCode();
        }
        if (getProduct_publish() != null) {
            _hashCode += getProduct_publish().hashCode();
        }
        if (getProduct_currency() != null) {
            _hashCode += getProduct_currency().hashCode();
        }
        if (getManufacturer_id() != null) {
            _hashCode += getManufacturer_id().hashCode();
        }
        if (getVendor_id() != null) {
            _hashCode += getVendor_id().hashCode();
        }
        if (getShopper_group_id() != null) {
            _hashCode += getShopper_group_id().hashCode();
        }
        if (getProduct_weight() != null) {
            _hashCode += getProduct_weight().hashCode();
        }
        if (getProduct_weight_uom() != null) {
            _hashCode += getProduct_weight_uom().hashCode();
        }
        if (getProduct_length() != null) {
            _hashCode += getProduct_length().hashCode();
        }
        if (getProduct_width() != null) {
            _hashCode += getProduct_width().hashCode();
        }
        if (getProduct_height() != null) {
            _hashCode += getProduct_height().hashCode();
        }
        if (getProduct_lwh_uom() != null) {
            _hashCode += getProduct_lwh_uom().hashCode();
        }
        if (getProduct_unit() != null) {
            _hashCode += getProduct_unit().hashCode();
        }
        if (getProduct_packaging() != null) {
            _hashCode += getProduct_packaging().hashCode();
        }
        if (getProduct_url() != null) {
            _hashCode += getProduct_url().hashCode();
        }
        if (getProduct_special() != null) {
            _hashCode += getProduct_special().hashCode();
        }
        if (getWith_childs() != null) {
            _hashCode += getWith_childs().hashCode();
        }
        if (getOperator() != null) {
            _hashCode += getOperator().hashCode();
        }
        if (getOperator_more_less_equal() != null) {
            _hashCode += getOperator_more_less_equal().hashCode();
        }
        if (getLimite_start() != null) {
            _hashCode += getLimite_start().hashCode();
        }
        if (getLimite_end() != null) {
            _hashCode += getLimite_end().hashCode();
        }
        if (getInclude_prices() != null) {
            _hashCode += getInclude_prices().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(SearchProductsInput.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://www.virtuemart.net/VM_Product/", "searchProductsInput"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("loginInfo");
        elemField.setXmlName(new javax.xml.namespace.QName("", "loginInfo"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.virtuemart.net/VM_Product/", "loginInfo"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("product_id");
        elemField.setXmlName(new javax.xml.namespace.QName("", "product_id"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("product_sku");
        elemField.setXmlName(new javax.xml.namespace.QName("", "product_sku"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("product_name");
        elemField.setXmlName(new javax.xml.namespace.QName("", "product_name"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("product_desc");
        elemField.setXmlName(new javax.xml.namespace.QName("", "product_desc"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("product_sdesc");
        elemField.setXmlName(new javax.xml.namespace.QName("", "product_sdesc"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("product_categories");
        elemField.setXmlName(new javax.xml.namespace.QName("", "product_categories"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("product_sales");
        elemField.setXmlName(new javax.xml.namespace.QName("", "product_sales"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("price");
        elemField.setXmlName(new javax.xml.namespace.QName("", "price"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("quantity");
        elemField.setXmlName(new javax.xml.namespace.QName("", "quantity"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("parent_product_id");
        elemField.setXmlName(new javax.xml.namespace.QName("", "parent_product_id"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("product_publish");
        elemField.setXmlName(new javax.xml.namespace.QName("", "product_publish"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("product_currency");
        elemField.setXmlName(new javax.xml.namespace.QName("", "product_currency"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("manufacturer_id");
        elemField.setXmlName(new javax.xml.namespace.QName("", "manufacturer_id"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("vendor_id");
        elemField.setXmlName(new javax.xml.namespace.QName("", "vendor_id"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("shopper_group_id");
        elemField.setXmlName(new javax.xml.namespace.QName("", "shopper_group_id"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("product_weight");
        elemField.setXmlName(new javax.xml.namespace.QName("", "product_weight"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("product_weight_uom");
        elemField.setXmlName(new javax.xml.namespace.QName("", "product_weight_uom"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("product_length");
        elemField.setXmlName(new javax.xml.namespace.QName("", "product_length"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("product_width");
        elemField.setXmlName(new javax.xml.namespace.QName("", "product_width"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("product_height");
        elemField.setXmlName(new javax.xml.namespace.QName("", "product_height"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("product_lwh_uom");
        elemField.setXmlName(new javax.xml.namespace.QName("", "product_lwh_uom"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("product_unit");
        elemField.setXmlName(new javax.xml.namespace.QName("", "product_unit"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("product_packaging");
        elemField.setXmlName(new javax.xml.namespace.QName("", "product_packaging"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("product_url");
        elemField.setXmlName(new javax.xml.namespace.QName("", "product_url"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("product_special");
        elemField.setXmlName(new javax.xml.namespace.QName("", "product_special"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("with_childs");
        elemField.setXmlName(new javax.xml.namespace.QName("", "with_childs"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("operator");
        elemField.setXmlName(new javax.xml.namespace.QName("", "operator"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("operator_more_less_equal");
        elemField.setXmlName(new javax.xml.namespace.QName("", "operator_more_less_equal"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("limite_start");
        elemField.setXmlName(new javax.xml.namespace.QName("", "limite_start"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("limite_end");
        elemField.setXmlName(new javax.xml.namespace.QName("", "limite_end"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("include_prices");
        elemField.setXmlName(new javax.xml.namespace.QName("", "include_prices"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
