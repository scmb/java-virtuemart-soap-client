/**
 * Currency.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package it.netgrid.virtuemart.soap.products;

public class Currency  implements java.io.Serializable {
    private java.lang.String currency_id;

    private java.lang.String vendor_id;

    private java.lang.String currency_name;

    private java.lang.String currency_code_2;

    private java.lang.String currency_code_3;

    private java.lang.String currency_numeric_code;

    private java.lang.String currency_exchange_rate;

    private java.lang.String currency_symbol;

    private java.lang.String currency_decimal_place;

    private java.lang.String currency_decimal_symbol;

    private java.lang.String currency_thousands;

    private java.lang.String currency_positive_style;

    private java.lang.String currency_negative_style;

    private java.lang.String ordering;

    private java.lang.String shared;

    private java.lang.String published;

    public Currency() {
    }

    public Currency(
           java.lang.String currency_id,
           java.lang.String vendor_id,
           java.lang.String currency_name,
           java.lang.String currency_code_2,
           java.lang.String currency_code_3,
           java.lang.String currency_numeric_code,
           java.lang.String currency_exchange_rate,
           java.lang.String currency_symbol,
           java.lang.String currency_decimal_place,
           java.lang.String currency_decimal_symbol,
           java.lang.String currency_thousands,
           java.lang.String currency_positive_style,
           java.lang.String currency_negative_style,
           java.lang.String ordering,
           java.lang.String shared,
           java.lang.String published) {
           this.currency_id = currency_id;
           this.vendor_id = vendor_id;
           this.currency_name = currency_name;
           this.currency_code_2 = currency_code_2;
           this.currency_code_3 = currency_code_3;
           this.currency_numeric_code = currency_numeric_code;
           this.currency_exchange_rate = currency_exchange_rate;
           this.currency_symbol = currency_symbol;
           this.currency_decimal_place = currency_decimal_place;
           this.currency_decimal_symbol = currency_decimal_symbol;
           this.currency_thousands = currency_thousands;
           this.currency_positive_style = currency_positive_style;
           this.currency_negative_style = currency_negative_style;
           this.ordering = ordering;
           this.shared = shared;
           this.published = published;
    }


    /**
     * Gets the currency_id value for this Currency.
     * 
     * @return currency_id
     */
    public java.lang.String getCurrency_id() {
        return currency_id;
    }


    /**
     * Sets the currency_id value for this Currency.
     * 
     * @param currency_id
     */
    public void setCurrency_id(java.lang.String currency_id) {
        this.currency_id = currency_id;
    }


    /**
     * Gets the vendor_id value for this Currency.
     * 
     * @return vendor_id
     */
    public java.lang.String getVendor_id() {
        return vendor_id;
    }


    /**
     * Sets the vendor_id value for this Currency.
     * 
     * @param vendor_id
     */
    public void setVendor_id(java.lang.String vendor_id) {
        this.vendor_id = vendor_id;
    }


    /**
     * Gets the currency_name value for this Currency.
     * 
     * @return currency_name
     */
    public java.lang.String getCurrency_name() {
        return currency_name;
    }


    /**
     * Sets the currency_name value for this Currency.
     * 
     * @param currency_name
     */
    public void setCurrency_name(java.lang.String currency_name) {
        this.currency_name = currency_name;
    }


    /**
     * Gets the currency_code_2 value for this Currency.
     * 
     * @return currency_code_2
     */
    public java.lang.String getCurrency_code_2() {
        return currency_code_2;
    }


    /**
     * Sets the currency_code_2 value for this Currency.
     * 
     * @param currency_code_2
     */
    public void setCurrency_code_2(java.lang.String currency_code_2) {
        this.currency_code_2 = currency_code_2;
    }


    /**
     * Gets the currency_code_3 value for this Currency.
     * 
     * @return currency_code_3
     */
    public java.lang.String getCurrency_code_3() {
        return currency_code_3;
    }


    /**
     * Sets the currency_code_3 value for this Currency.
     * 
     * @param currency_code_3
     */
    public void setCurrency_code_3(java.lang.String currency_code_3) {
        this.currency_code_3 = currency_code_3;
    }


    /**
     * Gets the currency_numeric_code value for this Currency.
     * 
     * @return currency_numeric_code
     */
    public java.lang.String getCurrency_numeric_code() {
        return currency_numeric_code;
    }


    /**
     * Sets the currency_numeric_code value for this Currency.
     * 
     * @param currency_numeric_code
     */
    public void setCurrency_numeric_code(java.lang.String currency_numeric_code) {
        this.currency_numeric_code = currency_numeric_code;
    }


    /**
     * Gets the currency_exchange_rate value for this Currency.
     * 
     * @return currency_exchange_rate
     */
    public java.lang.String getCurrency_exchange_rate() {
        return currency_exchange_rate;
    }


    /**
     * Sets the currency_exchange_rate value for this Currency.
     * 
     * @param currency_exchange_rate
     */
    public void setCurrency_exchange_rate(java.lang.String currency_exchange_rate) {
        this.currency_exchange_rate = currency_exchange_rate;
    }


    /**
     * Gets the currency_symbol value for this Currency.
     * 
     * @return currency_symbol
     */
    public java.lang.String getCurrency_symbol() {
        return currency_symbol;
    }


    /**
     * Sets the currency_symbol value for this Currency.
     * 
     * @param currency_symbol
     */
    public void setCurrency_symbol(java.lang.String currency_symbol) {
        this.currency_symbol = currency_symbol;
    }


    /**
     * Gets the currency_decimal_place value for this Currency.
     * 
     * @return currency_decimal_place
     */
    public java.lang.String getCurrency_decimal_place() {
        return currency_decimal_place;
    }


    /**
     * Sets the currency_decimal_place value for this Currency.
     * 
     * @param currency_decimal_place
     */
    public void setCurrency_decimal_place(java.lang.String currency_decimal_place) {
        this.currency_decimal_place = currency_decimal_place;
    }


    /**
     * Gets the currency_decimal_symbol value for this Currency.
     * 
     * @return currency_decimal_symbol
     */
    public java.lang.String getCurrency_decimal_symbol() {
        return currency_decimal_symbol;
    }


    /**
     * Sets the currency_decimal_symbol value for this Currency.
     * 
     * @param currency_decimal_symbol
     */
    public void setCurrency_decimal_symbol(java.lang.String currency_decimal_symbol) {
        this.currency_decimal_symbol = currency_decimal_symbol;
    }


    /**
     * Gets the currency_thousands value for this Currency.
     * 
     * @return currency_thousands
     */
    public java.lang.String getCurrency_thousands() {
        return currency_thousands;
    }


    /**
     * Sets the currency_thousands value for this Currency.
     * 
     * @param currency_thousands
     */
    public void setCurrency_thousands(java.lang.String currency_thousands) {
        this.currency_thousands = currency_thousands;
    }


    /**
     * Gets the currency_positive_style value for this Currency.
     * 
     * @return currency_positive_style
     */
    public java.lang.String getCurrency_positive_style() {
        return currency_positive_style;
    }


    /**
     * Sets the currency_positive_style value for this Currency.
     * 
     * @param currency_positive_style
     */
    public void setCurrency_positive_style(java.lang.String currency_positive_style) {
        this.currency_positive_style = currency_positive_style;
    }


    /**
     * Gets the currency_negative_style value for this Currency.
     * 
     * @return currency_negative_style
     */
    public java.lang.String getCurrency_negative_style() {
        return currency_negative_style;
    }


    /**
     * Sets the currency_negative_style value for this Currency.
     * 
     * @param currency_negative_style
     */
    public void setCurrency_negative_style(java.lang.String currency_negative_style) {
        this.currency_negative_style = currency_negative_style;
    }


    /**
     * Gets the ordering value for this Currency.
     * 
     * @return ordering
     */
    public java.lang.String getOrdering() {
        return ordering;
    }


    /**
     * Sets the ordering value for this Currency.
     * 
     * @param ordering
     */
    public void setOrdering(java.lang.String ordering) {
        this.ordering = ordering;
    }


    /**
     * Gets the shared value for this Currency.
     * 
     * @return shared
     */
    public java.lang.String getShared() {
        return shared;
    }


    /**
     * Sets the shared value for this Currency.
     * 
     * @param shared
     */
    public void setShared(java.lang.String shared) {
        this.shared = shared;
    }


    /**
     * Gets the published value for this Currency.
     * 
     * @return published
     */
    public java.lang.String getPublished() {
        return published;
    }


    /**
     * Sets the published value for this Currency.
     * 
     * @param published
     */
    public void setPublished(java.lang.String published) {
        this.published = published;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof Currency)) return false;
        Currency other = (Currency) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.currency_id==null && other.getCurrency_id()==null) || 
             (this.currency_id!=null &&
              this.currency_id.equals(other.getCurrency_id()))) &&
            ((this.vendor_id==null && other.getVendor_id()==null) || 
             (this.vendor_id!=null &&
              this.vendor_id.equals(other.getVendor_id()))) &&
            ((this.currency_name==null && other.getCurrency_name()==null) || 
             (this.currency_name!=null &&
              this.currency_name.equals(other.getCurrency_name()))) &&
            ((this.currency_code_2==null && other.getCurrency_code_2()==null) || 
             (this.currency_code_2!=null &&
              this.currency_code_2.equals(other.getCurrency_code_2()))) &&
            ((this.currency_code_3==null && other.getCurrency_code_3()==null) || 
             (this.currency_code_3!=null &&
              this.currency_code_3.equals(other.getCurrency_code_3()))) &&
            ((this.currency_numeric_code==null && other.getCurrency_numeric_code()==null) || 
             (this.currency_numeric_code!=null &&
              this.currency_numeric_code.equals(other.getCurrency_numeric_code()))) &&
            ((this.currency_exchange_rate==null && other.getCurrency_exchange_rate()==null) || 
             (this.currency_exchange_rate!=null &&
              this.currency_exchange_rate.equals(other.getCurrency_exchange_rate()))) &&
            ((this.currency_symbol==null && other.getCurrency_symbol()==null) || 
             (this.currency_symbol!=null &&
              this.currency_symbol.equals(other.getCurrency_symbol()))) &&
            ((this.currency_decimal_place==null && other.getCurrency_decimal_place()==null) || 
             (this.currency_decimal_place!=null &&
              this.currency_decimal_place.equals(other.getCurrency_decimal_place()))) &&
            ((this.currency_decimal_symbol==null && other.getCurrency_decimal_symbol()==null) || 
             (this.currency_decimal_symbol!=null &&
              this.currency_decimal_symbol.equals(other.getCurrency_decimal_symbol()))) &&
            ((this.currency_thousands==null && other.getCurrency_thousands()==null) || 
             (this.currency_thousands!=null &&
              this.currency_thousands.equals(other.getCurrency_thousands()))) &&
            ((this.currency_positive_style==null && other.getCurrency_positive_style()==null) || 
             (this.currency_positive_style!=null &&
              this.currency_positive_style.equals(other.getCurrency_positive_style()))) &&
            ((this.currency_negative_style==null && other.getCurrency_negative_style()==null) || 
             (this.currency_negative_style!=null &&
              this.currency_negative_style.equals(other.getCurrency_negative_style()))) &&
            ((this.ordering==null && other.getOrdering()==null) || 
             (this.ordering!=null &&
              this.ordering.equals(other.getOrdering()))) &&
            ((this.shared==null && other.getShared()==null) || 
             (this.shared!=null &&
              this.shared.equals(other.getShared()))) &&
            ((this.published==null && other.getPublished()==null) || 
             (this.published!=null &&
              this.published.equals(other.getPublished())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getCurrency_id() != null) {
            _hashCode += getCurrency_id().hashCode();
        }
        if (getVendor_id() != null) {
            _hashCode += getVendor_id().hashCode();
        }
        if (getCurrency_name() != null) {
            _hashCode += getCurrency_name().hashCode();
        }
        if (getCurrency_code_2() != null) {
            _hashCode += getCurrency_code_2().hashCode();
        }
        if (getCurrency_code_3() != null) {
            _hashCode += getCurrency_code_3().hashCode();
        }
        if (getCurrency_numeric_code() != null) {
            _hashCode += getCurrency_numeric_code().hashCode();
        }
        if (getCurrency_exchange_rate() != null) {
            _hashCode += getCurrency_exchange_rate().hashCode();
        }
        if (getCurrency_symbol() != null) {
            _hashCode += getCurrency_symbol().hashCode();
        }
        if (getCurrency_decimal_place() != null) {
            _hashCode += getCurrency_decimal_place().hashCode();
        }
        if (getCurrency_decimal_symbol() != null) {
            _hashCode += getCurrency_decimal_symbol().hashCode();
        }
        if (getCurrency_thousands() != null) {
            _hashCode += getCurrency_thousands().hashCode();
        }
        if (getCurrency_positive_style() != null) {
            _hashCode += getCurrency_positive_style().hashCode();
        }
        if (getCurrency_negative_style() != null) {
            _hashCode += getCurrency_negative_style().hashCode();
        }
        if (getOrdering() != null) {
            _hashCode += getOrdering().hashCode();
        }
        if (getShared() != null) {
            _hashCode += getShared().hashCode();
        }
        if (getPublished() != null) {
            _hashCode += getPublished().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(Currency.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://www.virtuemart.net/VM_Product/", "Currency"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("currency_id");
        elemField.setXmlName(new javax.xml.namespace.QName("", "currency_id"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("vendor_id");
        elemField.setXmlName(new javax.xml.namespace.QName("", "vendor_id"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("currency_name");
        elemField.setXmlName(new javax.xml.namespace.QName("", "currency_name"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("currency_code_2");
        elemField.setXmlName(new javax.xml.namespace.QName("", "currency_code_2"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("currency_code_3");
        elemField.setXmlName(new javax.xml.namespace.QName("", "currency_code_3"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("currency_numeric_code");
        elemField.setXmlName(new javax.xml.namespace.QName("", "currency_numeric_code"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("currency_exchange_rate");
        elemField.setXmlName(new javax.xml.namespace.QName("", "currency_exchange_rate"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("currency_symbol");
        elemField.setXmlName(new javax.xml.namespace.QName("", "currency_symbol"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("currency_decimal_place");
        elemField.setXmlName(new javax.xml.namespace.QName("", "currency_decimal_place"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("currency_decimal_symbol");
        elemField.setXmlName(new javax.xml.namespace.QName("", "currency_decimal_symbol"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("currency_thousands");
        elemField.setXmlName(new javax.xml.namespace.QName("", "currency_thousands"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("currency_positive_style");
        elemField.setXmlName(new javax.xml.namespace.QName("", "currency_positive_style"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("currency_negative_style");
        elemField.setXmlName(new javax.xml.namespace.QName("", "currency_negative_style"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("ordering");
        elemField.setXmlName(new javax.xml.namespace.QName("", "ordering"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("shared");
        elemField.setXmlName(new javax.xml.namespace.QName("", "shared"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("published");
        elemField.setXmlName(new javax.xml.namespace.QName("", "published"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
