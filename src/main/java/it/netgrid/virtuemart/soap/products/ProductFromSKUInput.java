/**
 * ProductFromSKUInput.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package it.netgrid.virtuemart.soap.products;

public class ProductFromSKUInput  implements java.io.Serializable {
    private it.netgrid.virtuemart.soap.products.LoginInfo loginInfo;

    private java.lang.String product_sku;

    private java.lang.String include_prices;

    public ProductFromSKUInput() {
    }

    public ProductFromSKUInput(
           it.netgrid.virtuemart.soap.products.LoginInfo loginInfo,
           java.lang.String product_sku,
           java.lang.String include_prices) {
           this.loginInfo = loginInfo;
           this.product_sku = product_sku;
           this.include_prices = include_prices;
    }


    /**
     * Gets the loginInfo value for this ProductFromSKUInput.
     * 
     * @return loginInfo
     */
    public it.netgrid.virtuemart.soap.products.LoginInfo getLoginInfo() {
        return loginInfo;
    }


    /**
     * Sets the loginInfo value for this ProductFromSKUInput.
     * 
     * @param loginInfo
     */
    public void setLoginInfo(it.netgrid.virtuemart.soap.products.LoginInfo loginInfo) {
        this.loginInfo = loginInfo;
    }


    /**
     * Gets the product_sku value for this ProductFromSKUInput.
     * 
     * @return product_sku
     */
    public java.lang.String getProduct_sku() {
        return product_sku;
    }


    /**
     * Sets the product_sku value for this ProductFromSKUInput.
     * 
     * @param product_sku
     */
    public void setProduct_sku(java.lang.String product_sku) {
        this.product_sku = product_sku;
    }


    /**
     * Gets the include_prices value for this ProductFromSKUInput.
     * 
     * @return include_prices
     */
    public java.lang.String getInclude_prices() {
        return include_prices;
    }


    /**
     * Sets the include_prices value for this ProductFromSKUInput.
     * 
     * @param include_prices
     */
    public void setInclude_prices(java.lang.String include_prices) {
        this.include_prices = include_prices;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof ProductFromSKUInput)) return false;
        ProductFromSKUInput other = (ProductFromSKUInput) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.loginInfo==null && other.getLoginInfo()==null) || 
             (this.loginInfo!=null &&
              this.loginInfo.equals(other.getLoginInfo()))) &&
            ((this.product_sku==null && other.getProduct_sku()==null) || 
             (this.product_sku!=null &&
              this.product_sku.equals(other.getProduct_sku()))) &&
            ((this.include_prices==null && other.getInclude_prices()==null) || 
             (this.include_prices!=null &&
              this.include_prices.equals(other.getInclude_prices())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getLoginInfo() != null) {
            _hashCode += getLoginInfo().hashCode();
        }
        if (getProduct_sku() != null) {
            _hashCode += getProduct_sku().hashCode();
        }
        if (getInclude_prices() != null) {
            _hashCode += getInclude_prices().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(ProductFromSKUInput.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://www.virtuemart.net/VM_Product/", "ProductFromSKUInput"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("loginInfo");
        elemField.setXmlName(new javax.xml.namespace.QName("", "loginInfo"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.virtuemart.net/VM_Product/", "loginInfo"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("product_sku");
        elemField.setXmlName(new javax.xml.namespace.QName("", "product_sku"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("include_prices");
        elemField.setXmlName(new javax.xml.namespace.QName("", "include_prices"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
