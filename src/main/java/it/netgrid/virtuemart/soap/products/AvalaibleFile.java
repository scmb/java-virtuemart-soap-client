/**
 * AvalaibleFile.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package it.netgrid.virtuemart.soap.products;

public class AvalaibleFile  implements java.io.Serializable {
    private java.lang.String file_name;

    private java.lang.String file_url;

    private java.lang.String realpath;

    public AvalaibleFile() {
    }

    public AvalaibleFile(
           java.lang.String file_name,
           java.lang.String file_url,
           java.lang.String realpath) {
           this.file_name = file_name;
           this.file_url = file_url;
           this.realpath = realpath;
    }


    /**
     * Gets the file_name value for this AvalaibleFile.
     * 
     * @return file_name
     */
    public java.lang.String getFile_name() {
        return file_name;
    }


    /**
     * Sets the file_name value for this AvalaibleFile.
     * 
     * @param file_name
     */
    public void setFile_name(java.lang.String file_name) {
        this.file_name = file_name;
    }


    /**
     * Gets the file_url value for this AvalaibleFile.
     * 
     * @return file_url
     */
    public java.lang.String getFile_url() {
        return file_url;
    }


    /**
     * Sets the file_url value for this AvalaibleFile.
     * 
     * @param file_url
     */
    public void setFile_url(java.lang.String file_url) {
        this.file_url = file_url;
    }


    /**
     * Gets the realpath value for this AvalaibleFile.
     * 
     * @return realpath
     */
    public java.lang.String getRealpath() {
        return realpath;
    }


    /**
     * Sets the realpath value for this AvalaibleFile.
     * 
     * @param realpath
     */
    public void setRealpath(java.lang.String realpath) {
        this.realpath = realpath;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof AvalaibleFile)) return false;
        AvalaibleFile other = (AvalaibleFile) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.file_name==null && other.getFile_name()==null) || 
             (this.file_name!=null &&
              this.file_name.equals(other.getFile_name()))) &&
            ((this.file_url==null && other.getFile_url()==null) || 
             (this.file_url!=null &&
              this.file_url.equals(other.getFile_url()))) &&
            ((this.realpath==null && other.getRealpath()==null) || 
             (this.realpath!=null &&
              this.realpath.equals(other.getRealpath())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getFile_name() != null) {
            _hashCode += getFile_name().hashCode();
        }
        if (getFile_url() != null) {
            _hashCode += getFile_url().hashCode();
        }
        if (getRealpath() != null) {
            _hashCode += getRealpath().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(AvalaibleFile.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://www.virtuemart.net/VM_Product/", "AvalaibleFile"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("file_name");
        elemField.setXmlName(new javax.xml.namespace.QName("", "file_name"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("file_url");
        elemField.setXmlName(new javax.xml.namespace.QName("", "file_url"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("realpath");
        elemField.setXmlName(new javax.xml.namespace.QName("", "realpath"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
