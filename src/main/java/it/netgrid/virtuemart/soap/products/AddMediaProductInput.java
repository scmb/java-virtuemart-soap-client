/**
 * AddMediaProductInput.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package it.netgrid.virtuemart.soap.products;

public class AddMediaProductInput  implements java.io.Serializable {
    private it.netgrid.virtuemart.soap.products.LoginInfo loginInfo;

    private java.lang.String product_id;

    private it.netgrid.virtuemart.soap.products.Media media;

    private java.lang.String filePath;

    private java.lang.String fileThumbPath;

    private java.lang.String replaceThumb;

    public AddMediaProductInput() {
    }

    public AddMediaProductInput(
           it.netgrid.virtuemart.soap.products.LoginInfo loginInfo,
           java.lang.String product_id,
           it.netgrid.virtuemart.soap.products.Media media,
           java.lang.String filePath,
           java.lang.String fileThumbPath,
           java.lang.String replaceThumb) {
           this.loginInfo = loginInfo;
           this.product_id = product_id;
           this.media = media;
           this.filePath = filePath;
           this.fileThumbPath = fileThumbPath;
           this.replaceThumb = replaceThumb;
    }


    /**
     * Gets the loginInfo value for this AddMediaProductInput.
     * 
     * @return loginInfo
     */
    public it.netgrid.virtuemart.soap.products.LoginInfo getLoginInfo() {
        return loginInfo;
    }


    /**
     * Sets the loginInfo value for this AddMediaProductInput.
     * 
     * @param loginInfo
     */
    public void setLoginInfo(it.netgrid.virtuemart.soap.products.LoginInfo loginInfo) {
        this.loginInfo = loginInfo;
    }


    /**
     * Gets the product_id value for this AddMediaProductInput.
     * 
     * @return product_id
     */
    public java.lang.String getProduct_id() {
        return product_id;
    }


    /**
     * Sets the product_id value for this AddMediaProductInput.
     * 
     * @param product_id
     */
    public void setProduct_id(java.lang.String product_id) {
        this.product_id = product_id;
    }


    /**
     * Gets the media value for this AddMediaProductInput.
     * 
     * @return media
     */
    public it.netgrid.virtuemart.soap.products.Media getMedia() {
        return media;
    }


    /**
     * Sets the media value for this AddMediaProductInput.
     * 
     * @param media
     */
    public void setMedia(it.netgrid.virtuemart.soap.products.Media media) {
        this.media = media;
    }


    /**
     * Gets the filePath value for this AddMediaProductInput.
     * 
     * @return filePath
     */
    public java.lang.String getFilePath() {
        return filePath;
    }


    /**
     * Sets the filePath value for this AddMediaProductInput.
     * 
     * @param filePath
     */
    public void setFilePath(java.lang.String filePath) {
        this.filePath = filePath;
    }


    /**
     * Gets the fileThumbPath value for this AddMediaProductInput.
     * 
     * @return fileThumbPath
     */
    public java.lang.String getFileThumbPath() {
        return fileThumbPath;
    }


    /**
     * Sets the fileThumbPath value for this AddMediaProductInput.
     * 
     * @param fileThumbPath
     */
    public void setFileThumbPath(java.lang.String fileThumbPath) {
        this.fileThumbPath = fileThumbPath;
    }


    /**
     * Gets the replaceThumb value for this AddMediaProductInput.
     * 
     * @return replaceThumb
     */
    public java.lang.String getReplaceThumb() {
        return replaceThumb;
    }


    /**
     * Sets the replaceThumb value for this AddMediaProductInput.
     * 
     * @param replaceThumb
     */
    public void setReplaceThumb(java.lang.String replaceThumb) {
        this.replaceThumb = replaceThumb;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof AddMediaProductInput)) return false;
        AddMediaProductInput other = (AddMediaProductInput) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.loginInfo==null && other.getLoginInfo()==null) || 
             (this.loginInfo!=null &&
              this.loginInfo.equals(other.getLoginInfo()))) &&
            ((this.product_id==null && other.getProduct_id()==null) || 
             (this.product_id!=null &&
              this.product_id.equals(other.getProduct_id()))) &&
            ((this.media==null && other.getMedia()==null) || 
             (this.media!=null &&
              this.media.equals(other.getMedia()))) &&
            ((this.filePath==null && other.getFilePath()==null) || 
             (this.filePath!=null &&
              this.filePath.equals(other.getFilePath()))) &&
            ((this.fileThumbPath==null && other.getFileThumbPath()==null) || 
             (this.fileThumbPath!=null &&
              this.fileThumbPath.equals(other.getFileThumbPath()))) &&
            ((this.replaceThumb==null && other.getReplaceThumb()==null) || 
             (this.replaceThumb!=null &&
              this.replaceThumb.equals(other.getReplaceThumb())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getLoginInfo() != null) {
            _hashCode += getLoginInfo().hashCode();
        }
        if (getProduct_id() != null) {
            _hashCode += getProduct_id().hashCode();
        }
        if (getMedia() != null) {
            _hashCode += getMedia().hashCode();
        }
        if (getFilePath() != null) {
            _hashCode += getFilePath().hashCode();
        }
        if (getFileThumbPath() != null) {
            _hashCode += getFileThumbPath().hashCode();
        }
        if (getReplaceThumb() != null) {
            _hashCode += getReplaceThumb().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(AddMediaProductInput.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://www.virtuemart.net/VM_Product/", "AddMediaProductInput"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("loginInfo");
        elemField.setXmlName(new javax.xml.namespace.QName("", "loginInfo"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.virtuemart.net/VM_Product/", "loginInfo"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("product_id");
        elemField.setXmlName(new javax.xml.namespace.QName("", "product_id"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("media");
        elemField.setXmlName(new javax.xml.namespace.QName("", "media"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.virtuemart.net/VM_Product/", "Media"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("filePath");
        elemField.setXmlName(new javax.xml.namespace.QName("", "filePath"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("fileThumbPath");
        elemField.setXmlName(new javax.xml.namespace.QName("", "fileThumbPath"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("replaceThumb");
        elemField.setXmlName(new javax.xml.namespace.QName("", "replaceThumb"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
