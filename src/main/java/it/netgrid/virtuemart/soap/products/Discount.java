/**
 * Discount.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package it.netgrid.virtuemart.soap.products;

public class Discount  implements java.io.Serializable {
    private java.lang.String discount_id;

    private java.lang.String vendor_id;

    private java.lang.String calc_name;

    private java.lang.String calc_descr;

    private java.lang.String calc_kind;

    private java.lang.String calc_value_mathop;

    private java.lang.String calc_value;

    private java.lang.String calc_currency;

    private java.lang.String calc_shopper_published;

    private java.lang.String calc_vendor_published;

    private java.lang.String publish_up;

    private java.lang.String publish_down;

    private java.lang.String calc_qualify;

    private java.lang.String calc_affected;

    private java.lang.String calc_amount_cond;

    private java.lang.String calc_amount_dimunit;

    private java.lang.String for_override;

    private java.lang.String ordering;

    private java.lang.String shared;

    private java.lang.String published;

    private java.lang.String discount_cat_ids;

    private java.lang.String discount_countries_ids;

    private java.lang.String discount_shoppergroups_ids;

    private java.lang.String discount_states_ids;

    public Discount() {
    }

    public Discount(
           java.lang.String discount_id,
           java.lang.String vendor_id,
           java.lang.String calc_name,
           java.lang.String calc_descr,
           java.lang.String calc_kind,
           java.lang.String calc_value_mathop,
           java.lang.String calc_value,
           java.lang.String calc_currency,
           java.lang.String calc_shopper_published,
           java.lang.String calc_vendor_published,
           java.lang.String publish_up,
           java.lang.String publish_down,
           java.lang.String calc_qualify,
           java.lang.String calc_affected,
           java.lang.String calc_amount_cond,
           java.lang.String calc_amount_dimunit,
           java.lang.String for_override,
           java.lang.String ordering,
           java.lang.String shared,
           java.lang.String published,
           java.lang.String discount_cat_ids,
           java.lang.String discount_countries_ids,
           java.lang.String discount_shoppergroups_ids,
           java.lang.String discount_states_ids) {
           this.discount_id = discount_id;
           this.vendor_id = vendor_id;
           this.calc_name = calc_name;
           this.calc_descr = calc_descr;
           this.calc_kind = calc_kind;
           this.calc_value_mathop = calc_value_mathop;
           this.calc_value = calc_value;
           this.calc_currency = calc_currency;
           this.calc_shopper_published = calc_shopper_published;
           this.calc_vendor_published = calc_vendor_published;
           this.publish_up = publish_up;
           this.publish_down = publish_down;
           this.calc_qualify = calc_qualify;
           this.calc_affected = calc_affected;
           this.calc_amount_cond = calc_amount_cond;
           this.calc_amount_dimunit = calc_amount_dimunit;
           this.for_override = for_override;
           this.ordering = ordering;
           this.shared = shared;
           this.published = published;
           this.discount_cat_ids = discount_cat_ids;
           this.discount_countries_ids = discount_countries_ids;
           this.discount_shoppergroups_ids = discount_shoppergroups_ids;
           this.discount_states_ids = discount_states_ids;
    }


    /**
     * Gets the discount_id value for this Discount.
     * 
     * @return discount_id
     */
    public java.lang.String getDiscount_id() {
        return discount_id;
    }


    /**
     * Sets the discount_id value for this Discount.
     * 
     * @param discount_id
     */
    public void setDiscount_id(java.lang.String discount_id) {
        this.discount_id = discount_id;
    }


    /**
     * Gets the vendor_id value for this Discount.
     * 
     * @return vendor_id
     */
    public java.lang.String getVendor_id() {
        return vendor_id;
    }


    /**
     * Sets the vendor_id value for this Discount.
     * 
     * @param vendor_id
     */
    public void setVendor_id(java.lang.String vendor_id) {
        this.vendor_id = vendor_id;
    }


    /**
     * Gets the calc_name value for this Discount.
     * 
     * @return calc_name
     */
    public java.lang.String getCalc_name() {
        return calc_name;
    }


    /**
     * Sets the calc_name value for this Discount.
     * 
     * @param calc_name
     */
    public void setCalc_name(java.lang.String calc_name) {
        this.calc_name = calc_name;
    }


    /**
     * Gets the calc_descr value for this Discount.
     * 
     * @return calc_descr
     */
    public java.lang.String getCalc_descr() {
        return calc_descr;
    }


    /**
     * Sets the calc_descr value for this Discount.
     * 
     * @param calc_descr
     */
    public void setCalc_descr(java.lang.String calc_descr) {
        this.calc_descr = calc_descr;
    }


    /**
     * Gets the calc_kind value for this Discount.
     * 
     * @return calc_kind
     */
    public java.lang.String getCalc_kind() {
        return calc_kind;
    }


    /**
     * Sets the calc_kind value for this Discount.
     * 
     * @param calc_kind
     */
    public void setCalc_kind(java.lang.String calc_kind) {
        this.calc_kind = calc_kind;
    }


    /**
     * Gets the calc_value_mathop value for this Discount.
     * 
     * @return calc_value_mathop
     */
    public java.lang.String getCalc_value_mathop() {
        return calc_value_mathop;
    }


    /**
     * Sets the calc_value_mathop value for this Discount.
     * 
     * @param calc_value_mathop
     */
    public void setCalc_value_mathop(java.lang.String calc_value_mathop) {
        this.calc_value_mathop = calc_value_mathop;
    }


    /**
     * Gets the calc_value value for this Discount.
     * 
     * @return calc_value
     */
    public java.lang.String getCalc_value() {
        return calc_value;
    }


    /**
     * Sets the calc_value value for this Discount.
     * 
     * @param calc_value
     */
    public void setCalc_value(java.lang.String calc_value) {
        this.calc_value = calc_value;
    }


    /**
     * Gets the calc_currency value for this Discount.
     * 
     * @return calc_currency
     */
    public java.lang.String getCalc_currency() {
        return calc_currency;
    }


    /**
     * Sets the calc_currency value for this Discount.
     * 
     * @param calc_currency
     */
    public void setCalc_currency(java.lang.String calc_currency) {
        this.calc_currency = calc_currency;
    }


    /**
     * Gets the calc_shopper_published value for this Discount.
     * 
     * @return calc_shopper_published
     */
    public java.lang.String getCalc_shopper_published() {
        return calc_shopper_published;
    }


    /**
     * Sets the calc_shopper_published value for this Discount.
     * 
     * @param calc_shopper_published
     */
    public void setCalc_shopper_published(java.lang.String calc_shopper_published) {
        this.calc_shopper_published = calc_shopper_published;
    }


    /**
     * Gets the calc_vendor_published value for this Discount.
     * 
     * @return calc_vendor_published
     */
    public java.lang.String getCalc_vendor_published() {
        return calc_vendor_published;
    }


    /**
     * Sets the calc_vendor_published value for this Discount.
     * 
     * @param calc_vendor_published
     */
    public void setCalc_vendor_published(java.lang.String calc_vendor_published) {
        this.calc_vendor_published = calc_vendor_published;
    }


    /**
     * Gets the publish_up value for this Discount.
     * 
     * @return publish_up
     */
    public java.lang.String getPublish_up() {
        return publish_up;
    }


    /**
     * Sets the publish_up value for this Discount.
     * 
     * @param publish_up
     */
    public void setPublish_up(java.lang.String publish_up) {
        this.publish_up = publish_up;
    }


    /**
     * Gets the publish_down value for this Discount.
     * 
     * @return publish_down
     */
    public java.lang.String getPublish_down() {
        return publish_down;
    }


    /**
     * Sets the publish_down value for this Discount.
     * 
     * @param publish_down
     */
    public void setPublish_down(java.lang.String publish_down) {
        this.publish_down = publish_down;
    }


    /**
     * Gets the calc_qualify value for this Discount.
     * 
     * @return calc_qualify
     */
    public java.lang.String getCalc_qualify() {
        return calc_qualify;
    }


    /**
     * Sets the calc_qualify value for this Discount.
     * 
     * @param calc_qualify
     */
    public void setCalc_qualify(java.lang.String calc_qualify) {
        this.calc_qualify = calc_qualify;
    }


    /**
     * Gets the calc_affected value for this Discount.
     * 
     * @return calc_affected
     */
    public java.lang.String getCalc_affected() {
        return calc_affected;
    }


    /**
     * Sets the calc_affected value for this Discount.
     * 
     * @param calc_affected
     */
    public void setCalc_affected(java.lang.String calc_affected) {
        this.calc_affected = calc_affected;
    }


    /**
     * Gets the calc_amount_cond value for this Discount.
     * 
     * @return calc_amount_cond
     */
    public java.lang.String getCalc_amount_cond() {
        return calc_amount_cond;
    }


    /**
     * Sets the calc_amount_cond value for this Discount.
     * 
     * @param calc_amount_cond
     */
    public void setCalc_amount_cond(java.lang.String calc_amount_cond) {
        this.calc_amount_cond = calc_amount_cond;
    }


    /**
     * Gets the calc_amount_dimunit value for this Discount.
     * 
     * @return calc_amount_dimunit
     */
    public java.lang.String getCalc_amount_dimunit() {
        return calc_amount_dimunit;
    }


    /**
     * Sets the calc_amount_dimunit value for this Discount.
     * 
     * @param calc_amount_dimunit
     */
    public void setCalc_amount_dimunit(java.lang.String calc_amount_dimunit) {
        this.calc_amount_dimunit = calc_amount_dimunit;
    }


    /**
     * Gets the for_override value for this Discount.
     * 
     * @return for_override
     */
    public java.lang.String getFor_override() {
        return for_override;
    }


    /**
     * Sets the for_override value for this Discount.
     * 
     * @param for_override
     */
    public void setFor_override(java.lang.String for_override) {
        this.for_override = for_override;
    }


    /**
     * Gets the ordering value for this Discount.
     * 
     * @return ordering
     */
    public java.lang.String getOrdering() {
        return ordering;
    }


    /**
     * Sets the ordering value for this Discount.
     * 
     * @param ordering
     */
    public void setOrdering(java.lang.String ordering) {
        this.ordering = ordering;
    }


    /**
     * Gets the shared value for this Discount.
     * 
     * @return shared
     */
    public java.lang.String getShared() {
        return shared;
    }


    /**
     * Sets the shared value for this Discount.
     * 
     * @param shared
     */
    public void setShared(java.lang.String shared) {
        this.shared = shared;
    }


    /**
     * Gets the published value for this Discount.
     * 
     * @return published
     */
    public java.lang.String getPublished() {
        return published;
    }


    /**
     * Sets the published value for this Discount.
     * 
     * @param published
     */
    public void setPublished(java.lang.String published) {
        this.published = published;
    }


    /**
     * Gets the discount_cat_ids value for this Discount.
     * 
     * @return discount_cat_ids
     */
    public java.lang.String getDiscount_cat_ids() {
        return discount_cat_ids;
    }


    /**
     * Sets the discount_cat_ids value for this Discount.
     * 
     * @param discount_cat_ids
     */
    public void setDiscount_cat_ids(java.lang.String discount_cat_ids) {
        this.discount_cat_ids = discount_cat_ids;
    }


    /**
     * Gets the discount_countries_ids value for this Discount.
     * 
     * @return discount_countries_ids
     */
    public java.lang.String getDiscount_countries_ids() {
        return discount_countries_ids;
    }


    /**
     * Sets the discount_countries_ids value for this Discount.
     * 
     * @param discount_countries_ids
     */
    public void setDiscount_countries_ids(java.lang.String discount_countries_ids) {
        this.discount_countries_ids = discount_countries_ids;
    }


    /**
     * Gets the discount_shoppergroups_ids value for this Discount.
     * 
     * @return discount_shoppergroups_ids
     */
    public java.lang.String getDiscount_shoppergroups_ids() {
        return discount_shoppergroups_ids;
    }


    /**
     * Sets the discount_shoppergroups_ids value for this Discount.
     * 
     * @param discount_shoppergroups_ids
     */
    public void setDiscount_shoppergroups_ids(java.lang.String discount_shoppergroups_ids) {
        this.discount_shoppergroups_ids = discount_shoppergroups_ids;
    }


    /**
     * Gets the discount_states_ids value for this Discount.
     * 
     * @return discount_states_ids
     */
    public java.lang.String getDiscount_states_ids() {
        return discount_states_ids;
    }


    /**
     * Sets the discount_states_ids value for this Discount.
     * 
     * @param discount_states_ids
     */
    public void setDiscount_states_ids(java.lang.String discount_states_ids) {
        this.discount_states_ids = discount_states_ids;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof Discount)) return false;
        Discount other = (Discount) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.discount_id==null && other.getDiscount_id()==null) || 
             (this.discount_id!=null &&
              this.discount_id.equals(other.getDiscount_id()))) &&
            ((this.vendor_id==null && other.getVendor_id()==null) || 
             (this.vendor_id!=null &&
              this.vendor_id.equals(other.getVendor_id()))) &&
            ((this.calc_name==null && other.getCalc_name()==null) || 
             (this.calc_name!=null &&
              this.calc_name.equals(other.getCalc_name()))) &&
            ((this.calc_descr==null && other.getCalc_descr()==null) || 
             (this.calc_descr!=null &&
              this.calc_descr.equals(other.getCalc_descr()))) &&
            ((this.calc_kind==null && other.getCalc_kind()==null) || 
             (this.calc_kind!=null &&
              this.calc_kind.equals(other.getCalc_kind()))) &&
            ((this.calc_value_mathop==null && other.getCalc_value_mathop()==null) || 
             (this.calc_value_mathop!=null &&
              this.calc_value_mathop.equals(other.getCalc_value_mathop()))) &&
            ((this.calc_value==null && other.getCalc_value()==null) || 
             (this.calc_value!=null &&
              this.calc_value.equals(other.getCalc_value()))) &&
            ((this.calc_currency==null && other.getCalc_currency()==null) || 
             (this.calc_currency!=null &&
              this.calc_currency.equals(other.getCalc_currency()))) &&
            ((this.calc_shopper_published==null && other.getCalc_shopper_published()==null) || 
             (this.calc_shopper_published!=null &&
              this.calc_shopper_published.equals(other.getCalc_shopper_published()))) &&
            ((this.calc_vendor_published==null && other.getCalc_vendor_published()==null) || 
             (this.calc_vendor_published!=null &&
              this.calc_vendor_published.equals(other.getCalc_vendor_published()))) &&
            ((this.publish_up==null && other.getPublish_up()==null) || 
             (this.publish_up!=null &&
              this.publish_up.equals(other.getPublish_up()))) &&
            ((this.publish_down==null && other.getPublish_down()==null) || 
             (this.publish_down!=null &&
              this.publish_down.equals(other.getPublish_down()))) &&
            ((this.calc_qualify==null && other.getCalc_qualify()==null) || 
             (this.calc_qualify!=null &&
              this.calc_qualify.equals(other.getCalc_qualify()))) &&
            ((this.calc_affected==null && other.getCalc_affected()==null) || 
             (this.calc_affected!=null &&
              this.calc_affected.equals(other.getCalc_affected()))) &&
            ((this.calc_amount_cond==null && other.getCalc_amount_cond()==null) || 
             (this.calc_amount_cond!=null &&
              this.calc_amount_cond.equals(other.getCalc_amount_cond()))) &&
            ((this.calc_amount_dimunit==null && other.getCalc_amount_dimunit()==null) || 
             (this.calc_amount_dimunit!=null &&
              this.calc_amount_dimunit.equals(other.getCalc_amount_dimunit()))) &&
            ((this.for_override==null && other.getFor_override()==null) || 
             (this.for_override!=null &&
              this.for_override.equals(other.getFor_override()))) &&
            ((this.ordering==null && other.getOrdering()==null) || 
             (this.ordering!=null &&
              this.ordering.equals(other.getOrdering()))) &&
            ((this.shared==null && other.getShared()==null) || 
             (this.shared!=null &&
              this.shared.equals(other.getShared()))) &&
            ((this.published==null && other.getPublished()==null) || 
             (this.published!=null &&
              this.published.equals(other.getPublished()))) &&
            ((this.discount_cat_ids==null && other.getDiscount_cat_ids()==null) || 
             (this.discount_cat_ids!=null &&
              this.discount_cat_ids.equals(other.getDiscount_cat_ids()))) &&
            ((this.discount_countries_ids==null && other.getDiscount_countries_ids()==null) || 
             (this.discount_countries_ids!=null &&
              this.discount_countries_ids.equals(other.getDiscount_countries_ids()))) &&
            ((this.discount_shoppergroups_ids==null && other.getDiscount_shoppergroups_ids()==null) || 
             (this.discount_shoppergroups_ids!=null &&
              this.discount_shoppergroups_ids.equals(other.getDiscount_shoppergroups_ids()))) &&
            ((this.discount_states_ids==null && other.getDiscount_states_ids()==null) || 
             (this.discount_states_ids!=null &&
              this.discount_states_ids.equals(other.getDiscount_states_ids())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getDiscount_id() != null) {
            _hashCode += getDiscount_id().hashCode();
        }
        if (getVendor_id() != null) {
            _hashCode += getVendor_id().hashCode();
        }
        if (getCalc_name() != null) {
            _hashCode += getCalc_name().hashCode();
        }
        if (getCalc_descr() != null) {
            _hashCode += getCalc_descr().hashCode();
        }
        if (getCalc_kind() != null) {
            _hashCode += getCalc_kind().hashCode();
        }
        if (getCalc_value_mathop() != null) {
            _hashCode += getCalc_value_mathop().hashCode();
        }
        if (getCalc_value() != null) {
            _hashCode += getCalc_value().hashCode();
        }
        if (getCalc_currency() != null) {
            _hashCode += getCalc_currency().hashCode();
        }
        if (getCalc_shopper_published() != null) {
            _hashCode += getCalc_shopper_published().hashCode();
        }
        if (getCalc_vendor_published() != null) {
            _hashCode += getCalc_vendor_published().hashCode();
        }
        if (getPublish_up() != null) {
            _hashCode += getPublish_up().hashCode();
        }
        if (getPublish_down() != null) {
            _hashCode += getPublish_down().hashCode();
        }
        if (getCalc_qualify() != null) {
            _hashCode += getCalc_qualify().hashCode();
        }
        if (getCalc_affected() != null) {
            _hashCode += getCalc_affected().hashCode();
        }
        if (getCalc_amount_cond() != null) {
            _hashCode += getCalc_amount_cond().hashCode();
        }
        if (getCalc_amount_dimunit() != null) {
            _hashCode += getCalc_amount_dimunit().hashCode();
        }
        if (getFor_override() != null) {
            _hashCode += getFor_override().hashCode();
        }
        if (getOrdering() != null) {
            _hashCode += getOrdering().hashCode();
        }
        if (getShared() != null) {
            _hashCode += getShared().hashCode();
        }
        if (getPublished() != null) {
            _hashCode += getPublished().hashCode();
        }
        if (getDiscount_cat_ids() != null) {
            _hashCode += getDiscount_cat_ids().hashCode();
        }
        if (getDiscount_countries_ids() != null) {
            _hashCode += getDiscount_countries_ids().hashCode();
        }
        if (getDiscount_shoppergroups_ids() != null) {
            _hashCode += getDiscount_shoppergroups_ids().hashCode();
        }
        if (getDiscount_states_ids() != null) {
            _hashCode += getDiscount_states_ids().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(Discount.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://www.virtuemart.net/VM_Product/", "Discount"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("discount_id");
        elemField.setXmlName(new javax.xml.namespace.QName("", "discount_id"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("vendor_id");
        elemField.setXmlName(new javax.xml.namespace.QName("", "vendor_id"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("calc_name");
        elemField.setXmlName(new javax.xml.namespace.QName("", "calc_name"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("calc_descr");
        elemField.setXmlName(new javax.xml.namespace.QName("", "calc_descr"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("calc_kind");
        elemField.setXmlName(new javax.xml.namespace.QName("", "calc_kind"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("calc_value_mathop");
        elemField.setXmlName(new javax.xml.namespace.QName("", "calc_value_mathop"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("calc_value");
        elemField.setXmlName(new javax.xml.namespace.QName("", "calc_value"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("calc_currency");
        elemField.setXmlName(new javax.xml.namespace.QName("", "calc_currency"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("calc_shopper_published");
        elemField.setXmlName(new javax.xml.namespace.QName("", "calc_shopper_published"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("calc_vendor_published");
        elemField.setXmlName(new javax.xml.namespace.QName("", "calc_vendor_published"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("publish_up");
        elemField.setXmlName(new javax.xml.namespace.QName("", "publish_up"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("publish_down");
        elemField.setXmlName(new javax.xml.namespace.QName("", "publish_down"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("calc_qualify");
        elemField.setXmlName(new javax.xml.namespace.QName("", "calc_qualify"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("calc_affected");
        elemField.setXmlName(new javax.xml.namespace.QName("", "calc_affected"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("calc_amount_cond");
        elemField.setXmlName(new javax.xml.namespace.QName("", "calc_amount_cond"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("calc_amount_dimunit");
        elemField.setXmlName(new javax.xml.namespace.QName("", "calc_amount_dimunit"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("for_override");
        elemField.setXmlName(new javax.xml.namespace.QName("", "for_override"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("ordering");
        elemField.setXmlName(new javax.xml.namespace.QName("", "ordering"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("shared");
        elemField.setXmlName(new javax.xml.namespace.QName("", "shared"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("published");
        elemField.setXmlName(new javax.xml.namespace.QName("", "published"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("discount_cat_ids");
        elemField.setXmlName(new javax.xml.namespace.QName("", "discount_cat_ids"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("discount_countries_ids");
        elemField.setXmlName(new javax.xml.namespace.QName("", "discount_countries_ids"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("discount_shoppergroups_ids");
        elemField.setXmlName(new javax.xml.namespace.QName("", "discount_shoppergroups_ids"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("discount_states_ids");
        elemField.setXmlName(new javax.xml.namespace.QName("", "discount_states_ids"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
