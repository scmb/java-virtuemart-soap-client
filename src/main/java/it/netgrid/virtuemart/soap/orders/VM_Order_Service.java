/**
 * VM_Order_Service.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package it.netgrid.virtuemart.soap.orders;

public interface VM_Order_Service extends javax.xml.rpc.Service {
    public java.lang.String getVM_OrderSOAPAddress();

    public it.netgrid.virtuemart.soap.orders.VM_Order_PortType getVM_OrderSOAP() throws javax.xml.rpc.ServiceException;

    public it.netgrid.virtuemart.soap.orders.VM_Order_PortType getVM_OrderSOAP(java.net.URL portAddress) throws javax.xml.rpc.ServiceException;
}
