/**
 * Order.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package it.netgrid.virtuemart.soap.orders;

public class Order  implements java.io.Serializable {
    private java.lang.String id;

    private java.lang.String user_id;

    private java.lang.String vendor_id;

    private java.lang.String order_number;

    private java.lang.String order_pass;

    private java.lang.String user_info_id;

    private java.lang.String order_total;

    private java.lang.String order_subtotal;

    private java.lang.String order_tax;

    private java.lang.String order_tax_details;

    private java.lang.String order_shipment;

    private java.lang.String order_shipment_tax;

    private java.lang.String order_payment;

    private java.lang.String order_payment_tax;

    private java.lang.String coupon_discount;

    private java.lang.String coupon_code;

    private java.lang.String order_discount;

    private java.lang.String order_currency;

    private java.lang.String order_status;

    private java.lang.String user_currency_id;

    private java.lang.String user_currency_rate;

    private java.lang.String virtuemart_paymentmethod_id;

    private java.lang.String virtuemart_shipmentmethod_id;

    private java.lang.String customer_note;

    private java.lang.String ip_address;

    private java.lang.String created_on;

    private java.lang.String modified_on;

    public Order() {
    }

    public Order(
           java.lang.String id,
           java.lang.String user_id,
           java.lang.String vendor_id,
           java.lang.String order_number,
           java.lang.String order_pass,
           java.lang.String user_info_id,
           java.lang.String order_total,
           java.lang.String order_subtotal,
           java.lang.String order_tax,
           java.lang.String order_tax_details,
           java.lang.String order_shipment,
           java.lang.String order_shipment_tax,
           java.lang.String order_payment,
           java.lang.String order_payment_tax,
           java.lang.String coupon_discount,
           java.lang.String coupon_code,
           java.lang.String order_discount,
           java.lang.String order_currency,
           java.lang.String order_status,
           java.lang.String user_currency_id,
           java.lang.String user_currency_rate,
           java.lang.String virtuemart_paymentmethod_id,
           java.lang.String virtuemart_shipmentmethod_id,
           java.lang.String customer_note,
           java.lang.String ip_address,
           java.lang.String created_on,
           java.lang.String modified_on) {
           this.id = id;
           this.user_id = user_id;
           this.vendor_id = vendor_id;
           this.order_number = order_number;
           this.order_pass = order_pass;
           this.user_info_id = user_info_id;
           this.order_total = order_total;
           this.order_subtotal = order_subtotal;
           this.order_tax = order_tax;
           this.order_tax_details = order_tax_details;
           this.order_shipment = order_shipment;
           this.order_shipment_tax = order_shipment_tax;
           this.order_payment = order_payment;
           this.order_payment_tax = order_payment_tax;
           this.coupon_discount = coupon_discount;
           this.coupon_code = coupon_code;
           this.order_discount = order_discount;
           this.order_currency = order_currency;
           this.order_status = order_status;
           this.user_currency_id = user_currency_id;
           this.user_currency_rate = user_currency_rate;
           this.virtuemart_paymentmethod_id = virtuemart_paymentmethod_id;
           this.virtuemart_shipmentmethod_id = virtuemart_shipmentmethod_id;
           this.customer_note = customer_note;
           this.ip_address = ip_address;
           this.created_on = created_on;
           this.modified_on = modified_on;
    }


    /**
     * Gets the id value for this Order.
     * 
     * @return id
     */
    public java.lang.String getId() {
        return id;
    }


    /**
     * Sets the id value for this Order.
     * 
     * @param id
     */
    public void setId(java.lang.String id) {
        this.id = id;
    }


    /**
     * Gets the user_id value for this Order.
     * 
     * @return user_id
     */
    public java.lang.String getUser_id() {
        return user_id;
    }


    /**
     * Sets the user_id value for this Order.
     * 
     * @param user_id
     */
    public void setUser_id(java.lang.String user_id) {
        this.user_id = user_id;
    }


    /**
     * Gets the vendor_id value for this Order.
     * 
     * @return vendor_id
     */
    public java.lang.String getVendor_id() {
        return vendor_id;
    }


    /**
     * Sets the vendor_id value for this Order.
     * 
     * @param vendor_id
     */
    public void setVendor_id(java.lang.String vendor_id) {
        this.vendor_id = vendor_id;
    }


    /**
     * Gets the order_number value for this Order.
     * 
     * @return order_number
     */
    public java.lang.String getOrder_number() {
        return order_number;
    }


    /**
     * Sets the order_number value for this Order.
     * 
     * @param order_number
     */
    public void setOrder_number(java.lang.String order_number) {
        this.order_number = order_number;
    }


    /**
     * Gets the order_pass value for this Order.
     * 
     * @return order_pass
     */
    public java.lang.String getOrder_pass() {
        return order_pass;
    }


    /**
     * Sets the order_pass value for this Order.
     * 
     * @param order_pass
     */
    public void setOrder_pass(java.lang.String order_pass) {
        this.order_pass = order_pass;
    }


    /**
     * Gets the user_info_id value for this Order.
     * 
     * @return user_info_id
     */
    public java.lang.String getUser_info_id() {
        return user_info_id;
    }


    /**
     * Sets the user_info_id value for this Order.
     * 
     * @param user_info_id
     */
    public void setUser_info_id(java.lang.String user_info_id) {
        this.user_info_id = user_info_id;
    }


    /**
     * Gets the order_total value for this Order.
     * 
     * @return order_total
     */
    public java.lang.String getOrder_total() {
        return order_total;
    }


    /**
     * Sets the order_total value for this Order.
     * 
     * @param order_total
     */
    public void setOrder_total(java.lang.String order_total) {
        this.order_total = order_total;
    }


    /**
     * Gets the order_subtotal value for this Order.
     * 
     * @return order_subtotal
     */
    public java.lang.String getOrder_subtotal() {
        return order_subtotal;
    }


    /**
     * Sets the order_subtotal value for this Order.
     * 
     * @param order_subtotal
     */
    public void setOrder_subtotal(java.lang.String order_subtotal) {
        this.order_subtotal = order_subtotal;
    }


    /**
     * Gets the order_tax value for this Order.
     * 
     * @return order_tax
     */
    public java.lang.String getOrder_tax() {
        return order_tax;
    }


    /**
     * Sets the order_tax value for this Order.
     * 
     * @param order_tax
     */
    public void setOrder_tax(java.lang.String order_tax) {
        this.order_tax = order_tax;
    }


    /**
     * Gets the order_tax_details value for this Order.
     * 
     * @return order_tax_details
     */
    public java.lang.String getOrder_tax_details() {
        return order_tax_details;
    }


    /**
     * Sets the order_tax_details value for this Order.
     * 
     * @param order_tax_details
     */
    public void setOrder_tax_details(java.lang.String order_tax_details) {
        this.order_tax_details = order_tax_details;
    }


    /**
     * Gets the order_shipment value for this Order.
     * 
     * @return order_shipment
     */
    public java.lang.String getOrder_shipment() {
        return order_shipment;
    }


    /**
     * Sets the order_shipment value for this Order.
     * 
     * @param order_shipment
     */
    public void setOrder_shipment(java.lang.String order_shipment) {
        this.order_shipment = order_shipment;
    }


    /**
     * Gets the order_shipment_tax value for this Order.
     * 
     * @return order_shipment_tax
     */
    public java.lang.String getOrder_shipment_tax() {
        return order_shipment_tax;
    }


    /**
     * Sets the order_shipment_tax value for this Order.
     * 
     * @param order_shipment_tax
     */
    public void setOrder_shipment_tax(java.lang.String order_shipment_tax) {
        this.order_shipment_tax = order_shipment_tax;
    }


    /**
     * Gets the order_payment value for this Order.
     * 
     * @return order_payment
     */
    public java.lang.String getOrder_payment() {
        return order_payment;
    }


    /**
     * Sets the order_payment value for this Order.
     * 
     * @param order_payment
     */
    public void setOrder_payment(java.lang.String order_payment) {
        this.order_payment = order_payment;
    }


    /**
     * Gets the order_payment_tax value for this Order.
     * 
     * @return order_payment_tax
     */
    public java.lang.String getOrder_payment_tax() {
        return order_payment_tax;
    }


    /**
     * Sets the order_payment_tax value for this Order.
     * 
     * @param order_payment_tax
     */
    public void setOrder_payment_tax(java.lang.String order_payment_tax) {
        this.order_payment_tax = order_payment_tax;
    }


    /**
     * Gets the coupon_discount value for this Order.
     * 
     * @return coupon_discount
     */
    public java.lang.String getCoupon_discount() {
        return coupon_discount;
    }


    /**
     * Sets the coupon_discount value for this Order.
     * 
     * @param coupon_discount
     */
    public void setCoupon_discount(java.lang.String coupon_discount) {
        this.coupon_discount = coupon_discount;
    }


    /**
     * Gets the coupon_code value for this Order.
     * 
     * @return coupon_code
     */
    public java.lang.String getCoupon_code() {
        return coupon_code;
    }


    /**
     * Sets the coupon_code value for this Order.
     * 
     * @param coupon_code
     */
    public void setCoupon_code(java.lang.String coupon_code) {
        this.coupon_code = coupon_code;
    }


    /**
     * Gets the order_discount value for this Order.
     * 
     * @return order_discount
     */
    public java.lang.String getOrder_discount() {
        return order_discount;
    }


    /**
     * Sets the order_discount value for this Order.
     * 
     * @param order_discount
     */
    public void setOrder_discount(java.lang.String order_discount) {
        this.order_discount = order_discount;
    }


    /**
     * Gets the order_currency value for this Order.
     * 
     * @return order_currency
     */
    public java.lang.String getOrder_currency() {
        return order_currency;
    }


    /**
     * Sets the order_currency value for this Order.
     * 
     * @param order_currency
     */
    public void setOrder_currency(java.lang.String order_currency) {
        this.order_currency = order_currency;
    }


    /**
     * Gets the order_status value for this Order.
     * 
     * @return order_status
     */
    public java.lang.String getOrder_status() {
        return order_status;
    }


    /**
     * Sets the order_status value for this Order.
     * 
     * @param order_status
     */
    public void setOrder_status(java.lang.String order_status) {
        this.order_status = order_status;
    }


    /**
     * Gets the user_currency_id value for this Order.
     * 
     * @return user_currency_id
     */
    public java.lang.String getUser_currency_id() {
        return user_currency_id;
    }


    /**
     * Sets the user_currency_id value for this Order.
     * 
     * @param user_currency_id
     */
    public void setUser_currency_id(java.lang.String user_currency_id) {
        this.user_currency_id = user_currency_id;
    }


    /**
     * Gets the user_currency_rate value for this Order.
     * 
     * @return user_currency_rate
     */
    public java.lang.String getUser_currency_rate() {
        return user_currency_rate;
    }


    /**
     * Sets the user_currency_rate value for this Order.
     * 
     * @param user_currency_rate
     */
    public void setUser_currency_rate(java.lang.String user_currency_rate) {
        this.user_currency_rate = user_currency_rate;
    }


    /**
     * Gets the virtuemart_paymentmethod_id value for this Order.
     * 
     * @return virtuemart_paymentmethod_id
     */
    public java.lang.String getVirtuemart_paymentmethod_id() {
        return virtuemart_paymentmethod_id;
    }


    /**
     * Sets the virtuemart_paymentmethod_id value for this Order.
     * 
     * @param virtuemart_paymentmethod_id
     */
    public void setVirtuemart_paymentmethod_id(java.lang.String virtuemart_paymentmethod_id) {
        this.virtuemart_paymentmethod_id = virtuemart_paymentmethod_id;
    }


    /**
     * Gets the virtuemart_shipmentmethod_id value for this Order.
     * 
     * @return virtuemart_shipmentmethod_id
     */
    public java.lang.String getVirtuemart_shipmentmethod_id() {
        return virtuemart_shipmentmethod_id;
    }


    /**
     * Sets the virtuemart_shipmentmethod_id value for this Order.
     * 
     * @param virtuemart_shipmentmethod_id
     */
    public void setVirtuemart_shipmentmethod_id(java.lang.String virtuemart_shipmentmethod_id) {
        this.virtuemart_shipmentmethod_id = virtuemart_shipmentmethod_id;
    }


    /**
     * Gets the customer_note value for this Order.
     * 
     * @return customer_note
     */
    public java.lang.String getCustomer_note() {
        return customer_note;
    }


    /**
     * Sets the customer_note value for this Order.
     * 
     * @param customer_note
     */
    public void setCustomer_note(java.lang.String customer_note) {
        this.customer_note = customer_note;
    }


    /**
     * Gets the ip_address value for this Order.
     * 
     * @return ip_address
     */
    public java.lang.String getIp_address() {
        return ip_address;
    }


    /**
     * Sets the ip_address value for this Order.
     * 
     * @param ip_address
     */
    public void setIp_address(java.lang.String ip_address) {
        this.ip_address = ip_address;
    }


    /**
     * Gets the created_on value for this Order.
     * 
     * @return created_on
     */
    public java.lang.String getCreated_on() {
        return created_on;
    }


    /**
     * Sets the created_on value for this Order.
     * 
     * @param created_on
     */
    public void setCreated_on(java.lang.String created_on) {
        this.created_on = created_on;
    }


    /**
     * Gets the modified_on value for this Order.
     * 
     * @return modified_on
     */
    public java.lang.String getModified_on() {
        return modified_on;
    }


    /**
     * Sets the modified_on value for this Order.
     * 
     * @param modified_on
     */
    public void setModified_on(java.lang.String modified_on) {
        this.modified_on = modified_on;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof Order)) return false;
        Order other = (Order) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.id==null && other.getId()==null) || 
             (this.id!=null &&
              this.id.equals(other.getId()))) &&
            ((this.user_id==null && other.getUser_id()==null) || 
             (this.user_id!=null &&
              this.user_id.equals(other.getUser_id()))) &&
            ((this.vendor_id==null && other.getVendor_id()==null) || 
             (this.vendor_id!=null &&
              this.vendor_id.equals(other.getVendor_id()))) &&
            ((this.order_number==null && other.getOrder_number()==null) || 
             (this.order_number!=null &&
              this.order_number.equals(other.getOrder_number()))) &&
            ((this.order_pass==null && other.getOrder_pass()==null) || 
             (this.order_pass!=null &&
              this.order_pass.equals(other.getOrder_pass()))) &&
            ((this.user_info_id==null && other.getUser_info_id()==null) || 
             (this.user_info_id!=null &&
              this.user_info_id.equals(other.getUser_info_id()))) &&
            ((this.order_total==null && other.getOrder_total()==null) || 
             (this.order_total!=null &&
              this.order_total.equals(other.getOrder_total()))) &&
            ((this.order_subtotal==null && other.getOrder_subtotal()==null) || 
             (this.order_subtotal!=null &&
              this.order_subtotal.equals(other.getOrder_subtotal()))) &&
            ((this.order_tax==null && other.getOrder_tax()==null) || 
             (this.order_tax!=null &&
              this.order_tax.equals(other.getOrder_tax()))) &&
            ((this.order_tax_details==null && other.getOrder_tax_details()==null) || 
             (this.order_tax_details!=null &&
              this.order_tax_details.equals(other.getOrder_tax_details()))) &&
            ((this.order_shipment==null && other.getOrder_shipment()==null) || 
             (this.order_shipment!=null &&
              this.order_shipment.equals(other.getOrder_shipment()))) &&
            ((this.order_shipment_tax==null && other.getOrder_shipment_tax()==null) || 
             (this.order_shipment_tax!=null &&
              this.order_shipment_tax.equals(other.getOrder_shipment_tax()))) &&
            ((this.order_payment==null && other.getOrder_payment()==null) || 
             (this.order_payment!=null &&
              this.order_payment.equals(other.getOrder_payment()))) &&
            ((this.order_payment_tax==null && other.getOrder_payment_tax()==null) || 
             (this.order_payment_tax!=null &&
              this.order_payment_tax.equals(other.getOrder_payment_tax()))) &&
            ((this.coupon_discount==null && other.getCoupon_discount()==null) || 
             (this.coupon_discount!=null &&
              this.coupon_discount.equals(other.getCoupon_discount()))) &&
            ((this.coupon_code==null && other.getCoupon_code()==null) || 
             (this.coupon_code!=null &&
              this.coupon_code.equals(other.getCoupon_code()))) &&
            ((this.order_discount==null && other.getOrder_discount()==null) || 
             (this.order_discount!=null &&
              this.order_discount.equals(other.getOrder_discount()))) &&
            ((this.order_currency==null && other.getOrder_currency()==null) || 
             (this.order_currency!=null &&
              this.order_currency.equals(other.getOrder_currency()))) &&
            ((this.order_status==null && other.getOrder_status()==null) || 
             (this.order_status!=null &&
              this.order_status.equals(other.getOrder_status()))) &&
            ((this.user_currency_id==null && other.getUser_currency_id()==null) || 
             (this.user_currency_id!=null &&
              this.user_currency_id.equals(other.getUser_currency_id()))) &&
            ((this.user_currency_rate==null && other.getUser_currency_rate()==null) || 
             (this.user_currency_rate!=null &&
              this.user_currency_rate.equals(other.getUser_currency_rate()))) &&
            ((this.virtuemart_paymentmethod_id==null && other.getVirtuemart_paymentmethod_id()==null) || 
             (this.virtuemart_paymentmethod_id!=null &&
              this.virtuemart_paymentmethod_id.equals(other.getVirtuemart_paymentmethod_id()))) &&
            ((this.virtuemart_shipmentmethod_id==null && other.getVirtuemart_shipmentmethod_id()==null) || 
             (this.virtuemart_shipmentmethod_id!=null &&
              this.virtuemart_shipmentmethod_id.equals(other.getVirtuemart_shipmentmethod_id()))) &&
            ((this.customer_note==null && other.getCustomer_note()==null) || 
             (this.customer_note!=null &&
              this.customer_note.equals(other.getCustomer_note()))) &&
            ((this.ip_address==null && other.getIp_address()==null) || 
             (this.ip_address!=null &&
              this.ip_address.equals(other.getIp_address()))) &&
            ((this.created_on==null && other.getCreated_on()==null) || 
             (this.created_on!=null &&
              this.created_on.equals(other.getCreated_on()))) &&
            ((this.modified_on==null && other.getModified_on()==null) || 
             (this.modified_on!=null &&
              this.modified_on.equals(other.getModified_on())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getId() != null) {
            _hashCode += getId().hashCode();
        }
        if (getUser_id() != null) {
            _hashCode += getUser_id().hashCode();
        }
        if (getVendor_id() != null) {
            _hashCode += getVendor_id().hashCode();
        }
        if (getOrder_number() != null) {
            _hashCode += getOrder_number().hashCode();
        }
        if (getOrder_pass() != null) {
            _hashCode += getOrder_pass().hashCode();
        }
        if (getUser_info_id() != null) {
            _hashCode += getUser_info_id().hashCode();
        }
        if (getOrder_total() != null) {
            _hashCode += getOrder_total().hashCode();
        }
        if (getOrder_subtotal() != null) {
            _hashCode += getOrder_subtotal().hashCode();
        }
        if (getOrder_tax() != null) {
            _hashCode += getOrder_tax().hashCode();
        }
        if (getOrder_tax_details() != null) {
            _hashCode += getOrder_tax_details().hashCode();
        }
        if (getOrder_shipment() != null) {
            _hashCode += getOrder_shipment().hashCode();
        }
        if (getOrder_shipment_tax() != null) {
            _hashCode += getOrder_shipment_tax().hashCode();
        }
        if (getOrder_payment() != null) {
            _hashCode += getOrder_payment().hashCode();
        }
        if (getOrder_payment_tax() != null) {
            _hashCode += getOrder_payment_tax().hashCode();
        }
        if (getCoupon_discount() != null) {
            _hashCode += getCoupon_discount().hashCode();
        }
        if (getCoupon_code() != null) {
            _hashCode += getCoupon_code().hashCode();
        }
        if (getOrder_discount() != null) {
            _hashCode += getOrder_discount().hashCode();
        }
        if (getOrder_currency() != null) {
            _hashCode += getOrder_currency().hashCode();
        }
        if (getOrder_status() != null) {
            _hashCode += getOrder_status().hashCode();
        }
        if (getUser_currency_id() != null) {
            _hashCode += getUser_currency_id().hashCode();
        }
        if (getUser_currency_rate() != null) {
            _hashCode += getUser_currency_rate().hashCode();
        }
        if (getVirtuemart_paymentmethod_id() != null) {
            _hashCode += getVirtuemart_paymentmethod_id().hashCode();
        }
        if (getVirtuemart_shipmentmethod_id() != null) {
            _hashCode += getVirtuemart_shipmentmethod_id().hashCode();
        }
        if (getCustomer_note() != null) {
            _hashCode += getCustomer_note().hashCode();
        }
        if (getIp_address() != null) {
            _hashCode += getIp_address().hashCode();
        }
        if (getCreated_on() != null) {
            _hashCode += getCreated_on().hashCode();
        }
        if (getModified_on() != null) {
            _hashCode += getModified_on().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(Order.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://www.virtuemart.net/VM_Order/", "Order"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("id");
        elemField.setXmlName(new javax.xml.namespace.QName("", "id"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("user_id");
        elemField.setXmlName(new javax.xml.namespace.QName("", "user_id"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("vendor_id");
        elemField.setXmlName(new javax.xml.namespace.QName("", "vendor_id"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("order_number");
        elemField.setXmlName(new javax.xml.namespace.QName("", "order_number"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("order_pass");
        elemField.setXmlName(new javax.xml.namespace.QName("", "order_pass"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("user_info_id");
        elemField.setXmlName(new javax.xml.namespace.QName("", "user_info_id"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("order_total");
        elemField.setXmlName(new javax.xml.namespace.QName("", "order_total"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("order_subtotal");
        elemField.setXmlName(new javax.xml.namespace.QName("", "order_subtotal"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("order_tax");
        elemField.setXmlName(new javax.xml.namespace.QName("", "order_tax"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("order_tax_details");
        elemField.setXmlName(new javax.xml.namespace.QName("", "order_tax_details"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("order_shipment");
        elemField.setXmlName(new javax.xml.namespace.QName("", "order_shipment"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("order_shipment_tax");
        elemField.setXmlName(new javax.xml.namespace.QName("", "order_shipment_tax"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("order_payment");
        elemField.setXmlName(new javax.xml.namespace.QName("", "order_payment"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("order_payment_tax");
        elemField.setXmlName(new javax.xml.namespace.QName("", "order_payment_tax"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("coupon_discount");
        elemField.setXmlName(new javax.xml.namespace.QName("", "coupon_discount"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("coupon_code");
        elemField.setXmlName(new javax.xml.namespace.QName("", "coupon_code"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("order_discount");
        elemField.setXmlName(new javax.xml.namespace.QName("", "order_discount"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("order_currency");
        elemField.setXmlName(new javax.xml.namespace.QName("", "order_currency"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("order_status");
        elemField.setXmlName(new javax.xml.namespace.QName("", "order_status"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("user_currency_id");
        elemField.setXmlName(new javax.xml.namespace.QName("", "user_currency_id"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("user_currency_rate");
        elemField.setXmlName(new javax.xml.namespace.QName("", "user_currency_rate"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("virtuemart_paymentmethod_id");
        elemField.setXmlName(new javax.xml.namespace.QName("", "virtuemart_paymentmethod_id"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("virtuemart_shipmentmethod_id");
        elemField.setXmlName(new javax.xml.namespace.QName("", "virtuemart_shipmentmethod_id"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("customer_note");
        elemField.setXmlName(new javax.xml.namespace.QName("", "customer_note"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("ip_address");
        elemField.setXmlName(new javax.xml.namespace.QName("", "ip_address"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("created_on");
        elemField.setXmlName(new javax.xml.namespace.QName("", "created_on"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("modified_on");
        elemField.setXmlName(new javax.xml.namespace.QName("", "modified_on"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
