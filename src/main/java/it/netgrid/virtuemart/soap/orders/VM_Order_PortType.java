/**
 * VM_Order_PortType.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package it.netgrid.virtuemart.soap.orders;

public interface VM_Order_PortType extends java.rmi.Remote {
    public it.netgrid.virtuemart.soap.orders.Order[] getAllOrders(it.netgrid.virtuemart.soap.orders.AllOrderRequest parameters) throws java.rmi.RemoteException;
    public it.netgrid.virtuemart.soap.orders.Order getOrder(it.netgrid.virtuemart.soap.orders.OrderRequest parameters) throws java.rmi.RemoteException;
    public it.netgrid.virtuemart.soap.orders.Order[] getOrdersFromStatus(it.netgrid.virtuemart.soap.orders.OrderRequestWithStatus parameters) throws java.rmi.RemoteException;
    public it.netgrid.virtuemart.soap.orders.OrderStatus[] getOrderStatus(it.netgrid.virtuemart.soap.orders.LoginInfo parameters) throws java.rmi.RemoteException;
    public it.netgrid.virtuemart.soap.orders.CommonReturn updateOrderStatus(it.netgrid.virtuemart.soap.orders.UpdateOrderStatusInput parameters) throws java.rmi.RemoteException;
    public it.netgrid.virtuemart.soap.orders.CommonReturn deleteOrder(it.netgrid.virtuemart.soap.orders.DeleteOrderInput parameters) throws java.rmi.RemoteException;
    public it.netgrid.virtuemart.soap.orders.CommonReturn createOrder(it.netgrid.virtuemart.soap.orders.CreateOrderInput parameters) throws java.rmi.RemoteException;
    public it.netgrid.virtuemart.soap.orders.Coupon[] getAllCouponCode(it.netgrid.virtuemart.soap.orders.GetAllCouponCodeInput parameters) throws java.rmi.RemoteException;
    public it.netgrid.virtuemart.soap.orders.CommonReturn addCouponCode(it.netgrid.virtuemart.soap.orders.AddCouponInput parameters) throws java.rmi.RemoteException;
    public it.netgrid.virtuemart.soap.orders.CommonReturn deleteCouponCode(it.netgrid.virtuemart.soap.orders.DelInput parameters) throws java.rmi.RemoteException;
    public it.netgrid.virtuemart.soap.orders.ShippingRate[] getAllShippingRate(it.netgrid.virtuemart.soap.orders.LoginInfo parameters) throws java.rmi.RemoteException;
    public it.netgrid.virtuemart.soap.orders.ShippingMethod[] getAllShippingMethod(it.netgrid.virtuemart.soap.orders.LoginInfo parameters) throws java.rmi.RemoteException;
    public it.netgrid.virtuemart.soap.orders.CommonReturn addShippingRate(it.netgrid.virtuemart.soap.orders.AddShippingRatesInput parameters) throws java.rmi.RemoteException;
    public it.netgrid.virtuemart.soap.orders.CommonReturn addShippingMethod(it.netgrid.virtuemart.soap.orders.AddShippingMethodsInput parameters) throws java.rmi.RemoteException;
    public it.netgrid.virtuemart.soap.orders.CommonReturn deleteShippingRate(it.netgrid.virtuemart.soap.orders.DelInput parameters) throws java.rmi.RemoteException;
    public it.netgrid.virtuemart.soap.orders.CommonReturn deleteShippingMethod(it.netgrid.virtuemart.soap.orders.DelInput parameters) throws java.rmi.RemoteException;
    public it.netgrid.virtuemart.soap.orders.PaymentMethod[] getAllPaymentMethod(it.netgrid.virtuemart.soap.orders.GetPaymentMethodInput parameters) throws java.rmi.RemoteException;
    public it.netgrid.virtuemart.soap.orders.CommonReturn addPaymentMethod(it.netgrid.virtuemart.soap.orders.AddPaymentMethodInput parameters) throws java.rmi.RemoteException;
    public it.netgrid.virtuemart.soap.orders.CommonReturn updatePaymentMethod(it.netgrid.virtuemart.soap.orders.AddPaymentMethodInput parameters) throws java.rmi.RemoteException;
    public it.netgrid.virtuemart.soap.orders.CommonReturn deletePaymentMethod(it.netgrid.virtuemart.soap.orders.DelInput parameters) throws java.rmi.RemoteException;
    public it.netgrid.virtuemart.soap.orders.Order[] getOrderFromDate(it.netgrid.virtuemart.soap.orders.GetOrderFromDateInput parameters) throws java.rmi.RemoteException;
    public it.netgrid.virtuemart.soap.orders.Creditcard[] getAllCreditCard(it.netgrid.virtuemart.soap.orders.LoginInfo parameters) throws java.rmi.RemoteException;
    public it.netgrid.virtuemart.soap.orders.CommonReturn addCreditCard(it.netgrid.virtuemart.soap.orders.AddCreditCardInput parameters) throws java.rmi.RemoteException;
    public it.netgrid.virtuemart.soap.orders.CommonReturn updateCreditCard(it.netgrid.virtuemart.soap.orders.AddCreditCardInput parameters) throws java.rmi.RemoteException;
    public it.netgrid.virtuemart.soap.orders.CommonReturn deleteCreditCard(it.netgrid.virtuemart.soap.orders.DelCreditCardInput parameters) throws java.rmi.RemoteException;
    public it.netgrid.virtuemart.soap.orders.CommonReturn addOrderStatusCode(it.netgrid.virtuemart.soap.orders.AddStatusInput parameters) throws java.rmi.RemoteException;
    public it.netgrid.virtuemart.soap.orders.CommonReturn updateOrderStatusCode(it.netgrid.virtuemart.soap.orders.AddStatusInput parameters) throws java.rmi.RemoteException;
    public it.netgrid.virtuemart.soap.orders.CommonReturn deleteOrderStatusCode(it.netgrid.virtuemart.soap.orders.DeleteStatusInput parameters) throws java.rmi.RemoteException;
    public it.netgrid.virtuemart.soap.orders.CommonReturn updateShippingMethod(it.netgrid.virtuemart.soap.orders.AddShippingMethodsInput parameters) throws java.rmi.RemoteException;
    public it.netgrid.virtuemart.soap.orders.CommonReturn updateShippingRate(it.netgrid.virtuemart.soap.orders.AddShippingRatesInput parameters) throws java.rmi.RemoteException;
    public it.netgrid.virtuemart.soap.orders.PaymentMethod getOrderPaymentInfo(it.netgrid.virtuemart.soap.orders.GetOrderPaymentInfoInput parameters) throws java.rmi.RemoteException;
    public it.netgrid.virtuemart.soap.orders.CommonReturn changeOrderPayment(it.netgrid.virtuemart.soap.orders.ChangeOrderPaymentInput parameters) throws java.rmi.RemoteException;
    public it.netgrid.virtuemart.soap.orders.CommonReturn changeProductItemPrice(it.netgrid.virtuemart.soap.orders.ChangeProductItemPriceInput parameters) throws java.rmi.RemoteException;
    public it.netgrid.virtuemart.soap.orders.CommonReturn changeItemQuantity(it.netgrid.virtuemart.soap.orders.ChangeItemQuantityInput parameters) throws java.rmi.RemoteException;
    public it.netgrid.virtuemart.soap.orders.CommonReturn addOrderItem(it.netgrid.virtuemart.soap.orders.AddOrderItemInput parameters) throws java.rmi.RemoteException;
    public it.netgrid.virtuemart.soap.orders.CommonReturn deleteOrderItem(it.netgrid.virtuemart.soap.orders.DeleteOrderItemInput parameters) throws java.rmi.RemoteException;
    public it.netgrid.virtuemart.soap.orders.CommonReturn changeOrderCouponDiscount(it.netgrid.virtuemart.soap.orders.ChangeOrderCouponDiscountInput parameters) throws java.rmi.RemoteException;
    public it.netgrid.virtuemart.soap.orders.CommonReturn changeOrderShipping(it.netgrid.virtuemart.soap.orders.ChangeOrderShippingInput parameters) throws java.rmi.RemoteException;
    public it.netgrid.virtuemart.soap.orders.CommonReturn changeOrderCustomerNote(it.netgrid.virtuemart.soap.orders.ChangeOrderCustomerNoteInput parameters) throws java.rmi.RemoteException;
    public it.netgrid.virtuemart.soap.orders.CommonReturn changeOrderShipTo(it.netgrid.virtuemart.soap.orders.ChangeOrderShipToInput parameters) throws java.rmi.RemoteException;
    public it.netgrid.virtuemart.soap.orders.CommonReturn changeOrderBillTo(it.netgrid.virtuemart.soap.orders.ChangeOrderBillToInput parameters) throws java.rmi.RemoteException;
    public it.netgrid.virtuemart.soap.orders.Plugin[] getPluginsInfo(it.netgrid.virtuemart.soap.orders.GetPluginsInfoInput parameters) throws java.rmi.RemoteException;
}
