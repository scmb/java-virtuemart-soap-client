/**
 * AddShippingMethodsInput.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package it.netgrid.virtuemart.soap.orders;

public class AddShippingMethodsInput  implements java.io.Serializable {
    private it.netgrid.virtuemart.soap.orders.LoginInfo loginInfo;

    private it.netgrid.virtuemart.soap.orders.ShippingMethod[] shippingMethods;

    public AddShippingMethodsInput() {
    }

    public AddShippingMethodsInput(
           it.netgrid.virtuemart.soap.orders.LoginInfo loginInfo,
           it.netgrid.virtuemart.soap.orders.ShippingMethod[] shippingMethods) {
           this.loginInfo = loginInfo;
           this.shippingMethods = shippingMethods;
    }


    /**
     * Gets the loginInfo value for this AddShippingMethodsInput.
     * 
     * @return loginInfo
     */
    public it.netgrid.virtuemart.soap.orders.LoginInfo getLoginInfo() {
        return loginInfo;
    }


    /**
     * Sets the loginInfo value for this AddShippingMethodsInput.
     * 
     * @param loginInfo
     */
    public void setLoginInfo(it.netgrid.virtuemart.soap.orders.LoginInfo loginInfo) {
        this.loginInfo = loginInfo;
    }


    /**
     * Gets the shippingMethods value for this AddShippingMethodsInput.
     * 
     * @return shippingMethods
     */
    public it.netgrid.virtuemart.soap.orders.ShippingMethod[] getShippingMethods() {
        return shippingMethods;
    }


    /**
     * Sets the shippingMethods value for this AddShippingMethodsInput.
     * 
     * @param shippingMethods
     */
    public void setShippingMethods(it.netgrid.virtuemart.soap.orders.ShippingMethod[] shippingMethods) {
        this.shippingMethods = shippingMethods;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof AddShippingMethodsInput)) return false;
        AddShippingMethodsInput other = (AddShippingMethodsInput) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.loginInfo==null && other.getLoginInfo()==null) || 
             (this.loginInfo!=null &&
              this.loginInfo.equals(other.getLoginInfo()))) &&
            ((this.shippingMethods==null && other.getShippingMethods()==null) || 
             (this.shippingMethods!=null &&
              java.util.Arrays.equals(this.shippingMethods, other.getShippingMethods())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getLoginInfo() != null) {
            _hashCode += getLoginInfo().hashCode();
        }
        if (getShippingMethods() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getShippingMethods());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getShippingMethods(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(AddShippingMethodsInput.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://www.virtuemart.net/VM_Order/", "AddShippingMethodsInput"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("loginInfo");
        elemField.setXmlName(new javax.xml.namespace.QName("", "loginInfo"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.virtuemart.net/VM_Order/", "loginInfo"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("shippingMethods");
        elemField.setXmlName(new javax.xml.namespace.QName("", "ShippingMethods"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.virtuemart.net/VM_Order/", "ShippingMethod"));
        elemField.setNillable(true);
        elemField.setItemQName(new javax.xml.namespace.QName("", "ShippingMethod"));
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
