package it.netgrid.virtuemart.soap.orders;

public class VM_OrderProxy implements it.netgrid.virtuemart.soap.orders.VM_Order_PortType {
  private String _endpoint = null;
  private it.netgrid.virtuemart.soap.orders.VM_Order_PortType vM_Order_PortType = null;
  
  public VM_OrderProxy() {
    _initVM_OrderProxy();
  }
  
  public VM_OrderProxy(String endpoint) {
    _endpoint = endpoint;
    _initVM_OrderProxy();
  }
  
  private void _initVM_OrderProxy() {
    try {
      vM_Order_PortType = (new it.netgrid.virtuemart.soap.orders.VM_Order_ServiceLocator()).getVM_OrderSOAP();
      if (vM_Order_PortType != null) {
        if (_endpoint != null)
          ((javax.xml.rpc.Stub)vM_Order_PortType)._setProperty("javax.xml.rpc.service.endpoint.address", _endpoint);
        else
          _endpoint = (String)((javax.xml.rpc.Stub)vM_Order_PortType)._getProperty("javax.xml.rpc.service.endpoint.address");
      }
      
    }
    catch (javax.xml.rpc.ServiceException serviceException) {}
  }
  
  public String getEndpoint() {
    return _endpoint;
  }
  
  public void setEndpoint(String endpoint) {
    _endpoint = endpoint;
    if (vM_Order_PortType != null)
      ((javax.xml.rpc.Stub)vM_Order_PortType)._setProperty("javax.xml.rpc.service.endpoint.address", _endpoint);
    
  }
  
  public it.netgrid.virtuemart.soap.orders.VM_Order_PortType getVM_Order_PortType() {
    if (vM_Order_PortType == null)
      _initVM_OrderProxy();
    return vM_Order_PortType;
  }
  
  public it.netgrid.virtuemart.soap.orders.Order[] getAllOrders(it.netgrid.virtuemart.soap.orders.AllOrderRequest parameters) throws java.rmi.RemoteException{
    if (vM_Order_PortType == null)
      _initVM_OrderProxy();
    return vM_Order_PortType.getAllOrders(parameters);
  }
  
  public it.netgrid.virtuemart.soap.orders.Order getOrder(it.netgrid.virtuemart.soap.orders.OrderRequest parameters) throws java.rmi.RemoteException{
    if (vM_Order_PortType == null)
      _initVM_OrderProxy();
    return vM_Order_PortType.getOrder(parameters);
  }
  
  public it.netgrid.virtuemart.soap.orders.Order[] getOrdersFromStatus(it.netgrid.virtuemart.soap.orders.OrderRequestWithStatus parameters) throws java.rmi.RemoteException{
    if (vM_Order_PortType == null)
      _initVM_OrderProxy();
    return vM_Order_PortType.getOrdersFromStatus(parameters);
  }
  
  public it.netgrid.virtuemart.soap.orders.OrderStatus[] getOrderStatus(it.netgrid.virtuemart.soap.orders.LoginInfo parameters) throws java.rmi.RemoteException{
    if (vM_Order_PortType == null)
      _initVM_OrderProxy();
    return vM_Order_PortType.getOrderStatus(parameters);
  }
  
  public it.netgrid.virtuemart.soap.orders.CommonReturn updateOrderStatus(it.netgrid.virtuemart.soap.orders.UpdateOrderStatusInput parameters) throws java.rmi.RemoteException{
    if (vM_Order_PortType == null)
      _initVM_OrderProxy();
    return vM_Order_PortType.updateOrderStatus(parameters);
  }
  
  public it.netgrid.virtuemart.soap.orders.CommonReturn deleteOrder(it.netgrid.virtuemart.soap.orders.DeleteOrderInput parameters) throws java.rmi.RemoteException{
    if (vM_Order_PortType == null)
      _initVM_OrderProxy();
    return vM_Order_PortType.deleteOrder(parameters);
  }
  
  public it.netgrid.virtuemart.soap.orders.CommonReturn createOrder(it.netgrid.virtuemart.soap.orders.CreateOrderInput parameters) throws java.rmi.RemoteException{
    if (vM_Order_PortType == null)
      _initVM_OrderProxy();
    return vM_Order_PortType.createOrder(parameters);
  }
  
  public it.netgrid.virtuemart.soap.orders.Coupon[] getAllCouponCode(it.netgrid.virtuemart.soap.orders.GetAllCouponCodeInput parameters) throws java.rmi.RemoteException{
    if (vM_Order_PortType == null)
      _initVM_OrderProxy();
    return vM_Order_PortType.getAllCouponCode(parameters);
  }
  
  public it.netgrid.virtuemart.soap.orders.CommonReturn addCouponCode(it.netgrid.virtuemart.soap.orders.AddCouponInput parameters) throws java.rmi.RemoteException{
    if (vM_Order_PortType == null)
      _initVM_OrderProxy();
    return vM_Order_PortType.addCouponCode(parameters);
  }
  
  public it.netgrid.virtuemart.soap.orders.CommonReturn deleteCouponCode(it.netgrid.virtuemart.soap.orders.DelInput parameters) throws java.rmi.RemoteException{
    if (vM_Order_PortType == null)
      _initVM_OrderProxy();
    return vM_Order_PortType.deleteCouponCode(parameters);
  }
  
  public it.netgrid.virtuemart.soap.orders.ShippingRate[] getAllShippingRate(it.netgrid.virtuemart.soap.orders.LoginInfo parameters) throws java.rmi.RemoteException{
    if (vM_Order_PortType == null)
      _initVM_OrderProxy();
    return vM_Order_PortType.getAllShippingRate(parameters);
  }
  
  public it.netgrid.virtuemart.soap.orders.ShippingMethod[] getAllShippingMethod(it.netgrid.virtuemart.soap.orders.LoginInfo parameters) throws java.rmi.RemoteException{
    if (vM_Order_PortType == null)
      _initVM_OrderProxy();
    return vM_Order_PortType.getAllShippingMethod(parameters);
  }
  
  public it.netgrid.virtuemart.soap.orders.CommonReturn addShippingRate(it.netgrid.virtuemart.soap.orders.AddShippingRatesInput parameters) throws java.rmi.RemoteException{
    if (vM_Order_PortType == null)
      _initVM_OrderProxy();
    return vM_Order_PortType.addShippingRate(parameters);
  }
  
  public it.netgrid.virtuemart.soap.orders.CommonReturn addShippingMethod(it.netgrid.virtuemart.soap.orders.AddShippingMethodsInput parameters) throws java.rmi.RemoteException{
    if (vM_Order_PortType == null)
      _initVM_OrderProxy();
    return vM_Order_PortType.addShippingMethod(parameters);
  }
  
  public it.netgrid.virtuemart.soap.orders.CommonReturn deleteShippingRate(it.netgrid.virtuemart.soap.orders.DelInput parameters) throws java.rmi.RemoteException{
    if (vM_Order_PortType == null)
      _initVM_OrderProxy();
    return vM_Order_PortType.deleteShippingRate(parameters);
  }
  
  public it.netgrid.virtuemart.soap.orders.CommonReturn deleteShippingMethod(it.netgrid.virtuemart.soap.orders.DelInput parameters) throws java.rmi.RemoteException{
    if (vM_Order_PortType == null)
      _initVM_OrderProxy();
    return vM_Order_PortType.deleteShippingMethod(parameters);
  }
  
  public it.netgrid.virtuemart.soap.orders.PaymentMethod[] getAllPaymentMethod(it.netgrid.virtuemart.soap.orders.GetPaymentMethodInput parameters) throws java.rmi.RemoteException{
    if (vM_Order_PortType == null)
      _initVM_OrderProxy();
    return vM_Order_PortType.getAllPaymentMethod(parameters);
  }
  
  public it.netgrid.virtuemart.soap.orders.CommonReturn addPaymentMethod(it.netgrid.virtuemart.soap.orders.AddPaymentMethodInput parameters) throws java.rmi.RemoteException{
    if (vM_Order_PortType == null)
      _initVM_OrderProxy();
    return vM_Order_PortType.addPaymentMethod(parameters);
  }
  
  public it.netgrid.virtuemart.soap.orders.CommonReturn updatePaymentMethod(it.netgrid.virtuemart.soap.orders.AddPaymentMethodInput parameters) throws java.rmi.RemoteException{
    if (vM_Order_PortType == null)
      _initVM_OrderProxy();
    return vM_Order_PortType.updatePaymentMethod(parameters);
  }
  
  public it.netgrid.virtuemart.soap.orders.CommonReturn deletePaymentMethod(it.netgrid.virtuemart.soap.orders.DelInput parameters) throws java.rmi.RemoteException{
    if (vM_Order_PortType == null)
      _initVM_OrderProxy();
    return vM_Order_PortType.deletePaymentMethod(parameters);
  }
  
  public it.netgrid.virtuemart.soap.orders.Order[] getOrderFromDate(it.netgrid.virtuemart.soap.orders.GetOrderFromDateInput parameters) throws java.rmi.RemoteException{
    if (vM_Order_PortType == null)
      _initVM_OrderProxy();
    return vM_Order_PortType.getOrderFromDate(parameters);
  }
  
  public it.netgrid.virtuemart.soap.orders.Creditcard[] getAllCreditCard(it.netgrid.virtuemart.soap.orders.LoginInfo parameters) throws java.rmi.RemoteException{
    if (vM_Order_PortType == null)
      _initVM_OrderProxy();
    return vM_Order_PortType.getAllCreditCard(parameters);
  }
  
  public it.netgrid.virtuemart.soap.orders.CommonReturn addCreditCard(it.netgrid.virtuemart.soap.orders.AddCreditCardInput parameters) throws java.rmi.RemoteException{
    if (vM_Order_PortType == null)
      _initVM_OrderProxy();
    return vM_Order_PortType.addCreditCard(parameters);
  }
  
  public it.netgrid.virtuemart.soap.orders.CommonReturn updateCreditCard(it.netgrid.virtuemart.soap.orders.AddCreditCardInput parameters) throws java.rmi.RemoteException{
    if (vM_Order_PortType == null)
      _initVM_OrderProxy();
    return vM_Order_PortType.updateCreditCard(parameters);
  }
  
  public it.netgrid.virtuemart.soap.orders.CommonReturn deleteCreditCard(it.netgrid.virtuemart.soap.orders.DelCreditCardInput parameters) throws java.rmi.RemoteException{
    if (vM_Order_PortType == null)
      _initVM_OrderProxy();
    return vM_Order_PortType.deleteCreditCard(parameters);
  }
  
  public it.netgrid.virtuemart.soap.orders.CommonReturn addOrderStatusCode(it.netgrid.virtuemart.soap.orders.AddStatusInput parameters) throws java.rmi.RemoteException{
    if (vM_Order_PortType == null)
      _initVM_OrderProxy();
    return vM_Order_PortType.addOrderStatusCode(parameters);
  }
  
  public it.netgrid.virtuemart.soap.orders.CommonReturn updateOrderStatusCode(it.netgrid.virtuemart.soap.orders.AddStatusInput parameters) throws java.rmi.RemoteException{
    if (vM_Order_PortType == null)
      _initVM_OrderProxy();
    return vM_Order_PortType.updateOrderStatusCode(parameters);
  }
  
  public it.netgrid.virtuemart.soap.orders.CommonReturn deleteOrderStatusCode(it.netgrid.virtuemart.soap.orders.DeleteStatusInput parameters) throws java.rmi.RemoteException{
    if (vM_Order_PortType == null)
      _initVM_OrderProxy();
    return vM_Order_PortType.deleteOrderStatusCode(parameters);
  }
  
  public it.netgrid.virtuemart.soap.orders.CommonReturn updateShippingMethod(it.netgrid.virtuemart.soap.orders.AddShippingMethodsInput parameters) throws java.rmi.RemoteException{
    if (vM_Order_PortType == null)
      _initVM_OrderProxy();
    return vM_Order_PortType.updateShippingMethod(parameters);
  }
  
  public it.netgrid.virtuemart.soap.orders.CommonReturn updateShippingRate(it.netgrid.virtuemart.soap.orders.AddShippingRatesInput parameters) throws java.rmi.RemoteException{
    if (vM_Order_PortType == null)
      _initVM_OrderProxy();
    return vM_Order_PortType.updateShippingRate(parameters);
  }
  
  public it.netgrid.virtuemart.soap.orders.PaymentMethod getOrderPaymentInfo(it.netgrid.virtuemart.soap.orders.GetOrderPaymentInfoInput parameters) throws java.rmi.RemoteException{
    if (vM_Order_PortType == null)
      _initVM_OrderProxy();
    return vM_Order_PortType.getOrderPaymentInfo(parameters);
  }
  
  public it.netgrid.virtuemart.soap.orders.CommonReturn changeOrderPayment(it.netgrid.virtuemart.soap.orders.ChangeOrderPaymentInput parameters) throws java.rmi.RemoteException{
    if (vM_Order_PortType == null)
      _initVM_OrderProxy();
    return vM_Order_PortType.changeOrderPayment(parameters);
  }
  
  public it.netgrid.virtuemart.soap.orders.CommonReturn changeProductItemPrice(it.netgrid.virtuemart.soap.orders.ChangeProductItemPriceInput parameters) throws java.rmi.RemoteException{
    if (vM_Order_PortType == null)
      _initVM_OrderProxy();
    return vM_Order_PortType.changeProductItemPrice(parameters);
  }
  
  public it.netgrid.virtuemart.soap.orders.CommonReturn changeItemQuantity(it.netgrid.virtuemart.soap.orders.ChangeItemQuantityInput parameters) throws java.rmi.RemoteException{
    if (vM_Order_PortType == null)
      _initVM_OrderProxy();
    return vM_Order_PortType.changeItemQuantity(parameters);
  }
  
  public it.netgrid.virtuemart.soap.orders.CommonReturn addOrderItem(it.netgrid.virtuemart.soap.orders.AddOrderItemInput parameters) throws java.rmi.RemoteException{
    if (vM_Order_PortType == null)
      _initVM_OrderProxy();
    return vM_Order_PortType.addOrderItem(parameters);
  }
  
  public it.netgrid.virtuemart.soap.orders.CommonReturn deleteOrderItem(it.netgrid.virtuemart.soap.orders.DeleteOrderItemInput parameters) throws java.rmi.RemoteException{
    if (vM_Order_PortType == null)
      _initVM_OrderProxy();
    return vM_Order_PortType.deleteOrderItem(parameters);
  }
  
  public it.netgrid.virtuemart.soap.orders.CommonReturn changeOrderCouponDiscount(it.netgrid.virtuemart.soap.orders.ChangeOrderCouponDiscountInput parameters) throws java.rmi.RemoteException{
    if (vM_Order_PortType == null)
      _initVM_OrderProxy();
    return vM_Order_PortType.changeOrderCouponDiscount(parameters);
  }
  
  public it.netgrid.virtuemart.soap.orders.CommonReturn changeOrderShipping(it.netgrid.virtuemart.soap.orders.ChangeOrderShippingInput parameters) throws java.rmi.RemoteException{
    if (vM_Order_PortType == null)
      _initVM_OrderProxy();
    return vM_Order_PortType.changeOrderShipping(parameters);
  }
  
  public it.netgrid.virtuemart.soap.orders.CommonReturn changeOrderCustomerNote(it.netgrid.virtuemart.soap.orders.ChangeOrderCustomerNoteInput parameters) throws java.rmi.RemoteException{
    if (vM_Order_PortType == null)
      _initVM_OrderProxy();
    return vM_Order_PortType.changeOrderCustomerNote(parameters);
  }
  
  public it.netgrid.virtuemart.soap.orders.CommonReturn changeOrderShipTo(it.netgrid.virtuemart.soap.orders.ChangeOrderShipToInput parameters) throws java.rmi.RemoteException{
    if (vM_Order_PortType == null)
      _initVM_OrderProxy();
    return vM_Order_PortType.changeOrderShipTo(parameters);
  }
  
  public it.netgrid.virtuemart.soap.orders.CommonReturn changeOrderBillTo(it.netgrid.virtuemart.soap.orders.ChangeOrderBillToInput parameters) throws java.rmi.RemoteException{
    if (vM_Order_PortType == null)
      _initVM_OrderProxy();
    return vM_Order_PortType.changeOrderBillTo(parameters);
  }
  
  public it.netgrid.virtuemart.soap.orders.Plugin[] getPluginsInfo(it.netgrid.virtuemart.soap.orders.GetPluginsInfoInput parameters) throws java.rmi.RemoteException{
    if (vM_Order_PortType == null)
      _initVM_OrderProxy();
    return vM_Order_PortType.getPluginsInfo(parameters);
  }
  
  
}