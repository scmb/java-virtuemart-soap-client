/**
 * ShippingMethod.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package it.netgrid.virtuemart.soap.orders;

public class ShippingMethod  implements java.io.Serializable {
    private java.lang.String virtuemart_shipmentmethod_id;

    private java.lang.String virtuemart_vendor_id;

    private java.lang.String shipment_jplugin_id;

    private java.lang.String slug;

    private java.lang.String shipment_element;

    private java.lang.String shipment_params;

    private java.lang.String shipment_name;

    private java.lang.String shipment_desc;

    private java.lang.String virtuemart_shoppergroup_id;

    private java.lang.String ordering;

    private java.lang.String shared;

    private java.lang.String published;

    public ShippingMethod() {
    }

    public ShippingMethod(
           java.lang.String virtuemart_shipmentmethod_id,
           java.lang.String virtuemart_vendor_id,
           java.lang.String shipment_jplugin_id,
           java.lang.String slug,
           java.lang.String shipment_element,
           java.lang.String shipment_params,
           java.lang.String shipment_name,
           java.lang.String shipment_desc,
           java.lang.String virtuemart_shoppergroup_id,
           java.lang.String ordering,
           java.lang.String shared,
           java.lang.String published) {
           this.virtuemart_shipmentmethod_id = virtuemart_shipmentmethod_id;
           this.virtuemart_vendor_id = virtuemart_vendor_id;
           this.shipment_jplugin_id = shipment_jplugin_id;
           this.slug = slug;
           this.shipment_element = shipment_element;
           this.shipment_params = shipment_params;
           this.shipment_name = shipment_name;
           this.shipment_desc = shipment_desc;
           this.virtuemart_shoppergroup_id = virtuemart_shoppergroup_id;
           this.ordering = ordering;
           this.shared = shared;
           this.published = published;
    }


    /**
     * Gets the virtuemart_shipmentmethod_id value for this ShippingMethod.
     * 
     * @return virtuemart_shipmentmethod_id
     */
    public java.lang.String getVirtuemart_shipmentmethod_id() {
        return virtuemart_shipmentmethod_id;
    }


    /**
     * Sets the virtuemart_shipmentmethod_id value for this ShippingMethod.
     * 
     * @param virtuemart_shipmentmethod_id
     */
    public void setVirtuemart_shipmentmethod_id(java.lang.String virtuemart_shipmentmethod_id) {
        this.virtuemart_shipmentmethod_id = virtuemart_shipmentmethod_id;
    }


    /**
     * Gets the virtuemart_vendor_id value for this ShippingMethod.
     * 
     * @return virtuemart_vendor_id
     */
    public java.lang.String getVirtuemart_vendor_id() {
        return virtuemart_vendor_id;
    }


    /**
     * Sets the virtuemart_vendor_id value for this ShippingMethod.
     * 
     * @param virtuemart_vendor_id
     */
    public void setVirtuemart_vendor_id(java.lang.String virtuemart_vendor_id) {
        this.virtuemart_vendor_id = virtuemart_vendor_id;
    }


    /**
     * Gets the shipment_jplugin_id value for this ShippingMethod.
     * 
     * @return shipment_jplugin_id
     */
    public java.lang.String getShipment_jplugin_id() {
        return shipment_jplugin_id;
    }


    /**
     * Sets the shipment_jplugin_id value for this ShippingMethod.
     * 
     * @param shipment_jplugin_id
     */
    public void setShipment_jplugin_id(java.lang.String shipment_jplugin_id) {
        this.shipment_jplugin_id = shipment_jplugin_id;
    }


    /**
     * Gets the slug value for this ShippingMethod.
     * 
     * @return slug
     */
    public java.lang.String getSlug() {
        return slug;
    }


    /**
     * Sets the slug value for this ShippingMethod.
     * 
     * @param slug
     */
    public void setSlug(java.lang.String slug) {
        this.slug = slug;
    }


    /**
     * Gets the shipment_element value for this ShippingMethod.
     * 
     * @return shipment_element
     */
    public java.lang.String getShipment_element() {
        return shipment_element;
    }


    /**
     * Sets the shipment_element value for this ShippingMethod.
     * 
     * @param shipment_element
     */
    public void setShipment_element(java.lang.String shipment_element) {
        this.shipment_element = shipment_element;
    }


    /**
     * Gets the shipment_params value for this ShippingMethod.
     * 
     * @return shipment_params
     */
    public java.lang.String getShipment_params() {
        return shipment_params;
    }


    /**
     * Sets the shipment_params value for this ShippingMethod.
     * 
     * @param shipment_params
     */
    public void setShipment_params(java.lang.String shipment_params) {
        this.shipment_params = shipment_params;
    }


    /**
     * Gets the shipment_name value for this ShippingMethod.
     * 
     * @return shipment_name
     */
    public java.lang.String getShipment_name() {
        return shipment_name;
    }


    /**
     * Sets the shipment_name value for this ShippingMethod.
     * 
     * @param shipment_name
     */
    public void setShipment_name(java.lang.String shipment_name) {
        this.shipment_name = shipment_name;
    }


    /**
     * Gets the shipment_desc value for this ShippingMethod.
     * 
     * @return shipment_desc
     */
    public java.lang.String getShipment_desc() {
        return shipment_desc;
    }


    /**
     * Sets the shipment_desc value for this ShippingMethod.
     * 
     * @param shipment_desc
     */
    public void setShipment_desc(java.lang.String shipment_desc) {
        this.shipment_desc = shipment_desc;
    }


    /**
     * Gets the virtuemart_shoppergroup_id value for this ShippingMethod.
     * 
     * @return virtuemart_shoppergroup_id
     */
    public java.lang.String getVirtuemart_shoppergroup_id() {
        return virtuemart_shoppergroup_id;
    }


    /**
     * Sets the virtuemart_shoppergroup_id value for this ShippingMethod.
     * 
     * @param virtuemart_shoppergroup_id
     */
    public void setVirtuemart_shoppergroup_id(java.lang.String virtuemart_shoppergroup_id) {
        this.virtuemart_shoppergroup_id = virtuemart_shoppergroup_id;
    }


    /**
     * Gets the ordering value for this ShippingMethod.
     * 
     * @return ordering
     */
    public java.lang.String getOrdering() {
        return ordering;
    }


    /**
     * Sets the ordering value for this ShippingMethod.
     * 
     * @param ordering
     */
    public void setOrdering(java.lang.String ordering) {
        this.ordering = ordering;
    }


    /**
     * Gets the shared value for this ShippingMethod.
     * 
     * @return shared
     */
    public java.lang.String getShared() {
        return shared;
    }


    /**
     * Sets the shared value for this ShippingMethod.
     * 
     * @param shared
     */
    public void setShared(java.lang.String shared) {
        this.shared = shared;
    }


    /**
     * Gets the published value for this ShippingMethod.
     * 
     * @return published
     */
    public java.lang.String getPublished() {
        return published;
    }


    /**
     * Sets the published value for this ShippingMethod.
     * 
     * @param published
     */
    public void setPublished(java.lang.String published) {
        this.published = published;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof ShippingMethod)) return false;
        ShippingMethod other = (ShippingMethod) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.virtuemart_shipmentmethod_id==null && other.getVirtuemart_shipmentmethod_id()==null) || 
             (this.virtuemart_shipmentmethod_id!=null &&
              this.virtuemart_shipmentmethod_id.equals(other.getVirtuemart_shipmentmethod_id()))) &&
            ((this.virtuemart_vendor_id==null && other.getVirtuemart_vendor_id()==null) || 
             (this.virtuemart_vendor_id!=null &&
              this.virtuemart_vendor_id.equals(other.getVirtuemart_vendor_id()))) &&
            ((this.shipment_jplugin_id==null && other.getShipment_jplugin_id()==null) || 
             (this.shipment_jplugin_id!=null &&
              this.shipment_jplugin_id.equals(other.getShipment_jplugin_id()))) &&
            ((this.slug==null && other.getSlug()==null) || 
             (this.slug!=null &&
              this.slug.equals(other.getSlug()))) &&
            ((this.shipment_element==null && other.getShipment_element()==null) || 
             (this.shipment_element!=null &&
              this.shipment_element.equals(other.getShipment_element()))) &&
            ((this.shipment_params==null && other.getShipment_params()==null) || 
             (this.shipment_params!=null &&
              this.shipment_params.equals(other.getShipment_params()))) &&
            ((this.shipment_name==null && other.getShipment_name()==null) || 
             (this.shipment_name!=null &&
              this.shipment_name.equals(other.getShipment_name()))) &&
            ((this.shipment_desc==null && other.getShipment_desc()==null) || 
             (this.shipment_desc!=null &&
              this.shipment_desc.equals(other.getShipment_desc()))) &&
            ((this.virtuemart_shoppergroup_id==null && other.getVirtuemart_shoppergroup_id()==null) || 
             (this.virtuemart_shoppergroup_id!=null &&
              this.virtuemart_shoppergroup_id.equals(other.getVirtuemart_shoppergroup_id()))) &&
            ((this.ordering==null && other.getOrdering()==null) || 
             (this.ordering!=null &&
              this.ordering.equals(other.getOrdering()))) &&
            ((this.shared==null && other.getShared()==null) || 
             (this.shared!=null &&
              this.shared.equals(other.getShared()))) &&
            ((this.published==null && other.getPublished()==null) || 
             (this.published!=null &&
              this.published.equals(other.getPublished())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getVirtuemart_shipmentmethod_id() != null) {
            _hashCode += getVirtuemart_shipmentmethod_id().hashCode();
        }
        if (getVirtuemart_vendor_id() != null) {
            _hashCode += getVirtuemart_vendor_id().hashCode();
        }
        if (getShipment_jplugin_id() != null) {
            _hashCode += getShipment_jplugin_id().hashCode();
        }
        if (getSlug() != null) {
            _hashCode += getSlug().hashCode();
        }
        if (getShipment_element() != null) {
            _hashCode += getShipment_element().hashCode();
        }
        if (getShipment_params() != null) {
            _hashCode += getShipment_params().hashCode();
        }
        if (getShipment_name() != null) {
            _hashCode += getShipment_name().hashCode();
        }
        if (getShipment_desc() != null) {
            _hashCode += getShipment_desc().hashCode();
        }
        if (getVirtuemart_shoppergroup_id() != null) {
            _hashCode += getVirtuemart_shoppergroup_id().hashCode();
        }
        if (getOrdering() != null) {
            _hashCode += getOrdering().hashCode();
        }
        if (getShared() != null) {
            _hashCode += getShared().hashCode();
        }
        if (getPublished() != null) {
            _hashCode += getPublished().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(ShippingMethod.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://www.virtuemart.net/VM_Order/", "ShippingMethod"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("virtuemart_shipmentmethod_id");
        elemField.setXmlName(new javax.xml.namespace.QName("", "virtuemart_shipmentmethod_id"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("virtuemart_vendor_id");
        elemField.setXmlName(new javax.xml.namespace.QName("", "virtuemart_vendor_id"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("shipment_jplugin_id");
        elemField.setXmlName(new javax.xml.namespace.QName("", "shipment_jplugin_id"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("slug");
        elemField.setXmlName(new javax.xml.namespace.QName("", "slug"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("shipment_element");
        elemField.setXmlName(new javax.xml.namespace.QName("", "shipment_element"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("shipment_params");
        elemField.setXmlName(new javax.xml.namespace.QName("", "shipment_params"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("shipment_name");
        elemField.setXmlName(new javax.xml.namespace.QName("", "shipment_name"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("shipment_desc");
        elemField.setXmlName(new javax.xml.namespace.QName("", "shipment_desc"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("virtuemart_shoppergroup_id");
        elemField.setXmlName(new javax.xml.namespace.QName("", "virtuemart_shoppergroup_id"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("ordering");
        elemField.setXmlName(new javax.xml.namespace.QName("", "ordering"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("shared");
        elemField.setXmlName(new javax.xml.namespace.QName("", "shared"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("published");
        elemField.setXmlName(new javax.xml.namespace.QName("", "published"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
