/**
 * ChangeProductItemPriceInput.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package it.netgrid.virtuemart.soap.orders;

public class ChangeProductItemPriceInput  implements java.io.Serializable {
    private it.netgrid.virtuemart.soap.orders.LoginInfo loginInfo;

    private java.lang.String order_id;

    private java.lang.String order_item_id;

    private java.lang.String product_item_price;

    private java.lang.String product_final_price;

    public ChangeProductItemPriceInput() {
    }

    public ChangeProductItemPriceInput(
           it.netgrid.virtuemart.soap.orders.LoginInfo loginInfo,
           java.lang.String order_id,
           java.lang.String order_item_id,
           java.lang.String product_item_price,
           java.lang.String product_final_price) {
           this.loginInfo = loginInfo;
           this.order_id = order_id;
           this.order_item_id = order_item_id;
           this.product_item_price = product_item_price;
           this.product_final_price = product_final_price;
    }


    /**
     * Gets the loginInfo value for this ChangeProductItemPriceInput.
     * 
     * @return loginInfo
     */
    public it.netgrid.virtuemart.soap.orders.LoginInfo getLoginInfo() {
        return loginInfo;
    }


    /**
     * Sets the loginInfo value for this ChangeProductItemPriceInput.
     * 
     * @param loginInfo
     */
    public void setLoginInfo(it.netgrid.virtuemart.soap.orders.LoginInfo loginInfo) {
        this.loginInfo = loginInfo;
    }


    /**
     * Gets the order_id value for this ChangeProductItemPriceInput.
     * 
     * @return order_id
     */
    public java.lang.String getOrder_id() {
        return order_id;
    }


    /**
     * Sets the order_id value for this ChangeProductItemPriceInput.
     * 
     * @param order_id
     */
    public void setOrder_id(java.lang.String order_id) {
        this.order_id = order_id;
    }


    /**
     * Gets the order_item_id value for this ChangeProductItemPriceInput.
     * 
     * @return order_item_id
     */
    public java.lang.String getOrder_item_id() {
        return order_item_id;
    }


    /**
     * Sets the order_item_id value for this ChangeProductItemPriceInput.
     * 
     * @param order_item_id
     */
    public void setOrder_item_id(java.lang.String order_item_id) {
        this.order_item_id = order_item_id;
    }


    /**
     * Gets the product_item_price value for this ChangeProductItemPriceInput.
     * 
     * @return product_item_price
     */
    public java.lang.String getProduct_item_price() {
        return product_item_price;
    }


    /**
     * Sets the product_item_price value for this ChangeProductItemPriceInput.
     * 
     * @param product_item_price
     */
    public void setProduct_item_price(java.lang.String product_item_price) {
        this.product_item_price = product_item_price;
    }


    /**
     * Gets the product_final_price value for this ChangeProductItemPriceInput.
     * 
     * @return product_final_price
     */
    public java.lang.String getProduct_final_price() {
        return product_final_price;
    }


    /**
     * Sets the product_final_price value for this ChangeProductItemPriceInput.
     * 
     * @param product_final_price
     */
    public void setProduct_final_price(java.lang.String product_final_price) {
        this.product_final_price = product_final_price;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof ChangeProductItemPriceInput)) return false;
        ChangeProductItemPriceInput other = (ChangeProductItemPriceInput) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.loginInfo==null && other.getLoginInfo()==null) || 
             (this.loginInfo!=null &&
              this.loginInfo.equals(other.getLoginInfo()))) &&
            ((this.order_id==null && other.getOrder_id()==null) || 
             (this.order_id!=null &&
              this.order_id.equals(other.getOrder_id()))) &&
            ((this.order_item_id==null && other.getOrder_item_id()==null) || 
             (this.order_item_id!=null &&
              this.order_item_id.equals(other.getOrder_item_id()))) &&
            ((this.product_item_price==null && other.getProduct_item_price()==null) || 
             (this.product_item_price!=null &&
              this.product_item_price.equals(other.getProduct_item_price()))) &&
            ((this.product_final_price==null && other.getProduct_final_price()==null) || 
             (this.product_final_price!=null &&
              this.product_final_price.equals(other.getProduct_final_price())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getLoginInfo() != null) {
            _hashCode += getLoginInfo().hashCode();
        }
        if (getOrder_id() != null) {
            _hashCode += getOrder_id().hashCode();
        }
        if (getOrder_item_id() != null) {
            _hashCode += getOrder_item_id().hashCode();
        }
        if (getProduct_item_price() != null) {
            _hashCode += getProduct_item_price().hashCode();
        }
        if (getProduct_final_price() != null) {
            _hashCode += getProduct_final_price().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(ChangeProductItemPriceInput.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://www.virtuemart.net/VM_Order/", "ChangeProductItemPriceInput"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("loginInfo");
        elemField.setXmlName(new javax.xml.namespace.QName("", "loginInfo"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.virtuemart.net/VM_Order/", "loginInfo"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("order_id");
        elemField.setXmlName(new javax.xml.namespace.QName("", "order_id"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("order_item_id");
        elemField.setXmlName(new javax.xml.namespace.QName("", "order_item_id"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("product_item_price");
        elemField.setXmlName(new javax.xml.namespace.QName("", "product_item_price"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("product_final_price");
        elemField.setXmlName(new javax.xml.namespace.QName("", "product_final_price"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
