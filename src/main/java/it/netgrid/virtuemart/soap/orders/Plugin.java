/**
 * Plugin.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package it.netgrid.virtuemart.soap.orders;

public class Plugin  implements java.io.Serializable {
    private java.lang.String extension_id;

    private java.lang.String name;

    private java.lang.String type;

    private java.lang.String element;

    private java.lang.String folder;

    private java.lang.String client_id;

    private java.lang.String enabled;

    private java.lang.String access;

    private java.lang.String _protected;

    private java.lang.String manifest_cache;

    private java.lang.String params;

    private java.lang.String custom_data;

    private java.lang.String system_data;

    private java.lang.String ordering;

    private java.lang.String state;

    public Plugin() {
    }

    public Plugin(
           java.lang.String extension_id,
           java.lang.String name,
           java.lang.String type,
           java.lang.String element,
           java.lang.String folder,
           java.lang.String client_id,
           java.lang.String enabled,
           java.lang.String access,
           java.lang.String _protected,
           java.lang.String manifest_cache,
           java.lang.String params,
           java.lang.String custom_data,
           java.lang.String system_data,
           java.lang.String ordering,
           java.lang.String state) {
           this.extension_id = extension_id;
           this.name = name;
           this.type = type;
           this.element = element;
           this.folder = folder;
           this.client_id = client_id;
           this.enabled = enabled;
           this.access = access;
           this._protected = _protected;
           this.manifest_cache = manifest_cache;
           this.params = params;
           this.custom_data = custom_data;
           this.system_data = system_data;
           this.ordering = ordering;
           this.state = state;
    }


    /**
     * Gets the extension_id value for this Plugin.
     * 
     * @return extension_id
     */
    public java.lang.String getExtension_id() {
        return extension_id;
    }


    /**
     * Sets the extension_id value for this Plugin.
     * 
     * @param extension_id
     */
    public void setExtension_id(java.lang.String extension_id) {
        this.extension_id = extension_id;
    }


    /**
     * Gets the name value for this Plugin.
     * 
     * @return name
     */
    public java.lang.String getName() {
        return name;
    }


    /**
     * Sets the name value for this Plugin.
     * 
     * @param name
     */
    public void setName(java.lang.String name) {
        this.name = name;
    }


    /**
     * Gets the type value for this Plugin.
     * 
     * @return type
     */
    public java.lang.String getType() {
        return type;
    }


    /**
     * Sets the type value for this Plugin.
     * 
     * @param type
     */
    public void setType(java.lang.String type) {
        this.type = type;
    }


    /**
     * Gets the element value for this Plugin.
     * 
     * @return element
     */
    public java.lang.String getElement() {
        return element;
    }


    /**
     * Sets the element value for this Plugin.
     * 
     * @param element
     */
    public void setElement(java.lang.String element) {
        this.element = element;
    }


    /**
     * Gets the folder value for this Plugin.
     * 
     * @return folder
     */
    public java.lang.String getFolder() {
        return folder;
    }


    /**
     * Sets the folder value for this Plugin.
     * 
     * @param folder
     */
    public void setFolder(java.lang.String folder) {
        this.folder = folder;
    }


    /**
     * Gets the client_id value for this Plugin.
     * 
     * @return client_id
     */
    public java.lang.String getClient_id() {
        return client_id;
    }


    /**
     * Sets the client_id value for this Plugin.
     * 
     * @param client_id
     */
    public void setClient_id(java.lang.String client_id) {
        this.client_id = client_id;
    }


    /**
     * Gets the enabled value for this Plugin.
     * 
     * @return enabled
     */
    public java.lang.String getEnabled() {
        return enabled;
    }


    /**
     * Sets the enabled value for this Plugin.
     * 
     * @param enabled
     */
    public void setEnabled(java.lang.String enabled) {
        this.enabled = enabled;
    }


    /**
     * Gets the access value for this Plugin.
     * 
     * @return access
     */
    public java.lang.String getAccess() {
        return access;
    }


    /**
     * Sets the access value for this Plugin.
     * 
     * @param access
     */
    public void setAccess(java.lang.String access) {
        this.access = access;
    }


    /**
     * Gets the _protected value for this Plugin.
     * 
     * @return _protected
     */
    public java.lang.String get_protected() {
        return _protected;
    }


    /**
     * Sets the _protected value for this Plugin.
     * 
     * @param _protected
     */
    public void set_protected(java.lang.String _protected) {
        this._protected = _protected;
    }


    /**
     * Gets the manifest_cache value for this Plugin.
     * 
     * @return manifest_cache
     */
    public java.lang.String getManifest_cache() {
        return manifest_cache;
    }


    /**
     * Sets the manifest_cache value for this Plugin.
     * 
     * @param manifest_cache
     */
    public void setManifest_cache(java.lang.String manifest_cache) {
        this.manifest_cache = manifest_cache;
    }


    /**
     * Gets the params value for this Plugin.
     * 
     * @return params
     */
    public java.lang.String getParams() {
        return params;
    }


    /**
     * Sets the params value for this Plugin.
     * 
     * @param params
     */
    public void setParams(java.lang.String params) {
        this.params = params;
    }


    /**
     * Gets the custom_data value for this Plugin.
     * 
     * @return custom_data
     */
    public java.lang.String getCustom_data() {
        return custom_data;
    }


    /**
     * Sets the custom_data value for this Plugin.
     * 
     * @param custom_data
     */
    public void setCustom_data(java.lang.String custom_data) {
        this.custom_data = custom_data;
    }


    /**
     * Gets the system_data value for this Plugin.
     * 
     * @return system_data
     */
    public java.lang.String getSystem_data() {
        return system_data;
    }


    /**
     * Sets the system_data value for this Plugin.
     * 
     * @param system_data
     */
    public void setSystem_data(java.lang.String system_data) {
        this.system_data = system_data;
    }


    /**
     * Gets the ordering value for this Plugin.
     * 
     * @return ordering
     */
    public java.lang.String getOrdering() {
        return ordering;
    }


    /**
     * Sets the ordering value for this Plugin.
     * 
     * @param ordering
     */
    public void setOrdering(java.lang.String ordering) {
        this.ordering = ordering;
    }


    /**
     * Gets the state value for this Plugin.
     * 
     * @return state
     */
    public java.lang.String getState() {
        return state;
    }


    /**
     * Sets the state value for this Plugin.
     * 
     * @param state
     */
    public void setState(java.lang.String state) {
        this.state = state;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof Plugin)) return false;
        Plugin other = (Plugin) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.extension_id==null && other.getExtension_id()==null) || 
             (this.extension_id!=null &&
              this.extension_id.equals(other.getExtension_id()))) &&
            ((this.name==null && other.getName()==null) || 
             (this.name!=null &&
              this.name.equals(other.getName()))) &&
            ((this.type==null && other.getType()==null) || 
             (this.type!=null &&
              this.type.equals(other.getType()))) &&
            ((this.element==null && other.getElement()==null) || 
             (this.element!=null &&
              this.element.equals(other.getElement()))) &&
            ((this.folder==null && other.getFolder()==null) || 
             (this.folder!=null &&
              this.folder.equals(other.getFolder()))) &&
            ((this.client_id==null && other.getClient_id()==null) || 
             (this.client_id!=null &&
              this.client_id.equals(other.getClient_id()))) &&
            ((this.enabled==null && other.getEnabled()==null) || 
             (this.enabled!=null &&
              this.enabled.equals(other.getEnabled()))) &&
            ((this.access==null && other.getAccess()==null) || 
             (this.access!=null &&
              this.access.equals(other.getAccess()))) &&
            ((this._protected==null && other.get_protected()==null) || 
             (this._protected!=null &&
              this._protected.equals(other.get_protected()))) &&
            ((this.manifest_cache==null && other.getManifest_cache()==null) || 
             (this.manifest_cache!=null &&
              this.manifest_cache.equals(other.getManifest_cache()))) &&
            ((this.params==null && other.getParams()==null) || 
             (this.params!=null &&
              this.params.equals(other.getParams()))) &&
            ((this.custom_data==null && other.getCustom_data()==null) || 
             (this.custom_data!=null &&
              this.custom_data.equals(other.getCustom_data()))) &&
            ((this.system_data==null && other.getSystem_data()==null) || 
             (this.system_data!=null &&
              this.system_data.equals(other.getSystem_data()))) &&
            ((this.ordering==null && other.getOrdering()==null) || 
             (this.ordering!=null &&
              this.ordering.equals(other.getOrdering()))) &&
            ((this.state==null && other.getState()==null) || 
             (this.state!=null &&
              this.state.equals(other.getState())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getExtension_id() != null) {
            _hashCode += getExtension_id().hashCode();
        }
        if (getName() != null) {
            _hashCode += getName().hashCode();
        }
        if (getType() != null) {
            _hashCode += getType().hashCode();
        }
        if (getElement() != null) {
            _hashCode += getElement().hashCode();
        }
        if (getFolder() != null) {
            _hashCode += getFolder().hashCode();
        }
        if (getClient_id() != null) {
            _hashCode += getClient_id().hashCode();
        }
        if (getEnabled() != null) {
            _hashCode += getEnabled().hashCode();
        }
        if (getAccess() != null) {
            _hashCode += getAccess().hashCode();
        }
        if (get_protected() != null) {
            _hashCode += get_protected().hashCode();
        }
        if (getManifest_cache() != null) {
            _hashCode += getManifest_cache().hashCode();
        }
        if (getParams() != null) {
            _hashCode += getParams().hashCode();
        }
        if (getCustom_data() != null) {
            _hashCode += getCustom_data().hashCode();
        }
        if (getSystem_data() != null) {
            _hashCode += getSystem_data().hashCode();
        }
        if (getOrdering() != null) {
            _hashCode += getOrdering().hashCode();
        }
        if (getState() != null) {
            _hashCode += getState().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(Plugin.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://www.virtuemart.net/VM_Order/", "Plugin"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("extension_id");
        elemField.setXmlName(new javax.xml.namespace.QName("", "extension_id"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("name");
        elemField.setXmlName(new javax.xml.namespace.QName("", "name"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("type");
        elemField.setXmlName(new javax.xml.namespace.QName("", "type"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("element");
        elemField.setXmlName(new javax.xml.namespace.QName("", "element"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("folder");
        elemField.setXmlName(new javax.xml.namespace.QName("", "folder"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("client_id");
        elemField.setXmlName(new javax.xml.namespace.QName("", "client_id"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("enabled");
        elemField.setXmlName(new javax.xml.namespace.QName("", "enabled"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("access");
        elemField.setXmlName(new javax.xml.namespace.QName("", "access"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("_protected");
        elemField.setXmlName(new javax.xml.namespace.QName("", "protected"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("manifest_cache");
        elemField.setXmlName(new javax.xml.namespace.QName("", "manifest_cache"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("params");
        elemField.setXmlName(new javax.xml.namespace.QName("", "params"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("custom_data");
        elemField.setXmlName(new javax.xml.namespace.QName("", "custom_data"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("system_data");
        elemField.setXmlName(new javax.xml.namespace.QName("", "system_data"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("ordering");
        elemField.setXmlName(new javax.xml.namespace.QName("", "ordering"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("state");
        elemField.setXmlName(new javax.xml.namespace.QName("", "state"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
