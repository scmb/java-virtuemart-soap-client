/**
 * ChangeOrderShippingInput.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package it.netgrid.virtuemart.soap.orders;

public class ChangeOrderShippingInput  implements java.io.Serializable {
    private it.netgrid.virtuemart.soap.orders.LoginInfo loginInfo;

    private java.lang.String order_id;

    private java.lang.String shipping;

    private java.lang.String shipping_tax;

    private java.lang.String shipping_rate_id;

    public ChangeOrderShippingInput() {
    }

    public ChangeOrderShippingInput(
           it.netgrid.virtuemart.soap.orders.LoginInfo loginInfo,
           java.lang.String order_id,
           java.lang.String shipping,
           java.lang.String shipping_tax,
           java.lang.String shipping_rate_id) {
           this.loginInfo = loginInfo;
           this.order_id = order_id;
           this.shipping = shipping;
           this.shipping_tax = shipping_tax;
           this.shipping_rate_id = shipping_rate_id;
    }


    /**
     * Gets the loginInfo value for this ChangeOrderShippingInput.
     * 
     * @return loginInfo
     */
    public it.netgrid.virtuemart.soap.orders.LoginInfo getLoginInfo() {
        return loginInfo;
    }


    /**
     * Sets the loginInfo value for this ChangeOrderShippingInput.
     * 
     * @param loginInfo
     */
    public void setLoginInfo(it.netgrid.virtuemart.soap.orders.LoginInfo loginInfo) {
        this.loginInfo = loginInfo;
    }


    /**
     * Gets the order_id value for this ChangeOrderShippingInput.
     * 
     * @return order_id
     */
    public java.lang.String getOrder_id() {
        return order_id;
    }


    /**
     * Sets the order_id value for this ChangeOrderShippingInput.
     * 
     * @param order_id
     */
    public void setOrder_id(java.lang.String order_id) {
        this.order_id = order_id;
    }


    /**
     * Gets the shipping value for this ChangeOrderShippingInput.
     * 
     * @return shipping
     */
    public java.lang.String getShipping() {
        return shipping;
    }


    /**
     * Sets the shipping value for this ChangeOrderShippingInput.
     * 
     * @param shipping
     */
    public void setShipping(java.lang.String shipping) {
        this.shipping = shipping;
    }


    /**
     * Gets the shipping_tax value for this ChangeOrderShippingInput.
     * 
     * @return shipping_tax
     */
    public java.lang.String getShipping_tax() {
        return shipping_tax;
    }


    /**
     * Sets the shipping_tax value for this ChangeOrderShippingInput.
     * 
     * @param shipping_tax
     */
    public void setShipping_tax(java.lang.String shipping_tax) {
        this.shipping_tax = shipping_tax;
    }


    /**
     * Gets the shipping_rate_id value for this ChangeOrderShippingInput.
     * 
     * @return shipping_rate_id
     */
    public java.lang.String getShipping_rate_id() {
        return shipping_rate_id;
    }


    /**
     * Sets the shipping_rate_id value for this ChangeOrderShippingInput.
     * 
     * @param shipping_rate_id
     */
    public void setShipping_rate_id(java.lang.String shipping_rate_id) {
        this.shipping_rate_id = shipping_rate_id;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof ChangeOrderShippingInput)) return false;
        ChangeOrderShippingInput other = (ChangeOrderShippingInput) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.loginInfo==null && other.getLoginInfo()==null) || 
             (this.loginInfo!=null &&
              this.loginInfo.equals(other.getLoginInfo()))) &&
            ((this.order_id==null && other.getOrder_id()==null) || 
             (this.order_id!=null &&
              this.order_id.equals(other.getOrder_id()))) &&
            ((this.shipping==null && other.getShipping()==null) || 
             (this.shipping!=null &&
              this.shipping.equals(other.getShipping()))) &&
            ((this.shipping_tax==null && other.getShipping_tax()==null) || 
             (this.shipping_tax!=null &&
              this.shipping_tax.equals(other.getShipping_tax()))) &&
            ((this.shipping_rate_id==null && other.getShipping_rate_id()==null) || 
             (this.shipping_rate_id!=null &&
              this.shipping_rate_id.equals(other.getShipping_rate_id())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getLoginInfo() != null) {
            _hashCode += getLoginInfo().hashCode();
        }
        if (getOrder_id() != null) {
            _hashCode += getOrder_id().hashCode();
        }
        if (getShipping() != null) {
            _hashCode += getShipping().hashCode();
        }
        if (getShipping_tax() != null) {
            _hashCode += getShipping_tax().hashCode();
        }
        if (getShipping_rate_id() != null) {
            _hashCode += getShipping_rate_id().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(ChangeOrderShippingInput.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://www.virtuemart.net/VM_Order/", "ChangeOrderShippingInput"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("loginInfo");
        elemField.setXmlName(new javax.xml.namespace.QName("", "loginInfo"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.virtuemart.net/VM_Order/", "loginInfo"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("order_id");
        elemField.setXmlName(new javax.xml.namespace.QName("", "order_id"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("shipping");
        elemField.setXmlName(new javax.xml.namespace.QName("", "shipping"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("shipping_tax");
        elemField.setXmlName(new javax.xml.namespace.QName("", "shipping_tax"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("shipping_rate_id");
        elemField.setXmlName(new javax.xml.namespace.QName("", "shipping_rate_id"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
