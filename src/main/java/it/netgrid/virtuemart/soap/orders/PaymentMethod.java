/**
 * PaymentMethod.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package it.netgrid.virtuemart.soap.orders;

public class PaymentMethod  implements java.io.Serializable {
    private java.lang.String payment_method_id;

    private java.lang.String virtuemart_vendor_id;

    private java.lang.String payment_jplugin_id;

    private java.lang.String payment_name;

    private java.lang.String payment_element;

    private java.lang.String discount;

    private java.lang.String discount_is_percentage;

    private java.lang.String discount_max_amount;

    private java.lang.String discount_min_amount;

    private java.lang.String payment_params;

    private java.lang.String shared;

    private java.lang.String ordering;

    private java.lang.String published;

    public PaymentMethod() {
    }

    public PaymentMethod(
           java.lang.String payment_method_id,
           java.lang.String virtuemart_vendor_id,
           java.lang.String payment_jplugin_id,
           java.lang.String payment_name,
           java.lang.String payment_element,
           java.lang.String discount,
           java.lang.String discount_is_percentage,
           java.lang.String discount_max_amount,
           java.lang.String discount_min_amount,
           java.lang.String payment_params,
           java.lang.String shared,
           java.lang.String ordering,
           java.lang.String published) {
           this.payment_method_id = payment_method_id;
           this.virtuemart_vendor_id = virtuemart_vendor_id;
           this.payment_jplugin_id = payment_jplugin_id;
           this.payment_name = payment_name;
           this.payment_element = payment_element;
           this.discount = discount;
           this.discount_is_percentage = discount_is_percentage;
           this.discount_max_amount = discount_max_amount;
           this.discount_min_amount = discount_min_amount;
           this.payment_params = payment_params;
           this.shared = shared;
           this.ordering = ordering;
           this.published = published;
    }


    /**
     * Gets the payment_method_id value for this PaymentMethod.
     * 
     * @return payment_method_id
     */
    public java.lang.String getPayment_method_id() {
        return payment_method_id;
    }


    /**
     * Sets the payment_method_id value for this PaymentMethod.
     * 
     * @param payment_method_id
     */
    public void setPayment_method_id(java.lang.String payment_method_id) {
        this.payment_method_id = payment_method_id;
    }


    /**
     * Gets the virtuemart_vendor_id value for this PaymentMethod.
     * 
     * @return virtuemart_vendor_id
     */
    public java.lang.String getVirtuemart_vendor_id() {
        return virtuemart_vendor_id;
    }


    /**
     * Sets the virtuemart_vendor_id value for this PaymentMethod.
     * 
     * @param virtuemart_vendor_id
     */
    public void setVirtuemart_vendor_id(java.lang.String virtuemart_vendor_id) {
        this.virtuemart_vendor_id = virtuemart_vendor_id;
    }


    /**
     * Gets the payment_jplugin_id value for this PaymentMethod.
     * 
     * @return payment_jplugin_id
     */
    public java.lang.String getPayment_jplugin_id() {
        return payment_jplugin_id;
    }


    /**
     * Sets the payment_jplugin_id value for this PaymentMethod.
     * 
     * @param payment_jplugin_id
     */
    public void setPayment_jplugin_id(java.lang.String payment_jplugin_id) {
        this.payment_jplugin_id = payment_jplugin_id;
    }


    /**
     * Gets the payment_name value for this PaymentMethod.
     * 
     * @return payment_name
     */
    public java.lang.String getPayment_name() {
        return payment_name;
    }


    /**
     * Sets the payment_name value for this PaymentMethod.
     * 
     * @param payment_name
     */
    public void setPayment_name(java.lang.String payment_name) {
        this.payment_name = payment_name;
    }


    /**
     * Gets the payment_element value for this PaymentMethod.
     * 
     * @return payment_element
     */
    public java.lang.String getPayment_element() {
        return payment_element;
    }


    /**
     * Sets the payment_element value for this PaymentMethod.
     * 
     * @param payment_element
     */
    public void setPayment_element(java.lang.String payment_element) {
        this.payment_element = payment_element;
    }


    /**
     * Gets the discount value for this PaymentMethod.
     * 
     * @return discount
     */
    public java.lang.String getDiscount() {
        return discount;
    }


    /**
     * Sets the discount value for this PaymentMethod.
     * 
     * @param discount
     */
    public void setDiscount(java.lang.String discount) {
        this.discount = discount;
    }


    /**
     * Gets the discount_is_percentage value for this PaymentMethod.
     * 
     * @return discount_is_percentage
     */
    public java.lang.String getDiscount_is_percentage() {
        return discount_is_percentage;
    }


    /**
     * Sets the discount_is_percentage value for this PaymentMethod.
     * 
     * @param discount_is_percentage
     */
    public void setDiscount_is_percentage(java.lang.String discount_is_percentage) {
        this.discount_is_percentage = discount_is_percentage;
    }


    /**
     * Gets the discount_max_amount value for this PaymentMethod.
     * 
     * @return discount_max_amount
     */
    public java.lang.String getDiscount_max_amount() {
        return discount_max_amount;
    }


    /**
     * Sets the discount_max_amount value for this PaymentMethod.
     * 
     * @param discount_max_amount
     */
    public void setDiscount_max_amount(java.lang.String discount_max_amount) {
        this.discount_max_amount = discount_max_amount;
    }


    /**
     * Gets the discount_min_amount value for this PaymentMethod.
     * 
     * @return discount_min_amount
     */
    public java.lang.String getDiscount_min_amount() {
        return discount_min_amount;
    }


    /**
     * Sets the discount_min_amount value for this PaymentMethod.
     * 
     * @param discount_min_amount
     */
    public void setDiscount_min_amount(java.lang.String discount_min_amount) {
        this.discount_min_amount = discount_min_amount;
    }


    /**
     * Gets the payment_params value for this PaymentMethod.
     * 
     * @return payment_params
     */
    public java.lang.String getPayment_params() {
        return payment_params;
    }


    /**
     * Sets the payment_params value for this PaymentMethod.
     * 
     * @param payment_params
     */
    public void setPayment_params(java.lang.String payment_params) {
        this.payment_params = payment_params;
    }


    /**
     * Gets the shared value for this PaymentMethod.
     * 
     * @return shared
     */
    public java.lang.String getShared() {
        return shared;
    }


    /**
     * Sets the shared value for this PaymentMethod.
     * 
     * @param shared
     */
    public void setShared(java.lang.String shared) {
        this.shared = shared;
    }


    /**
     * Gets the ordering value for this PaymentMethod.
     * 
     * @return ordering
     */
    public java.lang.String getOrdering() {
        return ordering;
    }


    /**
     * Sets the ordering value for this PaymentMethod.
     * 
     * @param ordering
     */
    public void setOrdering(java.lang.String ordering) {
        this.ordering = ordering;
    }


    /**
     * Gets the published value for this PaymentMethod.
     * 
     * @return published
     */
    public java.lang.String getPublished() {
        return published;
    }


    /**
     * Sets the published value for this PaymentMethod.
     * 
     * @param published
     */
    public void setPublished(java.lang.String published) {
        this.published = published;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof PaymentMethod)) return false;
        PaymentMethod other = (PaymentMethod) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.payment_method_id==null && other.getPayment_method_id()==null) || 
             (this.payment_method_id!=null &&
              this.payment_method_id.equals(other.getPayment_method_id()))) &&
            ((this.virtuemart_vendor_id==null && other.getVirtuemart_vendor_id()==null) || 
             (this.virtuemart_vendor_id!=null &&
              this.virtuemart_vendor_id.equals(other.getVirtuemart_vendor_id()))) &&
            ((this.payment_jplugin_id==null && other.getPayment_jplugin_id()==null) || 
             (this.payment_jplugin_id!=null &&
              this.payment_jplugin_id.equals(other.getPayment_jplugin_id()))) &&
            ((this.payment_name==null && other.getPayment_name()==null) || 
             (this.payment_name!=null &&
              this.payment_name.equals(other.getPayment_name()))) &&
            ((this.payment_element==null && other.getPayment_element()==null) || 
             (this.payment_element!=null &&
              this.payment_element.equals(other.getPayment_element()))) &&
            ((this.discount==null && other.getDiscount()==null) || 
             (this.discount!=null &&
              this.discount.equals(other.getDiscount()))) &&
            ((this.discount_is_percentage==null && other.getDiscount_is_percentage()==null) || 
             (this.discount_is_percentage!=null &&
              this.discount_is_percentage.equals(other.getDiscount_is_percentage()))) &&
            ((this.discount_max_amount==null && other.getDiscount_max_amount()==null) || 
             (this.discount_max_amount!=null &&
              this.discount_max_amount.equals(other.getDiscount_max_amount()))) &&
            ((this.discount_min_amount==null && other.getDiscount_min_amount()==null) || 
             (this.discount_min_amount!=null &&
              this.discount_min_amount.equals(other.getDiscount_min_amount()))) &&
            ((this.payment_params==null && other.getPayment_params()==null) || 
             (this.payment_params!=null &&
              this.payment_params.equals(other.getPayment_params()))) &&
            ((this.shared==null && other.getShared()==null) || 
             (this.shared!=null &&
              this.shared.equals(other.getShared()))) &&
            ((this.ordering==null && other.getOrdering()==null) || 
             (this.ordering!=null &&
              this.ordering.equals(other.getOrdering()))) &&
            ((this.published==null && other.getPublished()==null) || 
             (this.published!=null &&
              this.published.equals(other.getPublished())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getPayment_method_id() != null) {
            _hashCode += getPayment_method_id().hashCode();
        }
        if (getVirtuemart_vendor_id() != null) {
            _hashCode += getVirtuemart_vendor_id().hashCode();
        }
        if (getPayment_jplugin_id() != null) {
            _hashCode += getPayment_jplugin_id().hashCode();
        }
        if (getPayment_name() != null) {
            _hashCode += getPayment_name().hashCode();
        }
        if (getPayment_element() != null) {
            _hashCode += getPayment_element().hashCode();
        }
        if (getDiscount() != null) {
            _hashCode += getDiscount().hashCode();
        }
        if (getDiscount_is_percentage() != null) {
            _hashCode += getDiscount_is_percentage().hashCode();
        }
        if (getDiscount_max_amount() != null) {
            _hashCode += getDiscount_max_amount().hashCode();
        }
        if (getDiscount_min_amount() != null) {
            _hashCode += getDiscount_min_amount().hashCode();
        }
        if (getPayment_params() != null) {
            _hashCode += getPayment_params().hashCode();
        }
        if (getShared() != null) {
            _hashCode += getShared().hashCode();
        }
        if (getOrdering() != null) {
            _hashCode += getOrdering().hashCode();
        }
        if (getPublished() != null) {
            _hashCode += getPublished().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(PaymentMethod.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://www.virtuemart.net/VM_Order/", "PaymentMethod"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("payment_method_id");
        elemField.setXmlName(new javax.xml.namespace.QName("", "payment_method_id"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("virtuemart_vendor_id");
        elemField.setXmlName(new javax.xml.namespace.QName("", "virtuemart_vendor_id"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("payment_jplugin_id");
        elemField.setXmlName(new javax.xml.namespace.QName("", "payment_jplugin_id"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("payment_name");
        elemField.setXmlName(new javax.xml.namespace.QName("", "payment_name"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("payment_element");
        elemField.setXmlName(new javax.xml.namespace.QName("", "payment_element"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("discount");
        elemField.setXmlName(new javax.xml.namespace.QName("", "discount"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("discount_is_percentage");
        elemField.setXmlName(new javax.xml.namespace.QName("", "discount_is_percentage"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("discount_max_amount");
        elemField.setXmlName(new javax.xml.namespace.QName("", "discount_max_amount"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("discount_min_amount");
        elemField.setXmlName(new javax.xml.namespace.QName("", "discount_min_amount"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("payment_params");
        elemField.setXmlName(new javax.xml.namespace.QName("", "payment_params"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("shared");
        elemField.setXmlName(new javax.xml.namespace.QName("", "shared"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("ordering");
        elemField.setXmlName(new javax.xml.namespace.QName("", "ordering"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("published");
        elemField.setXmlName(new javax.xml.namespace.QName("", "published"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
