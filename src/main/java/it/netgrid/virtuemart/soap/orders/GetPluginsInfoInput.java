/**
 * GetPluginsInfoInput.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package it.netgrid.virtuemart.soap.orders;

public class GetPluginsInfoInput  implements java.io.Serializable {
    private it.netgrid.virtuemart.soap.orders.LoginInfo loginInfo;

    private it.netgrid.virtuemart.soap.orders.Plugin plugin;

    public GetPluginsInfoInput() {
    }

    public GetPluginsInfoInput(
           it.netgrid.virtuemart.soap.orders.LoginInfo loginInfo,
           it.netgrid.virtuemart.soap.orders.Plugin plugin) {
           this.loginInfo = loginInfo;
           this.plugin = plugin;
    }


    /**
     * Gets the loginInfo value for this GetPluginsInfoInput.
     * 
     * @return loginInfo
     */
    public it.netgrid.virtuemart.soap.orders.LoginInfo getLoginInfo() {
        return loginInfo;
    }


    /**
     * Sets the loginInfo value for this GetPluginsInfoInput.
     * 
     * @param loginInfo
     */
    public void setLoginInfo(it.netgrid.virtuemart.soap.orders.LoginInfo loginInfo) {
        this.loginInfo = loginInfo;
    }


    /**
     * Gets the plugin value for this GetPluginsInfoInput.
     * 
     * @return plugin
     */
    public it.netgrid.virtuemart.soap.orders.Plugin getPlugin() {
        return plugin;
    }


    /**
     * Sets the plugin value for this GetPluginsInfoInput.
     * 
     * @param plugin
     */
    public void setPlugin(it.netgrid.virtuemart.soap.orders.Plugin plugin) {
        this.plugin = plugin;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof GetPluginsInfoInput)) return false;
        GetPluginsInfoInput other = (GetPluginsInfoInput) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.loginInfo==null && other.getLoginInfo()==null) || 
             (this.loginInfo!=null &&
              this.loginInfo.equals(other.getLoginInfo()))) &&
            ((this.plugin==null && other.getPlugin()==null) || 
             (this.plugin!=null &&
              this.plugin.equals(other.getPlugin())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getLoginInfo() != null) {
            _hashCode += getLoginInfo().hashCode();
        }
        if (getPlugin() != null) {
            _hashCode += getPlugin().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(GetPluginsInfoInput.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://www.virtuemart.net/VM_Order/", "GetPluginsInfoInput"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("loginInfo");
        elemField.setXmlName(new javax.xml.namespace.QName("", "loginInfo"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.virtuemart.net/VM_Order/", "loginInfo"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("plugin");
        elemField.setXmlName(new javax.xml.namespace.QName("", "plugin"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.virtuemart.net/VM_Order/", "Plugin"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
