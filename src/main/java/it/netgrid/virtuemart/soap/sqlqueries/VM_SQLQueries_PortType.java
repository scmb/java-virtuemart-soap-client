/**
 * VM_SQLQueries_PortType.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package it.netgrid.virtuemart.soap.sqlqueries;

public interface VM_SQLQueries_PortType extends java.rmi.Remote {
    public it.netgrid.virtuemart.soap.sqlqueries.SQLResult[] executeSQLQuery(it.netgrid.virtuemart.soap.sqlqueries.SQLRequest parameters) throws java.rmi.RemoteException;
    public it.netgrid.virtuemart.soap.sqlqueries.SQLResult[] executeSQLSelectQuery(it.netgrid.virtuemart.soap.sqlqueries.SQLSelectRequest parameters) throws java.rmi.RemoteException;
    public it.netgrid.virtuemart.soap.sqlqueries.SQLResult[] executeSQLInsertQuery(it.netgrid.virtuemart.soap.sqlqueries.SQLInsertRequest parameters) throws java.rmi.RemoteException;
    public it.netgrid.virtuemart.soap.sqlqueries.SQLResult[] executeSQLUpdateQuery(it.netgrid.virtuemart.soap.sqlqueries.SQLUpdateRequest parameters) throws java.rmi.RemoteException;
}
