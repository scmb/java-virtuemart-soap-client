package it.netgrid.virtuemart.soap.sqlqueries;

public class VM_SQLQueriesProxy implements it.netgrid.virtuemart.soap.sqlqueries.VM_SQLQueries_PortType {
  private String _endpoint = null;
  private it.netgrid.virtuemart.soap.sqlqueries.VM_SQLQueries_PortType vM_SQLQueries_PortType = null;
  
  public VM_SQLQueriesProxy() {
    _initVM_SQLQueriesProxy();
  }
  
  public VM_SQLQueriesProxy(String endpoint) {
    _endpoint = endpoint;
    _initVM_SQLQueriesProxy();
  }
  
  private void _initVM_SQLQueriesProxy() {
    try {
      vM_SQLQueries_PortType = (new it.netgrid.virtuemart.soap.sqlqueries.VM_SQLQueries_ServiceLocator()).getVM_SQLQueriesSOAP();
      if (vM_SQLQueries_PortType != null) {
        if (_endpoint != null)
          ((javax.xml.rpc.Stub)vM_SQLQueries_PortType)._setProperty("javax.xml.rpc.service.endpoint.address", _endpoint);
        else
          _endpoint = (String)((javax.xml.rpc.Stub)vM_SQLQueries_PortType)._getProperty("javax.xml.rpc.service.endpoint.address");
      }
      
    }
    catch (javax.xml.rpc.ServiceException serviceException) {}
  }
  
  public String getEndpoint() {
    return _endpoint;
  }
  
  public void setEndpoint(String endpoint) {
    _endpoint = endpoint;
    if (vM_SQLQueries_PortType != null)
      ((javax.xml.rpc.Stub)vM_SQLQueries_PortType)._setProperty("javax.xml.rpc.service.endpoint.address", _endpoint);
    
  }
  
  public it.netgrid.virtuemart.soap.sqlqueries.VM_SQLQueries_PortType getVM_SQLQueries_PortType() {
    if (vM_SQLQueries_PortType == null)
      _initVM_SQLQueriesProxy();
    return vM_SQLQueries_PortType;
  }
  
  public it.netgrid.virtuemart.soap.sqlqueries.SQLResult[] executeSQLQuery(it.netgrid.virtuemart.soap.sqlqueries.SQLRequest parameters) throws java.rmi.RemoteException{
    if (vM_SQLQueries_PortType == null)
      _initVM_SQLQueriesProxy();
    return vM_SQLQueries_PortType.executeSQLQuery(parameters);
  }
  
  public it.netgrid.virtuemart.soap.sqlqueries.SQLResult[] executeSQLSelectQuery(it.netgrid.virtuemart.soap.sqlqueries.SQLSelectRequest parameters) throws java.rmi.RemoteException{
    if (vM_SQLQueries_PortType == null)
      _initVM_SQLQueriesProxy();
    return vM_SQLQueries_PortType.executeSQLSelectQuery(parameters);
  }
  
  public it.netgrid.virtuemart.soap.sqlqueries.SQLResult[] executeSQLInsertQuery(it.netgrid.virtuemart.soap.sqlqueries.SQLInsertRequest parameters) throws java.rmi.RemoteException{
    if (vM_SQLQueries_PortType == null)
      _initVM_SQLQueriesProxy();
    return vM_SQLQueries_PortType.executeSQLInsertQuery(parameters);
  }
  
  public it.netgrid.virtuemart.soap.sqlqueries.SQLResult[] executeSQLUpdateQuery(it.netgrid.virtuemart.soap.sqlqueries.SQLUpdateRequest parameters) throws java.rmi.RemoteException{
    if (vM_SQLQueries_PortType == null)
      _initVM_SQLQueriesProxy();
    return vM_SQLQueries_PortType.executeSQLUpdateQuery(parameters);
  }
  
  
}